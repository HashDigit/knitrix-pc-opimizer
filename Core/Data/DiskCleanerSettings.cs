using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using Common;
using Common.Extension;

namespace Data
{
	/// <summary>
	/// ��������� ����������� "Disk" -> "Disk Cleaner"
	/// </summary>
	[Serializable]
	public class DiskCleanerSettings
	{
		public bool searchSystem = false;
		public bool searchReadOnly = true;
		public bool searchHidden = true;
		public bool searchArchives = true;
		public bool searchZeroByte = false;
		public int removeMode = 0;
		public string moveFolder = "";
		public bool autoRestorePoints = true;
		public int filterMode = 0;
		public string searchFilters = "*.tmp; *.temp; *.gid; *.chk; *.~*;";
		public bool ignoreWriteProtected = true;
		public int findFilesMode = 0;
		public bool findFilesAfter = false;
		public bool findFilesBefore = false;
		public DateTime dateTimeAfter = new DateTime(2010, 1, 1);
		public DateTime dateTimeBefore = new DateTime(2010, 1, 1);
		[XmlIgnore]
		public StringCollection excludedDirs;
		[XmlIgnore]
		public StringCollection excludedFileTypes;
		public bool findNotAccessed = false;
		public int findNotAccessedValue = 0;
		public bool checkFileSize = false;
		public int checkFileSizeLeast = 0;
		public int checkFileSizeMost = 0;
		public bool autoClean = false;
		[XmlIgnore]
		public ArrayList diskDrives;
		public long lastUpdateDate = 0;
		[XmlIgnore]
		public StringCollection includedFolders;

		public DiskCleanerSettings()
		{
			excludedDirs = new StringCollection();
			excludedDirs.AddRange(new List<string>(@"?:\program files\common files
?:\program files\uninstall information
?:\win*\security
*\sendto
*\i386
*\resources\themes
*\microsoft\office\data
*\microsoft\money\*\webcache
*\chaos32
*\paprport\data
*\symantec shared\virusdefs
?:\system volume information
?:\_restore
?:\recycle?
*\temp\incredimail
*\norton antivirus\quarantine
*\system32\usmt
*\system32\catroot2
*\application data\microsoft
?:\windows
?:\winnt
?:\win*\pif
?:\win*\resources
\win*\system32\oldole
?:\win*\Sysbackup
?:\win*\system32\olebackup
?:\win*\system32\usmt
?:\$recycle.bin
*\Application Data\Aim
*\symantec shared\virusdefs".Split('\n')).Trim(new char[] { '\r' }).ToArray());

			excludedFileTypes = new StringCollection();
			excludedFileTypes.AddRange(new List<string>(@"config.*
autoexec.*
data.bak
_Default.pif
Calendar.tmp
Data.sav
Default.pif
Donotdel.tmp
DonotDelete.tmp
Drelres.pif
FTP.log
Http.log
Index.dat
Install.log
MM2048.DAT
MM256.DAT
Modem.log
Script.bak
System.old
Uninstall.log
User.old
WebPoolFileFile
Win.bak
Win.old
Wnccdctl.log
wz.pif".Split('\n')).Trim(new char[] { '\r' }).ToArray());

			includedFolders = new StringCollection();
			includedFolders.Add(Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.User));
			includedFolders.Add(Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine));
			includedFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.Recent));
			includedFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache));

			diskDrives = new ArrayList();
			string winDir = Environment.GetFolderPath(Environment.SpecialFolder.System);
			foreach (DriveInfo driveInfo in DriveInfo.GetDrives())
			{
				if (!driveInfo.IsReady || driveInfo.DriveType != DriveType.Fixed)
					continue;

				string freeSpace = SizeHelper.GetSize((ulong)driveInfo.TotalFreeSpace);
				string totalSpace = SizeHelper.GetSize((ulong)driveInfo.TotalSize);

				var listViewItem = new ListViewItem(new string[] { driveInfo.Name, driveInfo.DriveFormat, totalSpace, freeSpace });
				if (winDir.Contains(driveInfo.Name))
					listViewItem.Checked = true;
				listViewItem.Tag = driveInfo;

				// Store as listviewitem cause im too lazy 
				diskDrives.Add(listViewItem);
			}
		}
	}
}