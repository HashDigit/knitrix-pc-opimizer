using System;
using System.Security.Principal;

namespace Data.Privacy
{
	/// <summary>
	/// ���� Name, Value �� ��������� PrivacyAndHistoryCleaner.csv
	/// (����� ������ � ������, ���� � ����� � ���������� �������)
	/// </summary>
	public class PrivacyScannerPath : PrivacyScannerBase
	{
		public string Path { get; private set; }
		public bool IsDirectory { get; private set; }

		///// <summary>
		///// Directory or registry?
		///// </summary>
		//public bool IsDirectory { get; private set; }

		public PrivacyScannerPath(string name, string path, string isDir, string id)
		{
			Name = name;
			Id = new Guid(id);
			Path = path.ToLower();
			//Path = Path.Replace("{user name}", WindowsIdentity.GetCurrent().Name.Split('\\')[1]); // name without domain
			Path = Path.Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower()); // name without domain
			Path = Path.Replace(@"c:\", System.IO.Path.GetPathRoot(Environment.SystemDirectory).ToLower());

			//IsDirectory = Path.StartsWith(System.IO.Path.GetPathRoot(Environment.SystemDirectory).ToLower());
			IsDirectory = isDir.ToLower() == "dir";
		}
	}
}