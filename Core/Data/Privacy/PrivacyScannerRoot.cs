using System;
using System.Collections.Generic;

namespace Data.Privacy
{
	/// <summary>
	/// ����� �� ��������� PrivacyAndHistoryCleaner.csv (������� �������, ������ ����������)
	/// </summary>
	public class PrivacyScannerRoot : PrivacyScannerBase
	{
		/// <summary>
		/// ����������
		/// </summary>
		public List<PrivacyScannerApplication> ChildItems;

		public PrivacyScannerRoot(string name, string id)
		{
			Name = name;
			Id = new Guid(id);
			ChildItems = new List<PrivacyScannerApplication>();
		}

		public void AddItem(PrivacyScannerApplication item)
		{
			item.Parent = this;
			ChildItems.Add(item);
		}
	}
}