using System;

namespace Data.Privacy
{
	public class PrivacyScannerBase
	{
		public PrivacyScannerBase Parent { get; set; }
		public string Name { get; set; }
		public Guid Id { get; set; }
	}
}