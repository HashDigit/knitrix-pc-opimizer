using System;
using System.Collections.Generic;

namespace Data.Privacy
{
	/// <summary>
	/// ����� �� ��������� PrivacyAndHistoryCleaner.csv (���������� �� ������� ����������)
	/// </summary>
	public class PrivacyScannerApplication : PrivacyScannerBase
	{
		/// <summary>
		/// ����������
		/// </summary>
		public List<PrivacyScannerPath> ChildItems;

		public PrivacyScannerApplication(string name, string id)
		{
			Name = name;
			Id = new Guid(id);
			ChildItems = new List<PrivacyScannerPath>();
		}

		public void AddItem(PrivacyScannerPath item)
		{
			item.Parent = this;
			ChildItems.Add(item);
		}
	}
}