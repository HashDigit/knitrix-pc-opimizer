using System.ComponentModel;

namespace Data
{
	#region Cleaner
	/// <summary>
	/// Parent items in Cleaner tree view
	/// </summary>
	public enum CleanerParentItem
	{
		[Description("Internet Explorer")]
		InternetExplorer,
		[Description("Windows Explorer")]
		WindowsExplorer,
		System,
		Advanced,
		[Description("Firefox/Mozilla")]
		FirefoxMozilla,
		Applications,
		Multimedia,
		Utilitie,
		Windows,
	}

	/// <summary>
	/// Internet Explorer chils items (in Cleaner tree view)
	/// </summary>
	public enum CleanerInternetExplorerItem
	{
		[Description("Temprary Internet Files")]
		TempraryInternetFiles,
		Cookies,
		History,
		[Description("Recently Typed URLs")]
		RecentlyTypedUrls,
		[Description("Index.dat files")]
		IndexDatFiles,
		[Description("Last Download Location")]
		LastDownloadLocation,
		[Description("Autocomplete From History")]
		AutocompleteFromHistory,
	}

	/// <summary>
	/// Windows Explorer chils items (in Cleaner tree view)
	/// </summary>
	public enum CleanerWindowsExplorerItem
	{
		[Description("Recent Documents")]
		RecentDocuments,
		[Description("Run (in Start Menu)")]
		RunInStartMenu,
		[Description("Other Explorer MRUs")]
		OtherExplorerMrus,
		[Description("Thumbnail Cache")]
		ThumbnailCache,
		[Description("Taskbar Jump Lists")]
		TaskbarJumpLists,
	}

	/// <summary>
	/// System chils items (in Cleaner tree view)
	/// </summary>
	public enum CleanerSystemItem
	{
		[Description("Empty Recycle Bin")]
		EmptyRecycleBin,
		[Description("Temprary Files")]
		TempraryFiles,
		[Description("Clipboard")]
		Clipboard,
		[Description("Memory Dumps")]
		MemoryDumps,
		[Description("Chkdsk File Fragments")]
		ChkdskFileFragments,
		[Description("Windows Log Files")]
		WindowsLogFiles,
		[Description("Windows Error Reporting")]
		WindowsErrorReporting,
		[Description("DNS Cache")]
		DNSCache,
		[Description("Start Menu Shortcuts")]
		StartMenuShortcuts,
		[Description("Desktop Shortcuts")]
		DesktopShortcuts,
	}
	#endregion Cleaner

	#region Registry
	/// <summary>
	/// Parent items in Registry tree view
	/// </summary>
	public enum RegistryParentItem
	{
		[Description("Registry Integrity")]
		RegistryIntegrity,
	}
	#endregion Registry
}