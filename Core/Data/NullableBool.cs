namespace Data
{
	public enum NullableBool
	{
		NotSet = 0, // default
		False,
		True,
	}
}