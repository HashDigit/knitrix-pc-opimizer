using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

namespace Data
{
    public class Config
    {
        /// <summary>
        /// Application path
        /// </summary>
        public static string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;

    	/// <summary>
    	/// Directory with notifier sound files
    	/// </summary>
    	public static string SoundPath;

		public static string CommonDataFolder = Application.CommonAppDataPath;
		public static string UserDataFolder = Application.LocalUserAppDataPath;
		public static string BuyNowUrl = "http://www.knitrix.com/register/";
        public static string HelpUrl = "http://www.knitrix.com/help/";
        public static string SupportUrl = "http://www.knitrix.com/support/";
        public static string CompanyUrl = "http://www.knitrix.com";
        public static string RegisterUrl = "http://www.knitrix.com/register/";
        public static string LiveChatUrl = "http://www.knitrix.com/chat/";
		public static bool IsDebug = bool.Parse(ConfigurationSettings.AppSettings["IsDebug"]);
		public static bool EnableFixing = bool.Parse(ConfigurationSettings.AppSettings["EnableFixing"]);
		public static string DateTimeFormat = ConfigurationSettings.AppSettings["DateTimeFormat"];
		public static string TimeSpanFormat = ConfigurationSettings.AppSettings["TimeSpanFormat"];
		public static int CheckForUpdatesAppVersion = int.Parse(ConfigurationSettings.AppSettings["CheckForUpdatesAppVersion"]);
		public static string CheckForUpdatesUrl = ConfigurationSettings.AppSettings["CheckForUpdatesUrl"];
		public static string ScanScheduleTaskNamePrefix = "_scan_schedule_task_"; // prefix for scan schedule tasks 
    	public static bool IsFirstRun = false;

		#region SMTP
		public static string SmtpServer = @"smtp.gmail.com";
		public static int SmtpPort = 465;
		public static string SmtpLogin = @"apogeepcpro@gmail.com";
		public static string SmtpPassword = @"d7hSk3nsdwSh";
		public static string SmtpFrom = @"apogeepcpro@gmail.com";
		public static bool SmtpUseSsl = true;
		public static bool SmtpUseAuthentication = true;
		#endregion SMPT

		/// <summary>
		/// Url requsted by application after successfull application registration.
		/// To give ability to calculate number of different PCs which are using the same serial.
		/// </summary>
        public static string ActivateUrl = @"http://www.knitrix.net/serialusage_register.aspx?applicationname={0}&serial={1}&cpuid={2}";

		/// <summary>
		/// Url that gather statistics about app usage
		/// </summary>

		/// <summary>
		/// File containing list of black serials
		/// </summary>
        public static string BlackListFileUrl = @"http://www.knitrix.net/a.txt";

		/// <summary>
		/// Trial period (days)
		/// </summary>
		public static int TrialPeriod = 14;

        public static int OptionsVersion = int.Parse(ConfigurationSettings.AppSettings["OptionsVersion"]);

		static Config()
		{
			SoundPath = Path.Combine(Config.ApplicationPath, "Sound");
		}
	}
}