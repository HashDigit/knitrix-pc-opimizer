﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Business;
using Common;

namespace Data
{
	/// <summary>
	/// Naming convention comments:
	/// Current - selected by user
	/// Threaded - visibility scope is limited by thread
	/// </summary>
	public class Context
	{
		/// <summary>
		/// Auto start scan on application start
		/// </summary>
		public static bool AutoStartScan = false;

		public static List<Hive> Hives;

		static Context()
		{
			Hives = new List<Hive>();
		}
	}
}
