using System;

namespace Data
{
	/// <summary>
	/// ������ ��������� ������� ���� �����������.
	/// ���������� ������������� �� �������� "Optimize Window"->"Registry Manager".
	/// � �� �������� "Settings"->"Registry Scan Settings"
	/// </summary>
	[Serializable]
	public class RegistryScanSettings
	{
		/// <summary>
		/// ActiveX/COM
		/// </summary>
		public bool ActiveXCOM = true;

		/// <summary>
		/// Application Info
		/// </summary>
		public bool ApplicationInfo = true;

		/// <summary>
		/// Program Locations
		/// </summary>
		public bool ProgramLocations = true;

		/// <summary>
		/// Software Settings
		/// </summary>
		public bool SoftwareSettings = true;

		/// <summary>
		/// Startup
		/// </summary>
		public bool Startup = true;

		/// <summary>
		/// System Drivers
		/// </summary>
		public bool SystemDrivers = true;

		/// <summary>
		/// Shared DLLs
		/// </summary>
		public bool SharedDLLs = true;

		/// <summary>
		/// Help Files
		/// </summary>
		public bool HelpFiles = true;

		/// <summary>
		/// Sound Events
		/// </summary>
		public bool SoundEvents = true;

		/// <summary>
		/// History List
		/// </summary>
		public bool HistoryList = true;

		/// <summary>
		/// Windows Fonts
		/// </summary>
		public bool WindowsFonts = true;

		/*
		public int SectionCount
		{
			get
			{
				int sectionCount = 0;

				if (ActiveXCOM)
					sectionCount++;
				if (Startup)
					sectionCount++;
				if (WindowsFonts)
					sectionCount++;
				if (ApplicationInfo)
					sectionCount++;
				if (SystemDrivers)
					sectionCount++;
				if (HelpFiles)
					sectionCount++;
				if (SoundEvents)
					sectionCount++;
				if (ProgramLocations)
					sectionCount++;
				if (SoftwareSettings)
					sectionCount++;
				if (SharedDLLs)
					sectionCount++;
				if (HistoryList)
					sectionCount++;

				return sectionCount;
			}
		}
		*/
	}
}