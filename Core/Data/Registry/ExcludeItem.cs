﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Common;

namespace Data.Registry
{
    [Serializable()]
    public class RegistryExcludeItem : ICloneable
    {
        private string _pathRegistry = "";
        public string RegistryPath
        {
            get { return _pathRegistry; }
            set { 
                if (RegistryHelper.RegKeyExists(value))
                    _pathRegistry = value;
            }
        }

        private string _pathFolder = "";
        public string FolderPath
        {
            get { return _pathFolder; }
            set { 
                if (Directory.Exists(value))
                    _pathFolder = value;
            }
        }

        private string _pathFile = "";
        public string FilePath
        {
            get { return _pathFile; }
            set { 
                if (File.Exists(value))
                    _pathFile = value;
            }
        }

        /// <summary>
        /// Returns the assigned path (registry/file/folder)
        /// </summary>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(_pathRegistry))
                return string.Copy(_pathRegistry);

            if (!string.IsNullOrEmpty(_pathFile))
                return string.Copy(_pathFile);

            if (!string.IsNullOrEmpty(_pathFolder))
                return string.Copy(_pathFolder);

            return this.GetType().Name;
        }

		/// <summary>
		/// For serialization
		/// </summary>
		public RegistryExcludeItem()
		{
			
		}
        /// <summary>
        /// The constructor for this class
        /// Only one parameter can not be NULL!
        /// </summary>
        public RegistryExcludeItem(string regPath, string folderPath, string filePath)
        {
            if (!string.IsNullOrEmpty(regPath))
            {
                RegistryPath = regPath;
                return;
            }

            if (!string.IsNullOrEmpty(folderPath))
            {
                FolderPath = folderPath;
                return;
            }

            if (!string.IsNullOrEmpty(filePath))
            {
                FilePath = filePath;
                return;
            }
        }

        public Object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    [Serializable()]
    public class RegistryExcludeArray : CollectionBase
    {
        public RegistryExcludeItem this[int index]
        {
            get { return (RegistryExcludeItem)this.List[index]; }
            set { this.List[index] = value;  }
        }

        public RegistryExcludeArray()
        {
        }

		public RegistryExcludeArray(RegistryExcludeArray c)
        {
            if (c.Count > 0)
            {
                foreach (RegistryExcludeItem i in c)
                    this.List.Add(i.Clone());
            }
        }

        public int Add(RegistryExcludeItem item)
        {
            return this.List.Add(item);
        }

        public void Remove(RegistryExcludeItem item)
        {
            this.List.Remove(item);
        }

        public void Clear(RegistryExcludeItem item)
        {
            this.List.Clear();
        }

        protected override void OnValidate(object value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            if (value.GetType() != typeof(RegistryExcludeItem))
                throw new ArgumentException("value must be a RegistryExcludeItem type", "value");

            base.OnValidate(value);
        }
    }
}
