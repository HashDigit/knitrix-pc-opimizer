namespace Data
{
	public enum OneClickScanAreaEnum
	{
		Registry = 0, // default
		Process,
		Privacy,
	}
}