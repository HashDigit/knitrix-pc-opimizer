using System.Collections.Generic;
using Business;
using Common;
using Common_Tools.TreeViewAdv.Tree;
using Data.Registry;

namespace Data
{
	/// <summary>
	/// ������� ��������� �������� ������������ � ���� ����
	/// </summary>
	public class OneClickScanState
	{
		/// <summary>
		/// Count of found items to fix
		/// </summary>
		public int ItemsToFixCount { get; set; }

		/// <summary>
		/// Count of found items to fix in PrivacyFiles area
		/// </summary>
		public int PrivacyFilesItemsToFixCount { get; set; }

		/// <summary>
		/// Count of selected items to fix in PrivacyFiles area
		/// </summary>
		public int PrivacyFilesItemsSelectedCount { get; set; }

		/// <summary>
		/// Count of found items to fix in MalwareProcesses area
		/// </summary>
		public int MalwareProcessesItemsToFixCount { get; set; }

		/// <summary>
		/// Count of selected items to fix in MalwareProcesses area
		/// </summary>
		public int MalwareProcessesItemsSelectedCount { get; set; }

		/// <summary>
		/// Count of selected items to fix in StartupProcesses area
		/// </summary>
		public int StartupProcessesItemsSelectedCount { get; set; }

		/// <summary>
		/// Count of found items to fix in StartupProcesses area
		/// </summary>
		public int StartupProcessesItemsToFixCount { get; set; }

		/// <summary>
		/// Count of found items to fix in ActiveProcesses area
		/// </summary>
		public int ActiveProcessesItemsToFixCount { get; set; }

		/// <summary>
		/// Count of selected items to fix in ActiveProcesses area
		/// </summary>
		public int ActiveProcessesItemsSelectedCount { get; set; }

		/// <summary>
		/// Count of found items to fix in RegistryProblems area
		/// </summary>
		public int RegistryProblemsItemsToFixCount { get; set; }

		/// <summary>
		/// Count of selected items to fix in RegistryProblems area
		/// </summary>
		public int RegistryProblemsItemsSelectedCount { get; set; }

		/// <summary>
		/// Scanning progress (%)
		/// </summary>
		public int ScanningProgress { get; set; }

		/// <summary>
		/// Fixing progress (%)
		/// </summary>
		public int FixingProgress { get; set; }

		/// <summary>
		/// Area that is currently scanning (Registry, Process, Privacy)
		/// </summary>
		public OneClickScanAreaEnum ScanArea { get; set; }

		/// <summary>
		/// ������ ������ ������ items (����), ������� ������ �����������
		/// </summary>
		public TreeModel ItemsTreeModel;

		/// <summary>
		/// ������ ������ items (in Registry area), ������� ������ �������
		/// </summary>
		public List<BadRegistryKey> RegistryNodes;

		/// <summary>
		/// ������ ������ items (in Processes area), ������� ������ �������
		/// </summary>
		public List<BadRegistryKey> ProcessesNodes;

		/// <summary>
		/// ������ ������ items (in Privacy area), ������� ������ �������
		/// </summary>
		public List<BadRegistryKey> PrivacyNodes;

		private string _currentScannedObject = string.Empty;
		private string _currentFixedObject = string.Empty;

		/// <summary>
		/// ������� ����������� ������
		/// </summary>
		public string CurrentScannedObject
		{
			get
			{
				return _currentScannedObject;
			}
			set
			{
				_currentScannedObject = RegistryHelper.PrefixRegPath(value);
			}
		}

		/// <summary>
		/// ������� ������������ ������
		/// </summary>
		public string CurrentFixedObject
		{
			get
			{
				return _currentFixedObject;
			}
			set
			{
				_currentFixedObject = RegistryHelper.PrefixRegPath(value);
			}
		}

		public string CurrentSection { get; set; }

		public ScannerBase CurrentScanner;
		public string CurrentFixerName { get; set; }
		
		public OneClickScanState()
		{
			RegistryNodes = new List<BadRegistryKey>();
			ProcessesNodes = new List<BadRegistryKey>();
			PrivacyNodes = new List<BadRegistryKey>();
		}
	}
}