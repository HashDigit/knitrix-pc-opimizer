using System;
using System.Drawing;
using Common;

namespace Data
{
	public class Bho : BaseObject
	{
		/// <summary>
		/// BHO GUID
		/// </summary>
		public string Guid { get; set; }

		/// <summary>
		/// Full path to dll with BHO
		/// </summary>
		public string Path { get; set; }

		/// <summary>
		/// User friendly BHO name
		/// </summary>
		public string FriendlyName { get; set; }

		/// <summary>
		/// Does BHO currently enabled in IE?
		/// </summary>
		public bool IsEnabled { get; set; }

		/// <summary>
		/// Dll description
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Dll company
		/// </summary>
		public string Company { get; set; }

		/// <summary>
		/// Dll file-size
		/// </summary>
		public ulong Size { get; set; }

		/// <summary>
		/// Dll file-size (friendly output)
		/// </summary>
		public string FormattedSize
		{
			get
			{
				return SizeHelper.GetSize(Size);
			}
		}

		/// <summary>
		/// Dll product name
		/// </summary>
		public string ProductName { get; set; }

		/// <summary>
		/// Dll product version
		/// </summary>
		public string ProductVersion { get; set; }

		/// <summary>
		/// Dll date modified
		/// </summary>
		public DateTime DateModified { get; set; }

		public string FormattedDateModified
		{
			get
			{
				return DateModified.ToShortDateString();
			}
		}

		public Bho()
		{
			Guid = "";
			Path = "";
			FriendlyName = "";
			IsEnabled = true;
			Description = "";
			Company = "";
			ProductName = "";
			ProductVersion = "";
		}
	}
}