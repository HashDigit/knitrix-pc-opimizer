using System;
using System.ComponentModel;

namespace Data
{
	public class SystemRestorePoint
	{
		public DateTime CreationTime { get; set; }
		public string CreationTimeStr
		{
			set
			{
				int year = Int32.Parse(value.Substring(0, 4));
				int month = Int32.Parse(value.Substring(4, 2));
				int day = Int32.Parse(value.Substring(6, 2));
				int hour = Int32.Parse(value.Substring(8, 2));
				int minute = Int32.Parse(value.Substring(10, 2));
				int second = Int32.Parse(value.Substring(12, 2));
				CreationTime = new DateTime(year, month, day, hour, minute, second);//, DateTimeKind.Utc);
			}
		}
		public string Description { get; set; }
		public uint SequenceNumber { get; set; }
		public RestoreType RestoreType { get; set; }
	}

	// Type of restorations
	public enum RestoreType
	{
		[Description("Installing a new application")]
		ApplicationInstall = 0, // Installing a new application
		[Description("An application has been uninstalled")]
		ApplicationUninstall = 1, // An application has been uninstalled
		[Description("An application has had features added or removed")]
		ModifySettings = 12, // An application has had features added or removed
		[Description("An application needs to delete the restore point it created")]
		CancelledOperation = 13, // An application needs to delete the restore point it created
		[Description("System Restore")]
		Restore = 6, // System Restore
		[Description("Checkpoint")]
		Checkpoint = 7, // Checkpoint
		[Description("Device driver has been installed")]
		DeviceDriverInstall = 10, // Device driver has been installed
		[Description("Program used for 1st time")]
		FirstRun = 11, // Program used for 1st time 
		[Description("Restoring a backup")]
		BackupRecovery = 14, // Restoring a backup
		[Description("Critical Update")]
		CriticalUpdate = 18
	}
}