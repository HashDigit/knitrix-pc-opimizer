namespace Data
{
	public enum OneClickScanOverallAreaEnum
	{
		PrivacyFiles = 0, // default
		MalwareProcesses,
		StartupProcesses,
		ActiveProcesses,
		RegistryProblems,
	}
}