namespace Data.Startup
{
	public class StartupProcess : BaseObject
	{
		public string Folder { get; set; }
		private string strSection = "";
		public string Section
		{
			get { return strSection; }
			set { strSection = value; }
		}

		private string strItem = "";
		public string Item
		{
			get { return strItem; }
			set { strItem = value; }
		}

		private string strPath = "";
		public string Path
		{
			get { return strPath; }
			set { strPath = value; }
		}

		private string strArgs = "";
		public string Args
		{
			get { return strArgs; }
			set { strArgs = value; }
		}

		private string _path = "";
		public string ItemPath
		{
			get { return _path; }
			set { _path = value; }
		}

		public bool IsLeaf
		{
			get
			{
				return (string.IsNullOrEmpty(strSection));
			}
		}

		public StartupProcess()
			: base()
		{
		}

		public StartupProcess(string SectionName)
			: base()
		{
			this.strSection = SectionName;
		}
	}

}