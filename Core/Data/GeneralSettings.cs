using System;
using System.Windows.Forms;
using System.Xml.Serialization;
using Common;

namespace Data
{
	/// <summary>
	/// �������� ��������� ����������.
	/// "Settings"->"General Settings"
	/// </summary>
	[Serializable]
	public class GeneralSettings
	{
		/// <summary>
		/// Start Knitrix when Windows starts.
		/// </summary>
		[XmlIgnore]
		public bool AutoStartApp
		{
			get
			{
				try
				{
					var startupRegistryKey = RegistryHelper.Is64BitOS
					                         	? Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
													"SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run")
					                         	: Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
					                         		"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
					var filePath = startupRegistryKey.GetValue(Application.ProductName);
					if (filePath != null && filePath.ToString().ToLower() == string.Format("\"{0}\"", Application.ExecutablePath.ToLower()))
						return true;
				}
				catch(Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while getting GeneralSettings.AutoStartApp property.\r\nError:\r\n{0}", ex));
				}
				return false;
			}
			set
			{
				try
				{
					var startupRegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
					var filePath = startupRegistryKey.GetValue(Application.ProductName);
					if (value)
					{
						if (filePath == null || filePath.ToString().ToLower() != string.Format("\"{0}\"", Application.ExecutablePath.ToLower()))
							startupRegistryKey.SetValue(Application.ProductName, string.Format("\"{0}\"", Application.ExecutablePath));
					}
					else
					{
						if (filePath != null)
							startupRegistryKey.DeleteValue(Application.ProductName);
					}
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while setting GeneralSettings.AutoStartApp property.\r\nError:\r\n{0}", ex));
				}
			}
		}

		/// <summary>
		/// Automatically start scan when Knitrix starts.
		/// </summary>
		public bool AutoStartScan = false;

		/// <summary>
		/// Create a restore point before performing fixes or cleaning.
		/// </summary>
		public bool CreateRestorePointBeforeFixing = true;

		/// <summary>
		/// Automatically start fixing after scan is complete.
		/// </summary>
		public bool AutoStartFix = false;

		/// <summary>
        /// Show available Exit options before exiting Knitrix.
		/// </summary>
		public bool DisplayExitActions = false;

		/// <summary>
        /// Minimize to system tray instead of closing Knitrix.
		/// </summary>
		public bool MinimizeToTray = false;
	}
}