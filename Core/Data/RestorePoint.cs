using System;

namespace Data
{
    public class RestorePoint : BaseObject
    {
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        public RestorePoint()
        {
            DateCreated = DateTime.Now;
            Description = "";
            Type = "";
        }
    }
}