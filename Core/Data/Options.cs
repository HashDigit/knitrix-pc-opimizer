﻿using System;
using System.Collections.Generic;
using Common;
using Core.Properties;
using Data.Registry;

//using Data.Registry;

namespace Data
{
	/// <summary>
	/// Application Options
	/// </summary>
	[Serializable]
	public sealed class Options
	{
		/// <summary>
		/// Check for updates on application start
		/// </summary>
		public bool IsAutocheckForUpdates { get; set; }

		/// <summary>
		/// Last time application was checked for updates
		/// </summary>
		public DateTime CheckForUpdatesLastDateTime { get; set; }

		#region Registry Options
		///// <summary>
		///// Настройки функциональности "Registry Manager"
		///// </summary>
		//public bool Registry_Log { get; set; }
		//public string Registry_strBackupFile { get; set; }
		//public bool Registry_Rescan { get; set; }
		//public bool Registry_DelBackup { get; set; }
		//public bool Registry_Restore { get; set; }
		//public string Registry_strBugReportAddr { get; set; }
		//public bool Registry_AutoUpdate { get; set; }
		//public string Registry_strUpdateURL { get; set; }
		//public bool Registry_RemMedia { get; set; }
		//public long Registry_dtLastUpdate { get; set; }
		//public bool Registry_ShowLog { get; set; }
		//public uint Registry_nProgramStarts { get; set; }
		//public bool Registry_bUpgradeSettings { get; set; }
		//public bool Registry_bPortableEdition { get; set; }
		//public bool Registry_AutoRepair { get; set; }
		//public bool Registry_AutoExit { get; set; }

		public string Registry_BackupDir { get; set; }
		#endregion Registry Options

		/// <summary>
		/// Список регистров, которые не надо проверять.
		/// Этот список общий для "Start Scan" и для "Registry Manager".
		/// </summary>
		public RegistryExcludeArray RegistryExcludeArray = new RegistryExcludeArray();

		/// <summary>
		/// Список регистров которые надо сканировать.
		/// Выбираются пользователем на странице "Settings"->"Registry Scan Settings"
		/// </summary>
		public RegistryScanSettings RegistryScanSettings { get; set; }

		/// <summary>
		/// Основный настройки приложения.
		/// "Settings"->"General Settings"
		/// </summary>
		public GeneralSettings GeneralSettings { get; set; }

		/// <summary>
		/// Настройки функционала "Disk" -> "Disk Cleaner"
		/// </summary>
		public DiskCleanerSettings DiskCleanerSettings { get; set; }

		/// <summary>
		/// List of guids from checked PrivacyAndHistoryCleaner.csv
		/// </summary>
		public List<Guid> PrivacySettings { get; set; }

        /// <summary>
        /// Версия опций, используется чтобы сбрасывать опции, если их схема поменялась.
        /// </summary>
        public int Version { get; set; }

		private static volatile Options instance;
		private static object syncRoot = new Object();

		private Options() { }

		public static Options Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
						{
							//instance = new Options();
							try
							{
								instance = (Options)Serializer.LoadFromString(Settings.Default.Options, typeof(Options));
								if (instance == null || instance.Version < Config.OptionsVersion)
									instance = DefaultInit();
							}
							catch (Exception ex)
							{
								CLogger.WriteLog(ELogLevel.WARN, ex.ToString());

								instance = DefaultInit();
							}
						}
					}
				}

				return instance;
			}
			set
			{
				instance = value;
			}
		}

		/// <summary>
		/// Default options
		/// </summary>
		public static Options DefaultInit()
		{
			var options = new Options();

			options.IsAutocheckForUpdates = true;
			options.CheckForUpdatesLastDateTime = DateTime.MinValue;

			options.Registry_BackupDir = "Backup";

			options.RegistryScanSettings = new RegistryScanSettings();
			options.GeneralSettings = new GeneralSettings();
            options.DiskCleanerSettings = new DiskCleanerSettings();

			// fill PrivacySettings with all guids from PrivacyAndHistoryCleaner.csv
			options.PrivacySettings = new List<Guid>();
			foreach (var privacyScannerRoot in DataContext.PrivacyScannerRoots)
			{
				//options.PrivacySettings.Add(privacyScannerRoot.Id);

				foreach (var privacyScannerApplication in privacyScannerRoot.ChildItems)
				{
					//options.PrivacySettings.Add(privacyScannerApplication.Id);

					foreach (var privacyScannerDirectory in privacyScannerApplication.ChildItems)
					{
						options.PrivacySettings.Add(privacyScannerDirectory.Id);
					}
				}
			}
		    options.Version = Config.OptionsVersion;
			return options;
		}

		/// <summary>
		/// Save options to Properties.Settings
		/// </summary>
		public void Save()
		{
			try
			{
				string optionStr = Serializer.SaveToString(this);
				Settings.Default.Options = optionStr;
				Settings.Default.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex));
			} 
		}

		public void Reset()
		{
			try
			{
				instance = null;
				Settings.Default.Options = null;
				Settings.Default.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex));
			}
		}
	}
}
