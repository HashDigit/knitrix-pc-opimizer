using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;

using Common.Extension;
using Data.Privacy;
using Data.UninstallManager;
using LumenWorks.Framework.IO.Csv;

namespace Data
{
	public class DataContext
	{
		public static MainDS MainDS = new MainDS();

		/// <summary>
		/// Do processes on "Optimize Windows"->"Processes" contain unwanted process?
		/// </summary>
		public static bool FoundUnwantedProcess;

		/// <summary>
		/// �������� �������� ����������.
		/// ������������ �� ������ Process Manager
		/// </summary>
		public static BindingList<ProcessInfo> ProcessInfos { get; set; }

		/// <summary>
		/// ����� �������������� ����������.
		/// ������������ �� ������ Restore Point Manager
		/// </summary>
		public static List<RestorePoint> RestorePoints { get; set; }

		/// <summary>
		/// ������ ��������� ���������� ������� � �������.
		/// ������������ �� ������ "Optimize Windows"->"Registry Manager".
		/// </summary>
		public static List<RegistryEntry> ProblemRegistryEntries { get; set; }

		/// <summary>
		/// ������ ���������, ������� ��������� Malware
		/// </summary>
		public static List<string> MalwareProcesses;

		/// <summary>
		/// ������ ���� �� �����, ��� �������� ��������� (Privacy) ������,
		/// ������� �� ���������
		/// </summary>
		public static List<PrivacyScannerRoot> PrivacyScannerRoots;

		#region Registry to Scan
		/// <summary>
		/// ������ ��������� ������� ���� �����������.
		/// ���������� ������������� �� �������� "Optimize Window"->"Registry Manager"
		/// </summary>
		public static RegistryScanSettings RegistryScanSettings { get; set; }
		#endregion Registry to Scan

		public static OneClickScanState OneClickScanState { get; set; }

		/// <summary>
		/// ������ ��������, ������� ����� ����������������.
		/// ������������ �� ����� Disk -> Uninstall Manager
		/// </summary>
		public static List<ProgramInfo> ProgramsToUninstall;

		static DataContext()
		{
			ProcessInfos = new BindingList<ProcessInfo>();
			RestorePoints = new List<RestorePoint>();
			//ScannedItems = new List<ScannedItem>();
			ProblemRegistryEntries = new List<RegistryEntry>();

			#region MalwareProcesses
			MalwareProcesses = new List<string>();
			using (var sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(Assembly.GetExecutingAssembly().GetName().Name + "." + "MalwareProcessList.txt")))
			{
				var fileData = sr.ReadToEnd();
				MalwareProcesses.AddRange(fileData.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries));
				MalwareProcesses.Trim(new char[] { '\n', '\r', ' ' });
			}
			#endregion MalwareProcesses

			#region PrivacyScannerNodes
			PrivacyScannerRoots = new List<PrivacyScannerRoot>();
			PrivacyScannerRoot currentPrivacyScannerRoot = null;
			PrivacyScannerApplication currentPrivacyScannerApplication = null;
			using (var sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(Assembly.GetExecutingAssembly().GetName().Name + "." + "PrivacyAndHistoryCleaner.csv")))
			{
				using (var csv = new CsvReader(sr, false, ';'))
				{
					int fieldCount = csv.FieldCount;
					string[] headers = csv.GetFieldHeaders();

					while (csv.ReadNextRecord())
					{
						if (!string.IsNullOrEmpty(csv[0]))
						{
							currentPrivacyScannerRoot = new PrivacyScannerRoot(csv[0], csv[5]);
							currentPrivacyScannerApplication = null;
							PrivacyScannerRoots.Add(currentPrivacyScannerRoot);

						}
						if (!string.IsNullOrEmpty(csv[1]))
						{
							currentPrivacyScannerApplication = new PrivacyScannerApplication(csv[1], csv[5]);
							currentPrivacyScannerRoot.AddItem(currentPrivacyScannerApplication);

						}
						if (!string.IsNullOrEmpty(csv[2]))
						{
							currentPrivacyScannerApplication.AddItem(new PrivacyScannerPath(csv[2], csv[3], csv[4], csv[5]));

						}
					}
				}
			}
			/*
			NOTE :: (AK) good but old
			PrivacyScannerNodes = new List<PrivacyScannerNode>();
			PrivacyScannerNode privacyScannerNode = null;
			using (var sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(Assembly.GetExecutingAssembly().GetName().Name + "." + "PrivacyAndHistoryCleaner.csv")))
			{
				using (var csv = new CsvReader(sr, false, ';'))
				{
					int fieldCount = csv.FieldCount;
					string[] headers = csv.GetFieldHeaders();

					while (csv.ReadNextRecord())
					{
						for (int i = 0; i < fieldCount - 1; i++)
						{
							if (string.IsNullOrEmpty(csv[i]))
							{
								if (privacyScannerNode != null)
									privacyScannerNode = (PrivacyScannerNode)privacyScannerNode.ChildItems[privacyScannerNode.ChildItems.Count - 1];
								else
									privacyScannerNode = PrivacyScannerNodes[PrivacyScannerNodes.Count - 1];
							}
							else
							{
								if (i == 0)
								{
									privacyScannerNode = new PrivacyScannerNode(csv[i]);
									PrivacyScannerNodes.Add(privacyScannerNode);
								}
								else if (i == fieldCount - 2)
									privacyScannerNode.AddItem(new PrivacyScannerLeaf(csv[i], csv[i + 1]));
								else
									privacyScannerNode.AddItem(new PrivacyScannerNode(csv[i]));
								privacyScannerNode = null;
								break;
							}
						}
					}
				}
			}
			*/
			#endregion PrivacyScannerNodes

			RegistryScanSettings = new RegistryScanSettings();
			OneClickScanState = new OneClickScanState();
		}
	}
}