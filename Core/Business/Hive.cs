﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using Common;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Business
{
    public class Hive : IDisposable
    {
        private readonly string _hiveName, _hivePath;
		public string HivePath
		{
			get { return _hivePath; }
		}

        private volatile string _rootKey, _keyName;

        private volatile string _oldHivePath = HiveHelper.GetTempHivePath();
        public string OldHivePath
        {
            get { return _oldHivePath; }
        }

        private volatile string _newHivePath = HiveHelper.GetTempHivePath();
        public string NewHivePath
        {
            get { return _newHivePath; }
        }

        private readonly FileInfo _fi = null;
        public FileInfo HiveFileInfo
        {
            get { return _fi; }
        }

        private readonly long _oldHiveSize = 0;
        public long OldHiveSize
        {
            get { return _oldHiveSize; }
        }

        private volatile uint _newHiveSize = 0;
        public long NewHiveSize
        {
            get { return _newHiveSize; }
        }

        public volatile bool IsAnalyzed = false, IsCompacted = false;

        private volatile int _hKey = 0;
        private bool _isDisposed = false;

        /// <summary>
        /// Constructor for Hive class
        /// </summary>
        /// <param name="hiveName">Name of Hive (\REGISTRY\USER\...)</param>
        /// <param name="hivePath">Path to Hive (\Device\HarddiskVolumeX\Windows\System32\config\... or C:\Windows\System32\config\...)</param>
        public Hive(string hiveName, string hivePath)
        {
            this._hiveName = hiveName;
            if (File.Exists(hivePath))
                this._hivePath = hivePath;
            else
                this._hivePath = HiveHelper.ConvertDeviceToMSDOSName(hivePath);

            try
            {
                this._fi = new FileInfo(this._hivePath);
                this._oldHiveSize = HiveHelper.GetFileSize(this._hivePath);
            }
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error opening registry hive\r\n {0}", ex));
			}
        }

        public void Dispose()
        {
            if (!this._isDisposed)
            {
                if (this._hKey != 0)
                    PInvoke.RegCloseKey(_hKey);

                _hKey = 0;

                if (File.Exists(_oldHivePath))
                    File.Delete(_oldHivePath);

                _isDisposed = true;
            }

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Uses Windows RegSaveKeyA API to rewrite registry hive
        /// </summary>
        public void AnalyzeHive()
        {
            try
            {
                if (this.IsAnalyzed)
                    throw new Exception("Hive has already been analyzed");

                int nRet = 0, hkey = 0;

                this._rootKey = this._hiveName.ToLower();
                this._keyName = _rootKey.Substring(_rootKey.LastIndexOf('\\') + 1);

                // Open Handle to registry key
                if (_rootKey.StartsWith(@"\registry\machine"))
                    nRet = PInvoke.RegOpenKeyA((uint)PInvoke.HKEY.HKEY_LOCAL_MACHINE, _keyName, ref hkey);
                if (_rootKey.StartsWith(@"\registry\user"))
                    nRet = PInvoke.RegOpenKeyA((uint)PInvoke.HKEY.HKEY_USERS, _keyName, ref hkey);

                if (nRet != 0)
                    return;

                this._hKey = hkey;

                // Begin Critical Region
                Thread.BeginCriticalRegion();

                // Flush hive key
                PInvoke.RegFlushKey(hkey);

                // Function will fail if file already exists
                if (File.Exists(this._newHivePath))
                    File.Delete(this._newHivePath);

                // Use API to rewrite the registry hive
                nRet = PInvoke.RegSaveKeyA(hkey, this._newHivePath, 0);
                if (nRet != 0)
                    throw new Win32Exception(nRet);

                this._newHiveSize = (uint)HiveHelper.GetFileSize(this._newHivePath);

                if (File.Exists(this._newHivePath))
                    this.IsAnalyzed = true;

                // End Critical Region
                Thread.EndCriticalRegion();
            }
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error analyzing registry hive\r\n {0}", ex));
			}
        }

        /// <summary>
        /// Compacts the registry hive
        /// </summary>
        public void CompactHive() 
        {
            if (this.IsAnalyzed == false || this._hKey <= 0 || !File.Exists(this._newHivePath))
                throw new Exception("You must analyze the hive before you can compact it");

            if (this.IsCompacted)
                throw new Exception("The hive has already been compacted");

            // Begin Critical Region
            Thread.BeginCriticalRegion();

            // Old hive cant exist or function will fail
            if (File.Exists(this._oldHivePath))
                File.Delete(this._oldHivePath);

            // Replace hive with compressed hive
            int ret = PInvoke.RegReplaceKeyA(this._hKey, null, this._newHivePath, this._oldHivePath);
            if (ret != 0)
                throw new Win32Exception(ret);

            // Hive should now be replaced with temporary hive
            PInvoke.RegCloseKey(this._hKey);

            // End Critical Region
            Thread.EndCriticalRegion();

            this.IsCompacted = true;
        }

        /// <summary>
        /// Returns the filename of the hive
        /// </summary>
        /// <returns>Hive filename</returns>
        public override string ToString()
        {
            return (string)_fi.Name.Clone();
        }
    }

}
