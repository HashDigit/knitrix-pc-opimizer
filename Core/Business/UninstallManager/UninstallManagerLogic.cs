using System.Collections.Generic;
using Common;
using Data.UninstallManager;
using Microsoft.Win32;

namespace Business.UninstallManager
{
	public class UninstallManagerLogic
	{
		/// <summary>
		/// Get list of currently installed products that can be uninstalled
		/// </summary>
		public static List<ProgramInfo> List()
		{
			var listProgInfo = new List<ProgramInfo>();

			// Get the program info list
			using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"))
			{
				if (regKey != null)
				{
					foreach (string strSubKeyName in regKey.GetSubKeyNames())
					{
						using (RegistryKey subKey = regKey.OpenSubKey(strSubKeyName))
						{
							if (subKey != null)
								listProgInfo.Add(new ProgramInfo(subKey));
						}
					}
				}
			}

			// (x64 registry keys)
			if (RegistryHelper.Is64BitOS)
			{
				using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall"))
				{
					if (regKey != null)
					{
						foreach (string strSubKeyName in regKey.GetSubKeyNames())
						{
							using (RegistryKey subKey = regKey.OpenSubKey(strSubKeyName))
							{
								if (subKey != null)
									listProgInfo.Add(new ProgramInfo(subKey));
							}
						}
					}
				}
			}
			return listProgInfo;
		}
	}
}