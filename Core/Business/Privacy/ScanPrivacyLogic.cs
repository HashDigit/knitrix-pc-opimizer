using System;
using System.Collections.Generic;
using Business.Privacy.Scanners;
using Common_Tools.TreeViewAdv.Tree;
using Data;
using Data.Registry;

namespace Business.Privacy
{
	public class ScanPrivacyLogic
	{
		public delegate void ScanDelegate();

		//private static Thread threadScan;

		#region Scanning

		public static void StartScanning(TreeModel treeModel, int progressMin, int progressMax)
		{
			DataContext.OneClickScanState.ItemsTreeModel = treeModel;
			DataContext.OneClickScanState.PrivacyNodes = new List<BadRegistryKey>();
			//DataContext.OneClickScanState.ArrBadRegistryKeys.Clear(scanRegistrySettings.SectionCount);
			DataContext.OneClickScanState.PrivacyFilesItemsToFixCount = 0;

			// Begin scanning
			int index = 0;
			foreach (var privacyScannerNode in DataContext.PrivacyScannerRoots)
			{
				StartScanner(new PrivacyScanner(privacyScannerNode));
				index++;
				DataContext.OneClickScanState.ScanningProgress = index * (progressMax - progressMin) / DataContext.PrivacyScannerRoots.Count + progressMin;
			}
		}

		/// <summary>
		/// Starts a scanner
		/// </summary>
		private static void StartScanner(ScannerBase scannerName)
		{
			DataContext.OneClickScanState.CurrentScanner = scannerName;

			System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			ScanDelegate objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);

			// Update section name
			scannerName.RootNode.SectionName = scannerName.ScannerName;
			DataContext.OneClickScanState.CurrentSection = scannerName.ScannerName;

			try
			{
				// Start scanning
				objScan.Invoke();
			}
			finally
			{
				// next actions will be executed even thread was been aborted
				DataContext.OneClickScanState.CurrentScanner.RootNode.VerifyCheckState();
				if (scannerName.RootNode.Nodes.Count > 0)
				{
					DataContext.OneClickScanState.ItemsTreeModel.Nodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
					DataContext.OneClickScanState.PrivacyNodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
				}
			}
		}
		#endregion Scanning
	}
}