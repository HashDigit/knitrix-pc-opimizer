using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Business.TaskScheduler;
using Common;
using Data;

namespace Business
{
	public class ScanScheduleLogic
	{
		/// <summary>
		/// Getting list of task related to Knitrix
		/// </summary>
		/// <returns></returns>
		public static List<Task> List()
		{
			var scheduledTasks = new ScheduledTasks();
			var allTaskNames = scheduledTasks.GetTaskNames();
			var tasks = new List<Task>();
			foreach (var allTaskName in allTaskNames)
			{
				if (allTaskName.ToLower().StartsWith(Application.ProductName.ToLower() + Config.ScanScheduleTaskNamePrefix.ToLower()))
					tasks.Add(scheduledTasks.OpenTask(allTaskName));
			}
			return tasks;
		}

		/// <summary>
		/// Create new scan schedule task with default settings
		/// </summary>
		/// <returns></returns>
		public static Task Create()
		{
			try
			{
				var scheduledTasks = new ScheduledTasks();
				var newTask = scheduledTasks.CreateTask(Application.ProductName + Config.ScanScheduleTaskNamePrefix + Guid.NewGuid().ToString());
				newTask.ApplicationName = Application.ExecutablePath;
				newTask.Parameters = "scan";
				newTask.Comment = "";
				newTask.Creator = "";
				newTask.WorkingDirectory = Application.StartupPath;
				newTask.SetAccountInformation(Environment.UserName, (string)null);
				newTask.Flags = TaskFlags.RunOnlyIfLoggedOn;
				newTask.IdleWaitDeadlineMinutes = 20;
				newTask.IdleWaitMinutes = 10;
				newTask.MaxRunTime = new TimeSpan(50, 0, 0);
				newTask.Priority = System.Diagnostics.ProcessPriorityClass.Normal;
				return newTask;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while scan schedule task creation.\r\nError:\r\n{0}", ex));
				return null;
			}
		}

		public static void Save(Task task)
		{
			try
			{
				task.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving scan schedule task.\r\nError:\r\n{0}", ex));
			}
		}

		/// <summary>
		/// Open existing task
		/// </summary>
		/// <returns></returns>
		public static Task Open(string taskName)
		{
			try
			{
				var scheduledTasks = new ScheduledTasks();
				return scheduledTasks.OpenTask(taskName);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while opening scan schedule task '{0}'.\r\nError:\r\n{1}", taskName, ex));
				return null;
			}
		}

		/// <summary>
		/// Delete existing task
		/// </summary>
		/// <returns></returns>
		public static void Delete(string taskName)
		{
			try
			{
				var scheduledTasks = new ScheduledTasks();
				scheduledTasks.DeleteTask(taskName);
			}
			catch(Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while deletion scan schedule task '{0}'.\r\nError:\r\n{1}", taskName, ex));	
			}
		}
	}
}