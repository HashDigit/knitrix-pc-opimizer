using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
using Common;
using Data;

namespace Business
{
	public class RecentHistoryLogic
	{
		/// <summary>
		/// "Clear Recent History" functionality
		/// </summary>
		public static void Clear()
		{
			if (Config.EnableFixing)
			{
				var pathsToClear = new List<string>();
				// TODO :: (AK) �������� � ���� ������ ���-����?
				pathsToClear.Add(@"{userprofile}\appdata\roaming\microsoft\windows\recent".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\recent".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\searches".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"c:\windows\debug");
				pathsToClear.Add(@"{userprofile}\appdata\local\microsoft\windows\history".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\appdata\roaming\microsoft\windows\cookies".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				foreach (var pathToClear in pathsToClear)
					DirectoryHelper.Clear(pathToClear);

				// clear IE history
				var p = new Process();
				try
				{
					p.StartInfo.CreateNoWindow = true;
					p.StartInfo.UseShellExecute = false;
					p.StartInfo.RedirectStandardError = true;
					p.StartInfo.RedirectStandardOutput = true;
					p.StartInfo.FileName = "rundll32.exe";
					p.StartInfo.Arguments = "InetCpl.cpl,ClearMyTracksByProcess 1";
					p.Start();
					p.WaitForExit();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}
				finally
				{
					if (!p.HasExited)
					{
						p.Kill();
						p.WaitForExit();
					}
				}
			}
			else
			{
				// dummy functionality, just sleeping
				Thread.Sleep(2000);
			}
		}
	}
}