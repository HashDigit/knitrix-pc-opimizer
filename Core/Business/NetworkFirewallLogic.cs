using System;
using System.Diagnostics;
using System.Management;
using Common;
using Data;

namespace Core.Business
{
	public class NetworkFirewallLogic
	{
		public static NetworkFirewallStatusEnum GetStatus(int issueCount)
		{
			if (issueCount >= 75 || issueCount == -1)
				return NetworkFirewallStatusEnum.Red;

			if (IsAntiVirusExists())
				return NetworkFirewallStatusEnum.Green;
			else
				if (IsNativeFirewallUp())
					return NetworkFirewallStatusEnum.Yellow;
				else
					return NetworkFirewallStatusEnum.Red;

		}

		private static bool IsAntiVirusExists()
		{
			try
			{
				var search = new ManagementObjectSearcher("SELECT * FROM AntiVirusProduct");
				search.Scope = new ManagementScope(@"ROOT\SecurityCenter");
				string name = "";
				foreach (ManagementObject obj in search.Get())
					return true;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			try
			{
				var search = new ManagementObjectSearcher("SELECT * FROM AntiVirusProduct");
				search.Scope = new ManagementScope(@"ROOT\SecurityCenter2");
				string name = "";
				foreach (ManagementObject obj in search.Get())
					return true;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			return false;
		}

		private static bool IsNativeFirewallUp()
		{
			var p = new Process();
			try
			{
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.UseShellExecute = false;
				p.StartInfo.RedirectStandardError = true;
				p.StartInfo.RedirectStandardOutput = true;
				p.StartInfo.FileName = "netsh";
				p.StartInfo.Arguments = "firewall show opmode";
				p.Start();
				var str = p.StandardOutput.ReadToEnd();
				p.WaitForExit();
				return p.ExitCode == 0 && !str.Contains("Disable"); // error? 0 - OK, 1 - error
			}
			finally
			{
				if (!p.HasExited)
				{
					p.Kill();
					p.WaitForExit();
				}

			}
		}
	}
}