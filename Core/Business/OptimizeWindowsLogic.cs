﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Common;
using Microsoft.Win32;

namespace Core.Business
{
    public class OptimizeWindowsLogic
    {
        private const string _DESKTOP_SEARCH_SERVICE_NAME = "Windows Search";

        public static bool IsAllowHibernationEnabled
        {
            get
            {
                try
                {
                    // if you have a file "hiberfil.sys" in C: , a hibernate is possible.
                    // http://social.msdn.microsoft.com/forums/en-US/sidebargadfetdevelopment/thread/76ff4108-1212-4744-85ae-2a86cda53bcf
                    return File.Exists(Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), @"hiberfil.sys"));
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
                return false;
            }
            set
            {
                try
                {
                    var psi = new ProcessStartInfo("POWERCFG.exe", value ? "/HIBERNATE on" : "/HIBERNATE off");
                    psi.UseShellExecute = false;
                    psi.CreateNoWindow = true;
                    Process.Start(psi);
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
            }
        }

        public static bool IsErrorReportingEnabled
        {
            get
            {
                try
                {
                    RegistryKey regKey = null;
                    if (IsOldWindowsVersion)
                        regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\PCHealth\ErrorReporting");
                    else
                        regKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\Windows Error Reporting");
                    if (IsOldWindowsVersion)
                        return (int)regKey.GetValue("DoReport") == 1;

                    return (int)regKey.GetValue("Disabled") == 0;
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
                return false;
            }
            set
            {
                try
                {
                    RegistryKey regKey = null;
                    if (IsOldWindowsVersion)
                        regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\PCHealth\ErrorReporting", true);
                    else
                        regKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\Windows Error Reporting", true);
                    if (IsOldWindowsVersion)
                        regKey.SetValue("DoReport", value ? 1 : 0); // 0 - disabled, 1 - enabled
                    else
                        regKey.SetValue("Disabled", value ? 0 : 1); // 0 - enabled, 1 - disabled
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
            }
        }

        public static bool IsPrefetchEnabled
        {
            get
            {
                try
                {
                    var regKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\ControlSet001\Control\Session Manager\Memory Management\PrefetchParameters");
                    return (int)regKey.GetValue(IsVista ? "EnableSuperfetch" : "EnablePrefetcher") == 3;
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
                return false;
            }
            set
            {
                try
                {
                    var regKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\ControlSet001\Control\Session Manager\Memory Management\PrefetchParameters", true);
                    regKey.SetValue(IsVista ? "EnableSuperfetch" : "EnablePrefetcher", value ? 3 : 0); // 0 - disable, 3 - enabled
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
            }
        }

        public static bool IsClearPageFileAtShutdownEnabled
        {
            get
            {
                try
                {
                    var regKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management");
                    return (int)regKey.GetValue("ClearPageFileAtShutdown") == 1;
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
                return false;
            }
            set
            {
                try
                {
                    var regKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", true);
                    regKey.SetValue("ClearPageFileAtShutdown", value ? 1 : 0); // 0 - disable, 1 - enabled
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
            }
        }

        /// <summary>
        /// Desktop Search Available in Vista & Seven only?
        /// </summary>
        public static bool IsDesktopSearchAvailable
        {
            get
            {
                int majorVersion = Environment.OSVersion.Version.Major;
                int minorVersion = Environment.OSVersion.Version.Minor;

                // Windows ME
                if (majorVersion == 4 && minorVersion == 90)
                    return false;

                // Windows XP
                if (majorVersion == 5 && minorVersion == 1)
                    return false;

                // Windows Vista
                if (majorVersion == 6 && minorVersion == 0)
                    return true;

                // Windows Se7en
                if (majorVersion == 6 && minorVersion == 1)
                    return true;

                // All others : Win 95, 98, 2000, Server
                return false;
            }
        }

        public static bool IsDesktopSearchEnabled
        {
            get
            {
                if (!IsDesktopSearchAvailable)
                    return false;

                var service = new ServiceController(_DESKTOP_SEARCH_SERVICE_NAME);
                try
                {
                    return service.Status == ServiceControllerStatus.Running;
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
                return false;
            }
            set
            {
                if (!IsDesktopSearchAvailable)
                    return;

                var service = new ServiceController(_DESKTOP_SEARCH_SERVICE_NAME);
                try
                {
                    if (!value && service.Status != ServiceControllerStatus.Stopped)
                    {
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped);
                    }
                    var regKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\services\WSearch", true);
                    // (AK) START small magic perturbation, без этой залепы не работает, возможно с ней тоже не работает
                    regKey.SetValue("Start", 4);
                    Thread.Sleep(500);
                    // (AK) END small magic perturbation
                    regKey.SetValue("Start", value ? 2 : 4);  // 2 - Automatic, 4 - Disabled
                    // (AK) START small magic perturbation
                    Thread.Sleep(500);
                    // (AK) END small magic perturbation
                    if (value && service.Status != ServiceControllerStatus.Running)
                    {
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                    }
                }
                catch (Exception ex)
                {
                    CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
                }
            }
        }

        /// <summary>
        /// Is not Vista and not Seven?
        /// </summary>
        private static bool IsOldWindowsVersion
        {
            get
            {
                int majorVersion = Environment.OSVersion.Version.Major;
                int minorVersion = Environment.OSVersion.Version.Minor;

                // Windows ME
                if (majorVersion == 4 && minorVersion == 90)
                    return true;

                // Windows XP
                if (majorVersion == 5 && minorVersion == 1)
                    return true;

                // Windows Vista
                if (majorVersion == 6 && minorVersion == 0)
                    return false;

                // Windows Se7en
                if (majorVersion == 6 && minorVersion == 1)
                    return false;

                // All others : Win 95, 98, 2000, Server
                return true;
            }
        }

        /// <summary>
        /// Is Vista?
        /// </summary>
        private static bool IsVista
        {
            get
            {
                int majorVersion = Environment.OSVersion.Version.Major;
                int minorVersion = Environment.OSVersion.Version.Minor;

                // Windows ME
                if (majorVersion == 4 && minorVersion == 90)
                    return false;

                // Windows XP
                if (majorVersion == 5 && minorVersion == 1)
                    return false;

                // Windows Vista
                if (majorVersion == 6 && minorVersion == 0)
                    return true;

                // Windows Se7en
                if (majorVersion == 6 && minorVersion == 1)
                    return false;

                // All others : Win 95, 98, 2000, Server
                return false;
            }
        }
    }
}
