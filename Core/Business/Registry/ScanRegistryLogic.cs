using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using Business.Registry.Scanners;
using Common;
using Common_Tools.TreeViewAdv.Tree;
using Data;
using Data.Registry;

namespace Business.Registry
{
	public class ScanRegistryLogic
	{
		public delegate void ScanDelegate();

		//private static Thread threadScan;

		#region Scanning

		/// <summary>
		/// Begins scanning for errors in the registry
		/// </summary>
		public static void StartScanning(TreeModel treeModel, RegistryScanSettings registryScanSettings, int progressMin, int progressMax)
		{
			DataContext.OneClickScanState.ItemsTreeModel = treeModel;
			DataContext.OneClickScanState.RegistryNodes = new List<BadRegistryKey>();
			DataContext.OneClickScanState.RegistryProblemsItemsToFixCount = 0;

			// Begin scanning
			try
			{
				if (registryScanSettings.ApplicationInfo)
					StartScanner(new ApplicationInfo());
				DataContext.OneClickScanState.ScanningProgress = 1 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.ProgramLocations)
					StartScanner(new ApplicationPaths());
				DataContext.OneClickScanState.ScanningProgress = 2 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.SoftwareSettings)
					StartScanner(new ApplicationSettings());
				DataContext.OneClickScanState.ScanningProgress = 3 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.Startup)
					StartScanner(new StartupFiles());
				DataContext.OneClickScanState.ScanningProgress = 4 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.SystemDrivers)
					StartScanner(new SystemDrivers());
				DataContext.OneClickScanState.ScanningProgress = 5 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.SharedDLLs)
					StartScanner(new SharedDLLs());
				DataContext.OneClickScanState.ScanningProgress = 6 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.HelpFiles)
					StartScanner(new WindowsHelpFiles());
				DataContext.OneClickScanState.ScanningProgress = 7 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.SoundEvents)
					StartScanner(new WindowsSounds());
				DataContext.OneClickScanState.ScanningProgress = 8 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.HistoryList)
					StartScanner(new RecentDocs());
				DataContext.OneClickScanState.ScanningProgress = 9 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.WindowsFonts)
					StartScanner(new WindowsFonts());
				DataContext.OneClickScanState.ScanningProgress = 10 * (progressMax - progressMin) / 11 + progressMin;

				if (registryScanSettings.ActiveXCOM)
					StartScanner(new ActivexComObjects());
				DataContext.OneClickScanState.ScanningProgress = 11 * (progressMax - progressMin) / 11 + progressMin;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Starts a scanner
		/// </summary>
		private static void StartScanner(ScannerBase scannerName)
		{
			DataContext.OneClickScanState.CurrentScanner = scannerName;

			System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			ScanDelegate objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);

			// Update section name
			scannerName.RootNode.SectionName = scannerName.ScannerName;
			//scannerName.RootNode.Img = this.imageList.Images[scannerName.GetType().Name];
			DataContext.OneClickScanState.CurrentSection = scannerName.ScannerName;

			try
			{
				// Start scanning
				objScan.Invoke();
			}
			finally
			{
				// next actions will be executed even thread was been aborted
				DataContext.OneClickScanState.CurrentScanner.RootNode.VerifyCheckState();
				if (scannerName.RootNode.Nodes.Count > 0)
				{
					DataContext.OneClickScanState.ItemsTreeModel.Nodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
					DataContext.OneClickScanState.RegistryNodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
				}
			}
		}
		#endregion Scanning

		/// <summary>
		/// <para>Stores an invalid registry key to array list</para>
		/// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
		/// </summary>
		/// <param name="Problem">Reason its invalid</param>
		/// <param name="Path">The path to registry key (including registry hive)</param>
		/// <returns>True if it was added</returns>
		public static bool StoreInvalidKey(string Problem, string Path)
		{
			return StoreInvalidKey(Problem, Path, "");
		}

		/// <summary>
		/// <para>Stores an invalid registry key to array list</para>
		/// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
		/// </summary>
		/// <param name="problem">Reason its invalid</param>
		/// <param name="regPath">The path to registry key (including registry hive)</param>
		/// <param name="valueName">Value name (leave blank if theres none)</param>
		/// <returns>True if it was added. Otherwise, false.</returns>
		public static bool StoreInvalidKey(string problem, string regPath, string valueName)
		{
			string baseKey, subKey;

			// Check for null parameters
			if (string.IsNullOrEmpty(problem) || string.IsNullOrEmpty(regPath))
				return false;

			// Make sure registry key exists
			if (!RegistryHelper.RegKeyExists(regPath))
				return false;

			// Parse registry key to base and subkey
			if (!RegistryHelper.ParseRegKeyPath(regPath, out baseKey, out subKey))
				return false;

			// Check for ignored registry path
			if (RegistryHelper.IsOnIgnoreList(regPath))
				return false;

			// If value name is specified, see if it exists
			if (!string.IsNullOrEmpty(valueName))
				if (!RegistryHelper.ValueNameExists(baseKey, subKey, valueName))
					return false;

			try
			{
				// Throws exception if user doesnt have permission
				RegistryPermission regPermission = new RegistryPermission(RegistryPermissionAccess.AllAccess, regPath);
				regPermission.Demand();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
				return false;
			}

			var badRegistryKey = new BadRegistryKey(CheckState.Checked, problem, baseKey, subKey, valueName);
			badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.RegistryProblems);

			DataContext.OneClickScanState.CurrentScanner.RootNode.Nodes.Add(badRegistryKey);
			DataContext.OneClickScanState.ItemsToFixCount++;
			DataContext.OneClickScanState.RegistryProblemsItemsToFixCount++;
			DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount++;

			return true;
		}
	}
}