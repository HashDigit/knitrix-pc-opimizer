﻿using System;
using System.Collections.Generic;
using System.IO;
using Common;
using Data;
using Microsoft.Win32;

namespace Business.Registry.Scanners
{
	public class WindowsHelpFiles : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.WindowsHelpFiles; }
		}

		public static void Scan()
		{
			try
			{
				CheckHelpFiles(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\HTML Help"));
				CheckHelpFiles(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\Help"));
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Scans for invalid windows help files
		/// </summary>
		private static void CheckHelpFiles(RegistryKey regKey)
		{
			try
			{
				if (regKey == null)
					return;

				//////Main.Logger.WriteLine("Checking for missing help files in " + regKey.Name);

				foreach (string strHelpFile in regKey.GetValueNames())
				{
					string strHelpPath = regKey.GetValue(strHelpFile) as string;

					DataContext.OneClickScanState.CurrentScannedObject = strHelpPath;

					if (!HelpFileExists(strHelpFile, strHelpPath))
						ScanRegistryLogic.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), strHelpFile);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Sees if the help file exists
		/// </summary>
		/// <param name="helpFile">Should contain the filename</param>
		/// <param name="helpPath">Should be the path to file</param>
		/// <returns>True if it exists</returns>
		private static bool HelpFileExists(string helpFile, string helpPath)
		{
			if (string.IsNullOrEmpty(helpFile) || string.IsNullOrEmpty(helpPath))
				return true;

			if (RegistryHelper.FileExists(helpPath))
				return true;

			if (RegistryHelper.FileExists(helpFile))
				return true;

			// NOTE :: (AK) Path.Combine try-catch ?
			if (RegistryHelper.FileExists(Path.Combine(helpPath, helpFile)))
				return true;

			return false;
		}
	}
}
