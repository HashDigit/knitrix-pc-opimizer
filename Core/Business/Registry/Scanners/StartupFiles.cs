﻿using System;
using System.Collections.Generic;
using Common;
using Data;
using Microsoft.Win32;

namespace Business.Registry.Scanners
{
	public class StartupFiles : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.StartupFiles; }
		}

		public static void Scan()
		{
			try
			{
				// all user keys
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				// current user keys
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				if (RegistryHelper.Is64BitOS)
				{
					// all user keys
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					checkAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));

					// current user keys
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					checkAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Checks for invalid files in startup registry key
		/// </summary>
		/// <param name="regKey">The registry key to scan</param>
		private static void checkAutoRun(RegistryKey regKey)
		{
			try
			{
				if (regKey == null)
					return;

				//////Main.Logger.WriteLine("Checking for invalid files in " + regKey.Name);

				foreach (string strProgName in regKey.GetValueNames())
				{
					string strRunPath = regKey.GetValue(strProgName) as string;
					string strFilePath, strArgs;

					if (!string.IsNullOrEmpty(strRunPath))
					{
						DataContext.OneClickScanState.CurrentScannedObject = strRunPath;

						// Check run path by itself
						if (RegistryHelper.FileExists(strRunPath))
							continue;

						// See if file exists (also checks if string is null)
						if (RegistryHelper.ExtractArguments(strRunPath, out strFilePath, out strArgs))
							continue;

						if (RegistryHelper.ExtractArguments2(strRunPath, out strFilePath, out strArgs))
							continue;

						ScanRegistryLogic.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strProgName);
					}
				}

				regKey.Close();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

	}
}
