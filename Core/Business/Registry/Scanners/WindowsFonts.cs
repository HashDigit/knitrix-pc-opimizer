﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Common;
using Data;
using Microsoft.Win32;

namespace Business.Registry.Scanners
{
    public class WindowsFonts : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.WindowsFonts; }
        }

        [DllImport("shell32.dll")]
        public static extern bool SHGetSpecialFolderPath(IntPtr hwndOwner, [Out] StringBuilder strPath, int nFolder, bool fCreate);

        const int CSIDL_FONTS = 0x0014;    // windows\fonts 

        /// <summary>
        /// Finds invalid font references
        /// </summary>
        public static void Scan()
        {
            StringBuilder strPath = new StringBuilder(260);

            try
            {
                using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows NT\\CurrentVersion\\Fonts"))
                {
                    if (regKey == null)
                        return;

                    //////Main.Logger.WriteLine("Scanning for invalid fonts");

                    if (!SHGetSpecialFolderPath(IntPtr.Zero, strPath, CSIDL_FONTS, false))
                        return;

                    foreach (string strFontName in regKey.GetValueNames())
                    {
                        string strValue = regKey.GetValue(strFontName) as string;

                        // Skip if value is empty
                        if (string.IsNullOrEmpty(strValue))
                            continue;

                        // Check value by itself
						if (RegistryHelper.FileExists(strValue))
                            continue;

                        // Check for font in fonts folder
                        string strFontPath = String.Format("{0}\\{1}", strPath.ToString(), strValue);

                        DataContext.OneClickScanState.CurrentScannedObject = strFontPath;

                        if (!File.Exists(strFontPath))
                            ScanRegistryLogic.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), strFontName);
                    }

                }
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
    }
}
