﻿using System;
using System.Collections.Generic;
using Common;
using Data;
using Microsoft.Win32;

namespace Business.Registry.Scanners
{
	public class SystemDrivers : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.SystemDrivers; }
        }

        /// <summary>
        /// Scans for invalid references to fonts
        /// </summary>
        public static void Scan()
        {
            try
            {
                using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Drivers32"))
                {
                    if (regKey == null)
                        return;

                    //////Main.Logger.WriteLine("Scanning for missing drivers");

                    foreach (string strDriverName in regKey.GetValueNames())
                    {
                        string strValue = regKey.GetValue(strDriverName) as string;

                        DataContext.OneClickScanState.CurrentScannedObject = strValue;

                        if (!string.IsNullOrEmpty(strValue))
							if (!RegistryHelper.FileExists(strValue))
                                ScanRegistryLogic.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strDriverName);
                    }
                }
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
    }
}
