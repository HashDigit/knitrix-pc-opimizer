﻿using System;
using System.Collections.Generic;
using Common;
using Data;
using Microsoft.Win32;

namespace Business.Registry.Scanners
{
    public class SharedDLLs : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.SharedDLLs; }
        }

        /// <summary>
        /// Scan for missing links to DLLS
        /// </summary>
        public static void Scan()
        {
            try
            {
                RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs");

                if (regKey == null)
                    return;

                //////Main.Logger.WriteLine("Scanning for missing shared DLLs");

                // Validate Each DLL from the value names
                foreach (string strFilePath in regKey.GetValueNames())
                {
                    // Update scan dialog
					DataContext.OneClickScanState.CurrentScannedObject = strFilePath;

                    if (!string.IsNullOrEmpty(strFilePath))
						if (!RegistryHelper.FileExists(strFilePath))
                            ScanRegistryLogic.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strFilePath);
                }

                regKey.Close();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
    }
}
