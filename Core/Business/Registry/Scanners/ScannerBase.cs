﻿using System;
using Data.Registry.Misc;
using Business.Registry.Misc;

namespace Business.Registry.Scanners
{
	public abstract class ScannerBase
	{
		public delegate void CurrentScannedObject(string currentScannedObject);
		public static event CurrentScannedObject CurrentScannedObject_Changed;

		public delegate void StoreInvalidKey(string problem, string regPath, string valueName);
		public static event StoreInvalidKey InvalidKey_Found;

		/// <summary>
		/// Returns the scanner name
		/// </summary>
		abstract public string ScannerName
		{
			get;
		}

		/// <summary>
		/// The root node (used for scan dialog)
		/// </summary>
		public BadRegistryKey RootNode = new BadRegistryKey();

		/// <summary>
		/// Returns the scanner name
		/// </summary>
		public override string ToString()
		{
			return (string)ScannerName.Clone();
		}

		//public virtual void Scan()
		//{
		//    return;
		//}

		public static void ThrowEvent_CurrentScannedObject_Changed(string currentScannedObject)
		{
			if (CurrentScannedObject_Changed != null)
				CurrentScannedObject_Changed(currentScannedObject);
		}

		public static void ThrowEvent_InvalidKey_Found(string problem, string regPath, string valueName)
		{
			if (InvalidKey_Found != null)
				InvalidKey_Found(problem, regPath, valueName);
		}


		public static void UnsubscribeAllEvents()
		{
			// CurrentScannedObject_Changed
			if (CurrentScannedObject_Changed != null)
			{
				var subscribers = CurrentScannedObject_Changed.GetInvocationList();
				for (int i = 0; i < subscribers.Length; i++)
					CurrentScannedObject_Changed -= subscribers[i] as CurrentScannedObject;
			}

			// InvalidKey_Found
			if (CurrentScannedObject_Changed != null)
			{
				var subscribers = InvalidKey_Found.GetInvocationList();
				for (int i = 0; i < subscribers.Length; i++)
					InvalidKey_Found -= subscribers[i] as StoreInvalidKey;
			}
		}
	}
}
