using System;
using System.Threading;
using Business.Registry.Scanners;
using Data;

namespace Business.Registry
{
	public class RegistryLogic
	{
		public delegate void CurrentScanner(string currentScanner);
		public static event CurrentScanner CurrentScanner_Changed;

		public delegate void ScanDelegate();

		private static Thread threadMain, threadScan;


		public static void StartScanning()
		{
			// Starts scanning registry on seperate thread
			threadMain = new Thread(new ThreadStart(StartScanning_Threaded));
			threadMain.Start();
		}

		/// <summary>
		/// Begins scanning for errors in the registry
		/// </summary>
		private static void StartScanning_Threaded()
		{
			// Get start time of scan
			DateTime dateTimeStart = DateTime.Now;

			// Begin Critical Region
			Thread.BeginCriticalRegion();

			// Begin scanning
			try
			{
				// Main.Logger.WriteLine("Started scan at " + DateTime.Now.ToString());
				// Main.Logger.WriteLine();

				if (DataContext.ScanRegistrySettings.Startup)
					StartScanner(new StartupFiles());

				if (DataContext.ScanRegistrySettings.SharedDLL)
					StartScanner(new SharedDLLs());

				if (DataContext.ScanRegistrySettings.Fonts)
					StartScanner(new WindowsFonts());

				if (DataContext.ScanRegistrySettings.AppInfo)
					StartScanner(new ApplicationInfo());

				if (DataContext.ScanRegistrySettings.AppPaths)
					StartScanner(new ApplicationPaths());

				if (DataContext.ScanRegistrySettings.Activex)
					StartScanner(new ActivexComObjects());

				if (DataContext.ScanRegistrySettings.Drivers)
					StartScanner(new SystemDrivers());

				if (DataContext.ScanRegistrySettings.HelpFiles)
					StartScanner(new WindowsHelpFiles());

				if (DataContext.ScanRegistrySettings.Sounds)
					StartScanner(new WindowsSounds());

				if (DataContext.ScanRegistrySettings.AppSettings)
					StartScanner(new ApplicationSettings());

				if (DataContext.ScanRegistrySettings.HistoryList)
					StartScanner(new RecentDocs());

				//this.DialogResult = DialogResult.OK;
			}
			catch (ThreadAbortException)
			{
				// Scanning was aborted
				// Main.Logger.Write("User aborted scan... ");
				if (threadScan != null && threadScan.IsAlive)
					threadScan.Abort();

				//this.DialogResult = DialogResult.Abort;
				// Main.Logger.WriteLine("Exiting.\r\n");
			}
			finally
			{
				// Compute time between start and end of scan
				TimeSpan ts = DateTime.Now.Subtract(dateTimeStart);

				// Main.Logger.WriteLine(string.Format("Total time elapsed: {0} seconds", ts.TotalSeconds));
				// Main.Logger.WriteLine(string.Format("Total problems found: {0}", ScanRegistryDlg.arrBadRegistryKeys.Count));
				// Main.Logger.WriteLine(string.Format("Total objects scanned: {0}", ScanRegistryDlg.arrBadRegistryKeys.ItemsScanned));
				// Main.Logger.WriteLine();
				// Main.Logger.WriteLine("Finished scan at " + DateTime.Now.ToString());

				UnsubscribeAllEvents();
			}

			// End Critical Region
			Thread.EndCriticalRegion();

			// Dialog will be closed automatically

			return;
		}

		/// <summary>
		/// Starts a scanner
		/// </summary>
		private static void StartScanner(ScannerBase scannerName)
		{
			//ScannerBase.CurrentScannedObject_Changed += new ScannerBase.CurrentScannedObject(ScannerBase_CurrentScannedObject_Changed);
			//ScannerBase.InvalidKey_Found += new ScannerBase.StoreInvalidKey(ScannerBase_InvalidKey_Found);
			//currentScanner = scannerName;

			System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			var objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);

			// Main.Logger.WriteLine("Starting scanning: " + scannerName.ScannerName);

			// Update section name
			if (CurrentScanner_Changed != null)
				CurrentScanner_Changed(scannerName.ScannerName);
			//scannerName.RootNode.SectionName = scannerName.ScannerName;
			//scannerName.RootNode.Img = this.imageList.Images[scannerName.GetType().Name];
			//ScanRegistryDlg.CurrentSection = scannerName.ScannerName;

			// Start scanning
			threadScan = new Thread(new ThreadStart(objScan));
			threadScan.Start();
			threadScan.Join();

			// Wait 250ms
			Thread.Sleep(250);

			//if (scannerName.RootNode.Nodes.Count > 0)
			//	ScanRegistryDlg.arrBadRegistryKeys.Add(scannerName.RootNode);

			// Main.Logger.WriteLine("Finished scanning: " + scannerName.ScannerName);
			// Main.Logger.WriteLine();

			//this.progressBar.Position++;
			//ScannerBase.CurrentScannedObject_Changed -= new ScannerBase.CurrentScannedObject(ScannerBase_CurrentScannedObject_Changed);
			//ScannerBase.InvalidKey_Found -= new ScannerBase.StoreInvalidKey(ScannerBase_InvalidKey_Found);
		}

		private static void UnsubscribeAllEvents()
		{
			if (CurrentScanner_Changed != null)
			{
				Delegate[] subscribers = CurrentScanner_Changed.GetInvocationList();
				for (int i = 0; i < subscribers.Length; i++)
					CurrentScanner_Changed -= subscribers[i] as CurrentScanner;
			}

			ScannerBase.UnsubscribeAllEvents();
		}
	}
}