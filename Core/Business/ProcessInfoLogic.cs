using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using Common;
using Data;

namespace Business
{
	public class ProcessInfoLogic
	{
		private static bool _isRunning = false;
		/// <summary>
		/// Get info about currently running Processes on PC.
		/// Merge current processes into to DataContext.Processes. 
		/// </summary>
		public static void ListInDataContext_Old()
		{
			var processes = Process.GetProcesses();

			//DataContext.ProcessInfos.Clear();
			var processInfoList = new List<ProcessInfo>();
			ProcessInfo processInfo;
			for (var i = 0; i < processes.Length; i++)
			{
				processInfo = new ProcessInfo();
				//processInfo.InfoType = (ProcessInfoTypeEnum)IntUtils.GetRandom(5); // TODO :: (AK) set correct InfoType
				processInfo.ProcessName = processes[i].ProcessName;
				processInfo.Memory = (ulong)processes[i].WorkingSet64;
				try
				{
					processInfo.Description = processes[i].MainModule.FileVersionInfo.FileDescription;
				}
				catch
				{
					processInfo.Description = "";
				}
				try
				{
					processInfo.StartTime = processes[i].StartTime.ToLongTimeString();
				}
				catch
				{
					processInfo.StartTime = "";
				}
				try
				{
					processInfo.Path = processes[i].MainModule.FileName;
				}
				catch
				{
					processInfo.Path = "";
				}

				processInfo.ProcessId = processes[i].Id;
				try
				{
					processInfo.Company = processes[i].MainModule.FileVersionInfo.CompanyName;
				}
				catch
				{
					processInfo.Company = "";
				}
				processInfo.Priority = processes[i].BasePriority;
				processInfo.Handles = processes[i].HandleCount;
				processInfo.Threads = processes[i].Threads.Count;
				try
				{
					processInfo.TotalProcessorTime = processes[i].TotalProcessorTime;
				}
				catch
				{
					processInfo.TotalProcessorTime = new TimeSpan(0);
				}
				processInfo.VirtualMemory = (ulong)processes[i].VirtualMemorySize64;

				processInfoList.Add(processInfo);
			}
			// Merge to DataContext;
			Merge(processInfoList);
		}

		public static void ListInDataContext_Old2()
		{
			if (_isRunning)
				return;

			_isRunning = true;
			var oConn = new ConnectionOptions();
			var oMs = new ManagementScope("\\\\localhost", oConn);
			var oQuery = new ObjectQuery("select * from Win32_Process");
			var oSearcher = new ManagementObjectSearcher(oMs, oQuery);
			var processes = oSearcher.Get();

			var processInfoList = new List<ProcessInfo>();
			ProcessInfo processInfo;
			foreach (ManagementObject process in processes)
			{
				processInfo = new ProcessInfo();
				//processInfo.InfoType = (ProcessInfoTypeEnum)IntUtils.GetRandom(5); // TODO :: (AK) set correct InfoType
				processInfo.ProcessName = (string)process["Name"];
				processInfo.Memory = (ulong)process["WorkingSetSize"]; // WorkingSetSize64??
				/*
				try
				{
					processInfo.Description = processes[i].MainModule.FileVersionInfo.FileDescription;
				}
				catch
				{
					processInfo.Description = "";
				}
				*/
				try
				{
					processInfo.StartTime = ((DateTime)process["CreationDate"]).ToLongTimeString();
				}
				catch
				{
					processInfo.StartTime = "";
				}
				/*
				try
				{
					processInfo.Path = processes[i].MainModule.FileName;
				}
				catch
				{
					processInfo.Path = "";
				}
				*/

				processInfo.ProcessId = (int)(uint)process["ProcessId"];
				/*
				try
				{
					processInfo.Company = processes[i].MainModule.FileVersionInfo.CompanyName;
				}
				catch
				{
					processInfo.Company = "";
				}
				*/
				processInfo.Priority = (int)(uint)process["Priority"];
				/*
				processInfo.Handles = processes[i].HandleCount;
				processInfo.Threads = processes[i].Threads.Count;
				try
				{
					processInfo.TotalProcessorTime = processes[i].TotalProcessorTime;
				}
				catch
				{
					processInfo.TotalProcessorTime = new TimeSpan(0);
				}
				processInfo.VirtualMemory = processes[i].VirtualMemorySize64;
				*/

				processInfoList.Add(processInfo);
			}
			// Merge to DataContext;
			Merge(processInfoList);
			_isRunning = false;
		}

		public static void ListInDataContext()
		{
			if (_isRunning)
				return;

			_isRunning = true;
			var oConn = new ConnectionOptions();
			var oMs = new ManagementScope("\\\\localhost", oConn);
			var oQuery = new ObjectQuery("select * from Win32_Process");
			var oSearcher = new ManagementObjectSearcher(oMs, oQuery);
			var processes = oSearcher.Get();

			var processInfoList = new List<ProcessInfo>();
			MainDS.ProcessInfoRow processInfoRow;
			var tempDS = new MainDS();
			DataContext.FoundUnwantedProcess = false;
			foreach (ManagementObject process in processes)
			{
				processInfoRow = tempDS.ProcessInfo.NewProcessInfoRow();
				processInfoRow.TimeStamp = DateTime.Now;
				processInfoRow.ProcessName = (string)process["Name"];
				processInfoRow.Memory = (ulong)process["WorkingSetSize"];
				processInfoRow.ProcessId = (int)(uint)process["ProcessId"];
				processInfoRow.Path = (string)process["ExecutablePath"] ?? "";
				processInfoRow.Priority = (int)(uint)process["Priority"];
				processInfoRow.Handles = (int)(uint)process["HandleCount"];
				processInfoRow.Threads = (int)(uint)process["ThreadCount"];
				processInfoRow.StartTime = (string)process["CreationDate"] ?? "";
				processInfoRow.VirtualMemory = (ulong)process["VirtualSize"];
				if (!string.IsNullOrEmpty(processInfoRow.StartTime))
					processInfoRow.StartTime = DateTime.ParseExact(processInfoRow.StartTime.Substring(0, 14), "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture).ToLongTimeString();
				var prevProcessInfoRow = DataContext.MainDS.ProcessInfo.FindByProcessId(processInfoRow.ProcessId);
				try
				{
					if (prevProcessInfoRow != null)
					{
						double secondsCount = (processInfoRow.TimeStamp - prevProcessInfoRow.TimeStamp).TotalSeconds;
						processInfoRow.CpuPercent =
							(int)((new TimeSpan((long)(ulong)process["UserModeTime"] + (long)(ulong)process["KernelModeTime"]).TotalMilliseconds -
							prevProcessInfoRow.TotalProcessorTime.TotalMilliseconds) / 10 / secondsCount / Environment.ProcessorCount);
					}
					processInfoRow.TotalProcessorTime = new TimeSpan((long)(ulong)process["UserModeTime"] + (long)(ulong)process["KernelModeTime"]);
				}
				catch
				{
					processInfoRow.CpuPercent = 0;
					processInfoRow.TotalProcessorTime = new TimeSpan(0);
				}

				processInfoRow.FormattedMemory = SizeHelper.GetSizeInKb(processInfoRow.Memory, 12);
				processInfoRow.FormattedVirtualMemory = SizeHelper.GetSize(processInfoRow.VirtualMemory);
				processInfoRow.FormattedTotalProcessorTime = string.Format("{0:00}:{1:00}:{2:00}",
					processInfoRow.TotalProcessorTime.TotalHours,
					processInfoRow.TotalProcessorTime.Minutes,
					processInfoRow.TotalProcessorTime.Seconds);
				try
				{
					processInfoRow.FileName = Path.GetFileName(processInfoRow.Path);
				}
				catch
				{
					processInfoRow.FileName = "";
				}
				processInfoRow.InfoType = ProcessInfoHelper.GetProcessInfoTypeEnum(processInfoRow.Path);
				if (processInfoRow.InfoType == ProcessInfoTypeEnum.Malware)
				{
					DataContext.FoundUnwantedProcess = true;
				}

				if (!string.IsNullOrEmpty(processInfoRow.Path))
				{
					try
					{
						var fileInfo = FileVersionInfo.GetVersionInfo(processInfoRow.Path);
						processInfoRow.Description = fileInfo.FileDescription ?? "";
						processInfoRow.Company = fileInfo.CompanyName ?? "";
					}
					catch
					{
						processInfoRow.Description = "";
						processInfoRow.Company = "";
					}
					try
					{
						processInfoRow.Image = new Bitmap(Icon.ExtractAssociatedIcon(processInfoRow.Path).ToBitmap(), 16, 16);
					}
					catch
					{
						processInfoRow.Image = new Bitmap(16, 16);
					}
					try
					{
						processInfoRow.BigImage = Icon.ExtractAssociatedIcon(processInfoRow.Path).ToBitmap();
					}
					catch
					{
						processInfoRow.BigImage = new Bitmap(32, 32);
					}
				}
				else
				{
					processInfoRow.Description = "";
					processInfoRow.Company = "";
					processInfoRow.Image = new Bitmap(16, 16);
					processInfoRow.BigImage = new Bitmap(32, 32);
				}

				tempDS.ProcessInfo.AddProcessInfoRow(processInfoRow);
			}

			// Merge to DataContext;
			DataContext.MainDS.ProcessInfo.BeginLoadData();
			//DataContext.MainDS.ProcessInfo.Clear();
			// add nad update
			DataContext.MainDS.ProcessInfo.Merge(tempDS.ProcessInfo);
			// delete
			var idsToDel = new List<int>();
			foreach (MainDS.ProcessInfoRow row in DataContext.MainDS.ProcessInfo)
			{
				if (tempDS.ProcessInfo.FindByProcessId(row.ProcessId) == null)
					idsToDel.Add(row.ProcessId);

			}
			foreach (var id in idsToDel)
				DataContext.MainDS.ProcessInfo.RemoveProcessInfoRow(DataContext.MainDS.ProcessInfo.FindByProcessId(id));
			DataContext.MainDS.ProcessInfo.EndLoadData();
			_isRunning = false;
		}

		public static void ListInDataContext_Old4()
		{
			if (_isRunning)
				return;

			_isRunning = true;
			var processes = Process.GetProcesses();

			//DataContext.ProcessInfos.Clear();
			var processInfoList = new List<ProcessInfo>();
			MainDS.ProcessInfoRow processInfoRow;
			var tempDS = new MainDS();
			foreach (var process in processes)
			{
				processInfoRow = tempDS.ProcessInfo.NewProcessInfoRow();
				processInfoRow.ProcessName = process.ProcessName ?? "";
				processInfoRow.Memory = (ulong)process.WorkingSet64;
				try
				{
					processInfoRow.Description = process.MainModule.FileVersionInfo.FileDescription ?? "";
				}
				catch
				{
					processInfoRow.Description = "";
				}
				try
				{
					processInfoRow.StartTime = process.StartTime.ToLongTimeString();
				}
				catch
				{
					processInfoRow.StartTime = "";
				}
				try
				{
					processInfoRow.Path = process.MainModule.FileName ?? "";
					processInfoRow.FileName = Path.GetFileName(process.MainModule.FileName ?? "");
					processInfoRow.InfoType = ProcessInfoHelper.GetProcessInfoTypeEnum(processInfoRow.Path);
				}
				catch
				{
					processInfoRow.Path = "";
					processInfoRow.FileName = "";
					processInfoRow.InfoType = ProcessInfoTypeEnum.Other;
				}

				processInfoRow.ProcessId = process.Id;
				try
				{
					processInfoRow.Company = process.MainModule.FileVersionInfo.CompanyName ?? "";
				}
				catch
				{
					processInfoRow.Company = "";
				}
				processInfoRow.Priority = process.BasePriority;
				processInfoRow.Handles = process.HandleCount;
				processInfoRow.Threads = process.Threads.Count;
				var prevProcessInfoRow = DataContext.MainDS.ProcessInfo.FindByProcessId(processInfoRow.ProcessId);
				try
				{
					if (prevProcessInfoRow != null)
						processInfoRow.CpuPercent = (int)(process.TotalProcessorTime - prevProcessInfoRow.TotalProcessorTime).TotalMilliseconds / 100;
					processInfoRow.TotalProcessorTime = process.TotalProcessorTime;
				}
				catch
				{
					processInfoRow.CpuPercent = 0;
					processInfoRow.TotalProcessorTime = new TimeSpan(0);
				}
				processInfoRow.VirtualMemory = (ulong)process.VirtualMemorySize64;

				processInfoRow.FormattedMemory = SizeHelper.GetSizeInKb(processInfoRow.Memory, 12);
				processInfoRow.FormattedVirtualMemory = SizeHelper.GetSize(processInfoRow.VirtualMemory);
				processInfoRow.FormattedTotalProcessorTime = string.Format("{0:00}:{1:00}:{2:00}",
					processInfoRow.TotalProcessorTime.TotalHours,
					processInfoRow.TotalProcessorTime.Minutes,
					processInfoRow.TotalProcessorTime.Seconds);
				if (!string.IsNullOrEmpty(processInfoRow.Path))
				{
					try
					{
						processInfoRow.Image = new Bitmap(Icon.ExtractAssociatedIcon(processInfoRow.Path).ToBitmap(), 16, 16);
					}
					catch
					{
						processInfoRow.Image = new Bitmap(16, 16);
					}
					try
					{
						processInfoRow.BigImage = Icon.ExtractAssociatedIcon(processInfoRow.Path).ToBitmap();
					}
					catch
					{
						processInfoRow.BigImage = new Bitmap(32, 32);
					}
				}
				else
				{
					processInfoRow.Image = new Bitmap(16, 16);
					processInfoRow.BigImage = new Bitmap(32, 32);
				}
				tempDS.ProcessInfo.AddProcessInfoRow(processInfoRow);
			}

			// Merge to DataContext;
			DataContext.MainDS.ProcessInfo.BeginLoadData();
			//DataContext.MainDS.ProcessInfo.Clear();
			// add nad update
			DataContext.MainDS.ProcessInfo.Merge(tempDS.ProcessInfo);
			// delete
			var idsToDel = new List<int>();
			foreach (MainDS.ProcessInfoRow row in DataContext.MainDS.ProcessInfo)
			{
				if (tempDS.ProcessInfo.FindByProcessId(row.ProcessId) == null)
					idsToDel.Add(row.ProcessId);

			}
			foreach (var id in idsToDel)
				DataContext.MainDS.ProcessInfo.RemoveProcessInfoRow(DataContext.MainDS.ProcessInfo.FindByProcessId(id));
			DataContext.MainDS.ProcessInfo.EndLoadData();
			_isRunning = false;
		}

		/// <summary>
		/// Merge list of processes with existing in DataContext
		/// </summary>
		/// <param name="processInfoList"></param>
		private static void Merge(List<ProcessInfo> processInfoList)
		{
			var piListToDelete = new List<ProcessInfo>();
			// update
			foreach (var info in DataContext.ProcessInfos)
			{
				var newProcessInfo = processInfoList.Find(pi => pi.ProcessId == info.ProcessId);
				if (newProcessInfo != null)
					info.Update(newProcessInfo);
				else
					piListToDelete.Add(info);
			}
			// delete
			foreach (var info in piListToDelete)
			{
				DataContext.ProcessInfos.Remove(info);
			}
			// add
			foreach (var info in processInfoList)
			{
				var toAdd = true;
				foreach (var oldProcessInfo in DataContext.ProcessInfos)
				{
					if (oldProcessInfo.ProcessId == info.ProcessId)
						toAdd = false;
				}
				if (toAdd)
					DataContext.ProcessInfos.Add(info);
				//var oldProcessInfo = DataContext.ProcessInfos.Find(pi => pi.ProcessId == info.ProcessId);
				//if (oldProcessInfo == null)
				//	DataContext.ProcessInfos.Add(info);
			}
		}

		public static void KillByProcessId(int processId)
		{
			try
			{
				var process = Process.GetProcessById(processId);
				process.Kill();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
			}
		}

		public static void KillByProcessName(string processName)
		{
			Process[] processes = null;

			try
			{
				processes = Process.GetProcessesByName(processName);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
			}

			if (processes != null)
				foreach (var process in processes)
				{
					try
					{
						process.Kill();
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
					}
				}
		}

		/// <summary>
		/// Trying to kill all unwanted processes
		/// </summary>
		public static void OptimizeAll()
		{
			lock (DataContext.MainDS.ProcessInfo)
			{
				foreach (MainDS.ProcessInfoRow processInfoRow in DataContext.MainDS.ProcessInfo)
				{
					if (processInfoRow.InfoType == ProcessInfoTypeEnum.Malware)
						KillByProcessId(processInfoRow.ProcessId);
				}
			}
		}

		#region Show File Properties Window
		private static uint SEE_MASK_NOCLOSEPROCESS = 0x00000040;
		private static uint SEE_MASK_INVOKEIDLIST = 0x0000000c;
		private static uint SEE_MASK_FLAG_NO_UI = 0x00000400;
		private static int SW_SHOW = 5;

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
		private struct SHELLEXECUTEINFO
		{
			public int cbSize;
			public uint fMask;
			public IntPtr hwnd;
			[MarshalAs(UnmanagedType.LPStr)]
			public string lpVerb;
			[MarshalAs(UnmanagedType.LPStr)]
			public string lpFile;
			[MarshalAs(UnmanagedType.LPStr)]
			public string lpParameters;
			[MarshalAs(UnmanagedType.LPStr)]
			public string lpDirectory;
			public int nShow;
			public IntPtr hInstApp;
			public IntPtr lpIDList;
			[MarshalAs(UnmanagedType.LPStr)]
			public string lpClass;
			public IntPtr hkeyClass;
			public uint dwHotKey;
			public IntPtr hIconMonitorUnion;
			public IntPtr hProcess;
		}

		[DllImport("shell32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

		/// <summary>
		/// Show file/directory properties window
		/// </summary>
		/// <param name="path"></param>
		public static void ShowProperties(string path)
		{
			try
			{
				SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
				shInfo.lpFile = path;
				shInfo.fMask = SEE_MASK_INVOKEIDLIST;
				shInfo.lpVerb = "properties";
				shInfo.lpParameters = "";
				shInfo.nShow = SW_SHOW;
				shInfo.cbSize = Marshal.SizeOf(shInfo);
				bool result = ShellExecuteEx(ref shInfo);
				if (!result)
				{
					Win32Exception ex = new Win32Exception();
					throw ex;
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

		}
		#endregion Show File Properties Window
	}
}