using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Threading;
using Business.Processes.Scanners;
using Business.Registry.Scanners;
using Common;
using Common_Tools.TreeViewAdv.Tree;
using Data;
using Data.Registry;

namespace Business.Processes
{
	public class ScanProcessLogic
	{
		public delegate void ScanDelegate();

		//private static Thread threadScan;

		#region Scanning

		public static void StartScanning(TreeModel treeModel, int progressMin, int progressMax)
		{
			DataContext.OneClickScanState.ItemsTreeModel = treeModel;
			DataContext.OneClickScanState.ProcessesNodes = new List<BadRegistryKey>();

			//DataContext.OneClickScanState.ArrBadRegistryKeys.Clear(scanRegistrySettings.SectionCount);
			DataContext.OneClickScanState.MalwareProcessesItemsToFixCount = 0;
			DataContext.OneClickScanState.StartupProcessesItemsToFixCount = 0;
			DataContext.OneClickScanState.ActiveProcessesItemsToFixCount = 0;
			
			// Begin scanning
			try
			{
				StartScanner(new UserInstalledItemsProcessScanner());
				DataContext.OneClickScanState.ScanningProgress = 1 * (progressMax - progressMin) / 2 + progressMin;
				
				StartScanner(new OtherItemsProcessScanner());
				DataContext.OneClickScanState.ScanningProgress = 2 * (progressMax - progressMin) / 2 + progressMin;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Starts a scanner
		/// </summary>
		private static void StartScanner(ScannerBase scannerName)
		{
			DataContext.OneClickScanState.CurrentScanner = scannerName;

			System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			ScanDelegate objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);

			// Update section name
			scannerName.RootNode.SectionName = scannerName.ScannerName;
			//scannerName.RootNode.Img = this.imageList.Images[scannerName.GetType().Name];
			DataContext.OneClickScanState.CurrentSection = scannerName.ScannerName;

			try
			{
				// Start scanning
				objScan.Invoke();
			}
			finally
			{
				// next actions will be executed even thread was been aborted
				DataContext.OneClickScanState.CurrentScanner.RootNode.VerifyCheckState();
				if (scannerName.RootNode.Nodes.Count > 0)
				{
					DataContext.OneClickScanState.ItemsTreeModel.Nodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
					DataContext.OneClickScanState.ProcessesNodes.Add(DataContext.OneClickScanState.CurrentScanner.RootNode);
				}
			}
		}
		#endregion Scanning
	}
}