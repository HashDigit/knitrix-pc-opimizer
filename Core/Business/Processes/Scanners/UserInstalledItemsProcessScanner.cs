﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;
using System.Windows.Forms;
using Business.Startup;
using Common;
using Data;
using Data.Registry;
using Data.Startup;

namespace Business.Processes.Scanners
{
	public class UserInstalledItemsProcessScanner : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.ProcessScanner; }
		}

		/// <summary>
		/// Scan processes
		/// </summary>
		public static void Scan()
		{
			try
			{
				// Startup Items
				var startupItems = new BadRegistryKey("Startup Items", "", "", "");
				var startupProcesses = StartupProcessLogic.List();

				foreach (var startupProcess in startupProcesses)
				{
					var infoType = ProcessInfoHelper.GetProcessInfoTypeEnum(startupProcess.Path);
					if (infoType == ProcessInfoTypeEnum.UserInstalled)
					{
						var badRegistryKey = new BadRegistryKey(CheckState.Unchecked, startupProcess.Item, startupProcess.Path, "", startupProcess.Args);
						badRegistryKey.StartupProcess = startupProcess; 

						startupItems.Nodes.Add(badRegistryKey);
						DataContext.OneClickScanState.ItemsToFixCount++;
						if (infoType == ProcessInfoTypeEnum.Malware)
						{
							DataContext.OneClickScanState.MalwareProcessesItemsToFixCount++;
							DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount++;
							badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.MalwareProcesses);
						}
						DataContext.OneClickScanState.StartupProcessesItemsToFixCount++;
						//DataContext.OneClickScanState.StartupProcessesItemsSelectedCount++;
						badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.StartupProcesses);
					}
				}
				startupItems.VerifyCheckState();
				if (startupItems.Nodes.Count > 0)
					DataContext.OneClickScanState.CurrentScanner.RootNode.Nodes.Add(startupItems);

				// Running Processes
				var oConn = new ConnectionOptions();
				var oMs = new ManagementScope("\\\\localhost", oConn);
				var oQuery = new ObjectQuery("select * from Win32_Process");
				var oSearcher = new ManagementObjectSearcher(oMs, oQuery);
				var processes = oSearcher.Get();

				var runningProcesses = new BadRegistryKey("Running Processes", "", "", "");

				foreach (ManagementObject process in processes)
				{
					DataContext.OneClickScanState.CurrentScannedObject = (string)process["Name"];

					var processName = (string)process["Name"];
					var path = (string)process["ExecutablePath"] ?? "";
					var processId = (int)(uint)process["ProcessId"];
					var description = "";
					try
					{
						var fileInfo = FileVersionInfo.GetVersionInfo(path);
						description = fileInfo.FileDescription ?? "";
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.WARN, string.Format("Path: {0}\r\nError:\r\n{1}", path, ex.ToString()));
					}
					var infoType = ProcessInfoHelper.GetProcessInfoTypeEnum(path);
					if (infoType == ProcessInfoTypeEnum.UserInstalled)
					{
						bool isChecked = false;
						if (infoType == ProcessInfoTypeEnum.Malware)
							isChecked = true;
						var badRegistryKey = new BadRegistryKey(isChecked ? CheckState.Checked : CheckState.Unchecked,
								processName, path, processId.ToString(),
								string.Format("{0} Memory, {1}", SizeHelper.GetSize((ulong)process["WorkingSetSize"]), description));

						runningProcesses.Nodes.Add(badRegistryKey);
						DataContext.OneClickScanState.ItemsToFixCount++;
						if (infoType == ProcessInfoTypeEnum.Malware)
						{
							DataContext.OneClickScanState.MalwareProcessesItemsToFixCount++;
							DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount++;
							badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.MalwareProcesses);
						}
						DataContext.OneClickScanState.ActiveProcessesItemsToFixCount++;
						badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.ActiveProcesses);
					}
				}

				runningProcesses.VerifyCheckState();
				if (runningProcesses.Nodes.Count > 0)
					DataContext.OneClickScanState.CurrentScanner.RootNode.Nodes.Add(runningProcesses);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
	}
}
