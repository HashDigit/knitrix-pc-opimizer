using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Common;
using Data;

namespace Business
{
	/// <summary>
	/// Check for new application version
	/// </summary>
	public class CheckForUpdateLogic
	{
		private static Thread _threadMain;

		/// <summary>
		/// Check for update was successfully finished
		/// </summary>
		public static event EventHandler Finished;
		public static event EventHandler Aborted;
		public static event DownloadProgressHandler ProgressChanged;

		private static bool _isManual = false;
		private static bool _isRunning = false;

		public static void Run(bool isManual)
		{
			if (!_isManual)
				_isManual = isManual;

			if (_isRunning)
				return;

			_isRunning = true;
			_threadMain = new Thread(new ThreadStart(Run_Threaded));
			_threadMain.IsBackground = true;
			_threadMain.Start();
		}

		private static void Run_Threaded()
		{
			// get content of file with version and url of new Installer
			try
			{
				var versionFileContent = HttpRequestHelper.Request(Config.CheckForUpdatesUrl, Encoding.UTF8, "GET", null);
				var newAppVersion = int.Parse(versionFileContent.Split('\n')[0].Trim('\r'));

				Options.Instance.CheckForUpdatesLastDateTime = DateTime.Now;
				Options.Instance.Save();

				if (newAppVersion <= Config.CheckForUpdatesAppVersion)
				{
					if (_isManual)
                        MessageBox.Show("You have the latest version of Knitrix PC Optimizer.", "Check For Updates");
					return; // new app version is not existing
				}

				var newInstallerUrl = versionFileContent.Split('\n')[1].Trim('\r');
				var newInstallerFilePath = Path.Combine(Config.CommonDataFolder, Path.GetFileName(newInstallerUrl));
				if (File.Exists(newInstallerFilePath))
					File.Delete(newInstallerFilePath);
				//byte[] buffer = HttpRequestHelper.Request(newInstallerUrl);
				//File.WriteAllBytes(newInstallerFilePath, buffer);
				var downloader = new FileDownloader();
				downloader.DownloadComplete += new EventHandler(downloader_DownloadComplete);
				downloader.ProgressChanged += new DownloadProgressHandler(downloader_ProgressChanged);
				downloader.Download(newInstallerUrl, Config.CommonDataFolder);            

				if (MessageBox.Show("New application version is already available. Do you want to install?", "Check For Updates", MessageBoxButtons.YesNo) != DialogResult.Yes)
					return;

				Process.Start(newInstallerFilePath);
				Application.Exit();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while cheking for updates\r\n {0}", ex));
				if (_isManual)
					MessageBox.Show(string.Format("Error while cheking for updates\r\n {0}", ex.Message), "Check For Updates");
			}
			finally
			{
				_isManual = false;
				_isRunning = false;
				_threadMain = null;
				if (_isManual && Finished != null)
					Finished(null, new EventArgs());
			}
		}

		private static void downloader_ProgressChanged(object sender, DownloadEventArgs e)
		{
			if (_isManual && ProgressChanged != null)
				ProgressChanged(null, e);
		}

		private static void downloader_DownloadComplete(object sender, EventArgs e)
		{
			// nothing
		}

		/// <summary>
		/// Force stop updating
		/// </summary>
		public static void Abort()
		{
			if (_threadMain != null)
			{
				try
				{
					_threadMain.Abort();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				_isManual = false;
				_isRunning = false;
				_threadMain = null;
				if (Aborted != null)
					Aborted(null, new EventArgs());
			}
		}
	}
}