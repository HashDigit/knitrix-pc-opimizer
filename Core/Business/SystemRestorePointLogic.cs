using System;
using System.Collections.Generic;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Data;

namespace Business
{
	public class SystemRestorePointLogic
	{
		private const string SYSTEM_RESTORE = "SystemRestore";
		private const string CREATE_SYSTEM_RESTORE_POINT = "CreateRestorePoint";
		private const string ROLLBACK_SYSTEM_RESTORE_POINT = "Restore";
		private const string SET_ENABLED_SYSTEM_RESTORE_POINT = "enable";
		private const string SET_DISABLED_SYSTEM_RESTORE_POINT = "disable";
		private const string SYSTEM_RESTORE_POINT_DESCRIPTION = "Description";
		private const string SYSTEM_RESTORE_POINT_TYPE = "RestorePointType";
		private const string SYSTEM_RESTORE_EVENTTYPE = "EventType";

		[DllImport("Srclient.dll")]
		private static extern int SRRemoveRestorePoint(int index);

		[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		private static extern uint SearchPath(string lpPath,
							string lpFileName,
							string lpExtension,
							int nBufferLength,
							[MarshalAs(UnmanagedType.LPTStr)] StringBuilder lpBuffer,
							string lpFilePart);

		public static List<SystemRestorePoint> List()
		{
			var systemRestorePoints = new List<SystemRestorePoint>();
			ManagementClass objClass = new ManagementClass("\\\\.\\root\\default", "systemrestore", new ObjectGetOptions());
			ManagementObjectCollection objCol = objClass.GetInstances();

			foreach (ManagementObject objItem in objCol)
			{
				var systemRestorePoint = new SystemRestorePoint();
				systemRestorePoint.CreationTimeStr = (string)objItem["creationtime"];
				systemRestorePoint.Description = (string)objItem["description"];
				systemRestorePoint.SequenceNumber = (uint)objItem["sequencenumber"];
				systemRestorePoint.RestoreType = (RestoreType)(uint)objItem["RestorePointType"];
				if (systemRestorePoint.RestoreType != RestoreType.CancelledOperation)
					systemRestorePoints.Add(systemRestorePoint);
			}
			return systemRestorePoints;
		}

		/// <summary>
		/// Creates the restore point.
		/// </summary>
		/// <returns></returns>
		public static void Create(string desription)
		{
			SetEnaled();
			// remember prev restore point
			var systemRestorePoints = List();
			var prevRestorePointSequenceNumber = systemRestorePoints.Count > 0 ? systemRestorePoints[systemRestorePoints.Count - 1].SequenceNumber : 0;
			ManagementClass mcProcess = new ManagementClass
			(
				 new ManagementScope("\\\\localhost\\root\\default"),
				 new ManagementPath(SYSTEM_RESTORE),
				 new ObjectGetOptions()
			);

			ManagementBaseObject mbObjectInput = mcProcess.GetMethodParameters(CREATE_SYSTEM_RESTORE_POINT);
			mbObjectInput[SYSTEM_RESTORE_POINT_DESCRIPTION] = desription;
			mbObjectInput[SYSTEM_RESTORE_POINT_TYPE] = RestoreType.ModifySettings;
			mbObjectInput[SYSTEM_RESTORE_EVENTTYPE] = 100;

			ManagementBaseObject mbObjectOutput = mcProcess.InvokeMethod(CREATE_SYSTEM_RESTORE_POINT, mbObjectInput, null);

			// wait while restore point will be really created (but less than 60 seconds)
			uint newRestorePointSequenceNumber = 0;
			int maxIterCount = 60;
			do
			{
				Thread.Sleep(1000);
				maxIterCount--;
				systemRestorePoints = List();
				newRestorePointSequenceNumber = systemRestorePoints.Count > 0
													? systemRestorePoints[systemRestorePoints.Count - 1].SequenceNumber
													: 0;
			} while (newRestorePointSequenceNumber <= prevRestorePointSequenceNumber && maxIterCount > 0);
		}

		/// <summary>
		/// Roll Back to a Specific Restore Point.
		/// </summary>
		/// <returns></returns>
		public static void Rollback(uint sequenceNumber)
		{
			ManagementClass mcProcess = new ManagementClass
			(
				 new ManagementScope("\\\\localhost\\root\\default"),
				 new ManagementPath(SYSTEM_RESTORE),
				 new ObjectGetOptions()
			);

			mcProcess.InvokeMethod(ROLLBACK_SYSTEM_RESTORE_POINT, new object[] { sequenceNumber });
		}

		/// <summary>
		/// Delete a Specific Restore Point.
		/// </summary>
		/// <returns></returns>
		public static void Delete(uint sequenceNumber)
		{
			SRRemoveRestorePoint((int)sequenceNumber);
		}

		/// <summary>
		/// Enable system restore functionality if it was turned off.
		/// </summary>
		/// <returns></returns>
		public static void SetEnaled()
		{
			ManagementClass mcProcess = new ManagementClass
			(
				 new ManagementScope("\\\\localhost\\root\\default"),
				 new ManagementPath(SYSTEM_RESTORE),
				 new ObjectGetOptions()
			);

			mcProcess.InvokeMethod(SET_ENABLED_SYSTEM_RESTORE_POINT, new object[] { Path.GetPathRoot(Environment.SystemDirectory) });
			// ��������� ������, �� ������ ����� ����� ����� SET_ENABLED_SYSTEM_RESTORE_POINT ����������, �� ������� ����� ���������
			Thread.Sleep(3000);
		}

		/// <summary>
		/// Verifies that the OS can do system restores
		/// </summary>
		/// <returns>True if OS is either ME,XP,Vista,7</returns>
		public static bool SysRestoreAvailable()
		{
			int majorVersion = Environment.OSVersion.Version.Major;
			int minorVersion = Environment.OSVersion.Version.Minor;

			StringBuilder sbPath = new StringBuilder(260);

			// See if DLL exists
			if (SearchPath(null, "srclient.dll", null, 260, sbPath, null) != 0)
				return true;

			// Windows ME
			if (majorVersion == 4 && minorVersion == 90)
				return true;

			// Windows XP
			if (majorVersion == 5 && minorVersion == 1)
				return true;

			// Windows Vista
			if (majorVersion == 6 && minorVersion == 0)
				return true;

			// Windows Se7en
			if (majorVersion == 6 && minorVersion == 1)
				return true;

			// All others : Win 95, 98, 2000, Server
			return false;
		}
	}
}