﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Common;
using IWshRuntimeLibrary;

namespace Business
{
	public struct ShortcutDetails
	{
		public string Name;
		public string Target;
		public string ShortcutPath;
		public string Description;
	}

	public class ShortcutFixerLogic
	{
		static WshShellClass wsh = new WshShellClass();
		private static Thread _threadMain;

		/// <summary>
		/// Scanning was successfully finished
		/// </summary>
		public static event EventHandler ScanFinished;

		/// <summary>
		/// Scanning was manually aborted
		/// </summary>
		public static event EventHandler ScanAborted;

		/// <summary>
		/// Aborted scan does not throw Finished event
		/// </summary>
		private static bool _isScanAborted = false;

		/// <summary>
		/// Is in scanning method?
		/// </summary>
		public static bool IsScanning { get; private set; }

		///// <summary>
		///// Subfolders where to scan
		///// </summary>
		//static string[] _programs_folders = { @"Start Menu\Programs", "Desktop" };

		///// <summary>
		///// Areas where to scan
		///// </summary>
		//static string[] _programs_path = {
		//                                    Environment.GetEnvironmentVariable("ALLUSERSPROFILE"),
		//                                    Environment.GetEnvironmentVariable("USERPROFILE")
		//                                 };


		/// <summary>
		/// Scan start menu and desktop folders or custom drives?
		/// </summary>
		public static bool ScanStartMenuAndDesktopOnly = true;

		/// <summary>
		/// Include empty folders in scan search?
		/// </summary>
		public static bool ScanEmptyStartMenuDirectory = true;

		/// <summary>
		/// List of drivers to scan
		/// </summary>
		public static List<string> DrivesToScan = new List<string>();

		public static ArrayList ShortcutFiles = new ArrayList();
		public static ArrayList EmptyDirectories = new ArrayList();

		/// <summary>
		/// Current scanning path
		/// </summary>
		public static string CurrentScanningPath = "";
		/// <summary>
		/// Get shortcut target file path
		/// </summary>
		/// <param name="shortcut_file">the shortcut file path</param>
		/// <returns></returns>
		private static string GetTarget(string shortcut_file)
		{
			IWshShortcut shortcut = (IWshShortcut)wsh.CreateShortcut(shortcut_file);

			string t = string.Empty;

			if (shortcut.TargetPath != string.Empty)
			{
				t = shortcut.TargetPath;
			}
			else
			{
				// for URL shortcuts but with has an .LNK extension
				//IWshURLShortcut url_shortcut = (IWshURLShortcut)wsh.CreateShortcut(shortcut_file);

				// anyway .. we'll just assume this is a URL shortcut
				// so let's just return set the TargetPath the same as the shortcut file path.
				t = shortcut_file;
			}

			shortcut = null;

			return t;
		}

		private static string GetDescription(string shortcut_file)
		{
			WshShell shll = new WshShell();

			IWshShortcut shortcut = (IWshShortcut)shll.CreateShortcut(shortcut_file);

			string d = shortcut.Description;

			shortcut = null;

			return d;
		}

		/// <summary>
		/// Start scanning for broken shortcuts and empty folders
		/// </summary>
		public static void StartScan()
		{
			AbortScan();
			_threadMain = new Thread(new ThreadStart(StartScan_Threaded));
			_threadMain.IsBackground = true;
			_threadMain.Start();
		}

		/// <summary>
		/// Start scanning for broken shortcuts and empty folders
		/// </summary>
		private static void StartScan_Threaded()
		{
			try
			{
				_isScanAborted = false;
				IsScanning = true;
				// set paths to scan
				var pathsToScan = new List<string>();
				if (ScanStartMenuAndDesktopOnly)
				{
					string[] programsPaths = {Environment.GetEnvironmentVariable("ALLUSERSPROFILE"),
											Environment.GetEnvironmentVariable("USERPROFILE")};
					string[] programsFolders = { @"Start Menu\Programs", "Desktop" };

					foreach (string pr in programsPaths)
					{
						foreach (string programsFolder in programsFolders)
						{
							pathsToScan.Add(Path.Combine(pr, programsFolder));
						}
					}
				}
				else
					pathsToScan = DrivesToScan;

				// main
				ShortcutFiles.Clear();
				EmptyDirectories.Clear();
				CurrentScanningPath = "";

				foreach (string pathToScan in pathsToScan)
					DirRecurseSearch(pathToScan, new string[] { "lnk" });
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Erro while Scanning for Shortcuts.\r\n{0}", ex));
			}
			finally
			{
				_threadMain = null;
				IsScanning = false;
				if (!_isScanAborted && ScanFinished != null)
					ScanFinished(null, new EventArgs());
				_isScanAborted = false;
			}
		}

		/// <summary>
		/// Force abort scanning
		/// </summary>
		public static void AbortScan()
		{
			IsScanning = false;
			_isScanAborted = true;
			if (_threadMain != null)
			{
				try
				{
					_threadMain.Abort();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				_threadMain = null;
				if (ScanAborted != null)
					ScanAborted(null, new EventArgs());
			}
		}

		private static void DirRecurseSearch(string sDir, string[] filter)
		{
			CurrentScanningPath = sDir;
			int i = 0;

			try
			{
				foreach (string d in DirectoryHelper.GetDirectories(sDir))
				{
					i++;
					// Debug.WriteLine(i.ToString("00#", null) + " = " + d);

					DirectoryInfo info = new DirectoryInfo(d);
					DirectorySecurity security = info.GetAccessControl();
					// check if the current folder `d` is empty and user has rights to delete
					if (DirectoryHelper.GetFiles(d).Length == 0 && DirectoryHelper.GetDirectories(d).Length == 0)
					{
						// NOTE :: (AK) залепа START, проверяем что именно текущий пользователь имеет права на удаление папки
						//bool canDelete = false;
						//foreach (FileSystemAccessRule rule in security.GetAccessRules(true, true, typeof(NTAccount)))
						//{
						//    if (rule.IdentityReference.Value == string.Format("{0}\\{1}", SystemInformation.UserDomainName, SystemInformation.UserName)  && (rule.FileSystemRights & FileSystemRights.Delete) > 0)
						//    {
						//        canDelete = true;
						//        break;
						//    }
						//}

						//if (canDelete)
						// NOTE :: (AK) залепа END
						if (ScanStartMenuAndDesktopOnly && ScanEmptyStartMenuDirectory)
						{
							ShortcutDetails sd = new ShortcutDetails();
							sd.Name = Path.GetFileName(d);
							sd.Target = "Empty Directory";
							sd.ShortcutPath = d;
							sd.Description = "Empty Directory";

							EmptyDirectories.Add(sd);
						}

						continue;
					}

					DirRecurseSearch(d, filter);
				}
			}
			catch (Exception ex)
			{
				//Console.WriteLine(excpt.Message);
				// TODO :: (AK) disable log if there are lot of errors?
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			try
			{
				foreach (string f in DirectoryHelper.GetFiles(sDir))
				{
					string ext = Path.GetExtension(f).ToLower();

					if (ext == ".lnk")
					{
						string target_filepath = GetTarget(f);

						if (!System.IO.File.Exists(target_filepath) && !Directory.Exists(target_filepath))
						{
							ShortcutDetails sd = new ShortcutDetails();
							sd.Name = Path.GetFileNameWithoutExtension(f);
							sd.Target = target_filepath;
							sd.ShortcutPath = f;
							sd.Description = GetDescription(f);

							ShortcutFiles.Add(sd);
						}
					}
				}
			}
			catch (Exception ex)
			{
				//Console.WriteLine(excpt.Message);
				// TODO :: (AK) disable log if there are lot of errors?
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		public static void OpenInExplorer(string shortcut_file)
		{
			wsh.Exec(string.Format("Explorer /select,{0}", shortcut_file));
		}

		public static void DeleteFile(string filepath)
		{
			try
			{
				System.IO.File.Delete(filepath);
			}
			catch (Exception ex)
			{
				//System.Diagnostics.Debug.WriteLine(ex.Message + "\r\n" + ex.StackTrace);
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				System.Windows.Forms.MessageBox.Show(ex.Message, "Delete", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}

		public static void DeleteFolder(string folderpath)
		{
			try
			{
				Directory.Delete(folderpath);
			}
			catch (Exception ex)
			{
				//System.Diagnostics.Debug.WriteLine(ex.Message + "\r\n" + ex.StackTrace);
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				System.Windows.Forms.MessageBox.Show(ex.Message, "Delete", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Change Target Path of existing shortcut
		/// </summary>
		/// <param name="sd"></param>
		/// <param name="targetPath"></param>
		public static void UpdateTargetPath(ShortcutDetails sd, string newTargetPath)
		{
			try
			{
				IWshShortcut shortcut = (IWshShortcut)wsh.CreateShortcut(sd.ShortcutPath);
				shortcut.TargetPath = newTargetPath;
				shortcut.Save();
				sd.Target = newTargetPath;
			}
			catch (Exception ex)
			{
				//Console.WriteLine(excpt.Message);
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
	}
}
