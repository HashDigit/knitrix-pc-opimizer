using System.Text;
using System.Web.Mail;

namespace Common
{
	public class EmailHelper
	{
		public static void SendEmail(string smtp, int port, string login, string password, string from, string to, bool useSsl, bool useAuthentication, string subject, string body, MailFormat mailFormat)
		{
			// Details are here: http://msdn.microsoft.com/en-us/library/ms526227(v=EXCHG.10).aspx
			var mailMessage = new MailMessage();
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", useAuthentication ? 1 : 0);
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", login);
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtperver", smtp);
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", port);
			//send using: cdoSendUsingPort, value 2, for sending the message using the network.
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
			mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", useSsl ? 1 : 0);
			mailMessage.Subject = subject;
			mailMessage.Body = body;
			mailMessage.From = from;
			mailMessage.To = to;
			mailMessage.BodyEncoding = Encoding.UTF8;
			mailMessage.BodyFormat = mailFormat;
			SmtpMail.SmtpServer = smtp;
			SmtpMail.Send(mailMessage);
		}
	}
}
