﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

namespace Common
{
    static class HiveHelper
    {
        [DllImport("kernel32.dll")]
        public static extern uint QueryDosDevice([In, Optional] string lpDeviceName, [Out] StringBuilder lpTargetPath, [In] int ucchMax);

        /// <summary>
        /// Converts \\Device\\HarddiskVolumeX\... to X:\...
        /// </summary>
        /// <param name="devicePath">Device name with path</param>
        /// <returns>Drive path</returns>
        public static string ConvertDeviceToMSDOSName(string devicePath)
        {
			devicePath = devicePath.ToLower();
            var result = "";

            // Convert \Device\HarddiskVolumeX\... to X:\...
            foreach (KeyValuePair<string, string> kvp in QueryDosDevice())
            {
                string drivePath = kvp.Key.ToLower();
                string deviceName = kvp.Value.ToLower();

                if (devicePath.StartsWith(deviceName))
                {
                    result = devicePath.Replace(deviceName, drivePath);
                    break;
                }
            }

            return result;
        }

        private static Dictionary<string, string> QueryDosDevice()
        {
            var result = new Dictionary<string, string>();

            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                if (di.IsReady)
                {
                    var drivePath = di.Name.Substring(0, 2);
                    var deviceName = new StringBuilder(260);

                    // Convert C: to \Device\HarddiskVolume1
                    if (QueryDosDevice(drivePath, deviceName, 260) > 0)
                        result.Add(drivePath, deviceName.ToString());
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a temporary path for a registry hive
        /// </summary>
        public static string GetTempHivePath()
        {
            try
            {
                var tempPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                // File cant exists, keep retrying until we get a file that doesnt exist
                if (File.Exists(tempPath))
                    return GetTempHivePath();

                return tempPath;
            }
            catch (IOException)
            {
                return GetTempHivePath();
            }
        }

        /// <summary>
        /// Gets the file size
        /// </summary>
        /// <param name="filePath">Path to the filename</param>
        /// <returns>File Size</returns>
        public static long GetFileSize(string filePath)
        {
            try
            {
                var fi = new FileInfo(filePath);

                return fi.Length;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
