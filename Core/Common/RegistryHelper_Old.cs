using System;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Windows.Forms;

namespace Common
{
	public class RegistryHelper_Old
	{
		private static string FormRegKey(string sSect)
		{
			return sSect;
		}

		public static void SaveSetting(string Section, string Key, string Setting)
		{
			RegistryKey key1 = null;
			try
			{
				string text1 = FormRegKey(Section);
				key1 = Application.UserAppDataRegistry.CreateSubKey(text1);
				if (key1 == null)
				{
					return;
				}
				key1.SetValue(Key, Setting);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Section: {0}, Key: {1}, Setting: {2}\n{3}", Section, Key, Setting, ex));
				return;
			}
			finally
			{
				key1.Close();
			}
		}

		public static string GetSetting(string Section, string Key, string Default)
		{
			try
			{
				if (Default == null)
				{
					Default = "";
				}
				string text2 = FormRegKey(Section);
				RegistryKey key1 = Application.UserAppDataRegistry.OpenSubKey(text2);
				if (key1 != null)
				{
					object obj1 = key1.GetValue(Key, Default);
					key1.Close();
					if (obj1 != null)
					{
						if (!(obj1 is string))
						{
							return null;
						}
						return (string)obj1;
					}
					return null;
				}
				return Default;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
				return "";
			}
		}
	}
}

