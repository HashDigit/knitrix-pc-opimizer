using System;
using System.Collections.Generic;

namespace Common
{
	public class IntUtils
	{
		public static int CreateRandomHash()
		{
			return Guid.NewGuid().GetHashCode();
		}

		/// <summary>
		/// ��������� ����� �� 0 ������������ �� max �� ������������
		/// </summary>
		/// <param name="max"></param>
		/// <returns></returns>
		public static int GetRandom(int max)
		{
			return max == 0 ? 0 : Math.Abs(Guid.NewGuid().GetHashCode() % max);
		}

		/// <summary>
		/// ��������� ������ � id � ������ int ("1,2,3" -> new int[]{1,2,3})
		/// </summary>
		/// <param name="ids"></param>
		/// <returns></returns>
		public static int[] StrToIds(string ids)
		{
			List<int> idList = new List<int>();
			int id;
			for (int i = 0; i < ids.Split(',').Length; i++)
			{
				if (int.TryParse(ids.Split(',')[i].Trim(), out id))
				{
					idList.Add(id);
				}
			}
			return idList.ToArray();
		}

		/// <summary>
		/// ���������� null ������ 0.
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		public static int? ToNullable(int val)
		{
			return val == 0 ? null : (int?)val;
		}
	}
}