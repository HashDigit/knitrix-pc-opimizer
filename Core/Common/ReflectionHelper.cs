using System;
using System.Reflection;
using System.Collections;
using System.ComponentModel;

namespace Common
{
	public class ReflectionHelper
	{
		/// <summary>Hashtable of pre-created TypeConverters</summary> 
		static Hashtable hashConverters = null;
		/// <summary>Reflection Helper Static Constructor</summary> 
		static ReflectionHelper()
		{
			hashConverters = new Hashtable();
		}
		/// <summary>Prevent Instantiation</summary> 
		private ReflectionHelper()
		{
		}
		/// <summary>Convert from <paramref name="x"/> to <paramref name="type"/> 
		/// </summary> 
		/// <param name="x">String representation of the object</param> 
		/// <param name="type">Desired type</param> 
		/// <returns> 
		/// Null if no type converter is found or conversion unsuccessful 
		/// otherwise a converted object. 
		/// </returns> 
		public static object ConvertFrom(string x, Type type)
		{
			TypeConverter conv = (TypeConverter)hashConverters[type];
			if (conv == null)
			{
				// We need to create a new type converter 
				conv = TypeDescriptor.GetConverter(type);
				if (conv == null)
				{
					return null;
				}
				hashConverters[type] = conv;
			}
			return conv.ConvertFrom(x);
		}
		/// <summary>Returns the <see cref="System.Reflection.MemberInfo.MemberType" /> of <paramref name="member"/> 
		/// of <paramref name="type"/>.</summary> 
		/// <param name="member">Member to query for</param> 
		/// <param name="type"><see cref="System.Type" /> to which <paramref name="member"/> 
		/// belongs to.</param> 
		/// <returns> 
		/// <see cref="System.Reflection.MemberInfo.MemberType" /> instance 
		/// </returns> 
		public static MemberTypes GetMember(string member, Type type)
		{
			MemberInfo[] info = type.GetMember(member);
			// All except events are properties, a lenient check. 
			// At least one member is returned. 
			if (info != null && info.Length > 0)
			{
				return info[0].MemberType;
			}
			else
			{
				throw new Exception("No such property[" + member + "] exception");
			}
		}
		/// <summary>Looks up <see cref="System.Type"/> of the <paramref name="property"/> 
		/// of <paramref name="clazz"/>.</summary> 
		/// <param name="property">Name of the property for which type lookup has to 
		/// be done.</param> 
		/// <param name="clazz">Type which contains <paramref name="property"/></param> 
		/// <returns>An instance of <see cref="System.Type"/> if found, or null 
		/// if not found.</returns> 
		public static Type GetPropertyType(string property, Type clazz)
		{
			PropertyInfo prop = clazz.GetProperty(property);
			return prop.PropertyType;
		}
		/// <summary>Looks up <see cref="System.Type"/> of the <paramref name="Event"/> 
		/// of <paramref name="clazz"/>.</summary> 
		/// <param name="Event">Name of the event for which type lookup has to 
		/// be done.</param> 
		/// <param name="clazz">Type which contains <paramref name="Event"/></param> 
		/// <returns>An instance of <see cref="System.Type"/> if found, or null 
		/// if not found.</returns> 
		public static Type GetEventType(string Event, Type clazz)
		{
			EventInfo prop = clazz.GetEvent(Event);
			return prop.EventHandlerType;
		}

		/// <summary>
		/// Get attribute int value of an property
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="propertyName"></param>
		/// <param name="attributeType"></param>
		/// <returns></returns>
		public static int GetIntAttributeValue(object obj, string propertyName, Type attributeType)
		{
			PropertyInfo pis = obj.GetType().GetProperty(propertyName);
			var attributes = pis.GetCustomAttributes(attributeType, true);
			// properties
			foreach (object attribute in attributes)
			{
				return (int)pis.GetValue(attribute, null);
			}
			throw new Exception(string.Format("Attribute {0} is not found.", attributeType.ToString()));
		}
	}
}