using System;

namespace Common
{
	public class TimeSpanHelper
	{
		public static string ToString(TimeSpan timeSpan)
		{
			string result = "";
			int totalDays = (int) timeSpan.TotalDays;
			if (totalDays > 0)
				result += totalDays + (totalDays > 0 ? " days " : " day ");
			if (timeSpan.Hours > 0)
				result += timeSpan.Days + (timeSpan.Hours > 0 ? " hours " : " hour ");
			if (timeSpan.Minutes > 0)
				result += timeSpan.Minutes + (timeSpan.Minutes > 0 ? " minutes " : " minute ");
			if (timeSpan.Seconds > 0)
				result += timeSpan.Seconds + (timeSpan.Seconds > 0 ? " seconds " : " second ");

			return result.Trim();
		}
	}
}