using System;
using System.IO;

namespace Common
{
	public class DirectoryHelper
	{
		public static ulong DirSize(DirectoryInfo d)
		{
			ulong Size = 0;
			try
			{
				// Add file sizes.
				FileInfo[] fis = d.GetFiles();
				foreach (FileInfo fi in fis)
				{
					Size += (ulong)fi.Length;
				}
				// Add subdirectory sizes.
				DirectoryInfo[] dis = d.GetDirectories();
				foreach (DirectoryInfo di in dis)
				{
					Size += DirSize(di);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while calculating directory size. Directory path: {0}\r\nError:\r\n{1}", d.FullName, ex));
			}
			return Size;
		}

		/// <summary>
		/// Delete directory including subfolders and files
		/// </summary>
		/// <param name="path"></param>
		public static void Delete(string path)
		{
			try
			{
				Directory.Delete(path, true);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while directory deletion. Directory path: {0}\r\nError:\r\n{1}", path, ex));
			}
		}

		/// <summary>
		/// Delete all subdirectories and files, and keep directory blank
		/// </summary>
		/// <param name="path"></param>
		public static void Clear(string path)
		{
			string[] files = null;
			try
			{
				files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while getting list of file. Directory path: {0}\r\nError:\r\n{1}", path, ex));
			}

			if (files != null)
				foreach (var file in files)
				{
					try
					{
						File.Delete(file);
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while file deletion. File path: {0}\r\nError:\r\n{1}", file, ex));
					}
				}

			string[] subdirectories = null;
			try
			{
				subdirectories = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while getting list of directories. Parent directory path: {0}\r\nError:\r\n{1}", path, ex));
			}

			if (subdirectories != null)
				foreach (var subdirectory in subdirectories)
				{
					try
					{
						Directory.Delete(subdirectory, true);
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while directory deletion. Directory path: {0}\r\nError:\r\n{1}", subdirectory, ex));
					}
				}
		}

		/// <summary>
		/// Is directory clear? (i.e. does not contain files and folders)
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool IsClear(string path)
		{
			string[] files = null;
			try
			{
				files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while getting list of file. Directory path: {0}\r\nError:\r\n{1}", path, ex));
			}
			if (files != null && files.Length > 0)
				return false;

			string[] subdirectories = null;
			try
			{
				subdirectories = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while getting list of directories. Parent directory path: {0}\r\nError:\r\n{1}", path, ex));
			}

			if (subdirectories != null && subdirectories.Length > 0)
				return false;

			return true;
		}

		/// <summary>
		/// Exceptionless method to get list of directories in folder
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static string[] GetDirectories(string path)
		{
			try
			{
				return Directory.GetDirectories(path);
			}
			catch (Exception ex)
			{
				//Console.WriteLine(excpt.Message);
				// TODO :: (AK) disable log if there are lot of errors?
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			return new string[0];
		}

		/// <summary>
		/// Exceptionless method to get list of files in folder
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static string[] GetFiles(string path)
		{
			try
			{
				return Directory.GetFiles(path);
			}
			catch (Exception ex)
			{
				//Console.WriteLine(excpt.Message);
				// TODO :: (AK) disable log if there are lot of errors?
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			return new string[0];
		}
	}
}