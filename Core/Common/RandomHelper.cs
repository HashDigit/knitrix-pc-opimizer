using System;

namespace Common
{
	public class RandomHelper
	{
		/// <summary>
		/// Return random int value from 0 to max
		/// </summary>
		/// <param name="max"></param>
		public static int GetInt(int max)
		{
			return new Random(Guid.NewGuid().ToString().GetHashCode()).Next(max);
		}
	}
}