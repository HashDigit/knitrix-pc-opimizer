using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
	public class PathHelper
	{
		public static string RelativePath(string absolutePath, string relativeTo)
		{
			absolutePath = absolutePath.Replace('/', '\\');
			relativeTo = relativeTo.Replace('/', '\\');

			string[] absoluteDirectories = absolutePath.Split('\\');
			string[] relativeDirectories = relativeTo.Split('\\');

			//Get the shortest of the two paths
			int length = absoluteDirectories.Length < relativeDirectories.Length ? absoluteDirectories.Length : relativeDirectories.Length;

			//Use to determine where in the loop we exited
			int lastCommonRoot = -1;
			int index;

			//Find common root
			for (index = 0; index < length; index++)
				if (absoluteDirectories[index] == relativeDirectories[index])
					lastCommonRoot = index;
				else
					break;

			//If we didn't find a common prefix then throw
			if (lastCommonRoot == -1)
				return relativeTo;
			//throw new ArgumentException("Paths do not have a common base");

			//Build up the relative path
			StringBuilder relativePath = new StringBuilder();

			//Add on the ..
			for (index = lastCommonRoot + 1; index < absoluteDirectories.Length; index++)
				if (absoluteDirectories[index].Length > 0)
					relativePath.Append("..\\");

			//Add on the folders
			for (index = lastCommonRoot + 1; index < relativeDirectories.Length - 1; index++)
				relativePath.Append(relativeDirectories[index] + "\\");
			relativePath.Append(relativeDirectories[relativeDirectories.Length - 1]);

			return relativePath.ToString();
		}

		/// <summary>
		/// Is second argument is subfolder of first argument
		/// </summary>
		/// <param name="absolutePath"></param>
		/// <param name="relativeTo"></param>
		/// <returns></returns>
		public static bool IsSubdirectory(string absolutePath, string relativeTo)
		{
			absolutePath = absolutePath.Replace('/', '\\').ToLower();
			relativeTo = relativeTo.Replace('/', '\\').ToLower();
			return relativeTo.StartsWith(absolutePath);
		}

		public static bool IsAbsolute(string path)
		{
			return path.Contains(":");
		}
	}
}
