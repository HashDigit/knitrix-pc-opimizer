using System;
using System.Collections.Generic;

namespace System.Runtime.CompilerServices
{
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class
		 | AttributeTargets.Method)]
	public sealed class ExtensionAttribute : Attribute { }
}

namespace Common.Extension
{
	public static class StringMethods
	{
		public static List<string> Trim(this List<string> source, char[] chars)
		{
			for (int i = 0; i < source.Count; i++)
				source[i] = source[i].Trim(chars);
			
			return source;
		}
	}
}