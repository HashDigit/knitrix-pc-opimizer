using System;
using System.Collections.Generic;
using log4net.Config;
using log4net;
using System.Windows.Forms;
using System.IO;

namespace Common
{
	public static class CLogger
	{
		#region Properties
		public static string FileName
		{
			get
			{
				// Bind to the root hierarchy of log4net  
				log4net.Repository.Hierarchy.Hierarchy root =
				  log4net.LogManager.GetRepository()
				 as log4net.Repository.Hierarchy.Hierarchy;

				if (root != null)
				{
					// Bind to the RollingFileAppender  
					log4net.Appender.RollingFileAppender rfa =
					 (log4net.Appender.RollingFileAppender)root.Root.GetAppender("LogFileAppender");

					if (rfa != null)
					{
						return rfa.File;
					}
				}
				return null;
			}
		}
		#endregion Properties

		#region Members
		private static readonly ILog logger = LogManager.GetLogger(typeof(CLogger));
		#endregion

		#region Constructors
		static CLogger()
		{
			XmlConfigurator.Configure();
			InitLogger();
		}

		private static void InitLogger()
		{
			// Bind to the root hierarchy of log4net  
			log4net.Repository.Hierarchy.Hierarchy root =
			  log4net.LogManager.GetRepository()
			 as log4net.Repository.Hierarchy.Hierarchy;

			if (root != null)
			{
				// Bind to the RollingFileAppender  
				log4net.Appender.RollingFileAppender rfa =
				 (log4net.Appender.RollingFileAppender)root.Root.GetAppender("LogFileAppender");

				if (rfa != null)
				{
					// Assign the value to the appender  
					rfa.File = Path.Combine(Application.CommonAppDataPath, Path.GetFileName(rfa.File));

					// Apply changes to the appender  
					rfa.ActivateOptions();
				}
			}
		}
		#endregion

		#region Methods
		public static void WriteLog(ELogLevel logLevel, String log)
		{
			if (logLevel.Equals(ELogLevel.DEBUG))
			{
				logger.Debug(log);
			}
			else if (logLevel.Equals(ELogLevel.ERROR))
			{
				logger.Error(log);
			}
			else if (logLevel.Equals(ELogLevel.FATAL))
			{
				logger.Fatal(log);
			}
			else if (logLevel.Equals(ELogLevel.INFO))
			{
				logger.Info(log);
			}
			else if (logLevel.Equals(ELogLevel.WARN))
			{
				logger.Warn(log);
			}
		}
		#endregion
	}

	public enum ELogLevel
	{
		DEBUG = 1,
		ERROR,
		FATAL,
		INFO,
		WARN
	}
}
