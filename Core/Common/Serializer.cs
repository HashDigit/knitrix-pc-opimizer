using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Common
{
	public class Serializer
	{

		public static void Save(string filePath, object ds)
		{
			try
			{
				// Create reqiured subfolders
				string fullDirectory = new FileInfo(filePath).DirectoryName;
				if (!Directory.Exists(fullDirectory))
					Directory.CreateDirectory(fullDirectory);
				using (Stream stream = File.Create(filePath))
				{
					XmlSerializer xs = new XmlSerializer(ds.GetType());
					xs.Serialize(stream, ds);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving object to file.\r\nFile:{0}\r\nError:\r\n{1}", filePath, ex));
			}
		}

		public static object Load(string filePath, Type type)
		{
			object ds = null;
			try
			{
				using (Stream stream = File.OpenRead(filePath))
				{
					XmlSerializer xs = new XmlSerializer(type);
					ds = xs.Deserialize(stream);
				}
			}
			catch (Exception ex)
			{
				ds = null;
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while loading object from file.\r\nFile:{0}\r\nError:\r\n{1}", filePath, ex));
			}
			return ds;
		}

		/// <summary>
		/// Serialize object and return result string
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		public static string SaveToString(object ds)
		{
			using (Stream stream = new MemoryStream())
			{
				XmlSerializer xs = new XmlSerializer(ds.GetType());
				xs.Serialize(stream, ds);
				stream.Position = 0;
				using (StreamReader sr = new StreamReader(stream))
				{
					return sr.ReadToEnd();
				}
			}
		}

		public static object LoadFromString(string str, Type type)
		{
			object result = null;
			try
			{
				XmlSerializer xs = new XmlSerializer(type);
				using (MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(str)))
				{
					XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
					return xs.Deserialize(memoryStream);
				}
			}
			catch (Exception ex)
			{
				result = null;
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
			}
			return result;
		}

		/// <summary>
		/// Make full copy of object (include structure and data)
		/// </summary>
		/// <returns></returns>
		public static T Copy<T>(T obj)
		{
			T result = default(T);
			using (Stream stream = new MemoryStream())
			{
				//if (Config.UseBinaryFormatter)
				{
					BinaryFormatter bformatter = new BinaryFormatter();
					bformatter.Serialize(stream, obj);
					stream.Position = 0;
					result = (T)bformatter.Deserialize(stream);
				}
				//else
				//{
				//    XmlSerializer xs = new XmlSerializer(typeof(T));
				//    xs.Serialize(stream, obj);
				//    stream.Position = 0;
				//    result = (T)xs.Deserialize(stream);
				//}
			}
			return result;
		}

		/// <summary>
		/// Converts the String to UTF8 Byte array and is used in De serialization
		/// </summary>
		/// <param name="pXmlString"></param>
		/// <returns></returns>
		private static Byte[] StringToUTF8ByteArray(String pXmlString)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			Byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		}
	}
}
