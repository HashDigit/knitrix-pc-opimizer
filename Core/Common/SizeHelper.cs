namespace Common
{
    public class SizeHelper
    {
        public static string GetSize(ulong size)
        {
            double s = size;
            string[] format = new string[] { "{0} bytes", "{0} Kb", "{0} Mb", "{0} Gb", "{0} Tb", "{0} Pb", "{0} Eb" };
            int i = 0;
            while (i < format.Length && s >= 1024)
            {
				s = (ulong)(100 * s / 1024) / 100.0;
                i++;
            }
            return string.Format(format[i], s);
        }

		public static string GetSizeInKb(ulong size, int length)
		{
			return string.Format("{0} Kb", size / 1024).PadLeft(length);
		}
	}
}