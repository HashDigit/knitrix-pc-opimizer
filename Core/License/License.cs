using System;
using System.Management;
using System.Xml.Serialization;
using Business;
using Common;
using Core.Properties;
using Data;

namespace Business
{
	public class License
	{

		private static volatile License instance;
		private static object syncRoot = new Object();

		private License()
		{
			BlackSerials = new string[0];
		}

		public static License Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
						{
							//instance = new Options();
							try
							{
								instance = (License)Serializer.LoadFromString(Settings.Default.License, typeof(License));
								if (instance == null)
								{
									DefaultInit();
									instance.Save();
								}
							}
							catch
							{
								DefaultInit();
								instance.Save();
							}
							instance.InternalInit();
						}
					}
				}

				return instance;
			}
		}

		// public settings for serialization only
		public DateTime FirstApplicationStartTime { get; set; }

		public string Serial
		{
			get
			{
				return _serial;
			}
			set
			{
				_serial = value;
				if (DateOfFirstTimeSerialUsage != null &&
					!DateOfFirstTimeSerialUsage.ContainsKey(_serial))
					DateOfFirstTimeSerialUsage.Add(_serial, DateTime.Now);
				InternalInit();
			}
		}
		private string _serial { get; set; }

		/// <summary>
		/// Date of first time serial was used (for each serial).
		/// Used to control serial expiration
		/// </summary>
		// public settings for serialization only
		public SerializableDictionary<string, DateTime> DateOfFirstTimeSerialUsage { get; set; }

		// public settings for serialization only
		public LicenseStateEnum LicenseState { get; set; }

		// public settings for serialization only
		public string[] BlackSerials { get; set; }

		private string _privateKey = "<?xml version=\"1.0\"?>" +
"<RSAParameters xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
@"<Exponent>AQAB</Exponent>
  <Modulus>mRclCj5NkB4EyUA+kRjAwTFiSv92eaMpydkNxq7W33X39BchFRC0auTLrmanXZt5wpdcM9pqw5jSciUf9lqGvTfizH4PenRYBvqOgm6U0/kyVx0LlrTuVlIsRjKffsGHXff6EOy1T0+Yp3SQEYStFjQgEIHIxDYzWQ+FokwkoGk=</Modulus>
  <P>zPGCnB7uQAZqjtE6JP1W5ad3fp02r6k7IedhAyeHZdCRZbJK6k3c5JS5TFVpFYq9G+FzQO1hhx6Zf5V4KJjXFw==</P>
  <Q>vzqnOj0gPxgnPiqmmTGKM4aFVcY3wmVOk3ts+blpAddn5e2GA6HiJ9CYXxWPyjcNTmGHxHvklpvU80X9cTD0fw==</Q>
  <DP>F6QkdO38fdY/pInuMyYmSAexeSGZlVnoQFj4j+yXad9NZnXSvoSM3rFpTqzrmliOyBCRwwOfJH8bJeSQH97BsQ==</DP>
  <DQ>DKVmVbWhIksInVXYupzKFOo6exemrFZiweIRrNoUFakXF9+DU04rVJK44ODeJ3NIj/xnLBk9XrBjfssMHaYtJQ==</DQ>
  <InverseQ>VT8ybe0ncZi7ejw5N7k3Yqc+7D5S2Lv4ctyWTqV70gVzfqdzR1DTpaE56B8Fghm/aj3s2HPqP6banBxtEMAsxA==</InverseQ>
  <D>CBUUu4m2hplPH2Do7Lgl1Q1KZzM9igZ3OlTfSVIJvhp4UMa31GjhXZNevYJl66c9smxTTdmGMXVsBuK24Ua+PkI+ji/UZSq5MdqKF0c82qnBERMs7ZM6RY8xg8symEleZe5DhYVp4Mfreef4sWhjk8jAh1XdpMad+6TxxxJTgVE=</D>
</RSAParameters>";

		[XmlIgnore]
		public ProductKeyInfo ProductKeyInfo { get; private set; }
		private ProductKeyPublisher _keyPublisher;

		/// <summary>
		/// Setup internal application license info (internal initialization)
		/// </summary>
		private void InternalInit()
		{
			_keyPublisher = new ProductKeyPublisher(_privateKey);

			if (Config.TrialPeriod > 0 && FirstApplicationStartTime.AddDays(Config.TrialPeriod) < DateTime.Now)
				LicenseState = LicenseStateEnum.TrialExpired;

			try
			{
				if (!string.IsNullOrEmpty(Serial))
				{
					ProductKeyInfo = _keyPublisher.ValidateProductKey(Serial);

					if (ProductKeyInfo.TrialDays > 0 &&
						DateOfFirstTimeSerialUsage.ContainsKey(Serial) &&
						DateOfFirstTimeSerialUsage[Serial].AddDays(ProductKeyInfo.TrialDays) < DateTime.Now)
						LicenseState = LicenseStateEnum.LicenseExpired;
					else if (IsInBlackList())
						LicenseState = LicenseStateEnum.BlackList;
					else
						LicenseState = LicenseStateEnum.Registered;
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
			}
		}

		/// <summary>
		/// Default license values for first initialization only!
		/// </summary>
		private static void DefaultInit()
		{
			instance = new License();

			instance.FirstApplicationStartTime = DateTime.Now;
			instance.LicenseState = LicenseStateEnum.Trial;
			instance.DateOfFirstTimeSerialUsage = new SerializableDictionary<string, DateTime>();
			//instance.BlackSerials = new string[0];
			instance.Serial = ""; // serial setter should be last!
		}

		/// <summary>
		/// Save options to Properties.Settings
		/// </summary>
		public void Save()
		{
			try
			{
				string licenseStr = Serializer.SaveToString(this);
				Settings.Default.License = licenseStr;
				Settings.Default.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex));
			} 
		}

		/// <summary>
		/// Reset and save license info
		/// </summary>
		public void Reset()
		{
			try
			{
				instance = null;
				Settings.Default.License = null;
				Settings.Default.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex));
			} 
		}

		public ProductKeyInfo ValidateProductKey(string serial)
		{
			return _keyPublisher.ValidateProductKey(serial);
		}

		public static string GetCPUID()
		{
			var cpuInfo = string.Empty;
			var mc = new ManagementClass("win32_processor");
			var moc = mc.GetInstances();

			foreach (ManagementObject mo in moc)
			{
				if (cpuInfo == "")
				{
					//Get only the first CPU's ID
					cpuInfo = mo.Properties["processorID"].Value.ToString();
					break;
				}
			}
			return cpuInfo;
		}

		public bool IsInBlackList()
		{
			for (int i = 0; i < BlackSerials.Length; i++)
			{
				//if (BlackSerials[i] == ProductKeyInfo.SerialId.ToString())
				if (BlackSerials[i].Replace("-", "").ToLower() == Serial.Replace("-", "").ToLower())
					return true;
			}
			return false;
		}

		public void Reinit()
		{
			InternalInit();
		}
	}
}