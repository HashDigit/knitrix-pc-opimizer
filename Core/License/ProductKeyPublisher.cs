using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Common;
using Data;
using Exceptions;

namespace Business
{
	/// <summary>
	/// License Manager 2.0!
	/// </summary>
	public class ProductKeyPublisher
	{
		RSACryptoServiceProvider _cryptoService;

		#region Constructor
		public ProductKeyPublisher(string privateXmlKey)
		{
			_cryptoService = new RSACryptoServiceProvider();
			//_cryptoService.FromXmlString(privateXmlKey);
			_cryptoService.ImportParameters((RSAParameters)Serializer.LoadFromString(privateXmlKey, typeof(RSAParameters)));
			if (_cryptoService.PublicOnly)
			{
				throw new CryptographicException("A private key must be provided.");
			}
		}
		public ProductKeyPublisher(RSACryptoServiceProvider cryptoService)
		{
			_cryptoService = cryptoService;
		}
		#endregion

		#region ValidateProductKey
		public ProductKeyInfo ValidateProductKey(string productKey)
		{
			if (String.IsNullOrEmpty(productKey))
				throw new ArgumentNullException("Product Key is null or empty.");

			if (productKey.Length != 35)
				throw new ArgumentException("Product key is invalid.");

			productKey = productKey.Replace("-", "");

			byte[] keyBytes = Base32Converter.FromBase32String(productKey);

			byte[] signBytes = new byte[2];
			byte[] hiddenBytes = new byte[16];
			using (MemoryStream stream = new MemoryStream(keyBytes))
			{
				stream.Read(hiddenBytes, 0, 8);
				stream.Read(signBytes, 0, 2);
				stream.Read(hiddenBytes, 8, hiddenBytes.Length - 8);
				keyBytes = stream.ToArray();
			}

			byte[] sign = _cryptoService.SignData(signBytes, new SHA1CryptoServiceProvider());
			byte[] rkey = new byte[32];
			byte[] rjiv = new byte[16];
			Array.Copy(sign, rkey, 32);
			Array.Copy(sign, 32, rjiv, 0, 16);

			SymmetricAlgorithm algorithm = new RijndaelManaged();
			byte[] hiddenData;
			try
			{
				hiddenData = algorithm.CreateDecryptor(rkey, rjiv).TransformFinalBlock(hiddenBytes, 0, hiddenBytes.Length);
			}
			catch (Exception ex)
			{
				throw new BusinessException("Product key is invalid.", ex);
			}

			byte[] proid = new byte[2];
			byte[] pinfo = new byte[4];
			byte[] xinfo = new byte[4];
			//byte[] ticks = new byte[8];
			byte[] sid = new byte[4];

			using (MemoryStream memStream = new MemoryStream(hiddenData))
			{
				memStream.Read(proid, 0, 2);
				memStream.Read(pinfo, 0, 4);
				memStream.Read(xinfo, 0, 4);
				//memStream.Read(ticks, 0, 8);
				memStream.Read(sid, 0, 4);
			}

			if (signBytes[0] == proid[0] && signBytes[1] == proid[1])
			{
				return new ProductKeyInfo()
				{
					ProductId = BitConverter.ToInt16(proid, 0),
					//ProductFeatures = BitConverter.ToInt16(pinfo, 0),
					ProductFeatures = Encoding.Unicode.GetString(pinfo),
					SerialId = BitConverter.ToInt16(sid, 0),
					TrialDays = BitConverter.ToInt16(xinfo, 0),
				};

				//DateTime generatedDate = new DateTime(BitConverter.ToInt64(ticks, 0));
				//if (generatedDate.Year > 2000 && generatedDate.Year < 2100)
				//{
				//    return new ProductKeyInfo()
				//    {
				//        ProductId = BitConverter.ToInt16(proid, 0),
				//        ProductFeatures = BitConverter.ToInt16(pinfo, 0),
				//        TrialDays = BitConverter.ToInt16(xinfo, 0),
				//        GeneratedDate = generatedDate
				//    };
				//}
				//else
				//{
				//    throw new BusinessException("Product key date is incorrect.");
				//}
			}
			else
			{
				throw new BusinessException("Product key info is incorrect.");
			}
		}
		#endregion
	}
}