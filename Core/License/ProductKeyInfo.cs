namespace Data
{
	public class ProductKeyInfo
	{
		public short ProductId { get; set; }
		public string ProductFeatures { get; set; }
		public int SerialId { get; set; }
		public short TrialDays { get; set; }
	}
}