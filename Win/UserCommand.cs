using System.Collections.Generic;
using System.Windows.Forms;

namespace Win
{
	/// <summary>
	/// Command that user can send to 'Auto Clicker' applciation by clicking buttons, shortcuts, etc.
	/// </summary>
	public class UserCommand
	{
		public UserCommandTypeEnum Type { get; set; }

		/// <summary>
		/// List of all buttons associated with this user command
		/// </summary>
		public List<Control> Buttons { get; set; }

		/// <summary>
		/// List of all menu items associated with this user command
		/// </summary>
		public List<ToolStripItem> MenuItems { get; set; }

		/// <summary>
		/// Is user having ability to execute this command (because of application state)?
		/// </summary>
		public bool IsEnabled
		{
			get
			{
				return _isEnabled;
			}
			set
			{
				_isEnabled = value;
				foreach (Control control in Buttons)
				{
					control.Enabled = _isEnabled;
				}

				foreach (ToolStripItem toolStripItem in MenuItems)
				{
					toolStripItem.Enabled = _isEnabled;
				}
			}
		}
		private bool _isEnabled { get; set; }

		public UserCommand(UserCommandTypeEnum type, Control button, ToolStripItem[] menuItems)
		{
			Type = type;
			Buttons = new List<Control>();
			if (button != null)
				Buttons.Add(button);
			MenuItems = new List<ToolStripItem>();
			MenuItems.AddRange(menuItems);
		}
	}

	/// <summary>
	/// List of all commands
	/// </summary>
	public enum UserCommandTypeEnum
	{
		AddSyncPoint,
		AddBackup,
		ManageBackups,
	}
}