using System;
using System.Windows.Forms;
using Business;

namespace Win
{
	public class ExceptionHelper
	{
		/// <summary>
		/// ���������� ������ ������������
		/// </summary>
		/// <param name="ex"></param>
		public static void Show(Exception ex)
		{
			if (ex.InnerException != null)
			{
				MessageBox.Show(ex.InnerException.Message, "Error");
			}
			else
			{
				MessageBox.Show(ex.Message, "Error");
			}
		}
	}
}