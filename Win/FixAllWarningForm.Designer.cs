﻿namespace Win
{
	partial class FixAllWarningForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FixAllWarningForm));
			this.epMain = new System.Windows.Forms.ErrorProvider(this.components);
			this.lblTitle = new System.Windows.Forms.Label();
			this.pbLogo = new System.Windows.Forms.PictureBox();
			this.btnCancel = new Win.Controls.RegisterButton();
			this.btnSend = new Win.Controls.RegisterButton();
			this.btnInvisibleCancel = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)(this.epMain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
			this.SuspendLayout();
			// 
			// epMain
			// 
			this.epMain.ContainerControl = this;
			this.epMain.RightToLeft = true;
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTitle.BackColor = System.Drawing.Color.Transparent;
			this.lblTitle.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.lblTitle.Location = new System.Drawing.Point(89, 9);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(347, 121);
			this.lblTitle.TabIndex = 82;
			this.lblTitle.Text = resources.GetString("lblTitle.Text");
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pbLogo
			// 
			this.pbLogo.BackColor = System.Drawing.Color.Transparent;
			this.pbLogo.BackgroundImage = global::Win.Properties.Resources.need_register_icon;
			this.pbLogo.Location = new System.Drawing.Point(16, 12);
			this.pbLogo.Name = "pbLogo";
			this.pbLogo.Size = new System.Drawing.Size(65, 65);
			this.pbLogo.TabIndex = 81;
			this.pbLogo.TabStop = false;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(296, 237);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(140, 27);
			this.btnCancel.TabIndex = 80;
			this.btnCancel.Text = "No, cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSend
			// 
			this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSend.BackColor = System.Drawing.Color.Transparent;
			this.btnSend.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnSend.ForeColor = System.Drawing.Color.White;
			this.btnSend.Location = new System.Drawing.Point(12, 237);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(180, 27);
			this.btnSend.TabIndex = 79;
			this.btnSend.Text = "Yes, I agree!";
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// btnInvisibleCancel
			// 
			this.btnInvisibleCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnInvisibleCancel.Location = new System.Drawing.Point(-133, 144);
			this.btnInvisibleCancel.Name = "btnInvisibleCancel";
			this.btnInvisibleCancel.Size = new System.Drawing.Size(75, 23);
			this.btnInvisibleCancel.TabIndex = 83;
			this.btnInvisibleCancel.Text = "simpleButton1";
			// 
			// FixAllWarningForm
			// 
			this.Appearance.BackColor = System.Drawing.Color.White;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
			this.BackgroundImageStore = global::Win.Properties.Resources.need_register_back;
			this.CancelButton = this.btnInvisibleCancel;
			this.ClientSize = new System.Drawing.Size(448, 276);
			this.Controls.Add(this.btnInvisibleCancel);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.pbLogo);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSend);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FixAllWarningForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Fix All";
			((System.ComponentModel.ISupportInitialize)(this.epMain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Controls.RegisterButton btnSend;
		private Controls.RegisterButton btnCancel;
		private System.Windows.Forms.ErrorProvider epMain;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.PictureBox pbLogo;
		private DevExpress.XtraEditors.SimpleButton btnInvisibleCancel;
	}
}