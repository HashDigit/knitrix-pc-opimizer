﻿namespace Win
{
	partial class DummyForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.commonPanel1 = new Win.Controls.CommonPanel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabUserControl1 = new Win.Controls.TabUserControl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.regularButton1 = new Win.Controls.RegularButton();
			this.commonPanel1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// commonPanel1
			// 
			this.commonPanel1.BackColor = System.Drawing.Color.Gray;
			this.commonPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.commonPanel1.Controls.Add(this.tabUserControl1);
			this.commonPanel1.Controls.Add(this.panel1);
			this.commonPanel1.Controls.Add(this.regularButton1);
			this.commonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.commonPanel1.HideLogo = false;
			this.commonPanel1.Location = new System.Drawing.Point(0, 0);
			this.commonPanel1.Name = "commonPanel1";
			this.commonPanel1.NavBarImage = null;
			this.commonPanel1.Size = new System.Drawing.Size(820, 546);
			this.commonPanel1.TabIndex = 0;
			this.commonPanel1.Icon = null;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.Transparent;
			this.panel2.Location = new System.Drawing.Point(78, 73);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(315, 195);
			this.panel2.TabIndex = 83;
			// 
			// tabUserControl1
			// 
			this.tabUserControl1.BackColor = System.Drawing.Color.Transparent;
			this.tabUserControl1.Location = new System.Drawing.Point(127, 134);
			this.tabUserControl1.Name = "tabUserControl1";
			this.tabUserControl1.Size = new System.Drawing.Size(578, 323);
			this.tabUserControl1.TabIndex = 81;
			this.tabUserControl1.TabPageDisk = null;
			this.tabUserControl1.TabPageHome = null;
			this.tabUserControl1.TabPageSelected = null;
			this.tabUserControl1.TabPageSettings = null;
			this.tabUserControl1.TabPageOptimize = null;
			this.tabUserControl1.Load += new System.EventHandler(this.tabUserControl1_Load);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(427, 121);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(200, 100);
			this.panel1.TabIndex = 82;
			// 
			// regularButton1
			// 
			this.regularButton1.BackColor = System.Drawing.Color.Transparent;
			this.regularButton1.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.regularButton1.ForeColor = System.Drawing.Color.White;
			this.regularButton1.Location = new System.Drawing.Point(190, 286);
			this.regularButton1.Name = "regularButton1";
			this.regularButton1.Size = new System.Drawing.Size(116, 25);
			this.regularButton1.TabIndex = 79;
			this.regularButton1.Text = "regularButton1";
			// 
			// DummyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(820, 546);
			this.Controls.Add(this.commonPanel1);
			this.Name = "DummyForm";
			this.Text = "DummyForm";
			this.commonPanel1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Controls.CommonPanel commonPanel1;
		private Controls.RegularButton regularButton1;
		private Controls.TabUserControl tabUserControl1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
	}
}