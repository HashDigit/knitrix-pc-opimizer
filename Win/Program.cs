﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Business;
using Business.TaskScheduler;
using Common;
using Core.Business;
using Data;
using Win.Misc;
using Win.Properties;

namespace Win
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			#region IsFirstRun switcher
			try
			{
                
				Config.IsFirstRun = Win.Properties.Settings.Default.IsFirstRun;
				Settings.Default.IsFirstRun = false;
				Settings.Default.Save();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex));
			}
			#endregion IsFirstRun switcher

			// Context.AutoStartScan = Options.Instance.GeneralSettings.AutoStartScan || Config.IsFirstRun;

			//if (Config.IsFirstRun)
			//    // first run
			//    SetDefaultScanSchedule();

			if (args != null && args.Length > 0)
			{
				if (args[0] == "rs")
				{
					License.Instance.Reset();
					Options.Instance.Reset();
					Settings.Default.Reset();
					try
					{
						Settings.Default.Save();
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex.ToString()));
						ExceptionHelper.Show(ex);
					}
					return;
				}
				if (args[0] == "scan")
				{
					Context.AutoStartScan = true;
				}
			}

			if (SingleApplication.IsAlreadyRunning())
			{
				//set focus on previously running app
				//SingleApplication.SwitchToCurrentInstance();
				return;
			}

			//if (Context.License.IsTrialExpired)
			//	MessageBox.Show(string.Format("Thank you for trying {0}. Your trial has now expired. Please click Buy Now (hyperlinked) to purchase your lifetime license today.", Application.ProductName), Application.ProductName);

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

			// to speed up application start checking for updates in separate thread (Options are loading very slowly)
			new Thread(CheckForUpdates).Start();

			if (Context.AutoStartScan)
				KillAllCompetitors();

			Permissions.SetPrivileges(true);
			Application.Run(Controller.GetMainForm());
			//Application.Run(new DummyForm());
			Permissions.SetPrivileges(false);
		}

		private static void SetDefaultScanSchedule()
		{
			var newTask = ScanScheduleLogic.Create();
			if (newTask == null)
				return;
			newTask.Triggers.Clear();
			newTask.Triggers.Add(new WeeklyTrigger(15, 00, DaysOfTheWeek.Saturday));
			ScanScheduleLogic.Save(newTask);
		}

		private static void CheckForUpdates()
		{
			if (Options.Instance.IsAutocheckForUpdates)
				CheckForUpdateLogic.Run(false);
		}

		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			CLogger.WriteLog(ELogLevel.FATAL, e.ExceptionObject.ToString());
			MessageBox.Show(e.ExceptionObject.ToString(), "Error");
		}

		private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			CLogger.WriteLog(ELogLevel.FATAL, e.Exception.ToString());
			//if (Config.IsDebug)
			//NotifierLogic.Notify(ToolTipIcon.Error, "Error", e.Exception.ToString());
			MessageBox.Show(e.Exception.ToString(), "Error");
		}

		/// <summary>
		/// Kill all already running Knitrix applications exept this
		/// </summary>
		private static void KillAllCompetitors()
		{
			var competitorProcesses = Process.GetProcessesByName(Application.ProductName);
			foreach (var competitorProcess in competitorProcesses)
			{
				if (competitorProcess.Id != Process.GetCurrentProcess().Id)
					competitorProcess.Kill();
			}
		}
	}
}
