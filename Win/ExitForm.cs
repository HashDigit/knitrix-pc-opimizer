﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Data;
using DevExpress.XtraEditors;

namespace Win
{
	public partial class ExitForm : DevExpress.XtraEditors.XtraForm
	{
		public ExitForm()
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			if (cbDontAskAgain.Checked)
			{
				// change General Settings and save Option
				Options.Instance.GeneralSettings.DisplayExitActions = false;
				Options.Instance.GeneralSettings.MinimizeToTray = false;
				Options.Instance.Save();
			}
			Application.Exit();
		}

		private void btnMinimize_Click(object sender, EventArgs e)
		{
			if (cbDontAskAgain.Checked)
			{
				// change General Settings and save Option
				Options.Instance.GeneralSettings.DisplayExitActions = false;
				Options.Instance.GeneralSettings.MinimizeToTray = true;
				Options.Instance.Save();
				Controller.GetMainForm().SettingsGeneralSettingsRefresh();
			}
			var mainForm = Controller.GetMainForm();
			mainForm.MinimizeToTray();
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			if (cbDontAskAgain.Checked)
			{
				// change General Settings and save Option
				Options.Instance.GeneralSettings.DisplayExitActions = false;
				Options.Instance.Save();
				Controller.GetMainForm().SettingsGeneralSettingsRefresh();
			}
			Close();
		}
	}
}