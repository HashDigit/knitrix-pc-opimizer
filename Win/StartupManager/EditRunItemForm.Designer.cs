﻿namespace Win.StartupManager
{
	partial class EditRunItemForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtFile = new DevExpress.XtraEditors.TextEdit();
			this.lblFile = new System.Windows.Forms.Label();
			this.btnFileBrowse = new DevExpress.XtraEditors.SimpleButton();
			this.lblArguments = new System.Windows.Forms.Label();
			this.txtArguments = new DevExpress.XtraEditors.TextEdit();
			this.btnOk = new Win.Controls.RegularButton();
			this.btnCancel = new Win.Controls.RegularButton();
			((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArguments.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// txtFile
			// 
			this.txtFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFile.Location = new System.Drawing.Point(41, 5);
			this.txtFile.Name = "txtFile";
			this.txtFile.Properties.ReadOnly = true;
			this.txtFile.Size = new System.Drawing.Size(204, 20);
			this.txtFile.TabIndex = 0;
			// 
			// lblFile
			// 
			this.lblFile.AutoSize = true;
			this.lblFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblFile.Location = new System.Drawing.Point(12, 9);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(27, 13);
			this.lblFile.TabIndex = 1;
			this.lblFile.Text = "File:";
			// 
			// btnFileBrowse
			// 
			this.btnFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnFileBrowse.Location = new System.Drawing.Point(251, 5);
			this.btnFileBrowse.Name = "btnFileBrowse";
			this.btnFileBrowse.Size = new System.Drawing.Size(33, 20);
			this.btnFileBrowse.TabIndex = 3;
			this.btnFileBrowse.Text = "...";
			this.btnFileBrowse.Click += new System.EventHandler(this.btnFileBrowse_Click);
			// 
			// lblArguments
			// 
			this.lblArguments.AutoSize = true;
			this.lblArguments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblArguments.Location = new System.Drawing.Point(12, 38);
			this.lblArguments.Name = "lblArguments";
			this.lblArguments.Size = new System.Drawing.Size(63, 13);
			this.lblArguments.TabIndex = 4;
			this.lblArguments.Text = "Arguments:";
			// 
			// txtArguments
			// 
			this.txtArguments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtArguments.Location = new System.Drawing.Point(80, 35);
			this.txtArguments.Name = "txtArguments";
			this.txtArguments.Size = new System.Drawing.Size(204, 20);
			this.txtArguments.TabIndex = 5;
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOk.BackColor = System.Drawing.Color.Transparent;
			this.btnOk.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnOk.ForeColor = System.Drawing.Color.White;
			this.btnOk.Location = new System.Drawing.Point(20, 61);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(142, 27);
			this.btnOk.TabIndex = 92;
			this.btnOk.Text = "OK";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(168, 61);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(116, 27);
			this.btnCancel.TabIndex = 91;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// EditRunItemForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(296, 96);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.txtArguments);
			this.Controls.Add(this.lblArguments);
			this.Controls.Add(this.btnFileBrowse);
			this.Controls.Add(this.lblFile);
			this.Controls.Add(this.txtFile);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EditRunItemForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Edit Startup Entry";
			((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArguments.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.TextEdit txtFile;
		private System.Windows.Forms.Label lblFile;
		private DevExpress.XtraEditors.SimpleButton btnFileBrowse;
		private System.Windows.Forms.Label lblArguments;
		private DevExpress.XtraEditors.TextEdit txtArguments;
		private Controls.RegularButton btnOk;
		private Controls.RegularButton btnCancel;
	}
}