﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Common;
using DevExpress.XtraEditors;
using Microsoft.Win32;

namespace Win.StartupManager
{
	public partial class EditRunItemForm : DevExpress.XtraEditors.XtraForm
	{
		private string strItem;
		private string strSection;

        public EditRunItemForm(string item, string section, string path, string args)
        {
            InitializeComponent();

            strItem = item;
            strSection = section;

            this.Text = "Edit - " + strItem;

            this.txtFile.Text = path;
            this.txtArguments.Text = args;
        }


		private void btnOk_Click(object sender, EventArgs e)
		{
			if (!RegistryHelper.FileExists(this.txtFile.Text))
			{
				MessageBox.Show(this, Properties.Resources.smCannotFindFile, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if (strSection.StartsWith("HKEY"))
			{
				string strPath = "";

                //if (!string.IsNullOrEmpty(this.txtFile.Text) && !string.IsNullOrEmpty(this.txtArguments.Text))
                //    strPath = string.Format("\"{0}\" {1}", this.txtFile.Text, this.txtArguments.Text);
                //else
                //    strPath = string.Format("\"{0}\"", this.txtFile.Text);
                if (!string.IsNullOrEmpty(this.txtFile.Text) && !string.IsNullOrEmpty(this.txtArguments.Text))
                    strPath = string.Format("{0} {1}", this.txtFile.Text, this.txtArguments.Text);
                else
                    strPath = string.Format("{0}", this.txtFile.Text);

				string strMainKey = strSection.Substring(0, strSection.IndexOf('\\'));
				string strSubKey = strSection.Substring(strSection.IndexOf('\\') + 1);

				RegistryKey rk = RegistryHelper.RegOpenKey(strMainKey, strSubKey);

				if (rk != null)
				{
					rk.SetValue(strItem, strPath);
					rk.Close();
				}
			}
			else if (Directory.Exists(strSection))
			{
				string strItemPath = Path.Combine(strSection, strItem);

				File.Delete(strItemPath);

                //RegistryHelper.CreateShortcut(strItemPath, '"' + this.txtFile.Text + '"', this.txtArguments.Text);
                RegistryHelper.CreateShortcut(strItemPath, this.txtFile.Text, this.txtArguments.Text);
			}
			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void btnFileBrowse_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.InitialDirectory = Path.GetDirectoryName(this.txtFile.Text.Trim('"'));
			ofd.CheckFileExists = true;
			ofd.Multiselect = false;
			ofd.Filter = "All Files (*.*)|*.*";

			if (ofd.ShowDialog(this) == DialogResult.OK)
				this.txtFile.Text = ofd.FileName;

			this.DialogResult = DialogResult.None;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
	}
}