﻿namespace Win.StartupManager
{
	partial class NewRunItemForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblArguments = new System.Windows.Forms.Label();
			this.lblSection = new System.Windows.Forms.Label();
			this.lblPath = new System.Windows.Forms.Label();
			this.lblName = new System.Windows.Forms.Label();
			this.cmbSection = new DevExpress.XtraEditors.ComboBoxEdit();
			this.txtName = new DevExpress.XtraEditors.TextEdit();
			this.txtPath = new DevExpress.XtraEditors.TextEdit();
			this.txtArguments = new DevExpress.XtraEditors.TextEdit();
			this.btnFileBrowse = new DevExpress.XtraEditors.SimpleButton();
			this.btnOk = new Win.Controls.RegularButton();
			this.btnCancel = new Win.Controls.RegularButton();
			((System.ComponentModel.ISupportInitialize)(this.cmbSection.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArguments.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// lblArguments
			// 
			this.lblArguments.AutoSize = true;
			this.lblArguments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblArguments.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.lblArguments.Location = new System.Drawing.Point(12, 88);
			this.lblArguments.Name = "lblArguments";
			this.lblArguments.Size = new System.Drawing.Size(63, 13);
			this.lblArguments.TabIndex = 9;
			this.lblArguments.Text = "Arguments:";
			// 
			// lblSection
			// 
			this.lblSection.AutoSize = true;
			this.lblSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblSection.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.lblSection.Location = new System.Drawing.Point(12, 9);
			this.lblSection.Name = "lblSection";
			this.lblSection.Size = new System.Drawing.Size(46, 13);
			this.lblSection.TabIndex = 7;
			this.lblSection.Text = "Section:";
			// 
			// lblPath
			// 
			this.lblPath.AutoSize = true;
			this.lblPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblPath.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.lblPath.Location = new System.Drawing.Point(12, 62);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(52, 13);
			this.lblPath.TabIndex = 8;
			this.lblPath.Text = "File path:";
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.lblName.Location = new System.Drawing.Point(12, 34);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(38, 13);
			this.lblName.TabIndex = 6;
			this.lblName.Text = "Name:";
			// 
			// cmbSection
			// 
			this.cmbSection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cmbSection.Location = new System.Drawing.Point(80, 5);
			this.cmbSection.Name = "cmbSection";
			this.cmbSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbSection.Properties.Items.AddRange(new object[] {
            "StartUp\\All Users\t",
            "StartUp\\Current User\t",
            "Registry\\All Users\\Run\t",
            "Registry\\All Users\\Run Once\t",
            "Registry\\All Users\\Run Services\t",
            "Registry\\All Users\\Run Services Once\t",
            "Registry\\Current User\\Run\t",
            "Registry\\Current User\\Run Once\t",
            "Registry\\Current User\\Run Services\t",
            "Registry\\Current User\\Run Services Once\t"});
			this.cmbSection.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.cmbSection.Size = new System.Drawing.Size(239, 20);
			this.cmbSection.TabIndex = 10;
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.Location = new System.Drawing.Point(80, 30);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(239, 20);
			this.txtName.TabIndex = 11;
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtPath.Location = new System.Drawing.Point(80, 58);
			this.txtPath.Name = "txtPath";
			this.txtPath.Properties.ReadOnly = true;
			this.txtPath.Size = new System.Drawing.Size(200, 20);
			this.txtPath.TabIndex = 12;
			// 
			// txtArguments
			// 
			this.txtArguments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtArguments.Location = new System.Drawing.Point(80, 84);
			this.txtArguments.Name = "txtArguments";
			this.txtArguments.Size = new System.Drawing.Size(239, 20);
			this.txtArguments.TabIndex = 13;
			// 
			// btnFileBrowse
			// 
			this.btnFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnFileBrowse.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnFileBrowse.Location = new System.Drawing.Point(286, 58);
			this.btnFileBrowse.Name = "btnFileBrowse";
			this.btnFileBrowse.Size = new System.Drawing.Size(33, 20);
			this.btnFileBrowse.TabIndex = 14;
			this.btnFileBrowse.Text = "...";
			this.btnFileBrowse.Click += new System.EventHandler(this.btnFileBrowse_Click);
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOk.BackColor = System.Drawing.Color.Transparent;
			this.btnOk.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnOk.ForeColor = System.Drawing.Color.White;
			this.btnOk.Location = new System.Drawing.Point(55, 112);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(142, 27);
			this.btnOk.TabIndex = 94;
			this.btnOk.Text = "OK";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(203, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(116, 27);
			this.btnCancel.TabIndex = 93;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// NewRunItemForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(331, 151);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnFileBrowse);
			this.Controls.Add(this.txtArguments);
			this.Controls.Add(this.txtPath);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.cmbSection);
			this.Controls.Add(this.lblArguments);
			this.Controls.Add(this.lblSection);
			this.Controls.Add(this.lblPath);
			this.Controls.Add(this.lblName);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "NewRunItemForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "New Startup Entry";
			this.Load += new System.EventHandler(this.NewRunItemForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.cmbSection.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArguments.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblArguments;
		private System.Windows.Forms.Label lblSection;
		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.Label lblName;
		private DevExpress.XtraEditors.ComboBoxEdit cmbSection;
		private DevExpress.XtraEditors.TextEdit txtName;
		private DevExpress.XtraEditors.TextEdit txtPath;
		private DevExpress.XtraEditors.TextEdit txtArguments;
		private DevExpress.XtraEditors.SimpleButton btnFileBrowse;
		private Controls.RegularButton btnOk;
		private Controls.RegularButton btnCancel;
	}
}