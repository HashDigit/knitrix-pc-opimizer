﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Common;
using DevExpress.XtraEditors;
using Microsoft.Win32;

namespace Win.StartupManager
{
	public partial class NewRunItemForm : DevExpress.XtraEditors.XtraForm
	{
		public NewRunItemForm()
		{
			InitializeComponent();
		}

		private void NewRunItemForm_Load(object sender, EventArgs e)
		{
			cmbSection.SelectedItem = cmbSection.Properties.Items[0];
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			RegistryKey regKey;

			string strRegPath = this.cmbSection.SelectedItem.ToString().Trim().Substring(this.cmbSection.SelectedItem.ToString().Trim().LastIndexOf('\\') + 1);
			string strRunRegPath = "";

			if (strRegPath == "Run")
				strRunRegPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
			else if (strRegPath == "Run Once")
				strRunRegPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce";
			else if (strRegPath == "Run Services")
				strRunRegPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices";
			else if (strRegPath == "Run Services Once")
				strRunRegPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce";

			if (string.IsNullOrEmpty(this.txtName.Text))
			{
				MessageBox.Show(this, Properties.Resources.smEmptyName, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if (string.IsNullOrEmpty(this.txtPath.Text))
			{
				MessageBox.Show(this, Properties.Resources.smEmptyPath, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			this.DialogResult = DialogResult.OK;

			string strFullPath = "";
			if (!string.IsNullOrEmpty(this.txtArguments.Text))
				strFullPath = string.Format("\"{0}\" {1}", this.txtPath.Text, this.txtArguments.Text);
			else
				strFullPath = string.Format("\"{0}\"", this.txtPath.Text);

			if (this.cmbSection.SelectedItem.ToString().Trim().StartsWith(@"Registry\All Users"))
			{
				regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(strRunRegPath, true);
				regKey.SetValue(this.txtName.Text, strFullPath, RegistryValueKind.String);
				regKey.Close();
			}
			else if (this.cmbSection.SelectedItem.ToString().Trim().StartsWith(@"Registry\Current User"))
			{
				regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(strRunRegPath, true);
				regKey.SetValue(this.txtName.Text, strFullPath, RegistryValueKind.String);
				regKey.Close();
			}
			else if (this.cmbSection.SelectedItem.ToString().Trim().StartsWith("StartUp"))
			{
				string strStartUpPath = "";

				if (this.cmbSection.SelectedItem.ToString().Trim() == @"StartUp\All Users")
					strStartUpPath = RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP);
				else if (this.cmbSection.SelectedItem.ToString().Trim() == @"StartUp\Current User")
					strStartUpPath = RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP);

				string strShortcutName = this.txtName.Text;
				if (!strShortcutName.EndsWith(".lnk"))
					strShortcutName += ".lnk";

				string strShortcutPath = System.IO.Path.Combine(strStartUpPath, strShortcutName);

				if (!RegistryHelper.CreateShortcut(strShortcutPath, this.txtPath.Text, this.txtArguments.Text))
					this.DialogResult = DialogResult.Cancel;
			}

			this.Close();
		}

		private void btnFileBrowse_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();

			ofd.Multiselect = false;
			ofd.CheckFileExists = true;
			ofd.Filter = "All files (*.*)|*.*";

			if (ofd.ShowDialog(this) == DialogResult.OK)
				this.txtPath.Text = ofd.FileName;
			this.DialogResult = DialogResult.None;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
	}
}