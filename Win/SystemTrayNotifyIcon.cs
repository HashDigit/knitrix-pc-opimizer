﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Win
{
	/// <summary>
	/// Summary description for SystemTrayNotificationEventType.
	/// </summary>
	public enum SystemTrayNotificationEventType { Hiding, Showing, StartingAnimation, StopingAnimation, IconChanged, Disposing };

	/// <summary>
	/// Summary description for SystemTrayNotificationEventArgs.
	/// </summary>
	public class SystemTrayNotificationEventArgs : EventArgs
	{
		private SystemTrayNotificationEventType state;

		public SystemTrayNotificationEventArgs(SystemTrayNotificationEventType state)
			: base()
		{
			this.state = state;
		}

		public SystemTrayNotificationEventType State
		{
			get
			{
				return state;
			}
		}
	}

	/// <summary>
	/// Summary description for SystemTrayNotifyIcon.
	/// </summary>
	public class SystemTrayNotifyIcon
	{
		private System.Windows.Forms.NotifyIcon notifyIcon = new NotifyIcon();	// NotifyIcon object
		private System.Windows.Forms.ContextMenuStrip BaseMenu;
		private System.Windows.Forms.Form mainForm;
		private System.Drawing.Icon[] iconArray;
		private Dictionary<string, System.Drawing.Icon> iconDictionary;
		private System.Drawing.Icon mainIcon;
		private int iconCounter = 0;
		private bool iconsLoaded = false;

		// Declaring Event 
		public delegate void StatusChanged(object sender, SystemTrayNotificationEventArgs e);
		public event StatusChanged OnStatusChanged;

		public bool IsDisposed
		{
			get
			{
				return _isDisposed;
			}
		}
		private bool _isDisposed = false;

		/// <summary>
		/// Property for showing and hiding icon
		/// </summary>
		public bool Visibility
		{
			get
			{
				return notifyIcon.Visible;
			}
			set
			{
				switch (value)
				{
					case true:
						if (notifyIcon.Visible == false)
							OnStatusChanged(this, new SystemTrayNotificationEventArgs(SystemTrayNotificationEventType.Showing));
						break;
					case false:
						if (notifyIcon.Visible == true)
							OnStatusChanged(this, new SystemTrayNotificationEventArgs(SystemTrayNotificationEventType.Hiding));
						break;
				}
				notifyIcon.Visible = value;
			}
		}

		/// <summary>
		/// Overloaded Constructor -- 4 --
		/// Icon = Application Icon (Default),
		/// Tooltip = Programmer must provide,
		/// Visibility = Programmer must provide,
		/// ContextMenuStrip = Programmer must provide.
		/// </summary>
		public SystemTrayNotifyIcon(System.Windows.Forms.Form form, bool visible, string toolTip, ContextMenuStrip contextMenu)
		{
			OnStatusChanged += new SystemTrayNotifyIcon.StatusChanged(SystemTrayNotificationHandler);	// Setting Event Handler
			mainForm = form;
			notifyIcon.Visible = visible;
			notifyIcon.Icon = mainForm.Icon;
			notifyIcon.Text = toolTip;
			notifyIcon.ContextMenuStrip = contextMenu;
		}

		/// <summary>
		/// Overloaded Constructor -- 5 --
		/// Icon = Programmer must provide,
		/// Tooltip = Programmer must provide,
		/// Visibility = Programmer must provide,
		/// ContextMenuStrip = Programmer must provide.
		/// </summary>
		public SystemTrayNotifyIcon(System.Windows.Forms.Form form, bool visible, string toolTip, Icon icon, ContextMenuStrip contextMenu)
		{
			OnStatusChanged += new SystemTrayNotifyIcon.StatusChanged(SystemTrayNotificationHandler);	// Setting Event Handler
			mainForm = form;
			notifyIcon.Visible = visible;
			if (icon.Size.Height > 16 || icon.Size.Width > 16)
				notifyIcon.Icon = mainForm.Icon;
			else
				notifyIcon.Icon = icon;
			notifyIcon.Text = toolTip;
			notifyIcon.ContextMenuStrip = contextMenu;
		}

		public void Dispose()
		{
			_isDisposed = true;
			OnStatusChanged(this, new SystemTrayNotificationEventArgs(SystemTrayNotificationEventType.Disposing));
			if (BaseMenu != null)
				BaseMenu.Dispose();
			notifyIcon.Dispose();
		}
		/// <summary>
		/// Destructor
		/// </summary>
		~SystemTrayNotifyIcon()
		{
		}

		/// <summary>
		/// Loads default icons of size 16x16 for each application state,
		/// </summary>
		public void InitIconDictionary(Dictionary<string, Icon> icondictionary)
		{
			iconDictionary = icondictionary;
		}

		/// <summary>
		/// Set Icon showing in System Tray
		/// </summary>
		public void SetIcon(string key)
		{
			notifyIcon.Icon = iconDictionary[key];
		}

		/// <summary>
		/// Default Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void SystemTrayNotificationHandler(object sender, SystemTrayNotificationEventArgs e)
		{
			// This event handler is not required here exactly
			// It just facilitates the programmer to save him/her from
			// exceptions if he/she don't provides his/her own EventHandler
			// function
		}
	}
}
