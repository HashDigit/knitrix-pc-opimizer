﻿namespace Win.ScanSchedule
{
	partial class ScanScheduleEditForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanScheduleEditForm));
			this.teStartTime = new DevExpress.XtraEditors.TimeEdit();
			this.lblStartTime = new System.Windows.Forms.Label();
			this.lblScheduledOn = new System.Windows.Forms.Label();
			this.cbSunday = new System.Windows.Forms.CheckBox();
			this.cbMonday = new System.Windows.Forms.CheckBox();
			this.cbTuesday = new System.Windows.Forms.CheckBox();
			this.cbWednesday = new System.Windows.Forms.CheckBox();
			this.cbThursday = new System.Windows.Forms.CheckBox();
			this.cbFriday = new System.Windows.Forms.CheckBox();
			this.cbSaturday = new System.Windows.Forms.CheckBox();
			this.btnOk = new Win.Controls.RegularButton();
			this.btnCancel = new Win.Controls.RegularButton();
			((System.ComponentModel.ISupportInitialize)(this.teStartTime.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// teStartTime
			// 
			this.teStartTime.EditValue = new System.DateTime(2010, 9, 29, 0, 0, 0, 0);
			this.teStartTime.Location = new System.Drawing.Point(91, 11);
			this.teStartTime.Name = "teStartTime";
			this.teStartTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.teStartTime.Properties.Mask.EditMask = "HH:mm";
			this.teStartTime.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.teStartTime.Size = new System.Drawing.Size(100, 20);
			this.teStartTime.TabIndex = 0;
			// 
			// lblStartTime
			// 
			this.lblStartTime.AutoSize = true;
			this.lblStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblStartTime.Location = new System.Drawing.Point(12, 15);
			this.lblStartTime.Name = "lblStartTime";
			this.lblStartTime.Size = new System.Drawing.Size(58, 13);
			this.lblStartTime.TabIndex = 1;
			this.lblStartTime.Text = "Start time:";
			// 
			// lblScheduledOn
			// 
			this.lblScheduledOn.AutoSize = true;
			this.lblScheduledOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblScheduledOn.Location = new System.Drawing.Point(12, 45);
			this.lblScheduledOn.Name = "lblScheduledOn";
			this.lblScheduledOn.Size = new System.Drawing.Size(77, 13);
			this.lblScheduledOn.TabIndex = 2;
			this.lblScheduledOn.Text = "Scheduled On:";
			// 
			// cbSunday
			// 
			this.cbSunday.AutoSize = true;
			this.cbSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbSunday.Location = new System.Drawing.Point(95, 43);
			this.cbSunday.Name = "cbSunday";
			this.cbSunday.Size = new System.Drawing.Size(62, 17);
			this.cbSunday.TabIndex = 3;
			this.cbSunday.Text = "Sunday";
			this.cbSunday.UseVisualStyleBackColor = true;
			// 
			// cbMonday
			// 
			this.cbMonday.AutoSize = true;
			this.cbMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbMonday.Location = new System.Drawing.Point(95, 66);
			this.cbMonday.Name = "cbMonday";
			this.cbMonday.Size = new System.Drawing.Size(64, 17);
			this.cbMonday.TabIndex = 4;
			this.cbMonday.Text = "Monday";
			this.cbMonday.UseVisualStyleBackColor = true;
			// 
			// cbTuesday
			// 
			this.cbTuesday.AutoSize = true;
			this.cbTuesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbTuesday.Location = new System.Drawing.Point(95, 89);
			this.cbTuesday.Name = "cbTuesday";
			this.cbTuesday.Size = new System.Drawing.Size(67, 17);
			this.cbTuesday.TabIndex = 5;
			this.cbTuesday.Text = "Tuesday";
			this.cbTuesday.UseVisualStyleBackColor = true;
			// 
			// cbWednesday
			// 
			this.cbWednesday.AutoSize = true;
			this.cbWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbWednesday.Location = new System.Drawing.Point(95, 112);
			this.cbWednesday.Name = "cbWednesday";
			this.cbWednesday.Size = new System.Drawing.Size(83, 17);
			this.cbWednesday.TabIndex = 6;
			this.cbWednesday.Text = "Wednesday";
			this.cbWednesday.UseVisualStyleBackColor = true;
			// 
			// cbThursday
			// 
			this.cbThursday.AutoSize = true;
			this.cbThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbThursday.Location = new System.Drawing.Point(95, 135);
			this.cbThursday.Name = "cbThursday";
			this.cbThursday.Size = new System.Drawing.Size(71, 17);
			this.cbThursday.TabIndex = 7;
			this.cbThursday.Text = "Thursday";
			this.cbThursday.UseVisualStyleBackColor = true;
			// 
			// cbFriday
			// 
			this.cbFriday.AutoSize = true;
			this.cbFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbFriday.Location = new System.Drawing.Point(95, 158);
			this.cbFriday.Name = "cbFriday";
			this.cbFriday.Size = new System.Drawing.Size(56, 17);
			this.cbFriday.TabIndex = 8;
			this.cbFriday.Text = "Friday";
			this.cbFriday.UseVisualStyleBackColor = true;
			// 
			// cbSaturday
			// 
			this.cbSaturday.AutoSize = true;
			this.cbSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbSaturday.Location = new System.Drawing.Point(95, 181);
			this.cbSaturday.Name = "cbSaturday";
			this.cbSaturday.Size = new System.Drawing.Size(70, 17);
			this.cbSaturday.TabIndex = 9;
			this.cbSaturday.Text = "Saturday";
			this.cbSaturday.UseVisualStyleBackColor = true;
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOk.BackColor = System.Drawing.Color.Transparent;
			this.btnOk.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnOk.ForeColor = System.Drawing.Color.White;
			this.btnOk.Location = new System.Drawing.Point(12, 205);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(120, 27);
			this.btnOk.TabIndex = 92;
			this.btnOk.Text = "OK";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(138, 205);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(84, 27);
			this.btnCancel.TabIndex = 91;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// ScanScheduleEditForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(234, 244);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cbSaturday);
			this.Controls.Add(this.cbFriday);
			this.Controls.Add(this.cbThursday);
			this.Controls.Add(this.cbWednesday);
			this.Controls.Add(this.cbTuesday);
			this.Controls.Add(this.cbMonday);
			this.Controls.Add(this.cbSunday);
			this.Controls.Add(this.lblScheduledOn);
			this.Controls.Add(this.lblStartTime);
			this.Controls.Add(this.teStartTime);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScanScheduleEditForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Schedule Options";
			this.Load += new System.EventHandler(this.ScanScheduleEditForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.teStartTime.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.TimeEdit teStartTime;
		private System.Windows.Forms.Label lblStartTime;
		private System.Windows.Forms.Label lblScheduledOn;
		private System.Windows.Forms.CheckBox cbSunday;
		private System.Windows.Forms.CheckBox cbMonday;
		private System.Windows.Forms.CheckBox cbTuesday;
		private System.Windows.Forms.CheckBox cbWednesday;
		private System.Windows.Forms.CheckBox cbThursday;
		private System.Windows.Forms.CheckBox cbFriday;
		private System.Windows.Forms.CheckBox cbSaturday;
		private Controls.RegularButton btnOk;
		private Controls.RegularButton btnCancel;
	}
}