﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Business.TaskScheduler;
using DevExpress.XtraEditors;

namespace Win.ScanSchedule
{
	public partial class ScanScheduleEditForm : DevExpress.XtraEditors.XtraForm
	{
		/// <summary>
		/// Triggers with the same time (but different day of week)
		/// </summary>
		public TriggerList Triggers { get; set; }

		public ScanScheduleEditForm(TriggerList triggers)
		{
			InitializeComponent();

			Triggers = triggers;
		}

		private void ScanScheduleEditForm_Load(object sender, EventArgs e)
		{
			if (Triggers.Count < 1)
				return;

			var trigger = (WeeklyTrigger)Triggers[0];
			teStartTime.EditValue = new DateTime(DateTime.Now.Year, 1, 1, trigger.StartHour, trigger.StartMinute, 0);

			foreach (WeeklyTrigger tr in Triggers)
			{
				switch (tr.WeekDays)
				{
					case DaysOfTheWeek.Sunday:
						cbSunday.Checked = true;
						break;
					case DaysOfTheWeek.Monday:
						cbMonday.Checked = true;
						break;
					case DaysOfTheWeek.Tuesday:
						cbTuesday.Checked = true;
						break;
					case DaysOfTheWeek.Wednesday:
						cbWednesday.Checked = true;
						break;
					case DaysOfTheWeek.Thursday:
						cbThursday.Checked = true;
						break;
					case DaysOfTheWeek.Friday:
						cbFriday.Checked = true;
						break;
					case DaysOfTheWeek.Saturday:
						cbSaturday.Checked = true;
						break;
					default:
						break;
				}
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			// recreate Triggers
			Triggers.Clear();
			var time = (DateTime)teStartTime.EditValue;
			if (cbSunday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Sunday));
			if (cbMonday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Monday));
			if (cbTuesday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Tuesday));
			if (cbWednesday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Wednesday));
			if (cbThursday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Thursday));
			if (cbFriday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Friday));
			if (cbSaturday.Checked)
				Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Saturday));

			if (Triggers.Count < 1)
			{
				MessageBox.Show(this, "Please select days of week.", Application.ProductName);
				return;
			}

			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}