﻿namespace Win.ShortcutFixer
{
	partial class ScanNowForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("");
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("");
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("");
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanNowForm));
			this.rbStartMenuOnly = new System.Windows.Forms.RadioButton();
			this.rbDrives = new System.Windows.Forms.RadioButton();
			this.cbIncludeEmptyFolder = new System.Windows.Forms.CheckBox();
			this.lvDrives = new System.Windows.Forms.ListView();
			this.ilDrives = new System.Windows.Forms.ImageList(this.components);
			this.lblStatus = new System.Windows.Forms.Label();
			this.tmrDiskShortcutFixer = new System.Windows.Forms.Timer(this.components);
			this.lblTotal = new System.Windows.Forms.Label();
			this.bntScanNow = new Win.Controls.RegularButton();
			this.btnCancel = new Win.Controls.RegularButton();
			this.pbDiskShortcutFixer = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// rbStartMenuOnly
			// 
			this.rbStartMenuOnly.AutoSize = true;
			this.rbStartMenuOnly.Checked = true;
			this.rbStartMenuOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.rbStartMenuOnly.Location = new System.Drawing.Point(12, 12);
			this.rbStartMenuOnly.Name = "rbStartMenuOnly";
			this.rbStartMenuOnly.Size = new System.Drawing.Size(191, 17);
			this.rbStartMenuOnly.TabIndex = 0;
			this.rbStartMenuOnly.TabStop = true;
			this.rbStartMenuOnly.Text = "Only scan Start Menu and Desktop";
			this.rbStartMenuOnly.UseVisualStyleBackColor = true;
			this.rbStartMenuOnly.CheckedChanged += new System.EventHandler(this.rbStartMenuOnly_CheckedChanged);
			// 
			// rbDrives
			// 
			this.rbDrives.AutoSize = true;
			this.rbDrives.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.rbDrives.Location = new System.Drawing.Point(12, 78);
			this.rbDrives.Name = "rbDrives";
			this.rbDrives.Size = new System.Drawing.Size(240, 17);
			this.rbDrives.TabIndex = 1;
			this.rbDrives.Text = "Check the boxes of drive(s) you with to scan";
			this.rbDrives.UseVisualStyleBackColor = true;
			// 
			// cbIncludeEmptyFolder
			// 
			this.cbIncludeEmptyFolder.AutoSize = true;
			this.cbIncludeEmptyFolder.Checked = true;
			this.cbIncludeEmptyFolder.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbIncludeEmptyFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbIncludeEmptyFolder.Location = new System.Drawing.Point(22, 35);
			this.cbIncludeEmptyFolder.Name = "cbIncludeEmptyFolder";
			this.cbIncludeEmptyFolder.Size = new System.Drawing.Size(186, 17);
			this.cbIncludeEmptyFolder.TabIndex = 2;
			this.cbIncludeEmptyFolder.Text = "Scan for empty Start Menu folder";
			this.cbIncludeEmptyFolder.UseVisualStyleBackColor = true;
			// 
			// lvDrives
			// 
			this.lvDrives.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lvDrives.CheckBoxes = true;
			this.lvDrives.Enabled = false;
			listViewItem1.StateImageIndex = 0;
			listViewItem2.StateImageIndex = 0;
			listViewItem3.StateImageIndex = 0;
			listViewItem4.StateImageIndex = 0;
			this.lvDrives.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4});
			this.lvDrives.Location = new System.Drawing.Point(22, 101);
			this.lvDrives.Name = "lvDrives";
			this.lvDrives.Size = new System.Drawing.Size(223, 131);
			this.lvDrives.SmallImageList = this.ilDrives;
			this.lvDrives.TabIndex = 6;
			this.lvDrives.UseCompatibleStateImageBehavior = false;
			this.lvDrives.View = System.Windows.Forms.View.SmallIcon;
			// 
			// ilDrives
			// 
			this.ilDrives.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilDrives.ImageStream")));
			this.ilDrives.TransparentColor = System.Drawing.Color.Transparent;
			this.ilDrives.Images.SetKeyName(0, "drive_16x16.png");
			// 
			// lblStatus
			// 
			this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lblStatus.Location = new System.Drawing.Point(9, 279);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(249, 41);
			this.lblStatus.TabIndex = 28;
			this.lblStatus.Tag = "";
			this.lblStatus.Text = "Status";
			// 
			// tmrDiskShortcutFixer
			// 
			this.tmrDiskShortcutFixer.Tick += new System.EventHandler(this.tmrDiskShortcutFixer_Tick);
			// 
			// lblTotal
			// 
			this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lblTotal.AutoSize = true;
			this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblTotal.Location = new System.Drawing.Point(9, 235);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(31, 13);
			this.lblTotal.TabIndex = 31;
			this.lblTotal.Tag = "Found {0} dead shortcuts and {1} empty folders.";
			this.lblTotal.Text = "Total";
			// 
			// bntScanNow
			// 
			this.bntScanNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bntScanNow.BackColor = System.Drawing.Color.Transparent;
			this.bntScanNow.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.bntScanNow.ForeColor = System.Drawing.Color.White;
			this.bntScanNow.Location = new System.Drawing.Point(48, 325);
			this.bntScanNow.Name = "bntScanNow";
			this.bntScanNow.Size = new System.Drawing.Size(120, 27);
			this.bntScanNow.TabIndex = 94;
			this.bntScanNow.Text = "Scan Now";
			this.bntScanNow.Click += new System.EventHandler(this.bntScanNow_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(174, 325);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(84, 27);
			this.btnCancel.TabIndex = 93;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// pbDiskShortcutFixer
			// 
			this.pbDiskShortcutFixer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pbDiskShortcutFixer.Location = new System.Drawing.Point(9, 251);
			this.pbDiskShortcutFixer.Name = "pbDiskShortcutFixer";
			this.pbDiskShortcutFixer.Size = new System.Drawing.Size(249, 24);
			this.pbDiskShortcutFixer.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.pbDiskShortcutFixer.TabIndex = 95;
			this.pbDiskShortcutFixer.Visible = false;
			// 
			// ScanNowForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(270, 364);
			this.Controls.Add(this.bntScanNow);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.lblTotal);
			this.Controls.Add(this.lblStatus);
			this.Controls.Add(this.lvDrives);
			this.Controls.Add(this.cbIncludeEmptyFolder);
			this.Controls.Add(this.rbDrives);
			this.Controls.Add(this.rbStartMenuOnly);
			this.Controls.Add(this.pbDiskShortcutFixer);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScanNowForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Scan Now";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScanNowForm_FormClosing);
			this.Load += new System.EventHandler(this.ScanNowForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RadioButton rbStartMenuOnly;
		private System.Windows.Forms.RadioButton rbDrives;
		private System.Windows.Forms.CheckBox cbIncludeEmptyFolder;
		private System.Windows.Forms.ListView lvDrives;
		private System.Windows.Forms.ImageList ilDrives;
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Timer tmrDiskShortcutFixer;
		private System.Windows.Forms.Label lblTotal;
		private Controls.RegularButton bntScanNow;
		private Controls.RegularButton btnCancel;
		private System.Windows.Forms.ProgressBar pbDiskShortcutFixer;
	}
}