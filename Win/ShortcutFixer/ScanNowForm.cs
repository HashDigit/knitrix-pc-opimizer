﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Business;
using DevExpress.XtraEditors;

namespace Win.ShortcutFixer
{
	public partial class ScanNowForm : DevExpress.XtraEditors.XtraForm
	{
		private bool _isScanStarted = false;

		public ScanNowForm()
		{
			InitializeComponent();
		}

		private void ScanNowForm_Load(object sender, EventArgs e)
		{
			lblStatus.Text = "";
			lblTotal.Text = "";

			// fill clbDrives with drives
			var driveinfos = DriveInfo.GetDrives();
			lvDrives.Items.Clear();
			foreach (var driveInfo in driveinfos)
			{
				if (driveInfo.IsReady)
				{
					ListViewItem li;
					if (string.IsNullOrEmpty(driveInfo.VolumeLabel))
						li = new ListViewItem(driveInfo.Name);
					else
						li = new ListViewItem(string.Format("{0} ({1})", driveInfo.Name, driveInfo.VolumeLabel));

					li.Checked = true;
					li.Name = li.Text;
					li.ImageIndex = 0;
					li.Tag = driveInfo.RootDirectory.FullName;
					lvDrives.Items.Add(li);
				}
			}

			ShortcutFixerLogic.ScanFinished += new EventHandler(ShortcutFixerLogic_ScanFinished);
			ShortcutFixerLogic.ScanAborted += new EventHandler(ShortcutFixerLogic_ScanAborted);
		}

		private void ShortcutFixerLogic_ScanAborted(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { ShortcutFixerLogic_ScanAborted(sender, e); }));
			}
			else
			{
				// nothing
			}
		}

		private void ShortcutFixerLogic_ScanFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { ShortcutFixerLogic_ScanFinished(sender, e); }));
			}
			else
			{
				tmrDiskShortcutFixer.Enabled = false;
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void bntScanNow_Click(object sender, EventArgs e)
		{
			if (ShortcutFixerLogic.IsScanning)
				return;
			bntScanNow.Enabled = false;
			_isScanStarted = true;
			pbDiskShortcutFixer.Visible = true;

			// set ShortcutFixerLogic settings
			ShortcutFixerLogic.ScanStartMenuAndDesktopOnly = rbStartMenuOnly.Checked;
			ShortcutFixerLogic.ScanEmptyStartMenuDirectory = cbIncludeEmptyFolder.Checked;
			ShortcutFixerLogic.DrivesToScan = new List<string>();
			foreach (ListViewItem li in lvDrives.Items)
				if (li.Checked)
					ShortcutFixerLogic.DrivesToScan.Add(li.Tag.ToString());

			tmrDiskShortcutFixer.Enabled = true;
			ShortcutFixerLogic.StartScan();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void rbStartMenuOnly_CheckedChanged(object sender, EventArgs e)
		{
			cbIncludeEmptyFolder.Enabled = rbStartMenuOnly.Checked;
			lvDrives.Enabled = !rbStartMenuOnly.Checked;
		}

		private void tmrDiskShortcutFixer_Tick(object sender, EventArgs e)
		{
			lblStatus.Text = ShortcutFixerLogic.CurrentScanningPath;
			lblTotal.Text = string.Format(lblTotal.Tag.ToString(), ShortcutFixerLogic.ShortcutFiles.Count, ShortcutFixerLogic.EmptyDirectories.Count);
		}

		private void ScanNowForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.DialogResult != DialogResult.OK && e.CloseReason == CloseReason.UserClosing)
			{
				ShortcutFixerLogic.AbortScan();
			}
			tmrDiskShortcutFixer.Enabled = false;
			this.DialogResult = _isScanStarted ? DialogResult.OK : DialogResult.Cancel;
		}
	}
}