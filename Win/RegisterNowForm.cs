﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Win
{
	public partial class RegisterNowForm : DevExpress.XtraEditors.XtraForm
	{
		private int _issueCount;
		private RegisterNowState _state;

		public RegisterNowForm(int issueCount, RegisterNowState state)
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;

			_issueCount = issueCount;
			_state = state;
		}

		public RegisterNowForm(RegisterNowState state)
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;
            
			_state = state;
		}

		public RegisterNowForm()
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;
		}

		private void RegisterNowForm_Load(object sender, EventArgs e)
		{
			switch (_state)
			{
				case RegisterNowState.FixAll:
					lblTitle.Text = "Registry Cleaner has determined that your PC needs to be optimized.";
					if (_issueCount == 0)
						lblDescription.Text = "";
					else
						lblDescription.Text = "To fix the XXX selected items, you will need to register the software.\r\nFull scan feature is available in Pro version only. Please click the 'Registry Cleaner' button to clean your computer's registry.".Replace("XXX", _issueCount.ToString());
					this.btnRegisterNow.ImageDisabled = global::Win.Properties.Resources.upgrade_to_pro_inactive;
					this.btnRegisterNow.ImageNormal = global::Win.Properties.Resources.upgrade_to_pro_normal;
					this.btnRegisterNow.ImageRollover = global::Win.Properties.Resources.upgrade_to_pro_rollover;
					break;
				case RegisterNowState.Register:
					lblTitle.Text = "This feature is available in registered version only.";
					lblDescription.Text = "";
					this.btnRegisterNow.ImageDisabled = global::Win.Properties.Resources.register_now_inactive1;
					this.btnRegisterNow.ImageNormal = global::Win.Properties.Resources.register_now_normal1;
					this.btnRegisterNow.ImageRollover = global::Win.Properties.Resources.register_now_rollover1;
					break;
			}
		}

		private void btnRegisterNow_Click(object sender, EventArgs e)
		{
			Controller.GetMainForm().InfoAction();
			Controller.GetMainForm().BuyNowAction();
			/*
			switch (_state)
			{
				case RegisterNowState.FixAll:
					Controller.GetMainForm().UpgradeToProAction();
					break;
				case RegisterNowState.Register:
					Controller.GetMainForm().BuyNowAction();
					break;
			}
			*/
			Close();
		}
	}

	public enum RegisterNowState
	{
		FixAll,
		Register,
	}

}