using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace Win
{
	public class Controller
	{
		private static Dictionary<Type, Form> _forms;

		static Controller()
		{
			_forms = new Dictionary<Type, Form>();
		}

		/// <summary>
		/// Show single instance of any form
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public static void ShowForm<T>() where T : Form
		{
			if (_forms.ContainsKey(typeof(T)) && !_forms[typeof(T)].IsDisposed)
			{
				// bring form to front
				_forms[typeof(T)].Show();
				_forms[typeof(T)].WindowState = FormWindowState.Normal;
				_forms[typeof(T)].Activate();
			}
			else
			{
				// remove disposed form from collection
				if (_forms.ContainsKey(typeof(T)))
					_forms.Remove(typeof(T));

				// create new form
				var form = (T)Assembly.GetCallingAssembly().CreateInstance(typeof(T).FullName);
				if (form != null && !form.IsDisposed)
				{
					_forms.Add(typeof (T), form);
					form.Show();
				}
			}
		}

		/// <summary>
		/// Show instance of any form
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public static Form ShowForm(Form form)
		{
			if (form == null || form.IsDisposed)
				return null;

			// bring form to front
			form.Show();
			form.WindowState = FormWindowState.Normal;
			form.Activate();
			return form;
		}

		public static MainForm GetMainForm()
		{
			return GetInstance<MainForm>();
		}

		/// <summary>
		/// Set ShowInTaskbar property for all open forms
		/// </summary>
		/// <param name="showInTaskbar"></param>
		public static void SetShowInTaskbar(bool showInTaskbar)
		{
			foreach (var form in _forms)
			{
				if (!form.Value.IsDisposed)
					form.Value.ShowInTaskbar = showInTaskbar;
			}
		}

		private static T GetInstance<T>() where T : Form
		{
			if (!_forms.ContainsKey(typeof(T)) || _forms[typeof(T)].IsDisposed)
			{
				// create new form
				var form = (T)Assembly.GetCallingAssembly().CreateInstance(typeof(T).FullName);
				_forms.Add(typeof(T), form);
			}
			return (T)_forms[typeof(T)];
		}
	}
}