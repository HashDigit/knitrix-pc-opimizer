﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Common;
using Data;
using System.Web.Mail;

using DevExpress.XtraEditors;

namespace Win
{
	public partial class FixAllWarningForm : DevExpress.XtraEditors.XtraForm
	{
		public FixAllWarningForm()
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;
			//btnCancel.Select();
			//btnCancel.Focus();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		private void btnSend_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}
	}
}