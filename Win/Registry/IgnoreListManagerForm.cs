﻿using System;
using System.Windows.Forms;
using Data;
using Data.Registry;

namespace Win.Registry
{
	public partial class IgnoreListManagerForm : Form
	{
		private RegistryExcludeArray _registryExcludeArray = new RegistryExcludeArray();

		public RegistryExcludeArray RegistryExcludeArray
		{
			get { return _registryExcludeArray; }
		}

		public IgnoreListManagerForm()
		{
			InitializeComponent();
		}

		private void IgnoreListManagerForm_Load(object sender, EventArgs e)
        {
			/*
            // Load settings
            this.listViewOptions.Items[0].Checked = Data.Options.Instance.Registry_Log;
            this.listViewOptions.Items[1].Checked = Data.Options.Instance.Registry_Rescan;
            this.listViewOptions.Items[2].Checked = Data.Options.Instance.Registry_AutoUpdate;
            this.listViewOptions.Items[3].Checked = Data.Options.Instance.Registry_Restore;
            this.listViewOptions.Items[4].Checked = Data.Options.Instance.Registry_ShowLog;
            this.listViewOptions.Items[5].Checked = Data.Options.Instance.Registry_DelBackup;
            this.listViewOptions.Items[6].Checked = Data.Options.Instance.Registry_RemMedia;
            this.listViewOptions.Items[7].Checked = Data.Options.Instance.Registry_AutoRepair;
            this.listViewOptions.Items[8].Checked = Data.Options.Instance.Registry_AutoExit; 

            // Load backup directorys
            this.textBoxBackupFolder.Text = Properties.Settings.Default.strOptionsBackupDir;
            this.textBoxLogFolder.Text = Properties.Settings.Default.strOptionsLogDir;
			*/

            // Load exclude list from settings
               this._registryExcludeArray = new RegistryExcludeArray(Options.Instance.RegistryExcludeArray);
			
            PopulateExcludeList();
        }

		private void btnOK_Click(object sender, EventArgs e)
		{
			/*
            // Update settings
            Data.Options.Instance.Registry_Log = this.listViewOptions.Items[0].Checked;
            Data.Options.Instance.Registry_Rescan = this.listViewOptions.Items[1].Checked;
            Data.Options.Instance.Registry_AutoUpdate = this.listViewOptions.Items[2].Checked;
            Data.Options.Instance.Registry_Restore = this.listViewOptions.Items[3].Checked;
            Data.Options.Instance.Registry_ShowLog = this.listViewOptions.Items[4].Checked;
            Data.Options.Instance.Registry_DelBackup = this.listViewOptions.Items[5].Checked;
            Data.Options.Instance.Registry_RemMedia = this.listViewOptions.Items[6].Checked;
            Data.Options.Instance.Registry_AutoRepair = this.listViewOptions.Items[7].Checked;
            Data.Options.Instance.Registry_AutoExit = this.listViewOptions.Items[8].Checked;

            if (this.textBoxBackupFolder.Text != Properties.Settings.Default.strOptionsBackupDir)
                Properties.Settings.Default.strOptionsBackupDir = this.textBoxBackupFolder.Text;

            if (this.textBoxLogFolder.Text != Properties.Settings.Default.strOptionsLogDir)
                Properties.Settings.Default.strOptionsLogDir = this.textBoxLogFolder.Text;
			*/
            Options.Instance.RegistryExcludeArray = this._registryExcludeArray;
			Options.Instance.Save();
			
			this.Close();
		}
		#region Exclude List
		private void addRegistryPathToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AddRegistryPathForm addRegistryPath = new AddRegistryPathForm();
			if (addRegistryPath.ShowDialog(this) == DialogResult.OK)
			{
				RegistryExcludeArray.Add(new RegistryExcludeItem(addRegistryPath.RegistryPath, null, null));
				PopulateExcludeList();
			}
		}

		private void addFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.Title = "Select a file to exclude from the registry scan";
				openFileDialog.Filter = "All files (*.*)|*.*";
				openFileDialog.FilterIndex = 1;
				openFileDialog.RestoreDirectory = true;

				if (openFileDialog.ShowDialog(this) == DialogResult.OK)
				{
					RegistryExcludeArray.Add(new RegistryExcludeItem(null, null, openFileDialog.FileName));
					PopulateExcludeList();
				}
			}
		}

		private void addFolderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
			{
				folderBrowserDialog.Description = "Select the folder to exclude from the registry scan";

				if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
				{
					RegistryExcludeArray.Add(new RegistryExcludeItem(null, folderBrowserDialog.SelectedPath, null));
					PopulateExcludeList();
				}
			}
		}

		private void removeEntryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.listView1.SelectedIndices.Count > 0 && this.listView1.Items.Count > 0)
			{
				if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					foreach (RegistryExcludeItem i in RegistryExcludeArray)
					{
						if (i.ToString() == this.listView1.SelectedItems[0].Text)
						{
							RegistryExcludeArray.Remove(i);
							break;
						}
					}

					PopulateExcludeList();
				}
			}
		}

		private void PopulateExcludeList()
		{
			this.listView1.Items.Clear();

			foreach (RegistryExcludeItem item in RegistryExcludeArray)
				this.listView1.Items.Add(item.ToString());

			this.listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
		}
		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
