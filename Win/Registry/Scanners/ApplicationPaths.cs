﻿using System;
using Common;
using Business.Registry.Misc;
using Microsoft.Win32;

namespace Win.Registry.Scanners
{
    public class ApplicationPaths : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.ApplicationPaths; }
        }

        /// <summary>
        /// Verifies programs in App Paths
        /// </summary>
        public static void Scan()
        {
            try
            {

                // Main.Logger.WriteLine("Checking for invalid installer folders");
                ScanInstallFolders();

                // Main.Logger.WriteLine("Checking for invalid application paths");
                ScanAppPaths();
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private static void ScanInstallFolders()
        {
            RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\Folders");

            if (regKey == null)
                return;

            foreach (string strFolder in regKey.GetValueNames())
            {
                ScanRegistryDlg.CurrentScannedObject = strFolder;

				if (!RegistryHelper.DirExists(strFolder))
                    ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strFolder);
            }
        }

        private static void ScanAppPaths()
        {
            RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\App Paths");

            if (regKey == null)
                return;

            foreach (string strSubKey in regKey.GetSubKeyNames())
            {
                RegistryKey regKey2 = regKey.OpenSubKey(strSubKey);

                if (regKey2 != null)
                {
                    ScanRegistryDlg.CurrentScannedObject = regKey2.ToString();

                    if (Convert.ToInt32(regKey2.GetValue("BlockOnTSNonInstallMode")) == 1)
                        continue;

                    string strAppPath = regKey2.GetValue("") as string;
                    string strAppDir = regKey2.GetValue("Path") as string;

                    if (string.IsNullOrEmpty(strAppPath))
                    {
                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidRegKey, regKey2.ToString());
                        continue;
                    }

                    if (!string.IsNullOrEmpty(strAppDir))
                    {
						if (RegistryHelper.SearchPath(strAppPath, strAppDir))
                            continue;
						else if (RegistryHelper.SearchPath(strSubKey, strAppDir))
                            continue;
                    }
                    else
                    {
						if (RegistryHelper.FileExists(strAppPath))
                            continue;
                        else if (RegistryHelper.FileExists(strAppPath))
                            continue;
                    }

                    ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey2.Name);
                }
            }

            regKey.Close();
        }
    }
}
