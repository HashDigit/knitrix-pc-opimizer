﻿using Business.Registry.Misc;
using Microsoft.Win32;
using Common;

namespace Win.Registry.Scanners
{
    public class ApplicationSettings : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.ApplicationSettings; }
        }

        public static void Scan()
        {
            try
            {
				ScanRegistryKey(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE"));
                ScanRegistryKey(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE"));

                if (RegistryHelper.Is64BitOS)
                {
                    ScanRegistryKey(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node"));
                    ScanRegistryKey(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node"));
                }
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private static void ScanRegistryKey(RegistryKey baseRegKey)
        {
            if (baseRegKey == null)
                return;

            // Main.Logger.WriteLine("Scanning " + baseRegKey.Name + " for empty registry keys");

            foreach (string strSubKey in baseRegKey.GetSubKeyNames())
            {
                // Skip needed keys, we dont want to mess the system up
                //if (strSubKey == "Microsoft" ||
                //    strSubKey == "Policies" ||
                //    strSubKey == "Classes" ||
                //    strSubKey == "Printers" ||
                //    strSubKey == "Wow6432Node")
                //    continue;

                if (IsEmptyRegistryKey(baseRegKey.OpenSubKey(strSubKey, true)))
                    ScanRegistryDlg.StoreInvalidKey(Strings.NoRegKey, baseRegKey.Name + "\\" + strSubKey);
            }

            baseRegKey.Close();
            return;
        }

        /// <summary>
        /// Recursively goes through the registry keys and finds how many values there are
        /// </summary>
        /// <param name="regKey">The base registry key</param>
        /// <returns>True if the registry key is emtpy</returns>
        private static bool IsEmptyRegistryKey(RegistryKey regKey)
        {
            if (regKey == null)
                return false;

            ScanRegistryDlg.CurrentScannedObject = regKey.ToString();

            int nValueCount = regKey.ValueCount;
            int nSubKeyCount = regKey.SubKeyCount;

            if (regKey.ValueCount == 0)
                if (regKey.GetValue("") != null)
                    nValueCount = 1;

            return (nValueCount == 0 && nSubKeyCount == 0);
        }
    }
}
