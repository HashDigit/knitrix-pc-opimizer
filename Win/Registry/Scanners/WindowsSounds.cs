﻿using Common;
using Business.Registry.Misc;
using Microsoft.Win32;

namespace Win.Registry.Scanners
{
    public class WindowsSounds : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.WindowsSounds; }
        }

        public static void Scan()
        {
            try
            {
                using (RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("AppEvents\\Schemes\\Apps"))
                {
                    if (regKey != null)
                    {
                        // Main.Logger.WriteLine("Scanning for missing sound events");
                        ParseSoundKeys(regKey);
                    }
                }
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Goes deep into sub keys to see if files exist
        /// </summary>
        /// <param name="rk">Registry subkey</param>
        private static void ParseSoundKeys(RegistryKey rk)
        {
            foreach (string strSubKey in rk.GetSubKeyNames())
            {

                // Ignores ".Default" Subkey
                if ((strSubKey.CompareTo(".Current") == 0) || (strSubKey.CompareTo(".Modified") == 0))
                {
                    // Gets the (default) key and sees if the file exists
                    RegistryKey rk2 = rk.OpenSubKey(strSubKey);

                    if (rk2 != null)
                    {
                        ScanRegistryDlg.CurrentScannedObject = rk2.ToString();

                        string strSoundPath = rk2.GetValue("") as string;

                        if (!string.IsNullOrEmpty(strSoundPath))
							if (!RegistryHelper.FileExists(strSoundPath))
                                ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, rk2.Name, "(default)");
                    }

                }
                else if (!string.IsNullOrEmpty(strSubKey))
                {
                    RegistryKey rk2 = rk.OpenSubKey(strSubKey);
                    if (rk2 != null)
                    {
                        ScanRegistryDlg.CurrentScannedObject = rk2.ToString();
                        ParseSoundKeys(rk2);
                    }
                }

            }

            return;
        }
    }
}
