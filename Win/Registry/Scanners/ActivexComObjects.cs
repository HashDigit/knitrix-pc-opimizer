﻿using System.Collections.Generic;
using Common;
using Business.Registry.Misc;
using Microsoft.Win32;

namespace Win.Registry.Scanners
{
    public class ActivexComObjects : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.ActivexComObjects; }
        }

        /// <summary>
        /// Scans ActiveX/COM Objects
        /// </summary>
        public static void Scan()
        {
            try
            {
                // Scan all CLSID sub keys
                ScanCLSIDSubKey(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("CLSID"));
                ScanCLSIDSubKey(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Classes\\CLSID"));
                ScanCLSIDSubKey(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Classes\\CLSID"));
				if (RegistryHelper.Is64BitOS)
                {
                    ScanCLSIDSubKey(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node\\CLSID"));
                    ScanCLSIDSubKey(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Classes\\CLSID"));
                    ScanCLSIDSubKey(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Classes\\CLSID"));
                }

                // Scan file extensions + progids
                ScanClasses(Microsoft.Win32.Registry.ClassesRoot);
                ScanClasses(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Classes"));
                ScanClasses(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Classes"));
				if (RegistryHelper.Is64BitOS)
                {
                    ScanClasses(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node"));
                    ScanClasses(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Classes"));
                    ScanClasses(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Classes"));
                }

                // Scan appids
                ScanAppIds(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("AppID"));
                ScanAppIds(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Classes\\AppID"));
                ScanAppIds(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Classes\\AppID"));
				if (RegistryHelper.Is64BitOS)
                {
                    ScanAppIds(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node\\AppID"));
                    ScanAppIds(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\AppID"));
                    ScanAppIds(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\AppID"));
                }

                // Scan explorer subkey
                ScanExplorer();
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        #region Scan functions

        /// <summary>
        /// Scans for the CLSID subkey
        /// <param name="regKey">Location of CLSID Sub Key</param>
        /// </summary>
        private static void ScanCLSIDSubKey(RegistryKey regKey)
        {
            if (regKey == null)
                return;

            // Main.Logger.WriteLine("Scanning " + regKey.Name + " for invalid CLSID's");

            foreach (string strCLSID in regKey.GetSubKeyNames())
            {
                RegistryKey rkCLSID = regKey.OpenSubKey(strCLSID);

                if (rkCLSID == null)
                    continue;

                ScanRegistryDlg.CurrentScannedObject = rkCLSID.ToString();

                // Check for valid AppID
                string strAppID = regKey.GetValue("AppID") as string;
                if (!string.IsNullOrEmpty(strAppID))
                    if (!appidExists(strAppID))
                        ScanRegistryDlg.StoreInvalidKey(Strings.MissingAppID, rkCLSID.ToString(), "AppID");

                // See if DefaultIcon exists
                using (RegistryKey regKeyDefaultIcon = rkCLSID.OpenSubKey("DefaultIcon"))
                {
                    if (regKeyDefaultIcon != null)
                    {
                        string iconPath = regKeyDefaultIcon.GetValue("") as string;

                        if (!string.IsNullOrEmpty(iconPath))
							if (!RegistryHelper.IconExists(iconPath))
								if (!RegistryHelper.IsOnIgnoreList(iconPath))
                                    ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, string.Format("{0}\\DefaultIcon", rkCLSID.ToString()));
                    }
                }

                // Look for InprocServer files
                using (RegistryKey regKeyInprocSrvr = rkCLSID.OpenSubKey("InprocServer"))
                {
                    if (regKeyInprocSrvr != null)
                    {
                        string strInprocServer = regKeyInprocSrvr.GetValue("") as string;

                        if (!string.IsNullOrEmpty(strInprocServer))
                            if (!RegistryHelper.FileExists(strInprocServer))
                                ScanRegistryDlg.StoreInvalidKey(Strings.InvalidInprocServer, regKeyInprocSrvr.ToString());

                        regKeyInprocSrvr.Close();
                    }
                }

                using (RegistryKey regKeyInprocSrvr32 = rkCLSID.OpenSubKey("InprocServer32"))
                {
                    if (regKeyInprocSrvr32 != null)
                    {
                        string strInprocServer32 = regKeyInprocSrvr32.GetValue("") as string;

                        if (!string.IsNullOrEmpty(strInprocServer32))
                            if (!RegistryHelper.FileExists(strInprocServer32))
                                ScanRegistryDlg.StoreInvalidKey(Strings.InvalidInprocServer32, regKeyInprocSrvr32.ToString());

                        regKeyInprocSrvr32.Close();
                    }
                }

                rkCLSID.Close();
            }

            regKey.Close();
            return;
        }

        /// <summary>
        /// Looks for invalid references to AppIDs
        /// </summary>
        private static void ScanAppIds(RegistryKey regKey)
        {
            if (regKey == null)
                return;

            // Main.Logger.WriteLine("Scanning " + regKey.Name + " for invalid AppID's");

            foreach (string strAppId in regKey.GetSubKeyNames())
            {
                using (RegistryKey rkAppId = regKey.OpenSubKey(strAppId))
                {
                    if (rkAppId != null)
                    {
                        // Update scan dialog
                        ScanRegistryDlg.CurrentScannedObject = rkAppId.ToString();

                        // Check for reference to AppID
                        string strCLSID = rkAppId.GetValue("AppID") as string;

                        if (!string.IsNullOrEmpty(strCLSID))
                            if (!appidExists(strCLSID))
                                ScanRegistryDlg.StoreInvalidKey(Strings.MissingAppID, rkAppId.ToString());
                    }
                }
            }

            regKey.Close();
        }

        /// <summary>
        /// Finds invalid File extensions + ProgIDs referenced
        /// </summary>
        private static void ScanClasses(RegistryKey regKey)
        {
            if (regKey == null)
                return;

            // Main.Logger.WriteLine("Scanning " + regKey.Name + " for invalid Classes");

            foreach (string strSubKey in regKey.GetSubKeyNames())
            {
                // Update scan dialog
                ScanRegistryDlg.CurrentScannedObject = string.Format("{0}\\{1}", regKey.Name, strSubKey);

                // Skip any file (*)
                if (strSubKey == "*")
                    continue;

                if (strSubKey[0] == '.')
                {
                    // File Extension
                    using (RegistryKey rkFileExt = regKey.OpenSubKey(strSubKey))
                    {
                        if (rkFileExt != null)
                        {
                            // Find reference to ProgID
                            string strProgID = rkFileExt.GetValue("") as string;

                            if (!string.IsNullOrEmpty(strProgID))
                                if (!progIDExists(strProgID))
                                    ScanRegistryDlg.StoreInvalidKey(Strings.MissingProgID, rkFileExt.ToString());
                        }
                    }
                }
                else
                {
                    // ProgID or file class

                    // See if DefaultIcon exists
                    using (RegistryKey regKeyDefaultIcon = regKey.OpenSubKey(string.Format("{0}\\DefaultIcon", strSubKey)))
                    {
                        if (regKeyDefaultIcon != null)
                        {
                            string iconPath = regKeyDefaultIcon.GetValue("") as string;

                            if (!string.IsNullOrEmpty(iconPath))
								if (!RegistryHelper.IconExists(iconPath))
									if (!RegistryHelper.IsOnIgnoreList(iconPath))
                                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKeyDefaultIcon.Name);
                        }
                    }

                    // Check referenced CLSID
                    using (RegistryKey rkCLSID = regKey.OpenSubKey(string.Format("{0}\\CLSID", strSubKey)))
                    {
                        if (rkCLSID != null)
                        {
                            string guid = rkCLSID.GetValue("") as string;

                            if (!string.IsNullOrEmpty(guid))
                                if (!clsidExists(guid))
                                    ScanRegistryDlg.StoreInvalidKey(Strings.MissingCLSID, string.Format("{0}\\{1}", regKey.Name, strSubKey));
                        }
                    }
                }

                // Check for unused progid/extension
                using (RegistryKey rk = regKey.OpenSubKey(strSubKey))
                {
                    if (rk != null)
                    {
                        if (rk.ValueCount <= 0 && rk.SubKeyCount <= 0)
                            ScanRegistryDlg.StoreInvalidKey(Strings.InvalidProgIDFileExt, rk.Name);
                    }
                }
            }

            regKey.Close();

            return;
        }

        /// <summary>
        /// Finds invalid windows explorer entries
        /// </summary>
        private static void ScanExplorer()
        {
            // Check Browser Help Objects
            using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\explorer\\Browser Helper Objects"))
            {
                // Main.Logger.WriteLine("Checking for invalid browser helper objects");

                if (regKey != null)
                {
                    RegistryKey rkBHO = null;

                    foreach (string strGuid in regKey.GetSubKeyNames())
                    {
                        if ((rkBHO = regKey.OpenSubKey(strGuid)) != null)
                        {
                            // Update scan dialog
                            ScanRegistryDlg.CurrentScannedObject = rkBHO.ToString();

                            if (!clsidExists(strGuid))
                                ScanRegistryDlg.StoreInvalidKey(Strings.MissingCLSID, rkBHO.ToString());
                        }
                    }
                }
            }

            // Check IE Toolbars
            using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Toolbar"))
            {
                // Main.Logger.WriteLine("Checking for invalid explorer toolbars");

                if (regKey != null)
                {
                    foreach (string strGuid in regKey.GetValueNames())
                    {
                        // Update scan dialog
                        ScanRegistryDlg.CurrentScannedObject = "CLSID: " + strGuid;

                        if (!IEToolbarIsValid(strGuid))
                            ScanRegistryDlg.StoreInvalidKey(Strings.InvalidToolbar, regKey.ToString(), strGuid);
                    }
                }
            }

            // Check IE Extensions
            using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Extensions"))
            {
                RegistryKey rkExt = null;

                // Main.Logger.WriteLine("Checking for invalid explorer extensions");

                if (regKey != null)
                {
                    foreach (string strGuid in regKey.GetSubKeyNames())
                    {
                        if ((rkExt = regKey.OpenSubKey(strGuid)) != null)
                        {
                            // Update scan dialog
                            ScanRegistryDlg.CurrentScannedObject = rkExt.ToString();

                            ValidateExplorerExt(rkExt);
                        }
                    }
                }
            }

            // Check Explorer File Exts
            using (RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts"))
            {
                RegistryKey rkFileExt = null;

                // Main.Logger.WriteLine("Checking for invalid explorer file extensions");

                if (regKey != null)
                {
                    foreach (string strFileExt in regKey.GetSubKeyNames())
                    {
                        if ((rkFileExt = regKey.OpenSubKey(strFileExt)) == null || strFileExt[0] != '.')
                            continue;

                        // Update scan dialog
                        ScanRegistryDlg.CurrentScannedObject = rkFileExt.ToString();

                        ValidateFileExt(rkFileExt);
                    }
                }
            }

            return;
        }

        #endregion

        #region Scan Sub-Functions

        private static void ValidateFileExt(RegistryKey regKey)
        {
            bool bProgidExists = false, bAppExists = false;

            // Skip if UserChoice subkey exists
            if (regKey.OpenSubKey("UserChoice") != null)
                return;

            // Parse and verify OpenWithProgId List
            using (RegistryKey rkProgids = regKey.OpenSubKey("OpenWithProgids"))
            {
                if (rkProgids != null)
                {
                    foreach (string strProgid in rkProgids.GetValueNames())
                    {
                        if (progIDExists(strProgid))
                            bProgidExists = true;
                    }
                }
            }

            // Check if files in OpenWithList exist
            using (RegistryKey rkOpenList = regKey.OpenSubKey("OpenWithList"))
            {
                if (rkOpenList != null)
                {
                    foreach (string strValueName in rkOpenList.GetValueNames())
                    {
                        if (strValueName == "MRUList")
                            continue;

                        string strApp = rkOpenList.GetValue(strValueName) as string;

                        if (appExists(strApp))
                            bAppExists = true;
                    }

                }
            }

            if (!bProgidExists && !bAppExists)
                ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFileExt, regKey.ToString());

            return;
        }

        private static void ValidateExplorerExt(RegistryKey regKey)
        {
            try
            {
                // Sees if icon file exists
                string strHotIcon = regKey.GetValue("HotIcon") as string;
                if (!string.IsNullOrEmpty(strHotIcon))
					if (!RegistryHelper.IconExists(strHotIcon))
                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), "HotIcon");

                string strIcon = regKey.GetValue("Icon") as string;
                if (!string.IsNullOrEmpty(strIcon))
					if (!RegistryHelper.IconExists(strIcon))
                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), "Icon");

                // Lookup CLSID extension
                string strClsidExt = regKey.GetValue("ClsidExtension") as string;
                if (!string.IsNullOrEmpty(strClsidExt))
                    ScanRegistryDlg.StoreInvalidKey(Strings.MissingCLSID, regKey.ToString(), "ClsidExtension");

                // See if files exist
                string strExec = regKey.GetValue("Exec") as string;
                if (!string.IsNullOrEmpty(strExec))
                    if (!RegistryHelper.FileExists(strExec))
                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), "Exec");

                string strScript = regKey.GetValue("Script") as string;
                if (!string.IsNullOrEmpty(strScript))
                    if (!RegistryHelper.FileExists(strScript))
                        ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), "Script");
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Checks for inprocserver file
        /// </summary>
        /// <param name="regKey">The registry key contain Inprocserver subkey</param>
        /// <returns>False if Inprocserver is null or doesnt exist</returns>
        private static bool InprocServerExists(RegistryKey regKey)
        {
            if (regKey != null)
            {
                using (RegistryKey regKeyInprocSrvr = regKey.OpenSubKey("InprocServer"))
                {
                    if (regKeyInprocSrvr != null)
                    {
                        string strInprocServer = regKeyInprocSrvr.GetValue("") as string;

                        if (!string.IsNullOrEmpty(strInprocServer))
                            if (RegistryHelper.FileExists(strInprocServer))
                                return true;
                    }
                }

                using (RegistryKey regKeyInprocSrvr32 = regKey.OpenSubKey("InprocServer32"))
                {
                    if (regKeyInprocSrvr32 != null)
                    {
                        string strInprocServer32 = regKeyInprocSrvr32.GetValue("") as string;

                        if (!string.IsNullOrEmpty(strInprocServer32))
                            if (RegistryHelper.FileExists(strInprocServer32))
                                return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if IE toolbar GUID is valid
        /// </summary>
        private static bool IEToolbarIsValid(string strGuid)
        {
            bool bRet = false;

            if (!clsidExists(strGuid))
                bRet = false;

            if (InprocServerExists(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("CLSID\\" + strGuid)))
                bRet = true;

            if (InprocServerExists(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Classes\\CLSID\\" + strGuid)))
                bRet = true;

            if (InprocServerExists(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Classes\\CLSID\\" + strGuid)))
                bRet = true;

			if (RegistryHelper.Is64BitOS)
            {
                if (InprocServerExists(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node\\CLSID\\" + strGuid)))
                    bRet = true;

                if (InprocServerExists(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\Classes\\CLSID\\" + strGuid)))
                    bRet = true;

                if (InprocServerExists(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Wow6432Node\\Classes\\CLSID\\" + strGuid)))
                    bRet = true;
            }

            return bRet;
        }

        /// <summary>
        /// Sees if application exists
        /// </summary>
        /// <param name="appName">Application Name</param>
        /// <returns>True if it exists</returns>
        private static bool appExists(string appName)
        {
            List<RegistryKey> listRegKeys = new List<RegistryKey>();

            listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Applications"));
            listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Classes\Applications"));
            listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Classes\Applications"));

			if (RegistryHelper.Is64BitOS)
            {
                listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"Wow6432Node\Applications"));
                listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Classes\Applications"));
                listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Wow6432Node\Classes\Applications"));
            }

            try
            {
                foreach (RegistryKey rk in listRegKeys)
                {
                    if (rk == null)
                        continue;

                    using (RegistryKey subKey = rk.OpenSubKey(appName))
                    {
                        if (subKey != null)
							if (!RegistryHelper.IsOnIgnoreList(subKey.ToString()))
                                return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Sees if the specified CLSID exists
        /// </summary>
        /// <param name="clsid">The CLSID GUID</param>
        /// <returns>True if it exists</returns>
        private static bool clsidExists(string clsid)
        {
            List<RegistryKey> listRegKeys = new List<RegistryKey>();

            listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("CLSID"));
            listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Classes\CLSID"));
            listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Classes\CLSID"));

			if (RegistryHelper.Is64BitOS)
            {
                listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"Wow6432Node\CLSID"));
                listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Classes\CLSID"));
                listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Wow6432Node\Classes\CLSID"));
            }

            try
            {
                foreach (RegistryKey rk in listRegKeys)
                {
                    if (rk == null)
                        continue;

                    using (RegistryKey subKey = rk.OpenSubKey(clsid))
                    {
                        if (subKey != null)
							if (!RegistryHelper.IsOnIgnoreList(subKey.ToString()))
                                return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Checks if the ProgID exists in Classes subkey
        /// </summary>
        /// <param name="progID">The ProgID</param>
        /// <returns>True if it exists</returns>
        private static bool progIDExists(string progID)
        {
            List<RegistryKey> listRegKeys = new List<RegistryKey>();

            listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot);
            listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Classes"));
            listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Classes"));

			if (RegistryHelper.Is64BitOS)
            {
                listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"Wow6432Node"));
                listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Classes"));
                listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Wow6432Node\Classes"));
            }

            try
            {
                foreach (RegistryKey rk in listRegKeys)
                {
                    if (rk == null)
                        continue;

                    using (RegistryKey subKey = rk.OpenSubKey(progID))
                    {
                        if (subKey != null)
							if (!RegistryHelper.IsOnIgnoreList(subKey.ToString()))
                                return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Checks if the AppID exists
        /// </summary>
        /// <param name="appID">The AppID or GUID</param>
        /// <returns>True if it exists</returns>
        private static bool appidExists(string appID)
        {
            List<RegistryKey> listRegKeys = new List<RegistryKey>();

            listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"AppID"));
            listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Classes\AppID"));
            listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Classes\AppID"));

			if (RegistryHelper.Is64BitOS)
            {
                listRegKeys.Add(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"Wow6432Node\AppID"));
                listRegKeys.Add(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Classes\AppID"));
                listRegKeys.Add(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Wow6432Node\Classes\AppID"));
            }

            try
            {
                foreach (RegistryKey rk in listRegKeys)
                {
                    if (rk == null)
                        continue;

                    using (RegistryKey subKey = rk.OpenSubKey(appID))
                    {
                        if (subKey != null)
							if (!RegistryHelper.IsOnIgnoreList(subKey.ToString()))
                                return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }


        #endregion
    }
}