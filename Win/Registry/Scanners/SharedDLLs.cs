﻿using Common;
using Business.Registry.Misc;
using Microsoft.Win32;

namespace Win.Registry.Scanners
{
    public class SharedDLLs : ScannerBase
    {
        public override string ScannerName
        {
            get { return Strings.SharedDLLs; }
        }

        /// <summary>
        /// Scan for missing links to DLLS
        /// </summary>
        public static void Scan()
        {
            try
            {
                RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs");

                if (regKey == null)
                    return;

                // Main.Logger.WriteLine("Scanning for missing shared DLLs");

                // Validate Each DLL from the value names
                foreach (string strFilePath in regKey.GetValueNames())
                {
                    // Update scan dialog
                    ScanRegistryDlg.CurrentScannedObject = strFilePath;

                    if (!string.IsNullOrEmpty(strFilePath))
						if (!RegistryHelper.FileExists(strFilePath))
                            ScanRegistryDlg.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strFilePath);
                }

                regKey.Close();
            }
            catch (System.Security.SecurityException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}
