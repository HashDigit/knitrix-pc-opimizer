﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Security.Permissions;
using Common;
using Data;
using Data.Registry;
using Business.Registry;
using Business.Registry.Scanners;

namespace Win
{
    public partial class ScanRegistryDlg : Form
    {
        public delegate void ScanDelegate();

        Thread threadMain, threadScan;

        private static string _currentObject = string.Empty;
        private static int _objectsScanned = 0;

        public static string CurrentScannedObject
        {
            get 
            { 
                return _currentObject;
            }
            set 
            {
				ScanRegistryDlg._currentObject = RegistryHelper.PrefixRegPath(value);
                ScanRegistryDlg._objectsScanned++;
            }
        }

        public static string CurrentSection
        {
            get;
            set;
        }

        private static ScannerBase currentScanner;

        public static BadRegKeyArray arrBadRegistryKeys = new BadRegKeyArray();

        public ScanRegistryDlg(int nSectionCount)
        {
            InitializeComponent();

            // Set the section count so it can be accessed later
            ScanRegistryDlg.arrBadRegistryKeys.Clear(nSectionCount);
        }

        
        private void ScanDlg_Shown(object sender, EventArgs e)
        {
            // Setup progress bar
            this.progressBar.Position = 0;
            this.progressBar.PositionMin = 0;
            this.progressBar.PositionMax = ScanRegistryDlg.arrBadRegistryKeys.SectionCount;

            // Append 0 to problem label
            this.labelProblems.Text = string.Format("{0} 0", this.labelProblems.Text);

            // Starts scanning registry on seperate thread
            this.threadMain = new Thread(new ThreadStart(StartScanning));
            this.threadMain.Start();
        }

        /// <summary>
        /// Begins scanning for errors in the registry
        /// </summary>
        private void StartScanning()
        {
            // Get start time of scan
            DateTime dateTimeStart = DateTime.Now;

            // Begin Critical Region
            Thread.BeginCriticalRegion();

            // Begin scanning
            try
            {
                // Main.Logger.WriteLine("Started scan at " + DateTime.Now.ToString());
                // Main.Logger.WriteLine();

                if (DataContext.ScanRegistrySettings.Startup)
                    this.StartScanner(new StartupFiles());

                if (DataContext.ScanRegistrySettings.SharedDLL)
                    this.StartScanner(new SharedDLLs());

                if (DataContext.ScanRegistrySettings.Fonts)
                    this.StartScanner(new WindowsFonts());

                if (DataContext.ScanRegistrySettings.AppInfo)
                    this.StartScanner(new ApplicationInfo());

                if (DataContext.ScanRegistrySettings.AppPaths)
                    this.StartScanner(new ApplicationPaths());

                if (DataContext.ScanRegistrySettings.Activex)
                    this.StartScanner(new ActivexComObjects());

                if (DataContext.ScanRegistrySettings.Drivers)
                    this.StartScanner(new SystemDrivers());

                if (DataContext.ScanRegistrySettings.HelpFiles)
                    this.StartScanner(new WindowsHelpFiles());

                if (DataContext.ScanRegistrySettings.Sounds)
                    this.StartScanner(new WindowsSounds());

                if (DataContext.ScanRegistrySettings.AppSettings)
                    this.StartScanner(new ApplicationSettings());

                if (DataContext.ScanRegistrySettings.HistoryList)
                    this.StartScanner(new RecentDocs());

                this.DialogResult = DialogResult.OK;
            }
            catch (ThreadAbortException)
            {
                // Scanning was aborted
                // Main.Logger.Write("User aborted scan... ");
                if (this.threadScan.IsAlive)
                    this.threadScan.Abort();

                this.DialogResult = DialogResult.Abort;
                // Main.Logger.WriteLine("Exiting.\r\n");
            }
            finally
            {
                // Compute time between start and end of scan
                TimeSpan ts = DateTime.Now.Subtract(dateTimeStart);

                // Main.Logger.WriteLine(string.Format("Total time elapsed: {0} seconds", ts.TotalSeconds));
                // Main.Logger.WriteLine(string.Format("Total problems found: {0}", ScanRegistryDlg.arrBadRegistryKeys.Count));
                // Main.Logger.WriteLine(string.Format("Total objects scanned: {0}", ScanRegistryDlg.arrBadRegistryKeys.ItemsScanned));
                // Main.Logger.WriteLine();
                // Main.Logger.WriteLine("Finished scan at " + DateTime.Now.ToString());
            }

            // End Critical Region
            Thread.EndCriticalRegion();

            // Dialog will be closed automatically

            return;
        }

        /// <summary>
        /// Starts a scanner
        /// </summary>
        public void StartScanner(ScannerBase scannerName)
        {
			ScannerBase.CurrentScannedObject_Changed += new ScannerBase.CurrentScannedObject(ScannerBase_CurrentScannedObject_Changed);
			ScannerBase.InvalidKey_Found += new ScannerBase.StoreInvalidKey(ScannerBase_InvalidKey_Found);
            currentScanner = scannerName;

            System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            ScanDelegate objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);
            
            // Main.Logger.WriteLine("Starting scanning: " + scannerName.ScannerName);

            // Update section name
            scannerName.RootNode.SectionName = scannerName.ScannerName;
            scannerName.RootNode.Img = this.imageList.Images[scannerName.GetType().Name];
            ScanRegistryDlg.CurrentSection = scannerName.ScannerName;

            // Start scanning
            this.threadScan = new Thread(new ThreadStart(objScan));
            this.threadScan.Start();
            this.threadScan.Join();

            // Wait 250ms
            Thread.Sleep(250);

            if (scannerName.RootNode.Nodes.Count > 0)
                ScanRegistryDlg.arrBadRegistryKeys.Add(scannerName.RootNode);

            // Main.Logger.WriteLine("Finished scanning: " + scannerName.ScannerName);
            // Main.Logger.WriteLine();

            this.progressBar.Position++;
			ScannerBase.CurrentScannedObject_Changed -= new ScannerBase.CurrentScannedObject(ScannerBase_CurrentScannedObject_Changed);
			ScannerBase.InvalidKey_Found -= new ScannerBase.StoreInvalidKey(ScannerBase_InvalidKey_Found);
		}

        /// <summary>
        /// <para>Stores an invalid registry key to array list</para>
        /// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
        /// </summary>
        /// <param name="Problem">Reason its invalid</param>
        /// <param name="Path">The path to registry key (including registry hive)</param>
        /// <returns>True if it was added</returns>
        public static bool StoreInvalidKey(string Problem, string Path)
        {
            return StoreInvalidKey(Problem, Path, "");
        }

        /// <summary>
        /// <para>Stores an invalid registry key to array list</para>
        /// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
        /// </summary>
        /// <param name="problem">Reason its invalid</param>
        /// <param name="regPath">The path to registry key (including registry hive)</param>
        /// <param name="valueName">Value name (leave blank if theres none)</param>
        /// <returns>True if it was added. Otherwise, false.</returns>
        public static bool StoreInvalidKey(string problem, string regPath, string valueName)
        {
            string baseKey, subKey;

            // Check for null parameters
            if (string.IsNullOrEmpty(problem) || string.IsNullOrEmpty(regPath))
                return false;

            // Make sure registry key exists
			if (!RegistryHelper.RegKeyExists(regPath))
                return false;

            // Parse registry key to base and subkey
			if (!RegistryHelper.ParseRegKeyPath(regPath, out baseKey, out subKey))
                return false;

            // Check for ignored registry path
			if (RegistryHelper.IsOnIgnoreList(regPath))
                return false;

            // If value name is specified, see if it exists
            if (!string.IsNullOrEmpty(valueName))
				if (!RegistryHelper.ValueNameExists(baseKey, subKey, valueName))
                    return false;

            try
            {
                // Throws exception if user doesnt have permission
                RegistryPermission regPermission = new RegistryPermission(RegistryPermissionAccess.AllAccess, regPath);
                regPermission.Demand();
            }
            catch (System.Security.SecurityException)
            {
                return false;
            }

            ScanRegistryDlg.currentScanner.RootNode.Nodes.Add(new BadRegistryKey(problem, baseKey, subKey, valueName));

            ScanRegistryDlg.arrBadRegistryKeys.Problems++;

            //if (!string.IsNullOrEmpty(valueName))
                // Main.Logger.WriteLine(string.Format("Bad Registry Value Found! Problem: \"{0}\" Path: \"{1}\" Value Name: \"{2}\"", problem, regPath, valueName));
            //else
                // Main.Logger.WriteLine(string.Format("Bad Registry Key Found! Problem: \"{0}\" Path: \"{1}\"", problem, regPath));

            return true;
        }

        private void ScanRegistryDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.OK)
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
					if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        e.Cancel = true;
                    else
                        this.threadMain.Abort();
                }
            }
        }

		private void btnStop_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager(typeof(ScanRegistryDlg));

			this.progressBar.Text = "Scanning: " + ScanRegistryDlg.CurrentSection;
            this.labelProblems.Text = string.Format("{0} {1}", rm.GetString("labelProblems.Text"), ScanRegistryDlg.arrBadRegistryKeys.Problems);
            this.textBoxSubKey.Text = ScanRegistryDlg.CurrentScannedObject;
        }

		private void ScannerBase_CurrentScannedObject_Changed(string currentScannedObject)
		{
			ScanRegistryDlg.CurrentScannedObject = currentScannedObject;
		}

		private void ScannerBase_InvalidKey_Found(string problem, string regPath, string valueName)
		{
			ScanRegistryDlg.StoreInvalidKey(problem, regPath, valueName);
		}
	}
}
