﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Data;
using Data.Registry;

namespace Win.Registry
{
	public partial class IgnoreListForm : Form
	{
		private RegistryExcludeArray _arrayExclude = new RegistryExcludeArray();

		public RegistryExcludeArray RegistryExcludeArray
		{
			get { return _arrayExclude; }
		}

		public IgnoreListForm()
		{
			InitializeComponent();
		}

		private void Options_Load(object sender, EventArgs e)
		{
			// Load exclude list from DataContext
			this._arrayExclude = new RegistryExcludeArray(Data.Options.Instance.RegistryExcludeArray);

			PopulateExcludeList();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			Data.Options.Instance.RegistryExcludeArray = new RegistryExcludeArray(this._arrayExclude);
			Data.Options.Instance.Save();
			this.Close();
		}

		#region Exclude List
		private void addRegistryPathToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AddRegistryPathForm addRegistryPathForm = new AddRegistryPathForm();
			if (addRegistryPathForm.ShowDialog(this) == DialogResult.OK)
			{
				RegistryExcludeArray.Add(new RegistryExcludeItem(addRegistryPathForm.RegistryPath, null, null));
				PopulateExcludeList();
			}
		}

		private void addFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.Title = Properties.Resources.optionsExcludeFile;
				openFileDialog.Filter = "All files (*.*)|*.*";
				openFileDialog.FilterIndex = 1;
				openFileDialog.RestoreDirectory = true;

				if (openFileDialog.ShowDialog(this) == DialogResult.OK)
				{
					RegistryExcludeArray.Add(new RegistryExcludeItem(null, null, openFileDialog.FileName));
					PopulateExcludeList();
				}
			}
		}

		private void addFolderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
			{
				folderBrowserDialog.Description = Properties.Resources.optionsExcludeDir;

				if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
				{
					RegistryExcludeArray.Add(new RegistryExcludeItem(null, folderBrowserDialog.SelectedPath, null));
					PopulateExcludeList();
				}
			}
		}

		private void removeEntryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.listView1.SelectedIndices.Count > 0 && this.listView1.Items.Count > 0)
			{
				if (MessageBox.Show(this, "Are you sure you want to remove selected entry from Ignore List?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					foreach (RegistryExcludeItem i in RegistryExcludeArray)
					{
						if (i.ToString() == this.listView1.SelectedItems[0].Text)
						{
							RegistryExcludeArray.Remove(i);
							break;
						}
					}

					PopulateExcludeList();
				}
			}
		}

		private void PopulateExcludeList()
		{
			this.listView1.Items.Clear();

			foreach (RegistryExcludeItem item in RegistryExcludeArray)
				this.listView1.Items.Add(item.ToString());

			this.listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
		}
		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
