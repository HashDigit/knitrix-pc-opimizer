﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Common;

namespace Win.Registry
{
    public partial class AddRegistryPathForm : Form
    {
        private string _regPath;
        /// <summary>
        /// The registry path selected by the user
        /// </summary>
        public string RegistryPath
        {
            get { return _regPath; }
        }

        public AddRegistryPathForm()
        {
            InitializeComponent();

            this.comboBox1.Text = (string)this.comboBox1.Items[0];
        }


		private void btnAddEntry_Click(object sender, EventArgs e)
        {
			this.DialogResult = DialogResult.OK;
            string strBaseKey = this.comboBox1.Text, strSubKey = this.textBox1.Text;

            if (string.IsNullOrEmpty(strBaseKey) || string.IsNullOrEmpty(strSubKey))
            {
                MessageBox.Show(this, Properties.Resources.optionsExcludeEmptyRegPath, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.None;
                return;
            }

			if (!RegistryHelper.RegKeyExists(strBaseKey, strSubKey))
            {
                MessageBox.Show(this, Properties.Resources.optionsExcludeInvalidRegPath, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.None;
                return;
            }

            this._regPath = string.Format(@"{0}\{1}", strBaseKey, strSubKey);

            this.Close();
        }

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
    }
}
