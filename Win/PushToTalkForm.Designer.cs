﻿namespace Win
{
	partial class PushToTalkForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lblName = new DevExpress.XtraEditors.LabelControl();
			this.txtName = new DevExpress.XtraEditors.TextEdit();
			this.txtPhone = new DevExpress.XtraEditors.TextEdit();
			this.lblPhone = new DevExpress.XtraEditors.LabelControl();
			this.txtEmail = new DevExpress.XtraEditors.TextEdit();
			this.lblEmail = new DevExpress.XtraEditors.LabelControl();
			this.lblTime = new DevExpress.XtraEditors.LabelControl();
			this.lblDescription = new DevExpress.XtraEditors.LabelControl();
			this.meDescription = new DevExpress.XtraEditors.MemoEdit();
			this.teTime = new DevExpress.XtraEditors.TimeEdit();
			this.epMain = new System.Windows.Forms.ErrorProvider(this.components);
			this.btnCancel = new Win.Controls.RegisterButton();
			this.btnSend = new Win.Controls.RegisterButton();
			((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.epMain)).BeginInit();
			this.SuspendLayout();
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(12, 16);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(36, 13);
			this.lblName.TabIndex = 81;
			this.lblName.Text = "Name *";
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(91, 12);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(218, 20);
			this.txtName.TabIndex = 82;
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(91, 38);
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Properties.Mask.EditMask = "(999)000-0000";
			this.txtPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
			this.txtPhone.Size = new System.Drawing.Size(218, 20);
			this.txtPhone.TabIndex = 84;
			// 
			// lblPhone
			// 
			this.lblPhone.Location = new System.Drawing.Point(12, 42);
			this.lblPhone.Name = "lblPhone";
			this.lblPhone.Size = new System.Drawing.Size(30, 13);
			this.lblPhone.TabIndex = 83;
			this.lblPhone.Text = "Phone";
			// 
			// txtEmail
			// 
			this.txtEmail.Location = new System.Drawing.Point(91, 64);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Properties.Mask.EditMask = "(\\w|[\\.\\-])+@(\\w|[\\-]+\\.)*(\\w|[\\-]){2,63}\\.[a-zA-Z]{2,4}";
			this.txtEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.txtEmail.Size = new System.Drawing.Size(218, 20);
			this.txtEmail.TabIndex = 86;
			// 
			// lblEmail
			// 
			this.lblEmail.Location = new System.Drawing.Point(12, 68);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Size = new System.Drawing.Size(24, 13);
			this.lblEmail.TabIndex = 85;
			this.lblEmail.Text = "Email";
			// 
			// lblTime
			// 
			this.lblTime.Location = new System.Drawing.Point(12, 94);
			this.lblTime.Name = "lblTime";
			this.lblTime.Size = new System.Drawing.Size(79, 13);
			this.lblTime.TabIndex = 87;
			this.lblTime.Text = "Best Time to Call";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(12, 120);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(62, 13);
			this.lblDescription.TabIndex = 89;
			this.lblDescription.Text = "Description *";
			// 
			// meDescription
			// 
			this.meDescription.Location = new System.Drawing.Point(91, 116);
			this.meDescription.Name = "meDescription";
			this.meDescription.Size = new System.Drawing.Size(347, 120);
			this.meDescription.TabIndex = 90;
			// 
			// teTime
			// 
			this.teTime.EditValue = new System.DateTime(2012, 5, 8, 0, 0, 0, 0);
			this.teTime.Location = new System.Drawing.Point(162, 90);
			this.teTime.Name = "teTime";
			this.teTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.teTime.Properties.Mask.EditMask = "g";
			this.teTime.Size = new System.Drawing.Size(137, 20);
			this.teTime.TabIndex = 91;
			// 
			// epMain
			// 
			this.epMain.ContainerControl = this;
			this.epMain.RightToLeft = true;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(355, 242);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(83, 27);
			this.btnCancel.TabIndex = 80;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.ClickButtonArea += new CButtonLib.CButton.ClickButtonAreaEventHandler(this.btnCancel_ClickButtonArea);
			// 
			// btnSend
			// 
			this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSend.BackColor = System.Drawing.Color.Transparent;
			this.btnSend.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnSend.ForeColor = System.Drawing.Color.White;
			this.btnSend.Location = new System.Drawing.Point(91, 242);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(119, 27);
			this.btnSend.TabIndex = 79;
			this.btnSend.Text = "Send";
			this.btnSend.ClickButtonArea += new CButtonLib.CButton.ClickButtonAreaEventHandler(this.btnSend_ClickButtonArea);
			// 
			// PushToTalkForm
			// 
			this.Appearance.BackColor = System.Drawing.Color.White;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
			this.BackgroundImageStore = global::Win.Properties.Resources.need_register_back;
			this.ClientSize = new System.Drawing.Size(450, 281);
			this.Controls.Add(this.teTime);
			this.Controls.Add(this.meDescription);
			this.Controls.Add(this.lblDescription);
			this.Controls.Add(this.lblTime);
			this.Controls.Add(this.txtEmail);
			this.Controls.Add(this.lblEmail);
			this.Controls.Add(this.txtPhone);
			this.Controls.Add(this.lblPhone);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSend);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PushToTalkForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Push to Talk";
			((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.epMain)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Controls.RegisterButton btnSend;
		private Controls.RegisterButton btnCancel;
		private DevExpress.XtraEditors.LabelControl lblName;
		private DevExpress.XtraEditors.TextEdit txtName;
		private DevExpress.XtraEditors.TextEdit txtPhone;
		private DevExpress.XtraEditors.LabelControl lblPhone;
		private DevExpress.XtraEditors.TextEdit txtEmail;
		private DevExpress.XtraEditors.LabelControl lblEmail;
		private DevExpress.XtraEditors.LabelControl lblTime;
		private DevExpress.XtraEditors.LabelControl lblDescription;
		private DevExpress.XtraEditors.MemoEdit meDescription;
		private DevExpress.XtraEditors.TimeEdit teTime;
		private System.Windows.Forms.ErrorProvider epMain;
	}
}