﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Win.Misc
{
	public class Permissions
	{
		[DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
		internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

		[DllImport("kernel32.dll", ExactSpelling = true)]
		internal static extern IntPtr GetCurrentProcess();

		[DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
		internal static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

		[DllImport("advapi32.dll", SetLastError = true)]
		internal static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		internal struct TokPriv1Luid
		{
			public int Count;
			public long Luid;
			public int Attr;
		}

		internal const int SE_PRIVILEGE_ENABLED = 0x00000002;
		internal const int SE_PRIVILEGE_REMOVED = 0x00000004;
		internal const int TOKEN_QUERY = 0x00000008;
		internal const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;

		public static bool SetPrivilege(string privilege, bool enabled)
		{
			try
			{
				bool retVal;
				TokPriv1Luid tp; //its a structure declared with in the same class. count,luid and attr.
				IntPtr hproc = GetCurrentProcess();
				IntPtr htok = IntPtr.Zero;
				retVal = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
				tp.Count = 1;
				tp.Luid = 0;
				if (enabled)
					tp.Attr = SE_PRIVILEGE_ENABLED;
				else
					tp.Attr = SE_PRIVILEGE_REMOVED;
				retVal = LookupPrivilegeValue(null, privilege, ref tp.Luid);
				retVal = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
				return retVal;
			}
			catch
			{
				return false;
			}
		}

		public static void SetPrivileges(bool enable)
		{
			SetPrivilege("SeShutdownPrivilege", enable);
			SetPrivilege("SeBackupPrivilege", enable);
			SetPrivilege("SeRestorePrivilege", enable);
			SetPrivilege("SeDebugPrivilege", enable);
		}
	}
}
