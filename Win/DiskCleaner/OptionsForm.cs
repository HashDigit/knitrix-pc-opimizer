﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Data;
using Win.DiskCleaner.Misc;

namespace Win.DiskCleaner
{
	public partial class OptionsForm : DevExpress.XtraEditors.XtraForm
    {

        public OptionsForm()
        {
            InitializeComponent();
        }

        private void Options_Load(object sender, EventArgs e)
        {
            // Drives
            foreach (ListViewItem listViewItem in Options.Instance.DiskCleanerSettings.diskDrives)
                this.listViewDrives.Items.Add(listViewItem.Clone() as ListViewItem);

            // Advanced
            this.checkBoxArchive.Checked = Options.Instance.DiskCleanerSettings.searchArchives;
            this.checkBoxHidden.Checked = Options.Instance.DiskCleanerSettings.searchHidden;
            this.checkBoxReadOnly.Checked = Options.Instance.DiskCleanerSettings.searchReadOnly;
            this.checkBoxSystem.Checked = Options.Instance.DiskCleanerSettings.searchSystem;

            if (Options.Instance.DiskCleanerSettings.findFilesMode == 0)
                this.radioButtonFindCreated.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.findFilesMode == 1)
                this.radioButtonFindModified.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.findFilesMode == 2)
                this.radioButtonFindAccessed.Checked = true;

            this.checkBoxFindAfter.Checked = Options.Instance.DiskCleanerSettings.findFilesAfter;
            this.checkBoxFindBefore.Checked = Options.Instance.DiskCleanerSettings.findFilesBefore;

            this.dateTimePickerAfter.Value = Options.Instance.DiskCleanerSettings.dateTimeAfter;
            this.dateTimePickerBefore.Value = Options.Instance.DiskCleanerSettings.dateTimeBefore;

            this.checkBoxSize.Checked = Options.Instance.DiskCleanerSettings.checkFileSize;
            this.numericUpDownSizeAtLeast.Value = Convert.ToDecimal(Options.Instance.DiskCleanerSettings.checkFileSizeLeast);
            this.numericUpDownSizeAtMost.Value = Convert.ToDecimal(Options.Instance.DiskCleanerSettings.checkFileSizeMost);

            // Search OptionsForm
            this.checkBoxWriteProtected.Checked = Options.Instance.DiskCleanerSettings.ignoreWriteProtected;
            this.checkBoxZeroLength.Checked = Options.Instance.DiskCleanerSettings.searchZeroByte;

            if (Options.Instance.DiskCleanerSettings.filterMode == 0)
                this.radioButtonFilterSafe.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.filterMode == 1)
                this.radioButtonFilterMed.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.filterMode == 2)
                this.radioButtonFilterAgg.Checked = true;

            this.txtSearchFilters.Text = Options.Instance.DiskCleanerSettings.searchFilters;

            this.checkBoxAutoClean.Checked = Options.Instance.DiskCleanerSettings.autoClean;

            // Removal
            if (Options.Instance.DiskCleanerSettings.removeMode == 0)
                this.radioButtonRemove.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.removeMode == 1)
                this.radioButtonRecycle.Checked = true;
            else if (Options.Instance.DiskCleanerSettings.removeMode == 2)
                this.radioButtonMove.Checked = true;
            this.txtMoveFolder.Text = Options.Instance.DiskCleanerSettings.moveFolder;
            this.checkBoxAutoSysRestore.Checked = Options.Instance.DiskCleanerSettings.autoRestorePoints;

            // Excluded Dirs
            foreach (string excludeDir in Options.Instance.DiskCleanerSettings.excludedDirs)
                this.listViewExcludeFolders.Items.Add(excludeDir);
            this.listViewExcludeFolders.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            // Excluded Files
            foreach (string excludeFile in Options.Instance.DiskCleanerSettings.excludedFileTypes)
                this.listViewFiles.Items.Add(excludeFile);
            this.listViewFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            // Included Folders
            foreach (string includedFolder in Options.Instance.DiskCleanerSettings.includedFolders)
                this.listViewIncFolders.Items.Add(includedFolder);
            this.listViewIncFolders.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void FilterRadioChanged(object sender, EventArgs e)
        {
            if (this.radioButtonFilterSafe.Checked == true)
                this.txtSearchFilters.Text = "*.tmp; *.temp; *.gid; *.chk; *.~*;";
            else if (this.radioButtonFilterMed.Checked == true)
                this.txtSearchFilters.Text = "*.tmp; *.temp; *.gid; *.chk; *.~*;*.old; *.fts; *.ftg; *.$$$; *.---; ~*.*; *.??$; *.___; *._mp; *.dmp; *.prv; CHKLIST.MS; *.$db; *.??~; *.db$; chklist.*; mscreate.dir;";
            else if (this.radioButtonFilterAgg.Checked == true)
                this.txtSearchFilters.Text = "*.tmp; *.temp; *.gid; *.chk; *.~*;*.old; *.fts; *.ftg; *.$$$; *.---; ~*.*; *.??$; *.___; *._mp; *.dmp; *.prv; CHKLIST.MS; *.$db; *.??~; *.db$; chklist.*; mscreate.dir;*.wbk; *log.txt; *.err; *.log; *.sik; *.bak; *.ilk; *.aps; *.ncb; *.pch; *.?$?; *.?~?; *.^; *._dd; *._detmp; 0*.nch; *.*_previous; *_previous;";

        }

		private void btnOK_Click(object sender, EventArgs e)
        {
            // Drives
            Options.Instance.DiskCleanerSettings.diskDrives = new System.Collections.ArrayList(this.listViewDrives.Items);

            // Searching
            if (this.radioButtonFilterSafe.Checked)
                Options.Instance.DiskCleanerSettings.filterMode = 0;
            else if (this.radioButtonFilterMed.Checked)
                Options.Instance.DiskCleanerSettings.filterMode = 1;
            else if (this.radioButtonFilterAgg.Checked)
                Options.Instance.DiskCleanerSettings.filterMode = 2;

            Options.Instance.DiskCleanerSettings.searchFilters = this.txtSearchFilters.Text;

            Options.Instance.DiskCleanerSettings.ignoreWriteProtected = this.checkBoxWriteProtected.Checked;
            Options.Instance.DiskCleanerSettings.searchZeroByte = this.checkBoxZeroLength.Checked;

            Options.Instance.DiskCleanerSettings.autoClean = this.checkBoxAutoClean.Checked;

            // Advanced
            Options.Instance.DiskCleanerSettings.searchHidden = this.checkBoxHidden.Checked;
            Options.Instance.DiskCleanerSettings.searchArchives = this.checkBoxArchive.Checked;
            Options.Instance.DiskCleanerSettings.searchReadOnly = this.checkBoxReadOnly.Checked;
            Options.Instance.DiskCleanerSettings.searchSystem = this.checkBoxSystem.Checked;

            if (this.radioButtonFindCreated.Checked)
                Options.Instance.DiskCleanerSettings.findFilesMode = 0;
            else if (this.radioButtonFindModified.Checked)
                Options.Instance.DiskCleanerSettings.findFilesMode = 1;
            else if (this.radioButtonFindAccessed.Checked)
                Options.Instance.DiskCleanerSettings.findFilesMode = 2;

            Options.Instance.DiskCleanerSettings.findFilesAfter = this.checkBoxFindAfter.Checked;
            Options.Instance.DiskCleanerSettings.findFilesBefore = this.checkBoxFindBefore.Checked;

            Options.Instance.DiskCleanerSettings.dateTimeAfter = this.dateTimePickerAfter.Value;
            Options.Instance.DiskCleanerSettings.dateTimeBefore = this.dateTimePickerBefore.Value;

            Options.Instance.DiskCleanerSettings.checkFileSize = this.checkBoxSize.Checked;
            Options.Instance.DiskCleanerSettings.checkFileSizeLeast = Convert.ToInt32(this.numericUpDownSizeAtLeast.Value);
            Options.Instance.DiskCleanerSettings.checkFileSizeMost = Convert.ToInt32(this.numericUpDownSizeAtMost.Value);

            // Removal
            if (this.radioButtonRemove.Checked)
                Options.Instance.DiskCleanerSettings.removeMode = 0;
            else if (this.radioButtonRecycle.Checked)
                Options.Instance.DiskCleanerSettings.removeMode = 1;
            else if (this.radioButtonMove.Checked)
                Options.Instance.DiskCleanerSettings.removeMode = 2;

            Options.Instance.DiskCleanerSettings.moveFolder = this.txtMoveFolder.Text;

            Options.Instance.DiskCleanerSettings.autoRestorePoints = this.checkBoxAutoSysRestore.Checked;

            // Included Folders
            Options.Instance.DiskCleanerSettings.includedFolders.Clear();
            foreach (ListViewItem listViewItem in this.listViewIncFolders.Items)
                Options.Instance.DiskCleanerSettings.includedFolders.Add(listViewItem.Text);

            // Excluded Folders
            Options.Instance.DiskCleanerSettings.excludedDirs.Clear();
            foreach (ListViewItem lvi in this.listViewExcludeFolders.Items)
                Options.Instance.DiskCleanerSettings.excludedDirs.Add(lvi.Text);

            // Excluded Files
            Options.Instance.DiskCleanerSettings.excludedFileTypes.Clear();
            foreach (ListViewItem lvi in this.listViewFiles.Items)
                Options.Instance.DiskCleanerSettings.excludedFileTypes.Add(lvi.Text);

            this.Close();
        }

		private void btnFoldersRemove_Click(object sender, EventArgs e)
        {
            if (this.listViewExcludeFolders.SelectedItems.Count > 0 && this.listViewExcludeFolders.SelectedItems[0] != null)
            {
                if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.listViewExcludeFolders.SelectedItems[0].Remove();
                    this.listViewExcludeFolders.Refresh();
                }
            }
        }

		private void btnFilesRemove_Click(object sender, EventArgs e)
        {
			if (this.listViewFiles.SelectedItems.Count > 0 && this.listViewFiles.SelectedItems[0] != null)
            {
                if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.listViewFiles.SelectedItems[0].Remove();
                    this.listViewFiles.Refresh();
                }
            }
        }

		private void btnFilesAdd_Click(object sender, EventArgs e)
        {
            AddExcludeFileTypeForm addFileType = new AddExcludeFileTypeForm();
            addFileType.AddFileType += new AddFileTypeEventHandler(addFileType_AddFileType);
            addFileType.ShowDialog(this);
        }

        void addFileType_AddFileType(object sender, AddFileTypeEventArgs e)
        {
            this.listViewFiles.Items.Add(e.fileType);
            this.listViewFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

		private void btnFoldersAdd_Click(object sender, EventArgs e)
        {
            AddExcludeFolderForm addFolder = new AddExcludeFolderForm();
            addFolder.AddExcludeFolderDelegate += new AddExcludeFolderEventHandler(addFolder_AddExcludeFolder);
            addFolder.ShowDialog(this);
        }

        void addFolder_AddExcludeFolder(object sender, AddExcludeFolderEventArgs e)
        {
            this.listViewExcludeFolders.Items.Add(e.folderPath);
            this.listViewExcludeFolders.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

		private void btnIncFoldersAdd_Click(object sender, EventArgs e)
        {
            AddIncludeFolderForm addIncFolder = new AddIncludeFolderForm();
            addIncFolder.AddIncFolder += new AddIncFolderEventHandler(addIncFolder_AddIncFolder);
            addIncFolder.ShowDialog(this);
        }

        void addIncFolder_AddIncFolder(object sender, AddIncFolderEventArgs e)
        {
            this.listViewIncFolders.Items.Add(e.folderPath);
            this.listViewIncFolders.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

		private void btnIncFoldersRemove_Click(object sender, EventArgs e)
        {
            if (this.listViewIncFolders.SelectedItems.Count > 0 && this.listViewIncFolders.SelectedItems[0] != null)
            {
                if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.listViewIncFolders.SelectedItems[0].Remove();
                    this.listViewIncFolders.Refresh();
                }
            }
        }
		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if (fbdMoveToFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				this.txtMoveFolder.Text = fbdMoveToFolder.SelectedPath;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
    }
}
