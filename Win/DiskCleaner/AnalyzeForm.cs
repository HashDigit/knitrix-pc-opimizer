﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using Data;
using Win.DiskCleaner.Misc;

namespace Win.DiskCleaner
{
	public partial class AnalyzeForm : DevExpress.XtraEditors.XtraForm
    {
        public Thread threadMain
        {
            get;
            set;
        }

        public static List<DriveInfo> scanDrives
        {
            get;
            set;
        }

        public static List<FileInfo> fileList
        {
            get;
            set;
        }

        public static string CurrentFile
        {
            get;
            set;
        }

        public AnalyzeForm(List<DriveInfo> selectedDrives)
        {
            InitializeComponent();

            scanDrives = selectedDrives;
            if (AnalyzeForm.fileList == null)
                AnalyzeForm.fileList = new List<FileInfo>();
            else
                AnalyzeForm.fileList.Clear();

            this.timerUpdateFile.Start();

            this.threadMain = new Thread(new ThreadStart(AnalyzeDisk));
            this.threadMain.Start();
        }

        private void AnalyzeDisk()
        {
            try
            {
                foreach (DriveInfo driveInfo in scanDrives)
                {
                    ScanFiles(driveInfo.RootDirectory);
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (ThreadAbortException )
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
            }
        }

        private void ScanFiles(DirectoryInfo parentInfo)
        {
            try
            {
                foreach (FileInfo fileInfo in parentInfo.GetFiles())
                {
                    AnalyzeForm.CurrentFile = fileInfo.FullName;

                    // Check if file is exclude
                    if (FileTypeIsExcluded(fileInfo.Name))
                        continue;

                    // Check for zero-byte files
                    if (Options.Instance.DiskCleanerSettings.searchZeroByte)
                    {
                        if (fileInfo.Length == 0)
                        {
                            fileList.Add(fileInfo);
                            continue;
                        }
                    }

                    // Check if file matches types
                    if (!Utils.CompareWildcards(fileInfo.Name, Options.Instance.DiskCleanerSettings.searchFilters))
                        continue;

                    // Check if file is in use or write protected
                    if (Options.Instance.DiskCleanerSettings.ignoreWriteProtected && fileInfo.IsReadOnly)
                        continue;

                    // Check file attributes
                    if (!FileCheckAttributes(fileInfo))
                        continue;

                    // Check file dates
                    if (Options.Instance.DiskCleanerSettings.findFilesAfter || Options.Instance.DiskCleanerSettings.findFilesBefore)
                        if (!FileCheckDate(fileInfo))
                            continue;

                    // Check file size
                    if (Options.Instance.DiskCleanerSettings.checkFileSize)
                        if (!FileCheckSize(fileInfo))
                            continue;

                    AnalyzeForm.fileList.Add(fileInfo);
                }

                foreach (DirectoryInfo childInfo in parentInfo.GetDirectories())
                {
                    if (FolderIsIncluded(childInfo.FullName) && !FolderIsExcluded(childInfo.FullName))
                    {
                        AnalyzeForm.fileList.AddRange(childInfo.GetFiles());
                        continue;
                    }

                    if (!FolderIsExcluded(childInfo.FullName))
                        ScanFiles(childInfo);
                }
            }
            catch (Exception ex)
            {
                //TODO: Add better exception handling
            }
        }

        private bool FileCheckSize(FileInfo fileInfo)
        {
            long fileSize = fileInfo.Length / 1024;

            if (Options.Instance.DiskCleanerSettings.checkFileSizeLeast > 0)
                if (fileSize <= Options.Instance.DiskCleanerSettings.checkFileSizeLeast)
                    return false;

            if (Options.Instance.DiskCleanerSettings.checkFileSizeMost > 0)
                if (fileSize >= Options.Instance.DiskCleanerSettings.checkFileSizeMost)
                    return false;

            return true;
        }

        private bool FileCheckDate(FileInfo fileInfo)
        {
            DateTime dateTimeFile = DateTime.MinValue;
            bool bRet = false;

            if (Options.Instance.DiskCleanerSettings.findFilesMode == 0)
                dateTimeFile = fileInfo.CreationTime;
            else if (Options.Instance.DiskCleanerSettings.findFilesMode == 1)
                dateTimeFile = fileInfo.LastWriteTime;
            else if (Options.Instance.DiskCleanerSettings.findFilesMode == 2)
                dateTimeFile = fileInfo.LastAccessTime;

            if (Options.Instance.DiskCleanerSettings.findFilesAfter)
            {
                if (DateTime.Compare(dateTimeFile, Options.Instance.DiskCleanerSettings.dateTimeAfter) >= 0)
                    bRet = true;
            }

            if (Options.Instance.DiskCleanerSettings.findFilesBefore)
            {
                if (DateTime.Compare(dateTimeFile, Options.Instance.DiskCleanerSettings.dateTimeBefore) <= 0)
                    bRet = true;
            }

            return bRet;
        }

        /// <summary>
        /// Checks file attributes to match what user specified to search for
        /// </summary>
        /// <param name="fileInfo">File Information</param>
        /// <returns>True if file matches attributes</returns>
        private bool FileCheckAttributes(FileInfo fileInfo)
        {
            if ((!Options.Instance.DiskCleanerSettings.searchHidden) && ((fileInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden))
                return false;

            if ((!Options.Instance.DiskCleanerSettings.searchArchives) && ((fileInfo.Attributes & FileAttributes.Archive) == FileAttributes.Archive))
                return false;

            if ((!Options.Instance.DiskCleanerSettings.searchReadOnly) && ((fileInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly))
                return false;

            if ((!Options.Instance.DiskCleanerSettings.searchSystem) && ((fileInfo.Attributes & FileAttributes.System) == FileAttributes.System))
                return false;

            return true;
        }

        private bool FolderIsIncluded(string dirPath)
        {
            foreach (string includeDir in Options.Instance.DiskCleanerSettings.includedFolders)
            {
                if (string.Compare(includeDir, dirPath) == 0 || Utils.CompareWildcard(dirPath, includeDir))
                    return true;
            }

            return false;
        }

        private bool FolderIsExcluded(string dirPath)
        {
            foreach (string excludeDir in Options.Instance.DiskCleanerSettings.excludedDirs)
            {
                if (Utils.CompareWildcard(dirPath, excludeDir))
                    return true;
            }

            return false;
        }

        private bool FileTypeIsExcluded(string fileName)
        {
            foreach (string excludeFileType in Options.Instance.DiskCleanerSettings.excludedFileTypes)
            {
                if (Utils.CompareWildcard(fileName, excludeFileType))
                    return true;
            }

            return false;
        }

        private void timerUpdateFile_Tick(object sender, EventArgs e)
        {
            this.textBoxCurrentFile.Text = string.Copy(AnalyzeForm.CurrentFile);
            this.labelProblems.Text = string.Format("Files Found: {0}", AnalyzeForm.fileList.Count);
        }

		private void btnStop_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.Close();
        }

        private void Analyze_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.OK)
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        e.Cancel = true;
                    else
                        this.threadMain.Abort();
                }
            }
        }
    }
}
