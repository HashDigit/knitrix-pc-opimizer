﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Win.DiskCleaner.Misc
{
	public partial class AddExcludeFileTypeForm : DevExpress.XtraEditors.XtraForm
    {
        public event AddFileTypeEventHandler AddFileType;

        public AddExcludeFileTypeForm()
        {
            InitializeComponent();
        }

		private void btnOK_Click(object sender, EventArgs e)
        {
			if (string.IsNullOrEmpty(this.txtFileType.Text))
            {
                MessageBox.Show(this, "Please enter a file type", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (AddFileType != null)
            {
                AddFileTypeEventArgs eventArgs = new AddFileTypeEventArgs();
                eventArgs.fileType = this.txtFileType.Text;
                AddFileType(this, eventArgs);
            }

            this.Close();
        }
		
		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
    }

    public class AddFileTypeEventArgs : EventArgs
    {
        public string fileType
        {
            get;
            set;
        }
    }
    public delegate void AddFileTypeEventHandler(object sender, AddFileTypeEventArgs e);
}
