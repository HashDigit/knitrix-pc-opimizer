﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Win.DiskCleaner.Misc
{
	public partial class AddIncludeFolderForm : DevExpress.XtraEditors.XtraForm
    {
        public event AddIncFolderEventHandler AddIncFolder;

        public AddIncludeFolderForm()
        {
            InitializeComponent();
        }

		private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtFolder.Text))
            {
                MessageBox.Show(this, "Please enter a folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (AddIncFolder != null)
            {
                AddIncFolderEventArgs eventArgs = new AddIncFolderEventArgs();
                eventArgs.folderPath = this.txtFolder.Text;
                AddIncFolder(this, eventArgs);
            }

            this.Close();
        }

		private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDlg = new FolderBrowserDialog();
            browserDlg.ShowDialog(this);
            this.txtFolder.Text = browserDlg.SelectedPath;
        }

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
    }

    public class AddIncFolderEventArgs : EventArgs
    {
        public string folderPath
        {
            get;
            set;
        }
    }
    public delegate void AddIncFolderEventHandler(object sender, AddIncFolderEventArgs e);
}
