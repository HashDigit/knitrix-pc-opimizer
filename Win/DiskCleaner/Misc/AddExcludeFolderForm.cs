﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Win.DiskCleaner.Misc
{
	public partial class AddExcludeFolderForm : DevExpress.XtraEditors.XtraForm
    {
        public event AddExcludeFolderEventHandler AddExcludeFolderDelegate;

        public AddExcludeFolderForm()
        {
            InitializeComponent();
        }

		private void btnOK_Click(object sender, EventArgs e)
        {
			if (string.IsNullOrEmpty(this.txtFolder.Text))
            {
                MessageBox.Show(this, "Please enter a folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (AddExcludeFolderDelegate != null)
            {
                AddExcludeFolderEventArgs eventArgs = new AddExcludeFolderEventArgs();
                eventArgs.folderPath = this.txtFolder.Text;
                AddExcludeFolderDelegate(this, eventArgs);
            }

            this.Close();
        }

		private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDlg = new FolderBrowserDialog();
            browserDlg.ShowDialog(this);
            this.txtFolder.Text = browserDlg.SelectedPath;
        }

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
    }

    public class AddExcludeFolderEventArgs : EventArgs
    {
        public string folderPath
        {
            get;
            set;
        }
    }
    public delegate void AddExcludeFolderEventHandler(object sender, AddExcludeFolderEventArgs e);
}
