﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Win.DiskCleaner.Misc
{
    public partial class FileInfoCtrl : UserControl
    {
        public FileInfoCtrl()
        {
            InitializeComponent();

            ResetInfo();
        }

        public void UpdateInfo(FileInfo fileInfo)
        {
            if (fileInfo == null)
            {
                ResetInfo();
                return;
            }

            // Get icon
            Icon fileIcon = Icon.ExtractAssociatedIcon(fileInfo.FullName);
            if (fileIcon != null)
                this.pictureBox1.Image = fileIcon.ToBitmap();
            else
                this.pictureBox1.Image = SystemIcons.Application.ToBitmap();

            this.labelFileName.Text = "File Name: " + fileInfo.Name;
            this.labelFileSize.Text = "File Size: " + Utils.ConvertSizeToString(fileInfo.Length);
            this.labelLocation.Text = "Location: " + fileInfo.DirectoryName;
            this.labelAccessed.Text = "Last Accessed: " + fileInfo.LastAccessTime.ToLongDateString();
        }

        public void ResetInfo()
        {
            this.pictureBox1.Image = SystemIcons.Application.ToBitmap();

            this.labelFileName.Text = "File Name: ";
            this.labelFileSize.Text = "File Size: ";
            this.labelLocation.Text = "Location: ";
            this.labelAccessed.Text = "Last Accessed: ";
        }
    }
}
