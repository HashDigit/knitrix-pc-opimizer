﻿using System.Drawing;
using Win.Controls;

namespace Win
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Application Info");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Program Locations");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Software Settings");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Startup");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("System Drivers");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Shared DLLs");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Help Files");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Sound Events");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("History List");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Windows Fonts");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("ActiveX/COM");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Sections to Scan", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode18,
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23});
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tmrMain = new System.Windows.Forms.Timer(this.components);
            this.pnlSettings = new Win.Controls.DoublePanel();
            this.pnlSettingsMain = new Win.Controls.MainPanel();
            this.btnSettingsPrivacySettings = new Win.Controls.MainButton();
            this.btnSettingsGeneralSettings = new Win.Controls.MainButton();
            this.btnSettingsScanSchedule = new Win.Controls.MainButton();
            this.btnSettingsRegistrySettings = new Win.Controls.MainButton();
            this.pnlSettingsPrivacySettings = new Win.Controls.CommonPanel();
            this.tvaSettingsPrivacySettings = new Common_Tools.TreeViewAdv.Tree.TreeViewAdv();
            this.treeColumn14 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn24 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.nodeCheckBox4 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox();
            this.nodeIcon4 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeSection4 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeProblem4 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeLocation4 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.btnSettingsPrivacySettingsCancel = new Win.Controls.RegularButton();
            this.btnSettingsPrivacySettingsApply = new Win.Controls.RegularButton();
            this.pnlSettingsGeneralSettings = new Win.Controls.CommonPanel();
            this.pnlSettingsGeneralSettingsInside = new Win.Controls.DoublePanel();
            this.cbSettingsAutoStartApp = new System.Windows.Forms.CheckBox();
            this.cbSettingsMinimizeToTray = new System.Windows.Forms.CheckBox();
            this.cbSettingsAutoStartScan = new System.Windows.Forms.CheckBox();
            this.cbSettingsDisplayExitActions = new System.Windows.Forms.CheckBox();
            this.cbSettingsCreateRestorePointBeforeFixing = new System.Windows.Forms.CheckBox();
            this.cbSettingsAutoStartFix = new System.Windows.Forms.CheckBox();
            this.pnlSettingsScanSchedule = new Win.Controls.CommonPanel();
            this.btnSettingsScheduleDelete = new Win.Controls.RegularButton();
            this.lbSettingsSchedule = new DevExpress.XtraEditors.ListBoxControl();
            this.btnSettingsScheduleEdit = new Win.Controls.RegularButton();
            this.btnSettingsScheduleNew = new Win.Controls.RegularButton();
            this.pnlSettingsRegistrySettings = new Win.Controls.CommonPanel();
            this.pnlSettingsRegistrySettingsInside = new Win.Controls.DoublePanel();
            this.cbSettingsApplicationInfo = new System.Windows.Forms.CheckBox();
            this.cbSettingsWindowsFonts = new System.Windows.Forms.CheckBox();
            this.cbSettingsProgramLocations = new System.Windows.Forms.CheckBox();
            this.cbSettingsHistoryList = new System.Windows.Forms.CheckBox();
            this.cbSettingsSoftwareSettings = new System.Windows.Forms.CheckBox();
            this.cbSettingsSharedDLLs = new System.Windows.Forms.CheckBox();
            this.cbSettingsSystemDrivers = new System.Windows.Forms.CheckBox();
            this.cbSettingsHelpFiles = new System.Windows.Forms.CheckBox();
            this.cbSettingsStartup = new System.Windows.Forms.CheckBox();
            this.cbSettingsSoundEvents = new System.Windows.Forms.CheckBox();
            this.cbSettingsActiveXCOM = new System.Windows.Forms.CheckBox();
            this.pnlDisk = new Win.Controls.DoublePanel();
            this.pnlDiskMain = new Win.Controls.MainPanel();
            this.btnDiskClearRecentHistory = new Win.Controls.MainButton();
            this.btnDiskClearTempraryFiles = new Win.Controls.MainButton();
            this.btnDiskDiskCleaner = new Win.Controls.MainButton();
            this.btnDiskRestorePointManager = new Win.Controls.MainButton();
            this.btnDiskShortcutFixer = new Win.Controls.MainButton();
            this.btnDiskUninstallManager = new Win.Controls.MainButton();
            this.pnlDiskRestorePointManager = new Win.Controls.CommonPanel();
            this.lvDiskRestorePoint = new System.Windows.Forms.ListView();
            this.colDiskRestorePointNumber = new System.Windows.Forms.ColumnHeader();
            this.colDiskRestorePointDateTime = new System.Windows.Forms.ColumnHeader();
            this.colDiskRestorePointDescription = new System.Windows.Forms.ColumnHeader();
            this.colDiskRestorePointType = new System.Windows.Forms.ColumnHeader();
            this.btnDiskRemoveRestorePoint = new Win.Controls.RegularButton();
            this.btnDiskRestoreRestorePoint = new Win.Controls.RegularButton();
            this.txtDistRestorePointDescription = new DevExpress.XtraEditors.TextEdit();
            this.btnDiskCreateRestorePoint = new Win.Controls.RegularButton();
            this.lblDistRestorePointDescription = new System.Windows.Forms.Label();
            this.pnlDiskDiskCleaner = new Win.Controls.CommonPanel();
            this.lvDiskDiskCleaner = new System.Windows.Forms.ListView();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            this.cmnuDiskDiskCleaner = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiDiskDiskCleanerSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskDiskCleanerSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskDiskCleanerInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDiskDiskCleanerOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskDiskCleanerProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDiskDiskCleanerOptions = new Win.Controls.RegularButton();
            this.fileInfoCtrl1 = new Win.DiskCleaner.Misc.FileInfoCtrl();
            this.btnDiskDiskCleanerProperties = new Win.Controls.RegularButton();
            this.btnDiskDiskCleanerOpenFile = new Win.Controls.RegularButton();
            this.btnDiskDiskCleanerCleanFiles = new Win.Controls.RegularButton();
            this.btnDiskDiskCleanerScanDisks = new Win.Controls.RegularButton();
            this.pnlDiskShortcutFixer = new Win.Controls.CommonPanel();
            this.lvDiskShortcutFixerFiles = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.cmnuDiskShortcutFixer = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiDiskShortcutFixerDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerChangeTarget = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDiskShortcutFixerSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDiskShortcutFixerProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDiskShortcutFixerOpenFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.ilDiskShortcutFixer = new System.Windows.Forms.ImageList(this.components);
            this.btnDiskShortcutFixerDeleteSelected = new Win.Controls.RegularButton();
            this.lblDiskShortcutFixerStatus = new DevExpress.XtraEditors.LabelControl();
            this.btnDiskShortcutFixerScan = new Win.Controls.RegularButton();
            this.pnlDiskUninstallManager = new Win.Controls.CommonPanel();
            this.lvDiskUninstallManager = new System.Windows.Forms.ListView();
            this.colDiskUninstallManagerProgram = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerPublisher = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerEstimatedSize = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerInstallLocation = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerVersion = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerContact = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerHelpLink = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerInstallSource = new System.Windows.Forms.ColumnHeader();
            this.colDiskUninstallManagerUrlInfoAbout = new System.Windows.Forms.ColumnHeader();
            this.ilDiskUninstallManager = new System.Windows.Forms.ImageList(this.components);
            this.btnDiskUninstallManagerUninstall = new Win.Controls.RegularButton();
            this.txtDiskUninstallManagerSearchFilter = new System.Windows.Forms.TextBox();
            this.lblDiskUninstallManagerSearchFilter = new DevExpress.XtraEditors.LabelControl();
            this.btnDiskUninstallManagerRemoveEntry = new Win.Controls.RegularButton();
            this.pnlOptimize = new Win.Controls.DoublePanel();
            this.pnlOptimizeMain = new Win.Controls.MainPanel();
            this.btnOptimizeRegistryDefragmenter = new Win.Controls.MainButton();
            this.btnOptimizeBrowserObjectManager = new Win.Controls.MainButton();
            this.btnOptimizeStartupManager = new Win.Controls.MainButton();
            this.btnOptimizeProcessManager = new Win.Controls.MainButton();
            this.btnOptimizeOptimizeWindows = new Win.Controls.MainButton();
            this.btnOptimizeRegistryManager = new Win.Controls.MainButton();
            this.pnlOptimizeStartupManager = new Win.Controls.CommonPanel();
            this.tvaOptimizeStartupManager = new Common_Tools.TreeViewAdv.Tree.TreeViewAdv();
            this.treeColumn15 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn25 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn35 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.nodeIcon15 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeTextBoxSection5 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxItem5 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxPath5 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxArgs5 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.btnOptimizeStartupManagerRefresh = new Win.Controls.RegularButton();
            this.btnOptimizeStartupManagerRun = new Win.Controls.RegularButton();
            this.btnOptimizeStartupManagerEdit = new Win.Controls.RegularButton();
            this.btnOptimizeStartupManagerView = new Win.Controls.RegularButton();
            this.btnOptimizeStartupManagerDelete = new Win.Controls.RegularButton();
            this.btnOptimizeStartupManagerAdd = new Win.Controls.RegularButton();
            this.pnlOptimizeOptimizeWindows = new Win.Controls.CommonPanel();
            this.pnlOptimizeOptimizeWindowsInside = new Win.Controls.DoublePanel();
            this.cbOptimizeClearPageFileOnReboot = new System.Windows.Forms.CheckBox();
            this.cbOptimizeAllowHybernation = new System.Windows.Forms.CheckBox();
            this.cbOptimizeEnableDesktopSearch = new System.Windows.Forms.CheckBox();
            this.cbOptimizeSendErrorsToMicrosoft = new System.Windows.Forms.CheckBox();
            this.cbOptimizeEnablePrefetch = new System.Windows.Forms.CheckBox();
            this.pnlOptimizeRegistryManager = new Win.Controls.CommonPanel();
            this.btnOptimizeRegistryManagerScan = new Win.Controls.RegularButton();
            this.tvaOptimizeRegistryManager = new Common_Tools.TreeViewAdv.Tree.TreeViewAdv();
            this.treeColumn1 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn2 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn3 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.cmnuOptimizeRegistryManager = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOptimizeRegistryManagerSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiOptimizeRegistryManagerExcludeSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerViewinRegEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeCheckBox = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox();
            this.nodeIcon = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeSection = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeProblem = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeLocation = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeValueName = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageListTreeView = new System.Windows.Forms.ImageList(this.components);
            this.btnOptimizeRegistryManagerFixAll = new Win.Controls.PictureBoxButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnOptimizeRegistryManagerIgnoreList = new Win.Controls.RegularButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pnlOptimizeProcessManager = new Win.Controls.CommonPanel();
            this.tabOptimizeProcessManager = new DevExpress.XtraTab.XtraTabControl();
            this.tpProcesses = new DevExpress.XtraTab.XtraTabPage();
            this.btnOptimizeEndProcess = new Win.Controls.RegularButton();
            this.btnOptimizeOpenFolder = new Win.Controls.RegularButton();
            this.btnOptimizeProperties = new Win.Controls.RegularButton();
            this.lblOptimizeImage4Label = new System.Windows.Forms.Label();
            this.pbOptimizeImage4 = new System.Windows.Forms.PictureBox();
            this.lblOptimizeImage3Label = new System.Windows.Forms.Label();
            this.pbOptimizeImage3 = new System.Windows.Forms.PictureBox();
            this.lblOptimizeImage2Label = new System.Windows.Forms.Label();
            this.pbOptimizeImage2 = new System.Windows.Forms.PictureBox();
            this.lblOptimizeImage1Label = new System.Windows.Forms.Label();
            this.pbOptimizeImage1 = new System.Windows.Forms.PictureBox();
            this.groupOptimizeProcessDetails = new DevExpress.XtraEditors.GroupControl();
            this.lblOptimizeSelectedProcessPMemoryUsage = new System.Windows.Forms.Label();
            this.smdbOptimizeSelectedProcessPMemory = new Win.Controls.SystemMonitorDataBar();
            this.lblOptimizeSelectedProcessCpuUsage = new System.Windows.Forms.Label();
            this.smdbOptimizeSelectedProcessCpu = new Win.Controls.SystemMonitorDataBar();
            this.smdcOptimizeSelectedProcess = new Win.Controls.SystemMonitorDataChartAdv();
            this.lblOptimizeVirtualMemorySizeValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeVirtualMemorySize = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeTotalProcessorTimeValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeTotalProcessorTime = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizePriorityValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeHandlesValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeThreadsValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeCompanyValue = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeProcessIdValue = new DevExpress.XtraEditors.LabelControl();
            this.pbOptimizeProcessImage = new System.Windows.Forms.PictureBox();
            this.lblOptimizePriority = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeHandles = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeThreads = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeCompany = new DevExpress.XtraEditors.LabelControl();
            this.lblOptimizeProcessId = new DevExpress.XtraEditors.LabelControl();
            this.gvOptimizeProcess = new System.Windows.Forms.DataGridView();
            this.col_OptimizeWindow_Process_InfoTypeImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.col_OptimizeWindow_Process_InfoType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.col_OptimizeWindow_Process_ProcessName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCpu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMemory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSigned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOptimizeOptimizeAll = new Win.Controls.RegularButton();
            this.tpPerformance = new DevExpress.XtraTab.XtraTabPage();
            this.groupOptimizeMemoryUsage = new DevExpress.XtraEditors.GroupControl();
            this.lblOptimizeVMemoryUsage = new System.Windows.Forms.Label();
            this.smdbOptimizeVMemory = new Win.Controls.SystemMonitorDataBar();
            this.lblOptimizePMemoryUsage = new System.Windows.Forms.Label();
            this.smdbOptimizePMemory = new Win.Controls.SystemMonitorDataBar();
            this.smdcOptimizeMemory = new Win.Controls.SystemMonitorDataChart();
            this.groupOptimizeCpuUsage = new DevExpress.XtraEditors.GroupControl();
            this.lblOptimizeCpuUsage = new System.Windows.Forms.Label();
            this.smdbOptimizeCpu = new Win.Controls.SystemMonitorDataBar();
            this.smdcOptimizeCpu = new Win.Controls.SystemMonitorDataChart();
            this.pnlOptimizeBrowseObjectManager = new Win.Controls.CommonPanel();
            this.lvOptimizeBrowseObjectManager = new System.Windows.Forms.ListView();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.cmnuOptimizeBrowseObjectManager = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOptimizeBrowseObjectManagerSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeBrowseObjectManagerSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeBrowseObjectManagerInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeBrowseObjectManagerSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiOptimizeBrowseObjectManagerProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.ilOptimizeBrowseObjectManager = new System.Windows.Forms.ImageList(this.components);
            this.btnOptimizeBrowseObjectManagerEnableAll = new Win.Controls.RegularButton();
            this.lblOptimizeBrowserObjectManagerItemSelected = new System.Windows.Forms.Label();
            this.btnOptimizeBrowseObjectManagerDisableAll = new Win.Controls.RegularButton();
            this.pnlOptimizeRegistryDefragmenter = new Win.Controls.CommonPanel();
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize = new System.Windows.Forms.Label();
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize = new System.Windows.Forms.Label();
            this.lblOptimizeRegistryDefragmenterTotal = new System.Windows.Forms.Label();
            this.lvOptimizeRegistryDefragmenter = new System.Windows.Forms.ListView();
            this.colOptimizeRegistryDefragmenterHivePath = new System.Windows.Forms.ColumnHeader();
            this.colOptimizeRegistryDefragmenterOldHiveSize = new System.Windows.Forms.ColumnHeader();
            this.colOptimizeRegistryDefragmenterNewHiveSize = new System.Windows.Forms.ColumnHeader();
            this.btnOptimizeRegistryDefragmenterOptimize = new Win.Controls.RegularButton();
            this.pbOptimizeRegistryDefragmenter = new System.Windows.Forms.ProgressBar();
            this.lblOptimizeRegistryDefragmenterStatus = new System.Windows.Forms.Label();
            this.btnOptimizeRegistryDefragmenterAnalyze = new Win.Controls.RegularButton();
            this.cmnuOptimizeStartupManager = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOptimizeStartupManagerEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeStartupManagerDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeStartupManagerView = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeStartupManagerRun = new System.Windows.Forms.ToolStripMenuItem();
            this.tvaHomeStep2Problem = new Common_Tools.TreeViewAdv.Tree.TreeViewAdv();
            this.treeColumn12 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn22 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn32 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.nodeCheckBox2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox();
            this.nodeIcon2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeSection2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeProblem2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeLocation2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeValueName2 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.tvaHomeStep3Problem = new Common_Tools.TreeViewAdv.Tree.TreeViewAdv();
            this.treeColumn13 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn23 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn33 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.nodeCheckBox3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox();
            this.nodeIcon3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeSection3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeProblem3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeLocation3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeValueName3 = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.pnlHome = new Win.Controls.DoublePanel();
            this.pnlHomeMain = new Win.Controls.MainPanel();
            this.btnHomeStartupManager = new Win.Controls.MainButton();
            this.btnHomeClearTempraryFiles = new Win.Controls.MainButton();
            this.btnHomeRegistryManager = new Win.Controls.MainButton();
            //this.btnHomeTitle2 = new System.Windows.Forms.Label();
            //this.btnHomeTitle1 = new System.Windows.Forms.Label();
            this.btnHomeStartScan = new Win.Controls.PictureBoxButton();
            this.btnHomeDiskCleaner = new Win.Controls.MainButton();
            //this.btnHomeTitle3 = new System.Windows.Forms.Label();
            this.pnlHomeStep3 = new Win.Controls.CommonPanel();
            this.btnHomeStep3Cancel = new Win.Controls.RegularButton();
            this.btnHomeStep3FixAll = new Win.Controls.PictureBoxButton();
            this.lblHomeStep3DetectedItems = new Win.Controls.DetectedItemsLabel();
            this.osgHomeStep3OverallScanResult = new Win.Controls.OverallScanGridControl();
            this.pnlHomeStep4Fixing = new Win.Controls.DoublePanel();
            this.pbHomeStep4Progress = new System.Windows.Forms.ProgressBar();
            this.lblHomeStep4ProgressValue = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeStep4ProgressTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeStep4CurrentFixedObject = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeStep4CurrentFixer = new DevExpress.XtraEditors.LabelControl();
            this.pnlHomeStep2 = new Win.Controls.CommonPanel();
            this.btnHomeStep2Cancel = new Win.Controls.RegularButton();
            this.sucHomeStep2Privacy = new Win.Controls.ScanningUserControl();
            this.sucHomeStep2Registry = new Win.Controls.ScanningUserControl();
            this.sucHomeStep2Process = new Win.Controls.ScanningUserControl();
            this.lblHomeStep2ProgressValue = new DevExpress.XtraEditors.LabelControl();
            this.pbHomeStep2Progress = new System.Windows.Forms.ProgressBar();
            this.lblHomeStep2ProgressTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeStep2CurrentScannedObject = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeStep2CurrentScanner = new DevExpress.XtraEditors.LabelControl();
            this.btnHomeStep2Stop = new Win.Controls.RegularButton();
            this.lblHomeStep2DetectedItems = new Win.Controls.DetectedItemsLabel();
            this.pnlHomeInfo = new Win.Controls.CommonPanel();
            this.groupHomeInfoUpdates = new DevExpress.XtraEditors.GroupControl();
            this.cbHomeInfoAutocheckForUpdates = new DevExpress.XtraEditors.CheckEdit();
            this.lblHomeIntoCheckForUpdatesProgress = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeIntoCheckForUpdatesTotalSize = new DevExpress.XtraEditors.LabelControl();
            this.pbHomeIntoCheckForUpdatesProgress = new System.Windows.Forms.ProgressBar();
            this.lblHomeInfoCheckForUpdatesLatDateTime = new DevExpress.XtraEditors.LabelControl();
            this.btnHomeIntoCheckForUpdatesCancel = new Win.Controls.RegularButton();
            this.btnHomeIntoCheckForUpdates = new Win.Controls.RegularButton();
            this.lblHomeInfoVersion = new System.Windows.Forms.Label();
            this.groupHomeInfoRegistration = new DevExpress.XtraEditors.GroupControl();
            this.btnHomeIntoBuyNow = new Win.Controls.RegularButton();
            this.btnHomeInfoLicenseKeyOk = new Win.Controls.PictureBoxButton();
            this.lblHomeInfoLicenseStatusValue = new DevExpress.XtraEditors.LabelControl();
            this.lblHomeInfoLicenseStatusTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtHomeInfoLicenseKey = new DevExpress.XtraEditors.TextEdit();
            this.lblHomeInfoLicenseKey = new DevExpress.XtraEditors.LabelControl();
            this.fbdDiskFolders = new System.Windows.Forms.FolderBrowserDialog();
            this.tmrOneClickScan = new System.Windows.Forms.Timer(this.components);
            this.tmrOneClickFix = new System.Windows.Forms.Timer(this.components);
            this.niMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmnuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fbdDiskShortcutFixer = new System.Windows.Forms.FolderBrowserDialog();
            this.ofdDiskShortcutFixer = new System.Windows.Forms.OpenFileDialog();
            this.ilOptimizeStartupManager = new System.Windows.Forms.ImageList(this.components);
            this.btnInfo = new Win.Controls.PictureBoxButton();
            this.btnHelp = new Win.Controls.PictureBoxButton();
            this.cbMain = new MonolitForm.ControlBoxControl();
            this.tabMain = new Win.Controls.TabUserControl();
            this.pnlNetworkSecurityStatus = new Win.Controls.DoublePanel();
            this.pnlSettings.SuspendLayout();
            this.pnlSettingsMain.SuspendLayout();
            this.pnlSettingsPrivacySettings.SuspendLayout();
            this.pnlSettingsGeneralSettings.SuspendLayout();
            this.pnlSettingsGeneralSettingsInside.SuspendLayout();
            this.pnlSettingsScanSchedule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbSettingsSchedule)).BeginInit();
            this.pnlSettingsRegistrySettings.SuspendLayout();
            this.pnlSettingsRegistrySettingsInside.SuspendLayout();
            this.pnlDisk.SuspendLayout();
            this.pnlDiskMain.SuspendLayout();
            this.pnlDiskRestorePointManager.SuspendLayout();
            this.pnlDiskDiskCleaner.SuspendLayout();
            this.cmnuDiskDiskCleaner.SuspendLayout();
            this.pnlDiskShortcutFixer.SuspendLayout();
            this.cmnuDiskShortcutFixer.SuspendLayout();
            this.pnlDiskUninstallManager.SuspendLayout();
            this.pnlOptimize.SuspendLayout();
            this.pnlOptimizeMain.SuspendLayout();
            this.pnlOptimizeStartupManager.SuspendLayout();
            this.pnlOptimizeOptimizeWindows.SuspendLayout();
            this.pnlOptimizeOptimizeWindowsInside.SuspendLayout();
            this.pnlOptimizeRegistryManager.SuspendLayout();
            this.cmnuOptimizeRegistryManager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOptimizeRegistryManagerFixAll)).BeginInit();
            this.pnlOptimizeProcessManager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabOptimizeProcessManager)).BeginInit();
            this.tabOptimizeProcessManager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeProcessDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeProcessImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOptimizeProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeMemoryUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeCpuUsage)).BeginInit();
            this.pnlOptimizeBrowseObjectManager.SuspendLayout();
            this.cmnuOptimizeBrowseObjectManager.SuspendLayout();
            this.pnlOptimizeRegistryDefragmenter.SuspendLayout();
            this.cmnuOptimizeStartupManager.SuspendLayout();
            this.pnlHome.SuspendLayout();
            this.pnlHomeMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeStartScan)).BeginInit();
            this.pnlHomeStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeStep3FixAll)).BeginInit();
            this.pnlHomeStep4Fixing.SuspendLayout();
            this.pnlHomeStep2.SuspendLayout();
            this.pnlHomeInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeInfoUpdates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeInfoRegistration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeInfoLicenseKeyOk)).BeginInit();
            this.cmnuTray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "iMaginary";
            // 
            // tmrMain
            // 
            this.tmrMain.Interval = 1000;
            this.tmrMain.Tick += new System.EventHandler(this.tmrMain_Tick);
            // 
            // pnlSettings
            // 
            this.pnlSettings.BackColor = System.Drawing.Color.White;
            this.pnlSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettings.Controls.Add(this.pnlSettingsMain);
            this.pnlSettings.Controls.Add(this.pnlSettingsPrivacySettings);
            this.pnlSettings.Controls.Add(this.pnlSettingsGeneralSettings);
            this.pnlSettings.Controls.Add(this.pnlSettingsScanSchedule);
            this.pnlSettings.Controls.Add(this.pnlSettingsRegistrySettings);
            this.pnlSettings.Location = new System.Drawing.Point(8, 73);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(741, 416);
            this.pnlSettings.TabIndex = 2;
            // 
            // pnlSettingsMain
            // 
            this.pnlSettingsMain.BackgroundImage = global::Win.Properties.Resources.panel_background_main_left;
            this.pnlSettingsMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettingsMain.Caption = "Settings";
            this.pnlSettingsMain.Controls.Add(this.btnSettingsPrivacySettings);
            this.pnlSettingsMain.Controls.Add(this.btnSettingsGeneralSettings);
            this.pnlSettingsMain.Controls.Add(this.btnSettingsScanSchedule);
            this.pnlSettingsMain.Controls.Add(this.btnSettingsRegistrySettings);
            this.pnlSettingsMain.Description = "Modify your scan, schedule and other Knitrix tool settings";
            this.pnlSettingsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsMain.HideLogo = false;
            this.pnlSettingsMain.Icon = global::Win.Properties.Resources.settings_64x64;
            this.pnlSettingsMain.IsIconOnLeft = true;
            this.pnlSettingsMain.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsMain.Name = "pnlSettingsMain";
            this.pnlSettingsMain.NavBarImage = global::Win.Properties.Resources.settings_nav_bar;
            this.pnlSettingsMain.SeparatorCount = 1;
            this.pnlSettingsMain.Size = new System.Drawing.Size(741, 416);
            this.pnlSettingsMain.TabIndex = 19;
            this.pnlSettingsMain.RegisterClick += new System.EventHandler(this.btnRegister_Click);
            this.pnlSettingsMain.PushToTalkClick += new System.EventHandler(this.btnPushToTalkClick_Click);
            // 
            // btnSettingsPrivacySettings
            // 
            this.btnSettingsPrivacySettings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSettingsPrivacySettings.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsPrivacySettings.Description = "Select items for Knitrix PC Optimizer to include in its privacy scan.";
            this.btnSettingsPrivacySettings.Icon = global::Win.Properties.Resources.privacy_64x64;
            this.btnSettingsPrivacySettings.Location = new System.Drawing.Point(188, 92);
            this.btnSettingsPrivacySettings.Name = "btnSettingsPrivacySettings";
            this.btnSettingsPrivacySettings.Size = new System.Drawing.Size(285, 108);
            this.btnSettingsPrivacySettings.TabIndex = 82;
            this.btnSettingsPrivacySettings.Title = "Privacy Settings";
            this.btnSettingsPrivacySettings.ButtonClick += new System.EventHandler(this.btnSettingsPrivacySettings_Click);
            // 
            // btnSettingsGeneralSettings
            // 
            this.btnSettingsGeneralSettings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSettingsGeneralSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsGeneralSettings.Description = "Customize the basic settings of Knitrix PC Optimizer.";
            this.btnSettingsGeneralSettings.Icon = global::Win.Properties.Resources.general_64x64;
            this.btnSettingsGeneralSettings.Location = new System.Drawing.Point(473, 92);
            this.btnSettingsGeneralSettings.Name = "btnSettingsGeneralSettings";
            this.btnSettingsGeneralSettings.Size = new System.Drawing.Size(285, 108);
            this.btnSettingsGeneralSettings.TabIndex = 79;
            this.btnSettingsGeneralSettings.Title = "General Settings";
            this.btnSettingsGeneralSettings.ButtonClick += new System.EventHandler(this.btnSettingsGeneralSettings_Click);
            // 
            // btnSettingsScanSchedule
            // 
            this.btnSettingsScanSchedule.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSettingsScanSchedule.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsScanSchedule.Description = "Setup automatic scans to keep your computer healthy.";
            this.btnSettingsScanSchedule.Icon = global::Win.Properties.Resources.schedule_64x64;
            this.btnSettingsScanSchedule.Location = new System.Drawing.Point(473, 240);
            this.btnSettingsScanSchedule.Name = "btnSettingsScanSchedule";
            this.btnSettingsScanSchedule.Size = new System.Drawing.Size(285, 108);
            this.btnSettingsScanSchedule.TabIndex = 80;
            this.btnSettingsScanSchedule.Title = "Scan Schedule";
            this.btnSettingsScanSchedule.ButtonClick += new System.EventHandler(this.btnSettingsScanSchedule_Click);
            // 
            // btnSettingsRegistrySettings
            // 
            this.btnSettingsRegistrySettings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSettingsRegistrySettings.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsRegistrySettings.Description = "Select registry items you want Knitrix PC Optimizer to scan.";
            this.btnSettingsRegistrySettings.Icon = global::Win.Properties.Resources.registry_64x64;
            this.btnSettingsRegistrySettings.Location = new System.Drawing.Point(188, 240);
            this.btnSettingsRegistrySettings.Name = "btnSettingsRegistrySettings";
            this.btnSettingsRegistrySettings.Size = new System.Drawing.Size(285, 108);
            this.btnSettingsRegistrySettings.TabIndex = 81;
            this.btnSettingsRegistrySettings.Title = "Registry Settings";
            this.btnSettingsRegistrySettings.ButtonClick += new System.EventHandler(this.btnSettingsRegistrySettings_Click);
            // 
            // pnlSettingsPrivacySettings
            // 
            this.pnlSettingsPrivacySettings.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlSettingsPrivacySettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettingsPrivacySettings.Caption = "Privacy Scan Settings";
            this.pnlSettingsPrivacySettings.Controls.Add(this.tvaSettingsPrivacySettings);
            this.pnlSettingsPrivacySettings.Controls.Add(this.btnSettingsPrivacySettingsCancel);
            this.pnlSettingsPrivacySettings.Controls.Add(this.btnSettingsPrivacySettingsApply);
            this.pnlSettingsPrivacySettings.Description = "Select items for Knitrix PC Optimizer to include in its privacy scan";
            this.pnlSettingsPrivacySettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsPrivacySettings.HideLogo = true;
            this.pnlSettingsPrivacySettings.Icon = global::Win.Properties.Resources.privacy_64x64;
            this.pnlSettingsPrivacySettings.IsIconOnLeft = true;
            this.pnlSettingsPrivacySettings.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsPrivacySettings.Name = "pnlSettingsPrivacySettings";
            this.pnlSettingsPrivacySettings.NavBarImage = global::Win.Properties.Resources.privacy_settings_nav_bar;
            this.pnlSettingsPrivacySettings.Size = new System.Drawing.Size(741, 416);
            this.pnlSettingsPrivacySettings.TabIndex = 20;
            // 
            // tvaSettingsPrivacySettings
            // 
            this.tvaSettingsPrivacySettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvaSettingsPrivacySettings.BackColor = System.Drawing.SystemColors.Window;
            this.tvaSettingsPrivacySettings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvaSettingsPrivacySettings.Columns.Add(this.treeColumn14);
            this.tvaSettingsPrivacySettings.Columns.Add(this.treeColumn24);
            this.tvaSettingsPrivacySettings.DefaultToolTipProvider = null;
            this.tvaSettingsPrivacySettings.DragDropMarkColor = System.Drawing.Color.Black;
            this.tvaSettingsPrivacySettings.FullRowSelect = true;
            this.tvaSettingsPrivacySettings.GridLineStyle = Common_Tools.TreeViewAdv.Tree.GridLineStyle.Horizontal;
            this.tvaSettingsPrivacySettings.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tvaSettingsPrivacySettings.LoadOnDemand = true;
            this.tvaSettingsPrivacySettings.Location = new System.Drawing.Point(179, 55);
            this.tvaSettingsPrivacySettings.Model = null;
            this.tvaSettingsPrivacySettings.Name = "tvaSettingsPrivacySettings";
            this.tvaSettingsPrivacySettings.NodeControls.Add(this.nodeCheckBox4);
            this.tvaSettingsPrivacySettings.NodeControls.Add(this.nodeIcon4);
            this.tvaSettingsPrivacySettings.NodeControls.Add(this.nodeSection4);
            this.tvaSettingsPrivacySettings.NodeControls.Add(this.nodeProblem4);
            this.tvaSettingsPrivacySettings.NodeControls.Add(this.nodeLocation4);
            this.tvaSettingsPrivacySettings.SelectedNode = null;
            this.tvaSettingsPrivacySettings.SelectionMode = Common_Tools.TreeViewAdv.Tree.TreeSelectionMode.Multi;
            this.tvaSettingsPrivacySettings.Size = new System.Drawing.Size(560, 307);
            this.tvaSettingsPrivacySettings.TabIndex = 0;
            this.tvaSettingsPrivacySettings.UseColumns = true;
            this.tvaSettingsPrivacySettings.ColumnClicked += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeColumnEventArgs>(this.treeViewAdvResults_ColumnClicked);
            this.tvaSettingsPrivacySettings.Expanded += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeViewAdvEventArgs>(this.treeViewAdvResults_Expanded);
            // 
            // treeColumn14
            // 
            this.treeColumn14.Header = "Name";
            this.treeColumn14.Sortable = true;
            this.treeColumn14.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn14.TooltipText = null;
            this.treeColumn14.Width = 75;
            // 
            // treeColumn24
            // 
            this.treeColumn24.Header = "Location";
            this.treeColumn24.Sortable = true;
            this.treeColumn24.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn24.TooltipText = null;
            this.treeColumn24.Width = 175;
            // 
            // nodeCheckBox4
            // 
            this.nodeCheckBox4.DataPropertyName = "Checked";
            this.nodeCheckBox4.EditEnabled = true;
            this.nodeCheckBox4.LeftMargin = 0;
            this.nodeCheckBox4.ParentColumn = this.treeColumn14;
            // 
            // nodeIcon4
            // 
            this.nodeIcon4.DataPropertyName = "Img";
            this.nodeIcon4.LeftMargin = 1;
            this.nodeIcon4.ParentColumn = this.treeColumn14;
            this.nodeIcon4.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeSection4
            // 
            this.nodeSection4.DataPropertyName = "SectionName";
            this.nodeSection4.IncrementalSearchEnabled = true;
            this.nodeSection4.LeftMargin = 3;
            this.nodeSection4.ParentColumn = this.treeColumn14;
            this.nodeSection4.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeProblem4
            // 
            this.nodeProblem4.DataPropertyName = "Problem";
            this.nodeProblem4.IncrementalSearchEnabled = true;
            this.nodeProblem4.LeftMargin = 3;
            this.nodeProblem4.ParentColumn = this.treeColumn14;
            this.nodeProblem4.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeLocation4
            // 
            this.nodeLocation4.DataPropertyName = "RegKeyPath";
            this.nodeLocation4.IncrementalSearchEnabled = true;
            this.nodeLocation4.LeftMargin = 3;
            this.nodeLocation4.ParentColumn = this.treeColumn24;
            this.nodeLocation4.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // btnSettingsPrivacySettingsCancel
            // 
            this.btnSettingsPrivacySettingsCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettingsPrivacySettingsCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsPrivacySettingsCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSettingsPrivacySettingsCancel.ForeColor = System.Drawing.Color.White;
            this.btnSettingsPrivacySettingsCancel.Location = new System.Drawing.Point(632, 377);
            this.btnSettingsPrivacySettingsCancel.Name = "btnSettingsPrivacySettingsCancel";
            this.btnSettingsPrivacySettingsCancel.Size = new System.Drawing.Size(91, 27);
            this.btnSettingsPrivacySettingsCancel.TabIndex = 92;
            this.btnSettingsPrivacySettingsCancel.Text = "Cancel";
            this.btnSettingsPrivacySettingsCancel.Click += new System.EventHandler(this.btnSettingsPrivacySettingsCancel_Click);
            // 
            // btnSettingsPrivacySettingsApply
            // 
            this.btnSettingsPrivacySettingsApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettingsPrivacySettingsApply.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsPrivacySettingsApply.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSettingsPrivacySettingsApply.ForeColor = System.Drawing.Color.White;
            this.btnSettingsPrivacySettingsApply.Location = new System.Drawing.Point(535, 377);
            this.btnSettingsPrivacySettingsApply.Name = "btnSettingsPrivacySettingsApply";
            this.btnSettingsPrivacySettingsApply.Size = new System.Drawing.Size(91, 27);
            this.btnSettingsPrivacySettingsApply.TabIndex = 91;
            this.btnSettingsPrivacySettingsApply.Text = "Apply";
            this.btnSettingsPrivacySettingsApply.Click += new System.EventHandler(this.btnSettingsPrivacySettingsApply_Click);
            // 
            // pnlSettingsGeneralSettings
            // 
            this.pnlSettingsGeneralSettings.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlSettingsGeneralSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettingsGeneralSettings.Caption = "General Settings";
            this.pnlSettingsGeneralSettings.Controls.Add(this.pnlSettingsGeneralSettingsInside);
            this.pnlSettingsGeneralSettings.Description = "Customize how Knitrix PC Optimizer behaves";
            this.pnlSettingsGeneralSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsGeneralSettings.HideLogo = false;
            this.pnlSettingsGeneralSettings.Icon = global::Win.Properties.Resources.general_64x64;
            this.pnlSettingsGeneralSettings.IsIconOnLeft = true;
            this.pnlSettingsGeneralSettings.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsGeneralSettings.Name = "pnlSettingsGeneralSettings";
            this.pnlSettingsGeneralSettings.NavBarImage = global::Win.Properties.Resources.general_settings_nav_bar;
            this.pnlSettingsGeneralSettings.Size = new System.Drawing.Size(741, 416);
            this.pnlSettingsGeneralSettings.TabIndex = 22;
            // 
            // pnlSettingsGeneralSettingsInside
            // 
            this.pnlSettingsGeneralSettingsInside.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSettingsGeneralSettingsInside.BackColor = System.Drawing.Color.Transparent;
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsAutoStartApp);
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsMinimizeToTray);
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsAutoStartScan);
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsDisplayExitActions);
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsCreateRestorePointBeforeFixing);
            this.pnlSettingsGeneralSettingsInside.Controls.Add(this.cbSettingsAutoStartFix);
            this.pnlSettingsGeneralSettingsInside.Location = new System.Drawing.Point(179, 55);
            this.pnlSettingsGeneralSettingsInside.Name = "pnlSettingsGeneralSettingsInside";
            this.pnlSettingsGeneralSettingsInside.Size = new System.Drawing.Size(560, 307);
            this.pnlSettingsGeneralSettingsInside.TabIndex = 0;
            // 
            // cbSettingsAutoStartApp
            // 
            this.cbSettingsAutoStartApp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsAutoStartApp.AutoSize = true;
            this.cbSettingsAutoStartApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsAutoStartApp.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsAutoStartApp.Location = new System.Drawing.Point(126, 77);
            this.cbSettingsAutoStartApp.Name = "cbSettingsAutoStartApp";
            this.cbSettingsAutoStartApp.Size = new System.Drawing.Size(232, 17);
            this.cbSettingsAutoStartApp.TabIndex = 3;
            this.cbSettingsAutoStartApp.Text = "Start Knitrix PC Optimizer when Windows starts.";
            this.cbSettingsAutoStartApp.UseVisualStyleBackColor = true;
            this.cbSettingsAutoStartApp.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // cbSettingsMinimizeToTray
            // 
            this.cbSettingsMinimizeToTray.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsMinimizeToTray.AutoSize = true;
            this.cbSettingsMinimizeToTray.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsMinimizeToTray.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsMinimizeToTray.Location = new System.Drawing.Point(126, 212);
            this.cbSettingsMinimizeToTray.Name = "cbSettingsMinimizeToTray";
            this.cbSettingsMinimizeToTray.Size = new System.Drawing.Size(298, 17);
            this.cbSettingsMinimizeToTray.TabIndex = 8;
            this.cbSettingsMinimizeToTray.Text = "Minimize to system tray instead of closing Knitrix PC Optimizer.";
            this.cbSettingsMinimizeToTray.UseVisualStyleBackColor = true;
            this.cbSettingsMinimizeToTray.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // cbSettingsAutoStartScan
            // 
            this.cbSettingsAutoStartScan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsAutoStartScan.AutoSize = true;
            this.cbSettingsAutoStartScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsAutoStartScan.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsAutoStartScan.Location = new System.Drawing.Point(126, 101);
            this.cbSettingsAutoStartScan.Name = "cbSettingsAutoStartScan";
            this.cbSettingsAutoStartScan.Size = new System.Drawing.Size(274, 17);
            this.cbSettingsAutoStartScan.TabIndex = 4;
            this.cbSettingsAutoStartScan.Text = "Automatically start scan when Knitrix PC Optimizer starts.";
            this.cbSettingsAutoStartScan.UseVisualStyleBackColor = true;
            this.cbSettingsAutoStartScan.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // cbSettingsDisplayExitActions
            // 
            this.cbSettingsDisplayExitActions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsDisplayExitActions.AutoSize = true;
            this.cbSettingsDisplayExitActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsDisplayExitActions.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsDisplayExitActions.Location = new System.Drawing.Point(126, 190);
            this.cbSettingsDisplayExitActions.Name = "cbSettingsDisplayExitActions";
            this.cbSettingsDisplayExitActions.Size = new System.Drawing.Size(301, 17);
            this.cbSettingsDisplayExitActions.TabIndex = 7;
            this.cbSettingsDisplayExitActions.Text = "Show available Exit options before exiting Knitrix PC Optimizer.";
            this.cbSettingsDisplayExitActions.UseVisualStyleBackColor = true;
            this.cbSettingsDisplayExitActions.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // cbSettingsCreateRestorePointBeforeFixing
            // 
            this.cbSettingsCreateRestorePointBeforeFixing.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsCreateRestorePointBeforeFixing.AutoSize = true;
            this.cbSettingsCreateRestorePointBeforeFixing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsCreateRestorePointBeforeFixing.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsCreateRestorePointBeforeFixing.Location = new System.Drawing.Point(126, 125);
            this.cbSettingsCreateRestorePointBeforeFixing.Name = "cbSettingsCreateRestorePointBeforeFixing";
            this.cbSettingsCreateRestorePointBeforeFixing.Size = new System.Drawing.Size(294, 17);
            this.cbSettingsCreateRestorePointBeforeFixing.TabIndex = 5;
            this.cbSettingsCreateRestorePointBeforeFixing.Text = "Create a restore point before performing fixes or cleaning.";
            this.cbSettingsCreateRestorePointBeforeFixing.UseVisualStyleBackColor = true;
            this.cbSettingsCreateRestorePointBeforeFixing.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // cbSettingsAutoStartFix
            // 
            this.cbSettingsAutoStartFix.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsAutoStartFix.AutoSize = true;
            this.cbSettingsAutoStartFix.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsAutoStartFix.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsAutoStartFix.Location = new System.Drawing.Point(126, 149);
            this.cbSettingsAutoStartFix.Name = "cbSettingsAutoStartFix";
            this.cbSettingsAutoStartFix.Size = new System.Drawing.Size(247, 17);
            this.cbSettingsAutoStartFix.TabIndex = 6;
            this.cbSettingsAutoStartFix.Text = "Automatically start fixing after scan is complete.";
            this.cbSettingsAutoStartFix.UseVisualStyleBackColor = true;
            this.cbSettingsAutoStartFix.CheckedChanged += new System.EventHandler(this.cbSettingsGeneralSettings_CheckedChanged);
            // 
            // pnlSettingsScanSchedule
            // 
            this.pnlSettingsScanSchedule.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlSettingsScanSchedule.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettingsScanSchedule.Caption = "Scan Schedule";
            this.pnlSettingsScanSchedule.Controls.Add(this.btnSettingsScheduleDelete);
            this.pnlSettingsScanSchedule.Controls.Add(this.lbSettingsSchedule);
            this.pnlSettingsScanSchedule.Controls.Add(this.btnSettingsScheduleEdit);
            this.pnlSettingsScanSchedule.Controls.Add(this.btnSettingsScheduleNew);
            this.pnlSettingsScanSchedule.Description = "Setup automatic scans to keep your computer healthy";
            this.pnlSettingsScanSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsScanSchedule.HideLogo = true;
            this.pnlSettingsScanSchedule.Icon = global::Win.Properties.Resources.schedule_64x64;
            this.pnlSettingsScanSchedule.IsIconOnLeft = true;
            this.pnlSettingsScanSchedule.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsScanSchedule.Name = "pnlSettingsScanSchedule";
            this.pnlSettingsScanSchedule.NavBarImage = global::Win.Properties.Resources.scan_schedule_nav_bar;
            this.pnlSettingsScanSchedule.Size = new System.Drawing.Size(741, 416);
            this.pnlSettingsScanSchedule.TabIndex = 21;
            // 
            // btnSettingsScheduleDelete
            // 
            this.btnSettingsScheduleDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettingsScheduleDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsScheduleDelete.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSettingsScheduleDelete.ForeColor = System.Drawing.Color.White;
            this.btnSettingsScheduleDelete.Location = new System.Drawing.Point(635, 377);
            this.btnSettingsScheduleDelete.Name = "btnSettingsScheduleDelete";
            this.btnSettingsScheduleDelete.Size = new System.Drawing.Size(91, 27);
            this.btnSettingsScheduleDelete.TabIndex = 92;
            this.btnSettingsScheduleDelete.Text = "Delete";
            this.btnSettingsScheduleDelete.Click += new System.EventHandler(this.btnSettingsScheduleDelete_Click);
            // 
            // lbSettingsSchedule
            // 
            this.lbSettingsSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSettingsSchedule.Appearance.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lbSettingsSchedule.Appearance.Options.UseBackColor = true;
            this.lbSettingsSchedule.Appearance.Options.UseFont = true;
            this.lbSettingsSchedule.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbSettingsSchedule.DisplayMember = "DisplayAs";
            this.lbSettingsSchedule.Location = new System.Drawing.Point(179, 55);
            this.lbSettingsSchedule.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.lbSettingsSchedule.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbSettingsSchedule.Name = "lbSettingsSchedule";
            this.lbSettingsSchedule.Size = new System.Drawing.Size(560, 307);
            this.lbSettingsSchedule.TabIndex = 0;
            this.lbSettingsSchedule.DoubleClick += new System.EventHandler(this.lbSettingsSchedule_DoubleClick);
            // 
            // btnSettingsScheduleEdit
            // 
            this.btnSettingsScheduleEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettingsScheduleEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsScheduleEdit.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSettingsScheduleEdit.ForeColor = System.Drawing.Color.White;
            this.btnSettingsScheduleEdit.Location = new System.Drawing.Point(538, 377);
            this.btnSettingsScheduleEdit.Name = "btnSettingsScheduleEdit";
            this.btnSettingsScheduleEdit.Size = new System.Drawing.Size(91, 27);
            this.btnSettingsScheduleEdit.TabIndex = 91;
            this.btnSettingsScheduleEdit.Text = "Edit";
            this.btnSettingsScheduleEdit.Click += new System.EventHandler(this.btnSettingsScheduleEdit_Click);
            // 
            // btnSettingsScheduleNew
            // 
            this.btnSettingsScheduleNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettingsScheduleNew.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingsScheduleNew.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSettingsScheduleNew.ForeColor = System.Drawing.Color.White;
            this.btnSettingsScheduleNew.Location = new System.Drawing.Point(441, 377);
            this.btnSettingsScheduleNew.Name = "btnSettingsScheduleNew";
            this.btnSettingsScheduleNew.Size = new System.Drawing.Size(91, 27);
            this.btnSettingsScheduleNew.TabIndex = 90;
            this.btnSettingsScheduleNew.Text = "New";
            this.btnSettingsScheduleNew.Click += new System.EventHandler(this.btnSettingsScheduleNew_Click);
            // 
            // pnlSettingsRegistrySettings
            // 
            this.pnlSettingsRegistrySettings.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlSettingsRegistrySettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSettingsRegistrySettings.Caption = "Registry Scan Settings";
            this.pnlSettingsRegistrySettings.Controls.Add(this.pnlSettingsRegistrySettingsInside);
            this.pnlSettingsRegistrySettings.Description = "Choose registry items to include in the scan";
            this.pnlSettingsRegistrySettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsRegistrySettings.HideLogo = false;
            this.pnlSettingsRegistrySettings.Icon = global::Win.Properties.Resources.registry_64x64;
            this.pnlSettingsRegistrySettings.IsIconOnLeft = true;
            this.pnlSettingsRegistrySettings.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsRegistrySettings.Name = "pnlSettingsRegistrySettings";
            this.pnlSettingsRegistrySettings.NavBarImage = global::Win.Properties.Resources.registry_settings_nav_bar;
            this.pnlSettingsRegistrySettings.Size = new System.Drawing.Size(741, 416);
            this.pnlSettingsRegistrySettings.TabIndex = 20;
            // 
            // pnlSettingsRegistrySettingsInside
            // 
            this.pnlSettingsRegistrySettingsInside.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSettingsRegistrySettingsInside.BackColor = System.Drawing.Color.Transparent;
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsApplicationInfo);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsWindowsFonts);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsProgramLocations);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsHistoryList);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsSoftwareSettings);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsSharedDLLs);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsSystemDrivers);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsHelpFiles);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsStartup);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsSoundEvents);
            this.pnlSettingsRegistrySettingsInside.Controls.Add(this.cbSettingsActiveXCOM);
            this.pnlSettingsRegistrySettingsInside.Location = new System.Drawing.Point(179, 55);
            this.pnlSettingsRegistrySettingsInside.Name = "pnlSettingsRegistrySettingsInside";
            this.pnlSettingsRegistrySettingsInside.Size = new System.Drawing.Size(560, 307);
            this.pnlSettingsRegistrySettingsInside.TabIndex = 0;
            // 
            // cbSettingsApplicationInfo
            // 
            this.cbSettingsApplicationInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsApplicationInfo.AutoSize = true;
            this.cbSettingsApplicationInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsApplicationInfo.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsApplicationInfo.Location = new System.Drawing.Point(151, 87);
            this.cbSettingsApplicationInfo.Name = "cbSettingsApplicationInfo";
            this.cbSettingsApplicationInfo.Size = new System.Drawing.Size(99, 17);
            this.cbSettingsApplicationInfo.TabIndex = 16;
            this.cbSettingsApplicationInfo.Text = "Application Info";
            this.cbSettingsApplicationInfo.UseVisualStyleBackColor = true;
            this.cbSettingsApplicationInfo.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsWindowsFonts
            // 
            this.cbSettingsWindowsFonts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsWindowsFonts.AutoSize = true;
            this.cbSettingsWindowsFonts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsWindowsFonts.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsWindowsFonts.Location = new System.Drawing.Point(300, 179);
            this.cbSettingsWindowsFonts.Name = "cbSettingsWindowsFonts";
            this.cbSettingsWindowsFonts.Size = new System.Drawing.Size(99, 17);
            this.cbSettingsWindowsFonts.TabIndex = 21;
            this.cbSettingsWindowsFonts.Text = "Windows Fonts";
            this.cbSettingsWindowsFonts.UseVisualStyleBackColor = true;
            this.cbSettingsWindowsFonts.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsProgramLocations
            // 
            this.cbSettingsProgramLocations.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsProgramLocations.AutoSize = true;
            this.cbSettingsProgramLocations.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsProgramLocations.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsProgramLocations.Location = new System.Drawing.Point(151, 110);
            this.cbSettingsProgramLocations.Name = "cbSettingsProgramLocations";
            this.cbSettingsProgramLocations.Size = new System.Drawing.Size(114, 17);
            this.cbSettingsProgramLocations.TabIndex = 11;
            this.cbSettingsProgramLocations.Text = "Program Locations";
            this.cbSettingsProgramLocations.UseVisualStyleBackColor = true;
            this.cbSettingsProgramLocations.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsHistoryList
            // 
            this.cbSettingsHistoryList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsHistoryList.AutoSize = true;
            this.cbSettingsHistoryList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsHistoryList.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsHistoryList.Location = new System.Drawing.Point(300, 156);
            this.cbSettingsHistoryList.Name = "cbSettingsHistoryList";
            this.cbSettingsHistoryList.Size = new System.Drawing.Size(77, 17);
            this.cbSettingsHistoryList.TabIndex = 20;
            this.cbSettingsHistoryList.Text = "History List";
            this.cbSettingsHistoryList.UseVisualStyleBackColor = true;
            this.cbSettingsHistoryList.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsSoftwareSettings
            // 
            this.cbSettingsSoftwareSettings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsSoftwareSettings.AutoSize = true;
            this.cbSettingsSoftwareSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsSoftwareSettings.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsSoftwareSettings.Location = new System.Drawing.Point(151, 133);
            this.cbSettingsSoftwareSettings.Name = "cbSettingsSoftwareSettings";
            this.cbSettingsSoftwareSettings.Size = new System.Drawing.Size(109, 17);
            this.cbSettingsSoftwareSettings.TabIndex = 12;
            this.cbSettingsSoftwareSettings.Text = "Software Settings";
            this.cbSettingsSoftwareSettings.UseVisualStyleBackColor = true;
            this.cbSettingsSoftwareSettings.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsSharedDLLs
            // 
            this.cbSettingsSharedDLLs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsSharedDLLs.AutoSize = true;
            this.cbSettingsSharedDLLs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsSharedDLLs.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsSharedDLLs.Location = new System.Drawing.Point(300, 87);
            this.cbSettingsSharedDLLs.Name = "cbSettingsSharedDLLs";
            this.cbSettingsSharedDLLs.Size = new System.Drawing.Size(88, 17);
            this.cbSettingsSharedDLLs.TabIndex = 19;
            this.cbSettingsSharedDLLs.Text = "Shared DLLs";
            this.cbSettingsSharedDLLs.UseVisualStyleBackColor = true;
            this.cbSettingsSharedDLLs.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsSystemDrivers
            // 
            this.cbSettingsSystemDrivers.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsSystemDrivers.AutoSize = true;
            this.cbSettingsSystemDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsSystemDrivers.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsSystemDrivers.Location = new System.Drawing.Point(151, 178);
            this.cbSettingsSystemDrivers.Name = "cbSettingsSystemDrivers";
            this.cbSettingsSystemDrivers.Size = new System.Drawing.Size(96, 17);
            this.cbSettingsSystemDrivers.TabIndex = 13;
            this.cbSettingsSystemDrivers.Text = "System Drivers";
            this.cbSettingsSystemDrivers.UseVisualStyleBackColor = true;
            this.cbSettingsSystemDrivers.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsHelpFiles
            // 
            this.cbSettingsHelpFiles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsHelpFiles.AutoSize = true;
            this.cbSettingsHelpFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsHelpFiles.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsHelpFiles.Location = new System.Drawing.Point(300, 110);
            this.cbSettingsHelpFiles.Name = "cbSettingsHelpFiles";
            this.cbSettingsHelpFiles.Size = new System.Drawing.Size(72, 17);
            this.cbSettingsHelpFiles.TabIndex = 18;
            this.cbSettingsHelpFiles.Text = "Help Files";
            this.cbSettingsHelpFiles.UseVisualStyleBackColor = true;
            this.cbSettingsHelpFiles.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsStartup
            // 
            this.cbSettingsStartup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsStartup.AutoSize = true;
            this.cbSettingsStartup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsStartup.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsStartup.Location = new System.Drawing.Point(151, 156);
            this.cbSettingsStartup.Name = "cbSettingsStartup";
            this.cbSettingsStartup.Size = new System.Drawing.Size(60, 17);
            this.cbSettingsStartup.TabIndex = 14;
            this.cbSettingsStartup.Text = "Startup";
            this.cbSettingsStartup.UseVisualStyleBackColor = true;
            this.cbSettingsStartup.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsSoundEvents
            // 
            this.cbSettingsSoundEvents.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsSoundEvents.AutoSize = true;
            this.cbSettingsSoundEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsSoundEvents.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsSoundEvents.Location = new System.Drawing.Point(300, 133);
            this.cbSettingsSoundEvents.Name = "cbSettingsSoundEvents";
            this.cbSettingsSoundEvents.Size = new System.Drawing.Size(93, 17);
            this.cbSettingsSoundEvents.TabIndex = 17;
            this.cbSettingsSoundEvents.Text = "Sound Events";
            this.cbSettingsSoundEvents.UseVisualStyleBackColor = true;
            this.cbSettingsSoundEvents.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // cbSettingsActiveXCOM
            // 
            this.cbSettingsActiveXCOM.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbSettingsActiveXCOM.AutoSize = true;
            this.cbSettingsActiveXCOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbSettingsActiveXCOM.ForeColor = System.Drawing.Color.Black;
            this.cbSettingsActiveXCOM.Location = new System.Drawing.Point(300, 202);
            this.cbSettingsActiveXCOM.Name = "cbSettingsActiveXCOM";
            this.cbSettingsActiveXCOM.Size = new System.Drawing.Size(92, 17);
            this.cbSettingsActiveXCOM.TabIndex = 15;
            this.cbSettingsActiveXCOM.Text = "ActiveX/COM";
            this.cbSettingsActiveXCOM.UseVisualStyleBackColor = true;
            this.cbSettingsActiveXCOM.CheckedChanged += new System.EventHandler(this.cbSettingsRegistryScanSettings_CheckedChanged);
            // 
            // pnlDisk
            // 
            this.pnlDisk.BackColor = System.Drawing.Color.White;
            this.pnlDisk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDisk.Controls.Add(this.pnlDiskMain);
            this.pnlDisk.Controls.Add(this.pnlDiskRestorePointManager);
            this.pnlDisk.Controls.Add(this.pnlDiskDiskCleaner);
            this.pnlDisk.Controls.Add(this.pnlDiskShortcutFixer);
            this.pnlDisk.Controls.Add(this.pnlDiskUninstallManager);
            this.pnlDisk.Location = new System.Drawing.Point(8, 73);
            this.pnlDisk.Name = "pnlDisk";
            this.pnlDisk.Size = new System.Drawing.Size(741, 416);
            this.pnlDisk.TabIndex = 2;
            // 
            // pnlDiskMain
            // 
            this.pnlDiskMain.BackgroundImage = global::Win.Properties.Resources.panel_background_main_left;
            this.pnlDiskMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDiskMain.Caption = "Disk";
            this.pnlDiskMain.Controls.Add(this.btnDiskClearRecentHistory);
            this.pnlDiskMain.Controls.Add(this.btnDiskClearTempraryFiles);
            this.pnlDiskMain.Controls.Add(this.btnDiskDiskCleaner);
            this.pnlDiskMain.Controls.Add(this.btnDiskRestorePointManager);
            this.pnlDiskMain.Controls.Add(this.btnDiskShortcutFixer);
            this.pnlDiskMain.Controls.Add(this.btnDiskUninstallManager);
            this.pnlDiskMain.Description = "Free up disk space by removing unneccessary files";
            this.pnlDiskMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDiskMain.HideLogo = false;
            this.pnlDiskMain.Icon = global::Win.Properties.Resources.disk_64x64;
            this.pnlDiskMain.IsIconOnLeft = true;
            this.pnlDiskMain.Location = new System.Drawing.Point(0, 0);
            this.pnlDiskMain.Name = "pnlDiskMain";
            this.pnlDiskMain.NavBarImage = global::Win.Properties.Resources.disk_nav_bar;
            this.pnlDiskMain.SeparatorCount = 2;
            this.pnlDiskMain.Size = new System.Drawing.Size(741, 416);
            this.pnlDiskMain.TabIndex = 19;
            this.pnlDiskMain.RegisterClick += new System.EventHandler(this.btnRegister_Click);
            this.pnlDiskMain.PushToTalkClick += new System.EventHandler(this.btnPushToTalkClick_Click);
            // 
            // btnDiskClearRecentHistory
            // 
            this.btnDiskClearRecentHistory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskClearRecentHistory.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskClearRecentHistory.Description = "Remove Internet and Windows recent history.";
            this.btnDiskClearRecentHistory.Icon = global::Win.Properties.Resources.clear_recent_history_64x64;
            this.btnDiskClearRecentHistory.Location = new System.Drawing.Point(473, 269);
            this.btnDiskClearRecentHistory.Name = "btnDiskClearRecentHistory";
            this.btnDiskClearRecentHistory.Size = new System.Drawing.Size(285, 108);
            this.btnDiskClearRecentHistory.TabIndex = 86;
            this.btnDiskClearRecentHistory.Title = "Clear Recent History";
            this.btnDiskClearRecentHistory.ButtonClick += new System.EventHandler(this.btnDiskClearRecentHistory_Click);
            // 
            // btnDiskClearTempraryFiles
            // 
            this.btnDiskClearTempraryFiles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskClearTempraryFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskClearTempraryFiles.Description = "Remove Windows temporary files to free up disk space.";
            this.btnDiskClearTempraryFiles.Icon = global::Win.Properties.Resources.clear_temp_64x64;
            this.btnDiskClearTempraryFiles.Location = new System.Drawing.Point(473, 166);
            this.btnDiskClearTempraryFiles.Name = "btnDiskClearTempraryFiles";
            this.btnDiskClearTempraryFiles.Size = new System.Drawing.Size(285, 108);
            this.btnDiskClearTempraryFiles.TabIndex = 85;
            this.btnDiskClearTempraryFiles.Title = "Clear Temporary Files";
            this.btnDiskClearTempraryFiles.ButtonClick += new System.EventHandler(this.btnDiskClearTempraryFiles_Click);
            // 
            // btnDiskDiskCleaner
            // 
            this.btnDiskDiskCleaner.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskDiskCleaner.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleaner.Description = "Free up disk space by removing unnecessary files.";
            this.btnDiskDiskCleaner.Icon = global::Win.Properties.Resources.disc_cleaner_64x64;
            this.btnDiskDiskCleaner.Location = new System.Drawing.Point(473, 63);
            this.btnDiskDiskCleaner.Name = "btnDiskDiskCleaner";
            this.btnDiskDiskCleaner.Size = new System.Drawing.Size(285, 108);
            this.btnDiskDiskCleaner.TabIndex = 82;
            this.btnDiskDiskCleaner.Title = "Disk Cleaner";
            this.btnDiskDiskCleaner.ButtonClick += new System.EventHandler(this.btnDiskDiskCleaner_Click);
            // 
            // btnDiskRestorePointManager
            // 
            this.btnDiskRestorePointManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskRestorePointManager.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskRestorePointManager.Description = "Create, restore, or remove restore points.";
            this.btnDiskRestorePointManager.Icon = global::Win.Properties.Resources.restore_point_64x64;
            this.btnDiskRestorePointManager.Location = new System.Drawing.Point(190, 269);
            this.btnDiskRestorePointManager.Name = "btnDiskRestorePointManager";
            this.btnDiskRestorePointManager.Size = new System.Drawing.Size(285, 108);
            this.btnDiskRestorePointManager.TabIndex = 84;
            this.btnDiskRestorePointManager.Title = "Restore Point Manager";
            this.btnDiskRestorePointManager.ButtonClick += new System.EventHandler(this.btnDiskRestorePointManager_Click);
            // 
            // btnDiskShortcutFixer
            // 
            this.btnDiskShortcutFixer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskShortcutFixer.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskShortcutFixer.Description = "Repair corrupt desktop and start menu shortcuts.";
            this.btnDiskShortcutFixer.Icon = global::Win.Properties.Resources.shortcut_fixer_64x64;
            this.btnDiskShortcutFixer.Location = new System.Drawing.Point(190, 166);
            this.btnDiskShortcutFixer.Name = "btnDiskShortcutFixer";
            this.btnDiskShortcutFixer.Size = new System.Drawing.Size(285, 108);
            this.btnDiskShortcutFixer.TabIndex = 83;
            this.btnDiskShortcutFixer.Title = "Shortcut Fixer";
            this.btnDiskShortcutFixer.ButtonClick += new System.EventHandler(this.btnDiskShortcutFixer_Click);
            // 
            // btnDiskUninstallManager
            // 
            this.btnDiskUninstallManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDiskUninstallManager.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskUninstallManager.Description = "Uninstall software that you no longer use.";
            this.btnDiskUninstallManager.Icon = global::Win.Properties.Resources.uninstall_64x64;
            this.btnDiskUninstallManager.Location = new System.Drawing.Point(190, 63);
            this.btnDiskUninstallManager.Name = "btnDiskUninstallManager";
            this.btnDiskUninstallManager.Size = new System.Drawing.Size(285, 108);
            this.btnDiskUninstallManager.TabIndex = 87;
            this.btnDiskUninstallManager.Title = "Uninstall Software";
            this.btnDiskUninstallManager.ButtonClick += new System.EventHandler(this.btnDiskUninstallManager_Click);
            // 
            // pnlDiskRestorePointManager
            // 
            this.pnlDiskRestorePointManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlDiskRestorePointManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDiskRestorePointManager.Caption = "Restore Point Manager";
            this.pnlDiskRestorePointManager.Controls.Add(this.lvDiskRestorePoint);
            this.pnlDiskRestorePointManager.Controls.Add(this.btnDiskRemoveRestorePoint);
            this.pnlDiskRestorePointManager.Controls.Add(this.btnDiskRestoreRestorePoint);
            this.pnlDiskRestorePointManager.Controls.Add(this.txtDistRestorePointDescription);
            this.pnlDiskRestorePointManager.Controls.Add(this.btnDiskCreateRestorePoint);
            this.pnlDiskRestorePointManager.Controls.Add(this.lblDistRestorePointDescription);
            this.pnlDiskRestorePointManager.Description = "Create, restore, or remove restore points";
            this.pnlDiskRestorePointManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDiskRestorePointManager.HideLogo = true;
            this.pnlDiskRestorePointManager.Icon = global::Win.Properties.Resources.restore_point_64x64;
            this.pnlDiskRestorePointManager.IsIconOnLeft = true;
            this.pnlDiskRestorePointManager.Location = new System.Drawing.Point(0, 0);
            this.pnlDiskRestorePointManager.Name = "pnlDiskRestorePointManager";
            this.pnlDiskRestorePointManager.NavBarImage = global::Win.Properties.Resources.restore_point_manager_nav_bar;
            this.pnlDiskRestorePointManager.Size = new System.Drawing.Size(741, 416);
            this.pnlDiskRestorePointManager.TabIndex = 20;
            // 
            // lvDiskRestorePoint
            // 
            this.lvDiskRestorePoint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDiskRestorePoint.BackgroundImageTiled = true;
            this.lvDiskRestorePoint.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDiskRestorePoint.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDiskRestorePointNumber,
            this.colDiskRestorePointDateTime,
            this.colDiskRestorePointDescription,
            this.colDiskRestorePointType});
            this.lvDiskRestorePoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvDiskRestorePoint.FullRowSelect = true;
            this.lvDiskRestorePoint.GridLines = true;
            this.lvDiskRestorePoint.HideSelection = false;
            this.lvDiskRestorePoint.Location = new System.Drawing.Point(179, 55);
            this.lvDiskRestorePoint.MultiSelect = false;
            this.lvDiskRestorePoint.Name = "lvDiskRestorePoint";
            this.lvDiskRestorePoint.Size = new System.Drawing.Size(560, 307);
            this.lvDiskRestorePoint.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDiskRestorePoint.TabIndex = 3;
            this.lvDiskRestorePoint.UseCompatibleStateImageBehavior = false;
            this.lvDiskRestorePoint.View = System.Windows.Forms.View.Details;
            this.lvDiskRestorePoint.SelectedIndexChanged += new System.EventHandler(this.lvDiskRestorePoint_SelectedIndexChanged);
            // 
            // colDiskRestorePointNumber
            // 
            this.colDiskRestorePointNumber.Text = "Number";
            // 
            // colDiskRestorePointDateTime
            // 
            this.colDiskRestorePointDateTime.Text = "Date and Time";
            this.colDiskRestorePointDateTime.Width = 120;
            // 
            // colDiskRestorePointDescription
            // 
            this.colDiskRestorePointDescription.Text = "Description";
            this.colDiskRestorePointDescription.Width = 300;
            // 
            // colDiskRestorePointType
            // 
            this.colDiskRestorePointType.Text = "Type";
            this.colDiskRestorePointType.Width = 221;
            // 
            // btnDiskRemoveRestorePoint
            // 
            this.btnDiskRemoveRestorePoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskRemoveRestorePoint.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskRemoveRestorePoint.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskRemoveRestorePoint.ForeColor = System.Drawing.Color.White;
            this.btnDiskRemoveRestorePoint.Location = new System.Drawing.Point(632, 377);
            this.btnDiskRemoveRestorePoint.Name = "btnDiskRemoveRestorePoint";
            this.btnDiskRemoveRestorePoint.Size = new System.Drawing.Size(91, 27);
            this.btnDiskRemoveRestorePoint.TabIndex = 90;
            this.btnDiskRemoveRestorePoint.Text = "Remove";
            this.btnDiskRemoveRestorePoint.Click += new System.EventHandler(this.btnDiskRemoveRestorePoint_Click);
            // 
            // btnDiskRestoreRestorePoint
            // 
            this.btnDiskRestoreRestorePoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskRestoreRestorePoint.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskRestoreRestorePoint.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskRestoreRestorePoint.ForeColor = System.Drawing.Color.White;
            this.btnDiskRestoreRestorePoint.Location = new System.Drawing.Point(535, 377);
            this.btnDiskRestoreRestorePoint.Name = "btnDiskRestoreRestorePoint";
            this.btnDiskRestoreRestorePoint.Size = new System.Drawing.Size(91, 27);
            this.btnDiskRestoreRestorePoint.TabIndex = 89;
            this.btnDiskRestoreRestorePoint.Text = "Restore";
            this.btnDiskRestoreRestorePoint.Click += new System.EventHandler(this.btnDiskRestoreRestorePoint_Click);
            // 
            // txtDistRestorePointDescription
            // 
            this.txtDistRestorePointDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDistRestorePointDescription.Location = new System.Drawing.Point(75, 380);
            this.txtDistRestorePointDescription.Name = "txtDistRestorePointDescription";
            this.txtDistRestorePointDescription.Size = new System.Drawing.Size(152, 20);
            this.txtDistRestorePointDescription.TabIndex = 26;
            // 
            // btnDiskCreateRestorePoint
            // 
            this.btnDiskCreateRestorePoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskCreateRestorePoint.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskCreateRestorePoint.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskCreateRestorePoint.ForeColor = System.Drawing.Color.White;
            this.btnDiskCreateRestorePoint.Location = new System.Drawing.Point(233, 377);
            this.btnDiskCreateRestorePoint.Name = "btnDiskCreateRestorePoint";
            this.btnDiskCreateRestorePoint.Size = new System.Drawing.Size(166, 27);
            this.btnDiskCreateRestorePoint.TabIndex = 88;
            this.btnDiskCreateRestorePoint.Text = "Create Restore Point";
            this.btnDiskCreateRestorePoint.Click += new System.EventHandler(this.btnDiskCreateRestorePoint_Click);
            // 
            // lblDistRestorePointDescription
            // 
            this.lblDistRestorePointDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDistRestorePointDescription.AutoSize = true;
            this.lblDistRestorePointDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDistRestorePointDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblDistRestorePointDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblDistRestorePointDescription.Location = new System.Drawing.Point(5, 384);
            this.lblDistRestorePointDescription.Name = "lblDistRestorePointDescription";
            this.lblDistRestorePointDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDistRestorePointDescription.TabIndex = 27;
            this.lblDistRestorePointDescription.Text = "Description:";
            // 
            // pnlDiskDiskCleaner
            // 
            this.pnlDiskDiskCleaner.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlDiskDiskCleaner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDiskDiskCleaner.Caption = "Disk Cleaner";
            this.pnlDiskDiskCleaner.Controls.Add(this.lvDiskDiskCleaner);
            this.pnlDiskDiskCleaner.Controls.Add(this.btnDiskDiskCleanerOptions);
            this.pnlDiskDiskCleaner.Controls.Add(this.fileInfoCtrl1);
            this.pnlDiskDiskCleaner.Controls.Add(this.btnDiskDiskCleanerProperties);
            this.pnlDiskDiskCleaner.Controls.Add(this.btnDiskDiskCleanerOpenFile);
            this.pnlDiskDiskCleaner.Controls.Add(this.btnDiskDiskCleanerCleanFiles);
            this.pnlDiskDiskCleaner.Controls.Add(this.btnDiskDiskCleanerScanDisks);
            this.pnlDiskDiskCleaner.Description = "Free up disk space by removing unneccessary files";
            this.pnlDiskDiskCleaner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDiskDiskCleaner.HideLogo = true;
            this.pnlDiskDiskCleaner.Icon = global::Win.Properties.Resources.disc_cleaner_64x64;
            this.pnlDiskDiskCleaner.IsIconOnLeft = true;
            this.pnlDiskDiskCleaner.Location = new System.Drawing.Point(0, 0);
            this.pnlDiskDiskCleaner.Name = "pnlDiskDiskCleaner";
            this.pnlDiskDiskCleaner.NavBarImage = global::Win.Properties.Resources.cleaner_nav_bar;
            this.pnlDiskDiskCleaner.Size = new System.Drawing.Size(741, 416);
            this.pnlDiskDiskCleaner.TabIndex = 20;
            // 
            // lvDiskDiskCleaner
            // 
            this.lvDiskDiskCleaner.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDiskDiskCleaner.BackgroundImageTiled = true;
            this.lvDiskDiskCleaner.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDiskDiskCleaner.CheckBoxes = true;
            this.lvDiskDiskCleaner.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.lvDiskDiskCleaner.ContextMenuStrip = this.cmnuDiskDiskCleaner;
            this.lvDiskDiskCleaner.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvDiskDiskCleaner.FullRowSelect = true;
            this.lvDiskDiskCleaner.GridLines = true;
            this.lvDiskDiskCleaner.HideSelection = false;
            this.lvDiskDiskCleaner.Location = new System.Drawing.Point(179, 55);
            this.lvDiskDiskCleaner.Name = "lvDiskDiskCleaner";
            this.lvDiskDiskCleaner.Size = new System.Drawing.Size(560, 217);
            this.lvDiskDiskCleaner.TabIndex = 2;
            this.lvDiskDiskCleaner.UseCompatibleStateImageBehavior = false;
            this.lvDiskDiskCleaner.View = System.Windows.Forms.View.Details;
            this.lvDiskDiskCleaner.SelectedIndexChanged += new System.EventHandler(this.lvDiskDiskCleaner_SelectedIndexChanged);
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Name";
            this.columnHeader15.Width = 200;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Location";
            this.columnHeader16.Width = 400;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Size";
            this.columnHeader17.Width = 97;
            // 
            // cmnuDiskDiskCleaner
            // 
            this.cmnuDiskDiskCleaner.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDiskDiskCleanerSelectAll,
            this.tsmiDiskDiskCleanerSelectNone,
            this.tsmiDiskDiskCleanerInvertSelection,
            this.toolStripSeparator1,
            this.tsmiDiskDiskCleanerOpenFile,
            this.tsmiDiskDiskCleanerProperties});
            this.cmnuDiskDiskCleaner.Name = "cmnuDiskShortcutFixer";
            this.cmnuDiskDiskCleaner.Size = new System.Drawing.Size(156, 120);
            this.cmnuDiskDiskCleaner.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmnuDiskDiskCleaner_ItemClicked);
            // 
            // tsmiDiskDiskCleanerSelectAll
            // 
            this.tsmiDiskDiskCleanerSelectAll.Name = "tsmiDiskDiskCleanerSelectAll";
            this.tsmiDiskDiskCleanerSelectAll.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskDiskCleanerSelectAll.Text = "Select All";
            // 
            // tsmiDiskDiskCleanerSelectNone
            // 
            this.tsmiDiskDiskCleanerSelectNone.Name = "tsmiDiskDiskCleanerSelectNone";
            this.tsmiDiskDiskCleanerSelectNone.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskDiskCleanerSelectNone.Text = "Select None";
            // 
            // tsmiDiskDiskCleanerInvertSelection
            // 
            this.tsmiDiskDiskCleanerInvertSelection.Name = "tsmiDiskDiskCleanerInvertSelection";
            this.tsmiDiskDiskCleanerInvertSelection.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskDiskCleanerInvertSelection.Text = "Invert Selection";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // tsmiDiskDiskCleanerOpenFile
            // 
            this.tsmiDiskDiskCleanerOpenFile.Enabled = false;
            this.tsmiDiskDiskCleanerOpenFile.Image = global::Win.Properties.Resources.open_file_16x16;
            this.tsmiDiskDiskCleanerOpenFile.Name = "tsmiDiskDiskCleanerOpenFile";
            this.tsmiDiskDiskCleanerOpenFile.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskDiskCleanerOpenFile.Text = "Open File";
            // 
            // tsmiDiskDiskCleanerProperties
            // 
            this.tsmiDiskDiskCleanerProperties.Enabled = false;
            this.tsmiDiskDiskCleanerProperties.Image = global::Win.Properties.Resources.properties_16x16;
            this.tsmiDiskDiskCleanerProperties.Name = "tsmiDiskDiskCleanerProperties";
            this.tsmiDiskDiskCleanerProperties.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskDiskCleanerProperties.Text = "Properties";
            // 
            // btnDiskDiskCleanerOptions
            // 
            this.btnDiskDiskCleanerOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskDiskCleanerOptions.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleanerOptions.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskDiskCleanerOptions.ForeColor = System.Drawing.Color.White;
            this.btnDiskDiskCleanerOptions.Location = new System.Drawing.Point(638, 377);
            this.btnDiskDiskCleanerOptions.Name = "btnDiskDiskCleanerOptions";
            this.btnDiskDiskCleanerOptions.Size = new System.Drawing.Size(85, 27);
            this.btnDiskDiskCleanerOptions.TabIndex = 94;
            this.btnDiskDiskCleanerOptions.Text = "Options";
            this.btnDiskDiskCleanerOptions.Click += new System.EventHandler(this.btnDiskDiskCleanerOptions_Click);
            // 
            // fileInfoCtrl1
            // 
            this.fileInfoCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileInfoCtrl1.BackColor = System.Drawing.Color.Transparent;
            this.fileInfoCtrl1.Location = new System.Drawing.Point(179, 277);
            this.fileInfoCtrl1.Name = "fileInfoCtrl1";
            this.fileInfoCtrl1.Size = new System.Drawing.Size(560, 85);
            this.fileInfoCtrl1.TabIndex = 3;
            // 
            // btnDiskDiskCleanerProperties
            // 
            this.btnDiskDiskCleanerProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskDiskCleanerProperties.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleanerProperties.Enabled = false;
            this.btnDiskDiskCleanerProperties.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskDiskCleanerProperties.ForeColor = System.Drawing.Color.White;
            this.btnDiskDiskCleanerProperties.Location = new System.Drawing.Point(532, 377);
            this.btnDiskDiskCleanerProperties.Name = "btnDiskDiskCleanerProperties";
            this.btnDiskDiskCleanerProperties.Size = new System.Drawing.Size(100, 27);
            this.btnDiskDiskCleanerProperties.TabIndex = 93;
            this.btnDiskDiskCleanerProperties.Text = "Properties";
            this.btnDiskDiskCleanerProperties.Click += new System.EventHandler(this.btnDiskDiskCleanerProperties_Click);
            // 
            // btnDiskDiskCleanerOpenFile
            // 
            this.btnDiskDiskCleanerOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskDiskCleanerOpenFile.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleanerOpenFile.Enabled = false;
            this.btnDiskDiskCleanerOpenFile.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskDiskCleanerOpenFile.ForeColor = System.Drawing.Color.White;
            this.btnDiskDiskCleanerOpenFile.Location = new System.Drawing.Point(426, 377);
            this.btnDiskDiskCleanerOpenFile.Name = "btnDiskDiskCleanerOpenFile";
            this.btnDiskDiskCleanerOpenFile.Size = new System.Drawing.Size(100, 27);
            this.btnDiskDiskCleanerOpenFile.TabIndex = 92;
            this.btnDiskDiskCleanerOpenFile.Text = "Open File";
            this.btnDiskDiskCleanerOpenFile.Click += new System.EventHandler(this.btnDiskDiskCleanerOpenFile_Click);
            // 
            // btnDiskDiskCleanerCleanFiles
            // 
            this.btnDiskDiskCleanerCleanFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskDiskCleanerCleanFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleanerCleanFiles.Enabled = false;
            this.btnDiskDiskCleanerCleanFiles.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskDiskCleanerCleanFiles.ForeColor = System.Drawing.Color.White;
            this.btnDiskDiskCleanerCleanFiles.Location = new System.Drawing.Point(320, 377);
            this.btnDiskDiskCleanerCleanFiles.Name = "btnDiskDiskCleanerCleanFiles";
            this.btnDiskDiskCleanerCleanFiles.Size = new System.Drawing.Size(100, 27);
            this.btnDiskDiskCleanerCleanFiles.TabIndex = 91;
            this.btnDiskDiskCleanerCleanFiles.Text = "Clean Files";
            this.btnDiskDiskCleanerCleanFiles.Click += new System.EventHandler(this.btnDiskDiskCleanerCleanFiles_Click);
            // 
            // btnDiskDiskCleanerScanDisks
            // 
            this.btnDiskDiskCleanerScanDisks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiskDiskCleanerScanDisks.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskDiskCleanerScanDisks.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskDiskCleanerScanDisks.ForeColor = System.Drawing.Color.White;
            this.btnDiskDiskCleanerScanDisks.Location = new System.Drawing.Point(205, 377);
            this.btnDiskDiskCleanerScanDisks.Name = "btnDiskDiskCleanerScanDisks";
            this.btnDiskDiskCleanerScanDisks.Size = new System.Drawing.Size(109, 27);
            this.btnDiskDiskCleanerScanDisks.TabIndex = 90;
            this.btnDiskDiskCleanerScanDisks.Text = "Scan Disk(s)";
            this.btnDiskDiskCleanerScanDisks.Click += new System.EventHandler(this.btnDiskDiskCleanerScanDisks_Click);
            // 
            // pnlDiskShortcutFixer
            // 
            this.pnlDiskShortcutFixer.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlDiskShortcutFixer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDiskShortcutFixer.Caption = "Shortcut Fixer";
            this.pnlDiskShortcutFixer.Controls.Add(this.lvDiskShortcutFixerFiles);
            this.pnlDiskShortcutFixer.Controls.Add(this.btnDiskShortcutFixerDeleteSelected);
            this.pnlDiskShortcutFixer.Controls.Add(this.lblDiskShortcutFixerStatus);
            this.pnlDiskShortcutFixer.Controls.Add(this.btnDiskShortcutFixerScan);
            this.pnlDiskShortcutFixer.Description = "Repair corrupted desktop and start menu shortcuts";
            this.pnlDiskShortcutFixer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDiskShortcutFixer.HideLogo = true;
            this.pnlDiskShortcutFixer.Icon = global::Win.Properties.Resources.shortcut_fixer_64x64;
            this.pnlDiskShortcutFixer.IsIconOnLeft = true;
            this.pnlDiskShortcutFixer.Location = new System.Drawing.Point(0, 0);
            this.pnlDiskShortcutFixer.Name = "pnlDiskShortcutFixer";
            this.pnlDiskShortcutFixer.NavBarImage = global::Win.Properties.Resources.shortcut_nav_bar;
            this.pnlDiskShortcutFixer.Size = new System.Drawing.Size(741, 416);
            this.pnlDiskShortcutFixer.TabIndex = 21;
            // 
            // lvDiskShortcutFixerFiles
            // 
            this.lvDiskShortcutFixerFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDiskShortcutFixerFiles.BackgroundImageTiled = true;
            this.lvDiskShortcutFixerFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDiskShortcutFixerFiles.CheckBoxes = true;
            this.lvDiskShortcutFixerFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvDiskShortcutFixerFiles.ContextMenuStrip = this.cmnuDiskShortcutFixer;
            this.lvDiskShortcutFixerFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvDiskShortcutFixerFiles.FullRowSelect = true;
            this.lvDiskShortcutFixerFiles.GridLines = true;
            this.lvDiskShortcutFixerFiles.HideSelection = false;
            this.lvDiskShortcutFixerFiles.Location = new System.Drawing.Point(179, 80);
            this.lvDiskShortcutFixerFiles.MultiSelect = false;
            this.lvDiskShortcutFixerFiles.Name = "lvDiskShortcutFixerFiles";
            this.lvDiskShortcutFixerFiles.Size = new System.Drawing.Size(560, 282);
            this.lvDiskShortcutFixerFiles.SmallImageList = this.ilDiskShortcutFixer;
            this.lvDiskShortcutFixerFiles.TabIndex = 4;
            this.lvDiskShortcutFixerFiles.UseCompatibleStateImageBehavior = false;
            this.lvDiskShortcutFixerFiles.View = System.Windows.Forms.View.Details;
            this.lvDiskShortcutFixerFiles.SelectedIndexChanged += new System.EventHandler(this.lvDiskShortcutFixerFiles_SelectedIndexChanged);
            this.lvDiskShortcutFixerFiles.DoubleClick += new System.EventHandler(this.lvDiskShortcutFixerFiles_DoubleClick);
            this.lvDiskShortcutFixerFiles.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvDiskShortcutFixerFiles_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 124;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Target";
            this.columnHeader2.Width = 268;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Full Path";
            this.columnHeader3.Width = 326;
            // 
            // cmnuDiskShortcutFixer
            // 
            this.cmnuDiskShortcutFixer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDiskShortcutFixerDelete,
            this.tsmiDiskShortcutFixerChangeTarget,
            this.tsmiDiskShortcutFixerSeparator1,
            this.tsmiDiskShortcutFixerSelectAll,
            this.tsmiDiskShortcutFixerSelectNone,
            this.tsmiDiskShortcutFixerInvertSelection,
            this.tsmiDiskShortcutFixerSeparator2,
            this.tsmiDiskShortcutFixerProperties,
            this.tsmiDiskShortcutFixerOpenFolder});
            this.cmnuDiskShortcutFixer.Name = "cmnuDiskShortcutFixer";
            this.cmnuDiskShortcutFixer.Size = new System.Drawing.Size(156, 170);
            this.cmnuDiskShortcutFixer.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmnuDiskShortcutFixer_ItemClicked);
            // 
            // tsmiDiskShortcutFixerDelete
            // 
            this.tsmiDiskShortcutFixerDelete.Enabled = false;
            this.tsmiDiskShortcutFixerDelete.Image = global::Win.Properties.Resources.delete_16x16;
            this.tsmiDiskShortcutFixerDelete.Name = "tsmiDiskShortcutFixerDelete";
            this.tsmiDiskShortcutFixerDelete.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerDelete.Text = "Delete";
            // 
            // tsmiDiskShortcutFixerChangeTarget
            // 
            this.tsmiDiskShortcutFixerChangeTarget.Enabled = false;
            this.tsmiDiskShortcutFixerChangeTarget.Image = global::Win.Properties.Resources.shortcut_16x16;
            this.tsmiDiskShortcutFixerChangeTarget.Name = "tsmiDiskShortcutFixerChangeTarget";
            this.tsmiDiskShortcutFixerChangeTarget.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerChangeTarget.Text = "Change Target";
            // 
            // tsmiDiskShortcutFixerSeparator1
            // 
            this.tsmiDiskShortcutFixerSeparator1.Name = "tsmiDiskShortcutFixerSeparator1";
            this.tsmiDiskShortcutFixerSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // tsmiDiskShortcutFixerSelectAll
            // 
            this.tsmiDiskShortcutFixerSelectAll.Name = "tsmiDiskShortcutFixerSelectAll";
            this.tsmiDiskShortcutFixerSelectAll.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerSelectAll.Text = "Select All";
            // 
            // tsmiDiskShortcutFixerSelectNone
            // 
            this.tsmiDiskShortcutFixerSelectNone.Name = "tsmiDiskShortcutFixerSelectNone";
            this.tsmiDiskShortcutFixerSelectNone.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerSelectNone.Text = "Select None";
            // 
            // tsmiDiskShortcutFixerInvertSelection
            // 
            this.tsmiDiskShortcutFixerInvertSelection.Name = "tsmiDiskShortcutFixerInvertSelection";
            this.tsmiDiskShortcutFixerInvertSelection.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerInvertSelection.Text = "Invert Selection";
            // 
            // tsmiDiskShortcutFixerSeparator2
            // 
            this.tsmiDiskShortcutFixerSeparator2.Name = "tsmiDiskShortcutFixerSeparator2";
            this.tsmiDiskShortcutFixerSeparator2.Size = new System.Drawing.Size(152, 6);
            // 
            // tsmiDiskShortcutFixerProperties
            // 
            this.tsmiDiskShortcutFixerProperties.Enabled = false;
            this.tsmiDiskShortcutFixerProperties.Image = global::Win.Properties.Resources.properties_16x16;
            this.tsmiDiskShortcutFixerProperties.Name = "tsmiDiskShortcutFixerProperties";
            this.tsmiDiskShortcutFixerProperties.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerProperties.Text = "Properties";
            // 
            // tsmiDiskShortcutFixerOpenFolder
            // 
            this.tsmiDiskShortcutFixerOpenFolder.Enabled = false;
            this.tsmiDiskShortcutFixerOpenFolder.Image = global::Win.Properties.Resources.folder_16x16;
            this.tsmiDiskShortcutFixerOpenFolder.Name = "tsmiDiskShortcutFixerOpenFolder";
            this.tsmiDiskShortcutFixerOpenFolder.Size = new System.Drawing.Size(155, 22);
            this.tsmiDiskShortcutFixerOpenFolder.Text = "Open Folder";
            // 
            // ilDiskShortcutFixer
            // 
            this.ilDiskShortcutFixer.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilDiskShortcutFixer.ImageStream")));
            this.ilDiskShortcutFixer.TransparentColor = System.Drawing.Color.Transparent;
            this.ilDiskShortcutFixer.Images.SetKeyName(0, "shortcut_16x16.png");
            this.ilDiskShortcutFixer.Images.SetKeyName(1, "folder_16x16.png");
            // 
            // btnDiskShortcutFixerDeleteSelected
            // 
            this.btnDiskShortcutFixerDeleteSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskShortcutFixerDeleteSelected.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskShortcutFixerDeleteSelected.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskShortcutFixerDeleteSelected.ForeColor = System.Drawing.Color.White;
            this.btnDiskShortcutFixerDeleteSelected.Location = new System.Drawing.Point(576, 377);
            this.btnDiskShortcutFixerDeleteSelected.Name = "btnDiskShortcutFixerDeleteSelected";
            this.btnDiskShortcutFixerDeleteSelected.Size = new System.Drawing.Size(147, 27);
            this.btnDiskShortcutFixerDeleteSelected.TabIndex = 92;
            this.btnDiskShortcutFixerDeleteSelected.Text = "Delete Selected";
            this.btnDiskShortcutFixerDeleteSelected.Click += new System.EventHandler(this.btnDiskShortcutFixerDeleteSelected_Click);
            // 
            // lblDiskShortcutFixerStatus
            // 
            this.lblDiskShortcutFixerStatus.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblDiskShortcutFixerStatus.Location = new System.Drawing.Point(189, 60);
            this.lblDiskShortcutFixerStatus.Name = "lblDiskShortcutFixerStatus";
            this.lblDiskShortcutFixerStatus.Size = new System.Drawing.Size(31, 13);
            this.lblDiskShortcutFixerStatus.TabIndex = 26;
            this.lblDiskShortcutFixerStatus.Tag = "There are {0} dead shortcuts and {1} empty folders.";
            this.lblDiskShortcutFixerStatus.Text = "Status";
            // 
            // btnDiskShortcutFixerScan
            // 
            this.btnDiskShortcutFixerScan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskShortcutFixerScan.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskShortcutFixerScan.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskShortcutFixerScan.ForeColor = System.Drawing.Color.White;
            this.btnDiskShortcutFixerScan.Location = new System.Drawing.Point(503, 377);
            this.btnDiskShortcutFixerScan.Name = "btnDiskShortcutFixerScan";
            this.btnDiskShortcutFixerScan.Size = new System.Drawing.Size(67, 27);
            this.btnDiskShortcutFixerScan.TabIndex = 91;
            this.btnDiskShortcutFixerScan.Text = "Scan";
            this.btnDiskShortcutFixerScan.Click += new System.EventHandler(this.btnDiskShortcutFixerScan_Click);
            // 
            // pnlDiskUninstallManager
            // 
            this.pnlDiskUninstallManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlDiskUninstallManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlDiskUninstallManager.Caption = "Uninstall Manager";
            this.pnlDiskUninstallManager.Controls.Add(this.lvDiskUninstallManager);
            this.pnlDiskUninstallManager.Controls.Add(this.btnDiskUninstallManagerUninstall);
            this.pnlDiskUninstallManager.Controls.Add(this.txtDiskUninstallManagerSearchFilter);
            this.pnlDiskUninstallManager.Controls.Add(this.lblDiskUninstallManagerSearchFilter);
            this.pnlDiskUninstallManager.Controls.Add(this.btnDiskUninstallManagerRemoveEntry);
            this.pnlDiskUninstallManager.Description = "Uninstall software that you no longer use to increase free disk space";
            this.pnlDiskUninstallManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDiskUninstallManager.HideLogo = true;
            this.pnlDiskUninstallManager.Icon = global::Win.Properties.Resources.uninstall_64x64;
            this.pnlDiskUninstallManager.IsIconOnLeft = true;
            this.pnlDiskUninstallManager.Location = new System.Drawing.Point(0, 0);
            this.pnlDiskUninstallManager.Name = "pnlDiskUninstallManager";
            this.pnlDiskUninstallManager.NavBarImage = global::Win.Properties.Resources.uninstall_nav_bar;
            this.pnlDiskUninstallManager.Size = new System.Drawing.Size(741, 416);
            this.pnlDiskUninstallManager.TabIndex = 50;
            // 
            // lvDiskUninstallManager
            // 
            this.lvDiskUninstallManager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDiskUninstallManager.BackgroundImageTiled = true;
            this.lvDiskUninstallManager.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDiskUninstallManager.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDiskUninstallManagerProgram,
            this.colDiskUninstallManagerPublisher,
            this.colDiskUninstallManagerEstimatedSize,
            this.colDiskUninstallManagerInstallLocation,
            this.colDiskUninstallManagerVersion,
            this.colDiskUninstallManagerContact,
            this.colDiskUninstallManagerHelpLink,
            this.colDiskUninstallManagerInstallSource,
            this.colDiskUninstallManagerUrlInfoAbout});
            this.lvDiskUninstallManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvDiskUninstallManager.FullRowSelect = true;
            this.lvDiskUninstallManager.GridLines = true;
            this.lvDiskUninstallManager.HideSelection = false;
            this.lvDiskUninstallManager.Location = new System.Drawing.Point(179, 80);
            this.lvDiskUninstallManager.MultiSelect = false;
            this.lvDiskUninstallManager.Name = "lvDiskUninstallManager";
            this.lvDiskUninstallManager.Size = new System.Drawing.Size(560, 282);
            this.lvDiskUninstallManager.SmallImageList = this.ilDiskUninstallManager;
            this.lvDiskUninstallManager.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDiskUninstallManager.TabIndex = 2;
            this.lvDiskUninstallManager.UseCompatibleStateImageBehavior = false;
            this.lvDiskUninstallManager.View = System.Windows.Forms.View.Details;
            this.lvDiskUninstallManager.SelectedIndexChanged += new System.EventHandler(this.lvDiskUninstallManager_SelectedIndexChanged);
            this.lvDiskUninstallManager.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvDiskUninstallManager_ColumnClick);
            // 
            // colDiskUninstallManagerProgram
            // 
            this.colDiskUninstallManagerProgram.Text = "Program";
            this.colDiskUninstallManagerProgram.Width = 150;
            // 
            // colDiskUninstallManagerPublisher
            // 
            this.colDiskUninstallManagerPublisher.Text = "Publisher";
            this.colDiskUninstallManagerPublisher.Width = 70;
            // 
            // colDiskUninstallManagerEstimatedSize
            // 
            this.colDiskUninstallManagerEstimatedSize.Text = "Estimated Size";
            this.colDiskUninstallManagerEstimatedSize.Width = 50;
            // 
            // colDiskUninstallManagerInstallLocation
            // 
            this.colDiskUninstallManagerInstallLocation.Text = "Install Location";
            this.colDiskUninstallManagerInstallLocation.Width = 100;
            // 
            // colDiskUninstallManagerVersion
            // 
            this.colDiskUninstallManagerVersion.Text = "Version";
            this.colDiskUninstallManagerVersion.Width = 50;
            // 
            // colDiskUninstallManagerContact
            // 
            this.colDiskUninstallManagerContact.Text = "Contact";
            // 
            // colDiskUninstallManagerHelpLink
            // 
            this.colDiskUninstallManagerHelpLink.Text = "Help Link";
            // 
            // colDiskUninstallManagerInstallSource
            // 
            this.colDiskUninstallManagerInstallSource.Text = "Install Source";
            // 
            // colDiskUninstallManagerUrlInfoAbout
            // 
            this.colDiskUninstallManagerUrlInfoAbout.Text = "About Url";
            // 
            // ilDiskUninstallManager
            // 
            this.ilDiskUninstallManager.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilDiskUninstallManager.ImageSize = new System.Drawing.Size(16, 16);
            this.ilDiskUninstallManager.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btnDiskUninstallManagerUninstall
            // 
            this.btnDiskUninstallManagerUninstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskUninstallManagerUninstall.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskUninstallManagerUninstall.Enabled = false;
            this.btnDiskUninstallManagerUninstall.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskUninstallManagerUninstall.ForeColor = System.Drawing.Color.White;
            this.btnDiskUninstallManagerUninstall.Location = new System.Drawing.Point(633, 377);
            this.btnDiskUninstallManagerUninstall.Name = "btnDiskUninstallManagerUninstall";
            this.btnDiskUninstallManagerUninstall.Size = new System.Drawing.Size(90, 27);
            this.btnDiskUninstallManagerUninstall.TabIndex = 93;
            this.btnDiskUninstallManagerUninstall.Text = "Uninstall";
            this.btnDiskUninstallManagerUninstall.Click += new System.EventHandler(this.btnDiskUninstallManagerUninstall_Click);
            // 
            // txtDiskUninstallManagerSearchFilter
            // 
            this.txtDiskUninstallManagerSearchFilter.Location = new System.Drawing.Point(235, 56);
            this.txtDiskUninstallManagerSearchFilter.Name = "txtDiskUninstallManagerSearchFilter";
            this.txtDiskUninstallManagerSearchFilter.Size = new System.Drawing.Size(244, 19);
            this.txtDiskUninstallManagerSearchFilter.TabIndex = 3;
            this.txtDiskUninstallManagerSearchFilter.TextChanged += new System.EventHandler(this.txtDiskUninstallManagerSearchFilter_TextChanged);
            this.txtDiskUninstallManagerSearchFilter.Leave += new System.EventHandler(this.txtDiskUninstallManagerSearchFilter_Leave);
            this.txtDiskUninstallManagerSearchFilter.Enter += new System.EventHandler(this.txtDiskUninstallManagerSearchFilter_Enter);
            // 
            // lblDiskUninstallManagerSearchFilter
            // 
            this.lblDiskUninstallManagerSearchFilter.Location = new System.Drawing.Point(189, 60);
            this.lblDiskUninstallManagerSearchFilter.Name = "lblDiskUninstallManagerSearchFilter";
            this.lblDiskUninstallManagerSearchFilter.Size = new System.Drawing.Size(37, 13);
            this.lblDiskUninstallManagerSearchFilter.TabIndex = 94;
            this.lblDiskUninstallManagerSearchFilter.Text = "Search:";
            // 
            // btnDiskUninstallManagerRemoveEntry
            // 
            this.btnDiskUninstallManagerRemoveEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskUninstallManagerRemoveEntry.BackColor = System.Drawing.Color.Transparent;
            this.btnDiskUninstallManagerRemoveEntry.Enabled = false;
            this.btnDiskUninstallManagerRemoveEntry.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDiskUninstallManagerRemoveEntry.ForeColor = System.Drawing.Color.White;
            this.btnDiskUninstallManagerRemoveEntry.Location = new System.Drawing.Point(502, 377);
            this.btnDiskUninstallManagerRemoveEntry.Name = "btnDiskUninstallManagerRemoveEntry";
            this.btnDiskUninstallManagerRemoveEntry.Size = new System.Drawing.Size(125, 27);
            this.btnDiskUninstallManagerRemoveEntry.TabIndex = 92;
            this.btnDiskUninstallManagerRemoveEntry.Text = "Remove Entry";
            this.btnDiskUninstallManagerRemoveEntry.Click += new System.EventHandler(this.btnDiskUninstallManagerRemoveEntry_Click);
            // 
            // pnlOptimize
            // 
            this.pnlOptimize.BackColor = System.Drawing.Color.White;
            this.pnlOptimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimize.Controls.Add(this.pnlOptimizeMain);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeStartupManager);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeOptimizeWindows);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeRegistryManager);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeProcessManager);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeBrowseObjectManager);
            this.pnlOptimize.Controls.Add(this.pnlOptimizeRegistryDefragmenter);
            this.pnlOptimize.Location = new System.Drawing.Point(8, 73);
            this.pnlOptimize.Name = "pnlOptimize";
            this.pnlOptimize.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimize.TabIndex = 1;
            // 
            // pnlOptimizeMain
            // 
            this.pnlOptimizeMain.BackgroundImage = global::Win.Properties.Resources.panel_background_main_left;
            this.pnlOptimizeMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeMain.Caption = "Optimize Your PC";
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeRegistryDefragmenter);
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeBrowserObjectManager);
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeStartupManager);
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeProcessManager);
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeOptimizeWindows);
            this.pnlOptimizeMain.Controls.Add(this.btnOptimizeRegistryManager);
            this.pnlOptimizeMain.Description = "Access tools to optimize and tweak the performance of your computer";
            this.pnlOptimizeMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeMain.HideLogo = false;
            this.pnlOptimizeMain.Icon = global::Win.Properties.Resources.tune_up_64x64;
            this.pnlOptimizeMain.IsIconOnLeft = true;
            this.pnlOptimizeMain.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeMain.Name = "pnlOptimizeMain";
            this.pnlOptimizeMain.NavBarImage = global::Win.Properties.Resources.optimize_nav_bar;
            this.pnlOptimizeMain.SeparatorCount = 2;
            this.pnlOptimizeMain.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeMain.TabIndex = 19;
            this.pnlOptimizeMain.RegisterClick += new System.EventHandler(this.btnRegister_Click);
            this.pnlOptimizeMain.PushToTalkClick += new System.EventHandler(this.btnPushToTalkClick_Click);
            // 
            // btnOptimizeRegistryDefragmenter
            // 
            this.btnOptimizeRegistryDefragmenter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeRegistryDefragmenter.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryDefragmenter.Description = "Reduce the amount of RAM the registry takes up.";
            this.btnOptimizeRegistryDefragmenter.Icon = global::Win.Properties.Resources.registry_defragmenter_64x64;
            this.btnOptimizeRegistryDefragmenter.Location = new System.Drawing.Point(190, 269);
            this.btnOptimizeRegistryDefragmenter.Name = "btnOptimizeRegistryDefragmenter";
            this.btnOptimizeRegistryDefragmenter.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeRegistryDefragmenter.TabIndex = 84;
            this.btnOptimizeRegistryDefragmenter.Title = "Registry Defragmenter";
            this.btnOptimizeRegistryDefragmenter.ButtonClick += new System.EventHandler(this.btnOptimizeRegistryDefragmenter_Click);
            // 
            // btnOptimizeBrowserObjectManager
            // 
            this.btnOptimizeBrowserObjectManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeBrowserObjectManager.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeBrowserObjectManager.Description = "Enable or disable browser helper objects to improve web browsing.";
            this.btnOptimizeBrowserObjectManager.Icon = global::Win.Properties.Resources.browser_object_64x64;
            this.btnOptimizeBrowserObjectManager.Location = new System.Drawing.Point(473, 269);
            this.btnOptimizeBrowserObjectManager.Name = "btnOptimizeBrowserObjectManager";
            this.btnOptimizeBrowserObjectManager.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeBrowserObjectManager.TabIndex = 83;
            this.btnOptimizeBrowserObjectManager.Title = "Browser Object Manager";
            this.btnOptimizeBrowserObjectManager.ButtonClick += new System.EventHandler(this.btnOptimizeBrowserObjectManager_Click);
            // 
            // btnOptimizeStartupManager
            // 
            this.btnOptimizeStartupManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeStartupManager.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManager.Description = "Speed up Windows boot time by disabling unnecessary items.";
            this.btnOptimizeStartupManager.Icon = global::Win.Properties.Resources.startup_64x64;
            this.btnOptimizeStartupManager.Location = new System.Drawing.Point(473, 166);
            this.btnOptimizeStartupManager.Name = "btnOptimizeStartupManager";
            this.btnOptimizeStartupManager.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeStartupManager.TabIndex = 81;
            this.btnOptimizeStartupManager.Title = "Startup Manager";
            this.btnOptimizeStartupManager.ButtonClick += new System.EventHandler(this.btnOptimizeStartupManager_Click);
            // 
            // btnOptimizeProcessManager
            // 
            this.btnOptimizeProcessManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeProcessManager.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeProcessManager.Description = "View and manage processes that are running on your computer.";
            this.btnOptimizeProcessManager.Icon = global::Win.Properties.Resources.process_64x64;
            this.btnOptimizeProcessManager.Location = new System.Drawing.Point(190, 166);
            this.btnOptimizeProcessManager.Name = "btnOptimizeProcessManager";
            this.btnOptimizeProcessManager.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeProcessManager.TabIndex = 79;
            this.btnOptimizeProcessManager.Title = "Process Manager";
            this.btnOptimizeProcessManager.ButtonClick += new System.EventHandler(this.btnOptimizeProcessManager_Click);
            // 
            // btnOptimizeOptimizeWindows
            // 
            this.btnOptimizeOptimizeWindows.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeOptimizeWindows.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeOptimizeWindows.Description = "Manage Windows settings to fine tune your PC performance.";
            this.btnOptimizeOptimizeWindows.Icon = global::Win.Properties.Resources.optimize_windows_64x64;
            this.btnOptimizeOptimizeWindows.Location = new System.Drawing.Point(190, 63);
            this.btnOptimizeOptimizeWindows.Name = "btnOptimizeOptimizeWindows";
            this.btnOptimizeOptimizeWindows.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeOptimizeWindows.TabIndex = 80;
            this.btnOptimizeOptimizeWindows.Title = "Optimize Windows";
            this.btnOptimizeOptimizeWindows.ButtonClick += new System.EventHandler(this.btnOptimizeOptimizeWindows_Click);
            // 
            // btnOptimizeRegistryManager
            // 
            this.btnOptimizeRegistryManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptimizeRegistryManager.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryManager.Description = "Find and repair Windows registry problems.";
            this.btnOptimizeRegistryManager.Icon = global::Win.Properties.Resources.registry_64x64;
            this.btnOptimizeRegistryManager.Location = new System.Drawing.Point(473, 63);
            this.btnOptimizeRegistryManager.Name = "btnOptimizeRegistryManager";
            this.btnOptimizeRegistryManager.Size = new System.Drawing.Size(285, 108);
            this.btnOptimizeRegistryManager.TabIndex = 82;
            this.btnOptimizeRegistryManager.Title = "Registry Cleaner";
            this.btnOptimizeRegistryManager.ButtonClick += new System.EventHandler(this.btnOptimizeRegistryManager_Click);
            // 
            // pnlOptimizeStartupManager
            // 
            this.pnlOptimizeStartupManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeStartupManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeStartupManager.Caption = "Startup Manager";
            this.pnlOptimizeStartupManager.Controls.Add(this.tvaOptimizeStartupManager);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerRefresh);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerRun);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerEdit);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerView);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerDelete);
            this.pnlOptimizeStartupManager.Controls.Add(this.btnOptimizeStartupManagerAdd);
            this.pnlOptimizeStartupManager.Description = "Speed up Windows boot time by disabling unnecessary items";
            this.pnlOptimizeStartupManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeStartupManager.HideLogo = true;
            this.pnlOptimizeStartupManager.Icon = global::Win.Properties.Resources.startup_64x64;
            this.pnlOptimizeStartupManager.IsIconOnLeft = true;
            this.pnlOptimizeStartupManager.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeStartupManager.Name = "pnlOptimizeStartupManager";
            this.pnlOptimizeStartupManager.NavBarImage = global::Win.Properties.Resources.startup_nav_bar;
            this.pnlOptimizeStartupManager.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeStartupManager.TabIndex = 20;
            // 
            // tvaOptimizeStartupManager
            // 
            this.tvaOptimizeStartupManager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvaOptimizeStartupManager.BackColor = System.Drawing.SystemColors.Window;
            this.tvaOptimizeStartupManager.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvaOptimizeStartupManager.Columns.Add(this.treeColumn15);
            this.tvaOptimizeStartupManager.Columns.Add(this.treeColumn25);
            this.tvaOptimizeStartupManager.Columns.Add(this.treeColumn35);
            this.tvaOptimizeStartupManager.DefaultToolTipProvider = null;
            this.tvaOptimizeStartupManager.DragDropMarkColor = System.Drawing.Color.Black;
            this.tvaOptimizeStartupManager.FullRowSelect = true;
            this.tvaOptimizeStartupManager.GridLineStyle = Common_Tools.TreeViewAdv.Tree.GridLineStyle.Horizontal;
            this.tvaOptimizeStartupManager.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tvaOptimizeStartupManager.Location = new System.Drawing.Point(179, 55);
            this.tvaOptimizeStartupManager.Model = null;
            this.tvaOptimizeStartupManager.Name = "tvaOptimizeStartupManager";
            this.tvaOptimizeStartupManager.NodeControls.Add(this.nodeIcon15);
            this.tvaOptimizeStartupManager.NodeControls.Add(this.nodeTextBoxSection5);
            this.tvaOptimizeStartupManager.NodeControls.Add(this.nodeTextBoxItem5);
            this.tvaOptimizeStartupManager.NodeControls.Add(this.nodeTextBoxPath5);
            this.tvaOptimizeStartupManager.NodeControls.Add(this.nodeTextBoxArgs5);
            this.tvaOptimizeStartupManager.SelectedNode = null;
            this.tvaOptimizeStartupManager.Size = new System.Drawing.Size(560, 307);
            this.tvaOptimizeStartupManager.TabIndex = 1;
            this.tvaOptimizeStartupManager.Text = "tvaOptimizeStartupManager";
            this.tvaOptimizeStartupManager.UseColumns = true;
            this.tvaOptimizeStartupManager.SelectionChanged += new System.EventHandler(this.tvaOptimizeStartupManager_SelectionChanged);
            this.tvaOptimizeStartupManager.DoubleClick += new System.EventHandler(this.tvaOptimizeStartupManager_DoubleClick);
            // 
            // treeColumn15
            // 
            this.treeColumn15.Header = "Section";
            this.treeColumn15.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn15.TooltipText = null;
            this.treeColumn15.Width = 200;
            // 
            // treeColumn25
            // 
            this.treeColumn25.Header = "Path";
            this.treeColumn25.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn25.TooltipText = null;
            this.treeColumn25.Width = 250;
            // 
            // treeColumn35
            // 
            this.treeColumn35.Header = "Arguments";
            this.treeColumn35.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn35.TooltipText = null;
            this.treeColumn35.Width = 60;
            // 
            // nodeIcon15
            // 
            this.nodeIcon15.DataPropertyName = "Image";
            this.nodeIcon15.LeftMargin = 1;
            this.nodeIcon15.ParentColumn = this.treeColumn15;
            this.nodeIcon15.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeTextBoxSection5
            // 
            this.nodeTextBoxSection5.DataPropertyName = "Section";
            this.nodeTextBoxSection5.IncrementalSearchEnabled = true;
            this.nodeTextBoxSection5.LeftMargin = 3;
            this.nodeTextBoxSection5.ParentColumn = this.treeColumn15;
            this.nodeTextBoxSection5.UseCompatibleTextRendering = true;
            // 
            // nodeTextBoxItem5
            // 
            this.nodeTextBoxItem5.DataPropertyName = "Item";
            this.nodeTextBoxItem5.IncrementalSearchEnabled = true;
            this.nodeTextBoxItem5.LeftMargin = 3;
            this.nodeTextBoxItem5.ParentColumn = this.treeColumn15;
            // 
            // nodeTextBoxPath5
            // 
            this.nodeTextBoxPath5.DataPropertyName = "Path";
            this.nodeTextBoxPath5.IncrementalSearchEnabled = true;
            this.nodeTextBoxPath5.LeftMargin = 3;
            this.nodeTextBoxPath5.ParentColumn = this.treeColumn25;
            // 
            // nodeTextBoxArgs5
            // 
            this.nodeTextBoxArgs5.DataPropertyName = "Args";
            this.nodeTextBoxArgs5.IncrementalSearchEnabled = true;
            this.nodeTextBoxArgs5.LeftMargin = 3;
            this.nodeTextBoxArgs5.ParentColumn = this.treeColumn35;
            // 
            // btnOptimizeStartupManagerRefresh
            // 
            this.btnOptimizeStartupManagerRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerRefresh.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerRefresh.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerRefresh.Location = new System.Drawing.Point(632, 377);
            this.btnOptimizeStartupManagerRefresh.Name = "btnOptimizeStartupManagerRefresh";
            this.btnOptimizeStartupManagerRefresh.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerRefresh.TabIndex = 91;
            this.btnOptimizeStartupManagerRefresh.Text = "Refresh";
            this.btnOptimizeStartupManagerRefresh.Click += new System.EventHandler(this.btnOptimizeStartupManagerRefresh_Click);
            // 
            // btnOptimizeStartupManagerRun
            // 
            this.btnOptimizeStartupManagerRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerRun.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerRun.Enabled = false;
            this.btnOptimizeStartupManagerRun.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerRun.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerRun.Location = new System.Drawing.Point(535, 377);
            this.btnOptimizeStartupManagerRun.Name = "btnOptimizeStartupManagerRun";
            this.btnOptimizeStartupManagerRun.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerRun.TabIndex = 90;
            this.btnOptimizeStartupManagerRun.Text = "Run";
            this.btnOptimizeStartupManagerRun.Click += new System.EventHandler(this.btnOptimizeStartupManagerRun_Click);
            // 
            // btnOptimizeStartupManagerEdit
            // 
            this.btnOptimizeStartupManagerEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerEdit.Enabled = false;
            this.btnOptimizeStartupManagerEdit.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerEdit.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerEdit.Location = new System.Drawing.Point(244, 377);
            this.btnOptimizeStartupManagerEdit.Name = "btnOptimizeStartupManagerEdit";
            this.btnOptimizeStartupManagerEdit.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerEdit.TabIndex = 92;
            this.btnOptimizeStartupManagerEdit.Text = "Edit";
            this.btnOptimizeStartupManagerEdit.Click += new System.EventHandler(this.btnOptimizeStartupManagerEdit_Click);
            // 
            // btnOptimizeStartupManagerView
            // 
            this.btnOptimizeStartupManagerView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerView.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerView.Enabled = false;
            this.btnOptimizeStartupManagerView.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerView.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerView.Location = new System.Drawing.Point(438, 377);
            this.btnOptimizeStartupManagerView.Name = "btnOptimizeStartupManagerView";
            this.btnOptimizeStartupManagerView.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerView.TabIndex = 89;
            this.btnOptimizeStartupManagerView.Text = "View";
            this.btnOptimizeStartupManagerView.Click += new System.EventHandler(this.btnOptimizeStartupManagerView_Click);
            // 
            // btnOptimizeStartupManagerDelete
            // 
            this.btnOptimizeStartupManagerDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerDelete.Enabled = false;
            this.btnOptimizeStartupManagerDelete.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerDelete.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerDelete.Location = new System.Drawing.Point(341, 377);
            this.btnOptimizeStartupManagerDelete.Name = "btnOptimizeStartupManagerDelete";
            this.btnOptimizeStartupManagerDelete.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerDelete.TabIndex = 88;
            this.btnOptimizeStartupManagerDelete.Text = "Delete";
            this.btnOptimizeStartupManagerDelete.Click += new System.EventHandler(this.btnOptimizeStartupManagerDelete_Click);
            // 
            // btnOptimizeStartupManagerAdd
            // 
            this.btnOptimizeStartupManagerAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeStartupManagerAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeStartupManagerAdd.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeStartupManagerAdd.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeStartupManagerAdd.Location = new System.Drawing.Point(147, 377);
            this.btnOptimizeStartupManagerAdd.Name = "btnOptimizeStartupManagerAdd";
            this.btnOptimizeStartupManagerAdd.Size = new System.Drawing.Size(91, 27);
            this.btnOptimizeStartupManagerAdd.TabIndex = 87;
            this.btnOptimizeStartupManagerAdd.Text = "Add";
            this.btnOptimizeStartupManagerAdd.Click += new System.EventHandler(this.btnOptimizeStartupManagerAdd_Click);
            // 
            // pnlOptimizeOptimizeWindows
            // 
            this.pnlOptimizeOptimizeWindows.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeOptimizeWindows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeOptimizeWindows.Caption = "Optimize Windows";
            this.pnlOptimizeOptimizeWindows.Controls.Add(this.pnlOptimizeOptimizeWindowsInside);
            this.pnlOptimizeOptimizeWindows.Description = "Manage Windows settings to fine tune your PC performance";
            this.pnlOptimizeOptimizeWindows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeOptimizeWindows.HideLogo = false;
            this.pnlOptimizeOptimizeWindows.Icon = global::Win.Properties.Resources.optimize_windows_64x64;
            this.pnlOptimizeOptimizeWindows.IsIconOnLeft = true;
            this.pnlOptimizeOptimizeWindows.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeOptimizeWindows.Name = "pnlOptimizeOptimizeWindows";
            this.pnlOptimizeOptimizeWindows.NavBarImage = global::Win.Properties.Resources.windows_nav_bar;
            this.pnlOptimizeOptimizeWindows.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeOptimizeWindows.TabIndex = 20;
            // 
            // pnlOptimizeOptimizeWindowsInside
            // 
            this.pnlOptimizeOptimizeWindowsInside.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlOptimizeOptimizeWindowsInside.BackColor = System.Drawing.Color.Transparent;
            this.pnlOptimizeOptimizeWindowsInside.Controls.Add(this.cbOptimizeClearPageFileOnReboot);
            this.pnlOptimizeOptimizeWindowsInside.Controls.Add(this.cbOptimizeAllowHybernation);
            this.pnlOptimizeOptimizeWindowsInside.Controls.Add(this.cbOptimizeEnableDesktopSearch);
            this.pnlOptimizeOptimizeWindowsInside.Controls.Add(this.cbOptimizeSendErrorsToMicrosoft);
            this.pnlOptimizeOptimizeWindowsInside.Controls.Add(this.cbOptimizeEnablePrefetch);
            this.pnlOptimizeOptimizeWindowsInside.Location = new System.Drawing.Point(179, 55);
            this.pnlOptimizeOptimizeWindowsInside.Name = "pnlOptimizeOptimizeWindowsInside";
            this.pnlOptimizeOptimizeWindowsInside.Size = new System.Drawing.Size(560, 307);
            this.pnlOptimizeOptimizeWindowsInside.TabIndex = 0;
            // 
            // cbOptimizeClearPageFileOnReboot
            // 
            this.cbOptimizeClearPageFileOnReboot.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbOptimizeClearPageFileOnReboot.AutoSize = true;
            this.cbOptimizeClearPageFileOnReboot.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbOptimizeClearPageFileOnReboot.ForeColor = System.Drawing.Color.Black;
            this.cbOptimizeClearPageFileOnReboot.Location = new System.Drawing.Point(205, 191);
            this.cbOptimizeClearPageFileOnReboot.Name = "cbOptimizeClearPageFileOnReboot";
            this.cbOptimizeClearPageFileOnReboot.Size = new System.Drawing.Size(150, 17);
            this.cbOptimizeClearPageFileOnReboot.TabIndex = 11;
            this.cbOptimizeClearPageFileOnReboot.Text = "Clear Page File on Reboot";
            this.cbOptimizeClearPageFileOnReboot.UseVisualStyleBackColor = true;
            this.cbOptimizeClearPageFileOnReboot.CheckedChanged += new System.EventHandler(this.cbOptimizeOptimizeWindows_CheckedChanged);
            // 
            // cbOptimizeAllowHybernation
            // 
            this.cbOptimizeAllowHybernation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbOptimizeAllowHybernation.AutoSize = true;
            this.cbOptimizeAllowHybernation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbOptimizeAllowHybernation.ForeColor = System.Drawing.Color.Black;
            this.cbOptimizeAllowHybernation.Location = new System.Drawing.Point(205, 99);
            this.cbOptimizeAllowHybernation.Name = "cbOptimizeAllowHybernation";
            this.cbOptimizeAllowHybernation.Size = new System.Drawing.Size(108, 17);
            this.cbOptimizeAllowHybernation.TabIndex = 7;
            this.cbOptimizeAllowHybernation.Text = "Allow Hibernation";
            this.cbOptimizeAllowHybernation.UseVisualStyleBackColor = true;
            this.cbOptimizeAllowHybernation.CheckedChanged += new System.EventHandler(this.cbOptimizeOptimizeWindows_CheckedChanged);
            // 
            // cbOptimizeEnableDesktopSearch
            // 
            this.cbOptimizeEnableDesktopSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbOptimizeEnableDesktopSearch.AutoSize = true;
            this.cbOptimizeEnableDesktopSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbOptimizeEnableDesktopSearch.ForeColor = System.Drawing.Color.Black;
            this.cbOptimizeEnableDesktopSearch.Location = new System.Drawing.Point(205, 168);
            this.cbOptimizeEnableDesktopSearch.Name = "cbOptimizeEnableDesktopSearch";
            this.cbOptimizeEnableDesktopSearch.Size = new System.Drawing.Size(139, 17);
            this.cbOptimizeEnableDesktopSearch.TabIndex = 10;
            this.cbOptimizeEnableDesktopSearch.Text = "Enable Desktop Search";
            this.cbOptimizeEnableDesktopSearch.UseVisualStyleBackColor = true;
            this.cbOptimizeEnableDesktopSearch.CheckedChanged += new System.EventHandler(this.cbOptimizeOptimizeWindows_CheckedChanged);
            // 
            // cbOptimizeSendErrorsToMicrosoft
            // 
            this.cbOptimizeSendErrorsToMicrosoft.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbOptimizeSendErrorsToMicrosoft.AutoSize = true;
            this.cbOptimizeSendErrorsToMicrosoft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbOptimizeSendErrorsToMicrosoft.ForeColor = System.Drawing.Color.Black;
            this.cbOptimizeSendErrorsToMicrosoft.Location = new System.Drawing.Point(205, 122);
            this.cbOptimizeSendErrorsToMicrosoft.Name = "cbOptimizeSendErrorsToMicrosoft";
            this.cbOptimizeSendErrorsToMicrosoft.Size = new System.Drawing.Size(139, 17);
            this.cbOptimizeSendErrorsToMicrosoft.TabIndex = 8;
            this.cbOptimizeSendErrorsToMicrosoft.Text = "Send Errors to Microsoft";
            this.cbOptimizeSendErrorsToMicrosoft.UseVisualStyleBackColor = true;
            this.cbOptimizeSendErrorsToMicrosoft.CheckedChanged += new System.EventHandler(this.cbOptimizeOptimizeWindows_CheckedChanged);
            // 
            // cbOptimizeEnablePrefetch
            // 
            this.cbOptimizeEnablePrefetch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbOptimizeEnablePrefetch.AutoSize = true;
            this.cbOptimizeEnablePrefetch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.cbOptimizeEnablePrefetch.ForeColor = System.Drawing.Color.Black;
            this.cbOptimizeEnablePrefetch.Location = new System.Drawing.Point(205, 145);
            this.cbOptimizeEnablePrefetch.Name = "cbOptimizeEnablePrefetch";
            this.cbOptimizeEnablePrefetch.Size = new System.Drawing.Size(102, 17);
            this.cbOptimizeEnablePrefetch.TabIndex = 9;
            this.cbOptimizeEnablePrefetch.Text = "Enable Prefetch";
            this.cbOptimizeEnablePrefetch.UseVisualStyleBackColor = true;
            this.cbOptimizeEnablePrefetch.CheckedChanged += new System.EventHandler(this.cbOptimizeOptimizeWindows_CheckedChanged);
            // 
            // pnlOptimizeRegistryManager
            // 
            this.pnlOptimizeRegistryManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeRegistryManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeRegistryManager.Caption = "Registry Repair";
            this.pnlOptimizeRegistryManager.Controls.Add(this.btnOptimizeRegistryManagerScan);
            this.pnlOptimizeRegistryManager.Controls.Add(this.tvaOptimizeRegistryManager);
            this.pnlOptimizeRegistryManager.Controls.Add(this.treeView1);
            this.pnlOptimizeRegistryManager.Controls.Add(this.btnOptimizeRegistryManagerFixAll);
            this.pnlOptimizeRegistryManager.Controls.Add(this.labelControl4);
            this.pnlOptimizeRegistryManager.Controls.Add(this.btnOptimizeRegistryManagerIgnoreList);
            this.pnlOptimizeRegistryManager.Controls.Add(this.labelControl3);
            this.pnlOptimizeRegistryManager.Description = "Find and repair Windows registry problems";
            this.pnlOptimizeRegistryManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeRegistryManager.HideLogo = true;
            this.pnlOptimizeRegistryManager.Icon = global::Win.Properties.Resources.registry_64x64;
            this.pnlOptimizeRegistryManager.IsIconOnLeft = true;
            this.pnlOptimizeRegistryManager.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeRegistryManager.Name = "pnlOptimizeRegistryManager";
            this.pnlOptimizeRegistryManager.NavBarImage = global::Win.Properties.Resources.registry_nav_bar;
            this.pnlOptimizeRegistryManager.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeRegistryManager.TabIndex = 25;
            // 
            // btnOptimizeRegistryManagerScan
            // 
            this.btnOptimizeRegistryManagerScan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeRegistryManagerScan.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryManagerScan.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeRegistryManagerScan.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeRegistryManagerScan.Location = new System.Drawing.Point(294, 377);
            this.btnOptimizeRegistryManagerScan.Name = "btnOptimizeRegistryManagerScan";
            this.btnOptimizeRegistryManagerScan.Size = new System.Drawing.Size(210, 27);
            this.btnOptimizeRegistryManagerScan.TabIndex = 89;
            this.btnOptimizeRegistryManagerScan.Text = "Scan Registry for Problems";
            this.btnOptimizeRegistryManagerScan.Click += new System.EventHandler(this.btnOptimizeRegistryManagerScan_Click);
            // 
            // tvaOptimizeRegistryManager
            // 
            this.tvaOptimizeRegistryManager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvaOptimizeRegistryManager.BackColor = System.Drawing.Color.White;
            this.tvaOptimizeRegistryManager.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvaOptimizeRegistryManager.Columns.Add(this.treeColumn1);
            this.tvaOptimizeRegistryManager.Columns.Add(this.treeColumn2);
            this.tvaOptimizeRegistryManager.Columns.Add(this.treeColumn3);
            this.tvaOptimizeRegistryManager.ContextMenuStrip = this.cmnuOptimizeRegistryManager;
            this.tvaOptimizeRegistryManager.DefaultToolTipProvider = null;
            this.tvaOptimizeRegistryManager.DragDropMarkColor = System.Drawing.Color.Black;
            this.tvaOptimizeRegistryManager.FullRowSelect = true;
            this.tvaOptimizeRegistryManager.GridLineStyle = Common_Tools.TreeViewAdv.Tree.GridLineStyle.Horizontal;
            this.tvaOptimizeRegistryManager.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tvaOptimizeRegistryManager.LoadOnDemand = true;
            this.tvaOptimizeRegistryManager.Location = new System.Drawing.Point(366, 80);
            this.tvaOptimizeRegistryManager.Model = null;
            this.tvaOptimizeRegistryManager.Name = "tvaOptimizeRegistryManager";
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeCheckBox);
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeIcon);
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeSection);
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeProblem);
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeLocation);
            this.tvaOptimizeRegistryManager.NodeControls.Add(this.nodeValueName);
            this.tvaOptimizeRegistryManager.SelectedNode = null;
            this.tvaOptimizeRegistryManager.SelectionMode = Common_Tools.TreeViewAdv.Tree.TreeSelectionMode.Multi;
            this.tvaOptimizeRegistryManager.Size = new System.Drawing.Size(373, 282);
            this.tvaOptimizeRegistryManager.TabIndex = 0;
            this.tvaOptimizeRegistryManager.UseColumns = true;
            this.tvaOptimizeRegistryManager.NodeMouseDoubleClick += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeNodeAdvMouseEventArgs>(this.treeViewAdvResults_NodeMouseDoubleClick);
            this.tvaOptimizeRegistryManager.SelectionChanged += new System.EventHandler(this.tvaOptimizeRegistryManager_SelectionChanged);
            this.tvaOptimizeRegistryManager.ColumnClicked += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeColumnEventArgs>(this.treeViewAdvResults_ColumnClicked);
            this.tvaOptimizeRegistryManager.Expanded += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeViewAdvEventArgs>(this.treeViewAdvResults_Expanded);
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "Problem";
            this.treeColumn1.Sortable = true;
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 75;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Location";
            this.treeColumn2.Sortable = true;
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 175;
            // 
            // treeColumn3
            // 
            this.treeColumn3.Header = "Value Name";
            this.treeColumn3.Sortable = true;
            this.treeColumn3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn3.TooltipText = null;
            this.treeColumn3.Width = 175;
            // 
            // cmnuOptimizeRegistryManager
            // 
            this.cmnuOptimizeRegistryManager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOptimizeRegistryManagerSelectAll,
            this.tsmiOptimizeRegistryManagerSelectNone,
            this.tsmiOptimizeRegistryManagerInvertSelection,
            this.toolStripSeparator2,
            this.tsmiOptimizeRegistryManagerExcludeSelected,
            this.tsmiOptimizeRegistryManagerViewinRegEdit});
            this.cmnuOptimizeRegistryManager.Name = "contextMenuStrip1";
            this.cmnuOptimizeRegistryManager.Size = new System.Drawing.Size(162, 120);
            this.cmnuOptimizeRegistryManager.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmnuOptimizeRegistryManager_ItemClicked);
            // 
            // tsmiOptimizeRegistryManagerSelectAll
            // 
            this.tsmiOptimizeRegistryManagerSelectAll.Name = "tsmiOptimizeRegistryManagerSelectAll";
            this.tsmiOptimizeRegistryManagerSelectAll.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerSelectAll.Text = "Select All";
            // 
            // tsmiOptimizeRegistryManagerSelectNone
            // 
            this.tsmiOptimizeRegistryManagerSelectNone.Name = "tsmiOptimizeRegistryManagerSelectNone";
            this.tsmiOptimizeRegistryManagerSelectNone.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerSelectNone.Text = "Select None";
            // 
            // tsmiOptimizeRegistryManagerInvertSelection
            // 
            this.tsmiOptimizeRegistryManagerInvertSelection.Name = "tsmiOptimizeRegistryManagerInvertSelection";
            this.tsmiOptimizeRegistryManagerInvertSelection.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerInvertSelection.Text = "Invert Selection";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(158, 6);
            // 
            // tsmiOptimizeRegistryManagerExcludeSelected
            // 
            this.tsmiOptimizeRegistryManagerExcludeSelected.Enabled = false;
            this.tsmiOptimizeRegistryManagerExcludeSelected.Image = global::Win.Properties.Resources.exclude_16x16;
            this.tsmiOptimizeRegistryManagerExcludeSelected.Name = "tsmiOptimizeRegistryManagerExcludeSelected";
            this.tsmiOptimizeRegistryManagerExcludeSelected.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerExcludeSelected.Text = "Exclude Selected";
            // 
            // tsmiOptimizeRegistryManagerViewinRegEdit
            // 
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Enabled = false;
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Image = global::Win.Properties.Resources.view_16x16;
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Name = "tsmiOptimizeRegistryManagerViewinRegEdit";
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Text = "View in RegEdit";
            // 
            // nodeCheckBox
            // 
            this.nodeCheckBox.DataPropertyName = "Checked";
            this.nodeCheckBox.EditEnabled = true;
            this.nodeCheckBox.LeftMargin = 0;
            this.nodeCheckBox.ParentColumn = this.treeColumn1;
            // 
            // nodeIcon
            // 
            this.nodeIcon.DataPropertyName = "Img";
            this.nodeIcon.LeftMargin = 1;
            this.nodeIcon.ParentColumn = this.treeColumn1;
            this.nodeIcon.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeSection
            // 
            this.nodeSection.DataPropertyName = "SectionName";
            this.nodeSection.IncrementalSearchEnabled = true;
            this.nodeSection.LeftMargin = 3;
            this.nodeSection.ParentColumn = this.treeColumn1;
            this.nodeSection.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeProblem
            // 
            this.nodeProblem.DataPropertyName = "Problem";
            this.nodeProblem.IncrementalSearchEnabled = true;
            this.nodeProblem.LeftMargin = 3;
            this.nodeProblem.ParentColumn = this.treeColumn1;
            this.nodeProblem.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeLocation
            // 
            this.nodeLocation.DataPropertyName = "RegKeyPath";
            this.nodeLocation.IncrementalSearchEnabled = true;
            this.nodeLocation.LeftMargin = 3;
            this.nodeLocation.ParentColumn = this.treeColumn2;
            this.nodeLocation.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeValueName
            // 
            this.nodeValueName.DataPropertyName = "ValueName";
            this.nodeValueName.IncrementalSearchEnabled = true;
            this.nodeValueName.LeftMargin = 3;
            this.nodeValueName.ParentColumn = this.treeColumn3;
            this.nodeValueName.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.CheckBoxes = true;
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageListTreeView;
            this.treeView1.Location = new System.Drawing.Point(179, 80);
            this.treeView1.Name = "treeView1";
            treeNode13.Checked = true;
            treeNode13.ImageKey = "appinfo.ico";
            treeNode13.Name = "NodeAppInfo";
            treeNode13.SelectedImageKey = "appinfo.ico";
            treeNode13.Text = "Application Info";
            treeNode14.Checked = true;
            treeNode14.ImageKey = "programlocations.ico";
            treeNode14.Name = "NodeAppPaths";
            treeNode14.SelectedImageKey = "programlocations.ico";
            treeNode14.Text = "Program Locations";
            treeNode15.Checked = true;
            treeNode15.ImageKey = "softwaresettings.ico";
            treeNode15.Name = "NodeAppSettings";
            treeNode15.SelectedImageKey = "softwaresettings.ico";
            treeNode15.Text = "Software Settings";
            treeNode16.Checked = true;
            treeNode16.ImageKey = "startup.ico";
            treeNode16.Name = "NodeStartup";
            treeNode16.SelectedImageKey = "startup.ico";
            treeNode16.Text = "Startup";
            treeNode17.Checked = true;
            treeNode17.ImageKey = "drivers.ico";
            treeNode17.Name = "NodeDrivers";
            treeNode17.SelectedImageKey = "drivers.ico";
            treeNode17.Text = "System Drivers";
            treeNode18.Checked = true;
            treeNode18.ImageKey = "shareddlls.ico";
            treeNode18.Name = "NodeSharedDlls";
            treeNode18.SelectedImageKey = "shareddlls.ico";
            treeNode18.Text = "Shared DLLs";
            treeNode19.Checked = true;
            treeNode19.ImageKey = "helpfiles.ico";
            treeNode19.Name = "NodeHelp";
            treeNode19.SelectedImageKey = "helpfiles.ico";
            treeNode19.Text = "Help Files";
            treeNode20.Checked = true;
            treeNode20.ImageKey = "soundevents.ico";
            treeNode20.Name = "NodeSounds";
            treeNode20.SelectedImageKey = "soundevents.ico";
            treeNode20.Text = "Sound Events";
            treeNode21.Checked = true;
            treeNode21.ImageKey = "historylist.ico";
            treeNode21.Name = "NodeHistoryList";
            treeNode21.SelectedImageKey = "historylist.ico";
            treeNode21.Text = "History List";
            treeNode22.Checked = true;
            treeNode22.ImageKey = "fonts.ico";
            treeNode22.Name = "NodeFonts";
            treeNode22.SelectedImageKey = "fonts.ico";
            treeNode22.Text = "Windows Fonts";
            treeNode23.Checked = true;
            treeNode23.ImageKey = "activexcom.ico";
            treeNode23.Name = "NodeActiveX";
            treeNode23.SelectedImageKey = "activexcom.ico";
            treeNode23.Text = "ActiveX/COM";
            treeNode24.Checked = true;
            treeNode24.ImageKey = "mycomputer.ico";
            treeNode24.Name = "Node0";
            treeNode24.SelectedImageKey = "mycomputer.ico";
            treeNode24.Text = "Sections to Scan";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode24});
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.ShowNodeToolTips = true;
            this.treeView1.Size = new System.Drawing.Size(182, 282);
            this.treeView1.TabIndex = 36;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // imageListTreeView
            // 
            this.imageListTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTreeView.ImageStream")));
            this.imageListTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTreeView.Images.SetKeyName(0, "activexcom.ico");
            this.imageListTreeView.Images.SetKeyName(1, "drivers.ico");
            this.imageListTreeView.Images.SetKeyName(2, "fonts.ico");
            this.imageListTreeView.Images.SetKeyName(3, "helpfiles.ico");
            this.imageListTreeView.Images.SetKeyName(4, "historylist.ico");
            this.imageListTreeView.Images.SetKeyName(5, "mycomputer.ico");
            this.imageListTreeView.Images.SetKeyName(6, "shareddlls.ico");
            this.imageListTreeView.Images.SetKeyName(7, "softwaresettings.ico");
            this.imageListTreeView.Images.SetKeyName(8, "soundevents.ico");
            this.imageListTreeView.Images.SetKeyName(9, "startup.ico");
            this.imageListTreeView.Images.SetKeyName(10, "appinfo.ico");
            this.imageListTreeView.Images.SetKeyName(11, "programlocations.ico");
            // 
            // btnOptimizeRegistryManagerFixAll
            // 
            this.btnOptimizeRegistryManagerFixAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeRegistryManagerFixAll.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryManagerFixAll.Caption = "";
            this.btnOptimizeRegistryManagerFixAll.CaptionColor = System.Drawing.Color.Black;
            this.btnOptimizeRegistryManagerFixAll.CaptionColorActive = System.Drawing.Color.Black;
            this.btnOptimizeRegistryManagerFixAll.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnOptimizeRegistryManagerFixAll.Image = global::Win.Properties.Resources.fix_all_normal;
            this.btnOptimizeRegistryManagerFixAll.ImageActive = null;
            this.btnOptimizeRegistryManagerFixAll.ImageDisabled = global::Win.Properties.Resources.fix_all_disabled;
            this.btnOptimizeRegistryManagerFixAll.ImageNormal = global::Win.Properties.Resources.fix_all_normal;
            this.btnOptimizeRegistryManagerFixAll.ImageRollover = global::Win.Properties.Resources.fix_all_rollover;
            this.btnOptimizeRegistryManagerFixAll.IsActive = false;
            this.btnOptimizeRegistryManagerFixAll.Location = new System.Drawing.Point(588, 371);
            this.btnOptimizeRegistryManagerFixAll.Name = "btnOptimizeRegistryManagerFixAll";
            this.btnOptimizeRegistryManagerFixAll.Size = new System.Drawing.Size(135, 39);
            this.btnOptimizeRegistryManagerFixAll.TabIndex = 75;
            this.btnOptimizeRegistryManagerFixAll.TabStop = false;
            this.btnOptimizeRegistryManagerFixAll.Click += new System.EventHandler(this.btnOptimizeRegistryManagerFixAll_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(376, 60);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(138, 13);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "Summary of problems found:";
            // 
            // btnOptimizeRegistryManagerIgnoreList
            // 
            this.btnOptimizeRegistryManagerIgnoreList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeRegistryManagerIgnoreList.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryManagerIgnoreList.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeRegistryManagerIgnoreList.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeRegistryManagerIgnoreList.Location = new System.Drawing.Point(180, 377);
            this.btnOptimizeRegistryManagerIgnoreList.Name = "btnOptimizeRegistryManagerIgnoreList";
            this.btnOptimizeRegistryManagerIgnoreList.Size = new System.Drawing.Size(108, 27);
            this.btnOptimizeRegistryManagerIgnoreList.TabIndex = 88;
            this.btnOptimizeRegistryManagerIgnoreList.Text = "Ignore List";
            this.btnOptimizeRegistryManagerIgnoreList.Click += new System.EventHandler(this.btnOptimizeRegistryManagerIgnoreList_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(189, 60);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(109, 13);
            this.labelControl3.TabIndex = 24;
            this.labelControl3.Text = "Select sections to scan";
            // 
            // pnlOptimizeProcessManager
            // 
            this.pnlOptimizeProcessManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeProcessManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeProcessManager.Caption = "Process Manager";
            this.pnlOptimizeProcessManager.Controls.Add(this.tabOptimizeProcessManager);
            this.pnlOptimizeProcessManager.Description = "Stop unwanted processes to help speed up your PC";
            this.pnlOptimizeProcessManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeProcessManager.HideLogo = false;
            this.pnlOptimizeProcessManager.Icon = global::Win.Properties.Resources.process_manager_64x64;
            this.pnlOptimizeProcessManager.IsIconOnLeft = true;
            this.pnlOptimizeProcessManager.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeProcessManager.Name = "pnlOptimizeProcessManager";
            this.pnlOptimizeProcessManager.NavBarImage = global::Win.Properties.Resources.process_nav_bar;
            this.pnlOptimizeProcessManager.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeProcessManager.TabIndex = 21;
            // 
            // tabOptimizeProcessManager
            // 
            this.tabOptimizeProcessManager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabOptimizeProcessManager.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.tabOptimizeProcessManager.Appearance.Options.UseFont = true;
            this.tabOptimizeProcessManager.AppearancePage.Header.Font = new System.Drawing.Font("Arial", 6.75F);
            this.tabOptimizeProcessManager.AppearancePage.Header.Options.UseFont = true;
            this.tabOptimizeProcessManager.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Arial", 6.75F);
            this.tabOptimizeProcessManager.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tabOptimizeProcessManager.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("Arial", 6.75F);
            this.tabOptimizeProcessManager.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.tabOptimizeProcessManager.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("Arial", 6.75F);
            this.tabOptimizeProcessManager.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.tabOptimizeProcessManager.Location = new System.Drawing.Point(179, 55);
            this.tabOptimizeProcessManager.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tabOptimizeProcessManager.LookAndFeel.UseWindowsXPTheme = true;
            this.tabOptimizeProcessManager.Name = "tabOptimizeProcessManager";
            this.tabOptimizeProcessManager.SelectedTabPage = this.tpProcesses;
            this.tabOptimizeProcessManager.Size = new System.Drawing.Size(560, 306);
            this.tabOptimizeProcessManager.TabIndex = 0;
            this.tabOptimizeProcessManager.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpProcesses,
            this.tpPerformance});
            // 
            // tpProcesses
            // 
            this.tpProcesses.Controls.Add(this.btnOptimizeEndProcess);
            this.tpProcesses.Controls.Add(this.btnOptimizeOpenFolder);
            this.tpProcesses.Controls.Add(this.btnOptimizeProperties);
            this.tpProcesses.Controls.Add(this.lblOptimizeImage4Label);
            this.tpProcesses.Controls.Add(this.pbOptimizeImage4);
            this.tpProcesses.Controls.Add(this.lblOptimizeImage3Label);
            this.tpProcesses.Controls.Add(this.pbOptimizeImage3);
            this.tpProcesses.Controls.Add(this.lblOptimizeImage2Label);
            this.tpProcesses.Controls.Add(this.pbOptimizeImage2);
            this.tpProcesses.Controls.Add(this.lblOptimizeImage1Label);
            this.tpProcesses.Controls.Add(this.pbOptimizeImage1);
            this.tpProcesses.Controls.Add(this.groupOptimizeProcessDetails);
            this.tpProcesses.Controls.Add(this.gvOptimizeProcess);
            this.tpProcesses.Controls.Add(this.btnOptimizeOptimizeAll);
            this.tpProcesses.Enabled = true;
            this.tpProcesses.Name = "tpProcesses";
            this.tpProcesses.Size = new System.Drawing.Size(552, 279);
            this.tpProcesses.Text = "Processes";
            // 
            // btnOptimizeEndProcess
            // 
            this.btnOptimizeEndProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeEndProcess.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeEndProcess.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeEndProcess.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeEndProcess.Location = new System.Drawing.Point(445, 146);
            this.btnOptimizeEndProcess.Name = "btnOptimizeEndProcess";
            this.btnOptimizeEndProcess.Size = new System.Drawing.Size(104, 27);
            this.btnOptimizeEndProcess.TabIndex = 90;
            this.btnOptimizeEndProcess.Text = "End Process";
            this.btnOptimizeEndProcess.Click += new System.EventHandler(this.btnOptimizeEndProcess_Click);
            // 
            // btnOptimizeOpenFolder
            // 
            this.btnOptimizeOpenFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeOpenFolder.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeOpenFolder.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeOpenFolder.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeOpenFolder.Location = new System.Drawing.Point(335, 146);
            this.btnOptimizeOpenFolder.Name = "btnOptimizeOpenFolder";
            this.btnOptimizeOpenFolder.Size = new System.Drawing.Size(104, 27);
            this.btnOptimizeOpenFolder.TabIndex = 89;
            this.btnOptimizeOpenFolder.Text = "Open Folder";
            this.btnOptimizeOpenFolder.Click += new System.EventHandler(this.btnOptimizeOpenFolder_Click);
            // 
            // btnOptimizeProperties
            // 
            this.btnOptimizeProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeProperties.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeProperties.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeProperties.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeProperties.Location = new System.Drawing.Point(225, 146);
            this.btnOptimizeProperties.Name = "btnOptimizeProperties";
            this.btnOptimizeProperties.Size = new System.Drawing.Size(104, 27);
            this.btnOptimizeProperties.TabIndex = 88;
            this.btnOptimizeProperties.Text = "Properties";
            this.btnOptimizeProperties.Click += new System.EventHandler(this.btnOptimizeProperties_Click);
            // 
            // lblOptimizeImage4Label
            // 
            this.lblOptimizeImage4Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeImage4Label.AutoSize = true;
            this.lblOptimizeImage4Label.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeImage4Label.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeImage4Label.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeImage4Label.Location = new System.Drawing.Point(511, 3);
            this.lblOptimizeImage4Label.Name = "lblOptimizeImage4Label";
            this.lblOptimizeImage4Label.Size = new System.Drawing.Size(34, 10);
            this.lblOptimizeImage4Label.TabIndex = 35;
            this.lblOptimizeImage4Label.Text = "Malware";
            // 
            // pbOptimizeImage4
            // 
            this.pbOptimizeImage4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOptimizeImage4.Image = global::Win.Properties.Resources.Unwanted_16x16;
            this.pbOptimizeImage4.Location = new System.Drawing.Point(489, -1);
            this.pbOptimizeImage4.Name = "pbOptimizeImage4";
            this.pbOptimizeImage4.Size = new System.Drawing.Size(16, 16);
            this.pbOptimizeImage4.TabIndex = 36;
            this.pbOptimizeImage4.TabStop = false;
            // 
            // lblOptimizeImage3Label
            // 
            this.lblOptimizeImage3Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeImage3Label.AutoSize = true;
            this.lblOptimizeImage3Label.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeImage3Label.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeImage3Label.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeImage3Label.Location = new System.Drawing.Point(451, 3);
            this.lblOptimizeImage3Label.Name = "lblOptimizeImage3Label";
            this.lblOptimizeImage3Label.Size = new System.Drawing.Size(24, 10);
            this.lblOptimizeImage3Label.TabIndex = 27;
            this.lblOptimizeImage3Label.Text = "Other";
            // 
            // pbOptimizeImage3
            // 
            this.pbOptimizeImage3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOptimizeImage3.Image = global::Win.Properties.Resources.Other_16x16;
            this.pbOptimizeImage3.Location = new System.Drawing.Point(429, -1);
            this.pbOptimizeImage3.Name = "pbOptimizeImage3";
            this.pbOptimizeImage3.Size = new System.Drawing.Size(16, 16);
            this.pbOptimizeImage3.TabIndex = 28;
            this.pbOptimizeImage3.TabStop = false;
            // 
            // lblOptimizeImage2Label
            // 
            this.lblOptimizeImage2Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeImage2Label.AutoSize = true;
            this.lblOptimizeImage2Label.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeImage2Label.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeImage2Label.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeImage2Label.Location = new System.Drawing.Point(353, 3);
            this.lblOptimizeImage2Label.Name = "lblOptimizeImage2Label";
            this.lblOptimizeImage2Label.Size = new System.Drawing.Size(52, 10);
            this.lblOptimizeImage2Label.TabIndex = 25;
            this.lblOptimizeImage2Label.Text = "User Installed";
            // 
            // pbOptimizeImage2
            // 
            this.pbOptimizeImage2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOptimizeImage2.Image = global::Win.Properties.Resources.User_Installed_16x16;
            this.pbOptimizeImage2.Location = new System.Drawing.Point(331, -1);
            this.pbOptimizeImage2.Name = "pbOptimizeImage2";
            this.pbOptimizeImage2.Size = new System.Drawing.Size(16, 16);
            this.pbOptimizeImage2.TabIndex = 26;
            this.pbOptimizeImage2.TabStop = false;
            // 
            // lblOptimizeImage1Label
            // 
            this.lblOptimizeImage1Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeImage1Label.AutoSize = true;
            this.lblOptimizeImage1Label.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeImage1Label.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeImage1Label.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeImage1Label.Location = new System.Drawing.Point(286, 3);
            this.lblOptimizeImage1Label.Name = "lblOptimizeImage1Label";
            this.lblOptimizeImage1Label.Size = new System.Drawing.Size(29, 10);
            this.lblOptimizeImage1Label.TabIndex = 23;
            this.lblOptimizeImage1Label.Text = "System";
            // 
            // pbOptimizeImage1
            // 
            this.pbOptimizeImage1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOptimizeImage1.Image = global::Win.Properties.Resources.System_16x16;
            this.pbOptimizeImage1.Location = new System.Drawing.Point(264, -1);
            this.pbOptimizeImage1.Name = "pbOptimizeImage1";
            this.pbOptimizeImage1.Size = new System.Drawing.Size(16, 16);
            this.pbOptimizeImage1.TabIndex = 24;
            this.pbOptimizeImage1.TabStop = false;
            // 
            // groupOptimizeProcessDetails
            // 
            this.groupOptimizeProcessDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupOptimizeProcessDetails.Appearance.BackColor = System.Drawing.Color.White;
            this.groupOptimizeProcessDetails.Appearance.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.groupOptimizeProcessDetails.Appearance.Options.UseBackColor = true;
            this.groupOptimizeProcessDetails.Appearance.Options.UseFont = true;
            this.groupOptimizeProcessDetails.AppearanceCaption.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.groupOptimizeProcessDetails.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.groupOptimizeProcessDetails.AppearanceCaption.Options.UseFont = true;
            this.groupOptimizeProcessDetails.AppearanceCaption.Options.UseForeColor = true;
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeSelectedProcessPMemoryUsage);
            this.groupOptimizeProcessDetails.Controls.Add(this.smdbOptimizeSelectedProcessPMemory);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeSelectedProcessCpuUsage);
            this.groupOptimizeProcessDetails.Controls.Add(this.smdbOptimizeSelectedProcessCpu);
            this.groupOptimizeProcessDetails.Controls.Add(this.smdcOptimizeSelectedProcess);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeVirtualMemorySizeValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeVirtualMemorySize);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeTotalProcessorTimeValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeTotalProcessorTime);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizePriorityValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeHandlesValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeThreadsValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeCompanyValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeProcessIdValue);
            this.groupOptimizeProcessDetails.Controls.Add(this.pbOptimizeProcessImage);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizePriority);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeHandles);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeThreads);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeCompany);
            this.groupOptimizeProcessDetails.Controls.Add(this.lblOptimizeProcessId);
            this.groupOptimizeProcessDetails.Location = new System.Drawing.Point(2, 175);
            this.groupOptimizeProcessDetails.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupOptimizeProcessDetails.Name = "groupOptimizeProcessDetails";
            this.groupOptimizeProcessDetails.Size = new System.Drawing.Size(548, 100);
            this.groupOptimizeProcessDetails.TabIndex = 23;
            this.groupOptimizeProcessDetails.Text = "Process: {0}";
            // 
            // lblOptimizeSelectedProcessPMemoryUsage
            // 
            this.lblOptimizeSelectedProcessPMemoryUsage.AutoSize = true;
            this.lblOptimizeSelectedProcessPMemoryUsage.BackColor = System.Drawing.Color.Black;
            this.lblOptimizeSelectedProcessPMemoryUsage.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeSelectedProcessPMemoryUsage.ForeColor = System.Drawing.Color.White;
            this.lblOptimizeSelectedProcessPMemoryUsage.Location = new System.Drawing.Point(377, 48);
            this.lblOptimizeSelectedProcessPMemoryUsage.Name = "lblOptimizeSelectedProcessPMemoryUsage";
            this.lblOptimizeSelectedProcessPMemoryUsage.Size = new System.Drawing.Size(64, 10);
            this.lblOptimizeSelectedProcessPMemoryUsage.TabIndex = 47;
            this.lblOptimizeSelectedProcessPMemoryUsage.Text = "Physical Memory:";
            // 
            // smdbOptimizeSelectedProcessPMemory
            // 
            this.smdbOptimizeSelectedProcessPMemory.BackColor = System.Drawing.Color.Silver;
            this.smdbOptimizeSelectedProcessPMemory.BarColor = System.Drawing.Color.White;
            this.smdbOptimizeSelectedProcessPMemory.Location = new System.Drawing.Point(224, 47);
            this.smdbOptimizeSelectedProcessPMemory.Name = "smdbOptimizeSelectedProcessPMemory";
            this.smdbOptimizeSelectedProcessPMemory.Size = new System.Drawing.Size(150, 16);
            this.smdbOptimizeSelectedProcessPMemory.TabIndex = 46;
            this.smdbOptimizeSelectedProcessPMemory.Value = 0;
            // 
            // lblOptimizeSelectedProcessCpuUsage
            // 
            this.lblOptimizeSelectedProcessCpuUsage.AutoSize = true;
            this.lblOptimizeSelectedProcessCpuUsage.BackColor = System.Drawing.Color.Black;
            this.lblOptimizeSelectedProcessCpuUsage.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeSelectedProcessCpuUsage.ForeColor = System.Drawing.Color.White;
            this.lblOptimizeSelectedProcessCpuUsage.Location = new System.Drawing.Point(377, 26);
            this.lblOptimizeSelectedProcessCpuUsage.Name = "lblOptimizeSelectedProcessCpuUsage";
            this.lblOptimizeSelectedProcessCpuUsage.Size = new System.Drawing.Size(46, 10);
            this.lblOptimizeSelectedProcessCpuUsage.TabIndex = 45;
            this.lblOptimizeSelectedProcessCpuUsage.Text = "Cpu Usage:";
            // 
            // smdbOptimizeSelectedProcessCpu
            // 
            this.smdbOptimizeSelectedProcessCpu.BackColor = System.Drawing.Color.Silver;
            this.smdbOptimizeSelectedProcessCpu.BarColor = System.Drawing.Color.GreenYellow;
            this.smdbOptimizeSelectedProcessCpu.Location = new System.Drawing.Point(224, 25);
            this.smdbOptimizeSelectedProcessCpu.Name = "smdbOptimizeSelectedProcessCpu";
            this.smdbOptimizeSelectedProcessCpu.Size = new System.Drawing.Size(150, 16);
            this.smdbOptimizeSelectedProcessCpu.TabIndex = 43;
            this.smdbOptimizeSelectedProcessCpu.Value = 0;
            // 
            // smdcOptimizeSelectedProcess
            // 
            this.smdcOptimizeSelectedProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.smdcOptimizeSelectedProcess.BackColor = System.Drawing.Color.Black;
            this.smdcOptimizeSelectedProcess.ChartType = Win.Controls.ChartType.Line;
            this.smdcOptimizeSelectedProcess.GridColor = System.Drawing.Color.Green;
            this.smdcOptimizeSelectedProcess.GridPixels = 7;
            this.smdcOptimizeSelectedProcess.InitialHeight = 70;
            this.smdcOptimizeSelectedProcess.LineColors = null;
            this.smdcOptimizeSelectedProcess.Location = new System.Drawing.Point(224, 22);
            this.smdcOptimizeSelectedProcess.Name = "smdcOptimizeSelectedProcess";
            this.smdcOptimizeSelectedProcess.Size = new System.Drawing.Size(320, 73);
            this.smdcOptimizeSelectedProcess.TabIndex = 44;
            // 
            // lblOptimizeVirtualMemorySizeValue
            // 
            this.lblOptimizeVirtualMemorySizeValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeVirtualMemorySizeValue.Location = new System.Drawing.Point(172, 36);
            this.lblOptimizeVirtualMemorySizeValue.Name = "lblOptimizeVirtualMemorySizeValue";
            this.lblOptimizeVirtualMemorySizeValue.Size = new System.Drawing.Size(55, 10);
            this.lblOptimizeVirtualMemorySizeValue.TabIndex = 42;
            this.lblOptimizeVirtualMemorySizeValue.Text = "Virtual Memory";
            // 
            // lblOptimizeVirtualMemorySize
            // 
            this.lblOptimizeVirtualMemorySize.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeVirtualMemorySize.Location = new System.Drawing.Point(93, 36);
            this.lblOptimizeVirtualMemorySize.Name = "lblOptimizeVirtualMemorySize";
            this.lblOptimizeVirtualMemorySize.Size = new System.Drawing.Size(56, 10);
            this.lblOptimizeVirtualMemorySize.TabIndex = 41;
            this.lblOptimizeVirtualMemorySize.Text = "Virtual Memory";
            // 
            // lblOptimizeTotalProcessorTimeValue
            // 
            this.lblOptimizeTotalProcessorTimeValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeTotalProcessorTimeValue.Location = new System.Drawing.Point(172, 22);
            this.lblOptimizeTotalProcessorTimeValue.Name = "lblOptimizeTotalProcessorTimeValue";
            this.lblOptimizeTotalProcessorTimeValue.Size = new System.Drawing.Size(39, 10);
            this.lblOptimizeTotalProcessorTimeValue.TabIndex = 40;
            this.lblOptimizeTotalProcessorTimeValue.Text = "Total Time";
            // 
            // lblOptimizeTotalProcessorTime
            // 
            this.lblOptimizeTotalProcessorTime.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeTotalProcessorTime.Location = new System.Drawing.Point(93, 22);
            this.lblOptimizeTotalProcessorTime.Name = "lblOptimizeTotalProcessorTime";
            this.lblOptimizeTotalProcessorTime.Size = new System.Drawing.Size(58, 10);
            this.lblOptimizeTotalProcessorTime.TabIndex = 39;
            this.lblOptimizeTotalProcessorTime.Text = "Total CPU Time";
            // 
            // lblOptimizePriorityValue
            // 
            this.lblOptimizePriorityValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizePriorityValue.Location = new System.Drawing.Point(55, 36);
            this.lblOptimizePriorityValue.Name = "lblOptimizePriorityValue";
            this.lblOptimizePriorityValue.Size = new System.Drawing.Size(26, 10);
            this.lblOptimizePriorityValue.TabIndex = 38;
            this.lblOptimizePriorityValue.Text = "Priority";
            // 
            // lblOptimizeHandlesValue
            // 
            this.lblOptimizeHandlesValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeHandlesValue.Location = new System.Drawing.Point(55, 64);
            this.lblOptimizeHandlesValue.Name = "lblOptimizeHandlesValue";
            this.lblOptimizeHandlesValue.Size = new System.Drawing.Size(30, 10);
            this.lblOptimizeHandlesValue.TabIndex = 37;
            this.lblOptimizeHandlesValue.Text = "Handles";
            // 
            // lblOptimizeThreadsValue
            // 
            this.lblOptimizeThreadsValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeThreadsValue.Location = new System.Drawing.Point(55, 78);
            this.lblOptimizeThreadsValue.Name = "lblOptimizeThreadsValue";
            this.lblOptimizeThreadsValue.Size = new System.Drawing.Size(30, 10);
            this.lblOptimizeThreadsValue.TabIndex = 36;
            this.lblOptimizeThreadsValue.Text = "Threads";
            // 
            // lblOptimizeCompanyValue
            // 
            this.lblOptimizeCompanyValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeCompanyValue.Location = new System.Drawing.Point(55, 50);
            this.lblOptimizeCompanyValue.Name = "lblOptimizeCompanyValue";
            this.lblOptimizeCompanyValue.Size = new System.Drawing.Size(35, 10);
            this.lblOptimizeCompanyValue.TabIndex = 35;
            this.lblOptimizeCompanyValue.Text = "Company";
            // 
            // lblOptimizeProcessIdValue
            // 
            this.lblOptimizeProcessIdValue.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeProcessIdValue.Location = new System.Drawing.Point(55, 22);
            this.lblOptimizeProcessIdValue.Name = "lblOptimizeProcessIdValue";
            this.lblOptimizeProcessIdValue.Size = new System.Drawing.Size(37, 10);
            this.lblOptimizeProcessIdValue.TabIndex = 34;
            this.lblOptimizeProcessIdValue.Text = "ProcessID";
            // 
            // pbOptimizeProcessImage
            // 
            this.pbOptimizeProcessImage.BackColor = System.Drawing.Color.Transparent;
            this.pbOptimizeProcessImage.Location = new System.Drawing.Point(186, 63);
            this.pbOptimizeProcessImage.Name = "pbOptimizeProcessImage";
            this.pbOptimizeProcessImage.Size = new System.Drawing.Size(32, 32);
            this.pbOptimizeProcessImage.TabIndex = 33;
            this.pbOptimizeProcessImage.TabStop = false;
            // 
            // lblOptimizePriority
            // 
            this.lblOptimizePriority.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizePriority.Location = new System.Drawing.Point(5, 36);
            this.lblOptimizePriority.Name = "lblOptimizePriority";
            this.lblOptimizePriority.Size = new System.Drawing.Size(28, 10);
            this.lblOptimizePriority.TabIndex = 4;
            this.lblOptimizePriority.Text = "Priority";
            // 
            // lblOptimizeHandles
            // 
            this.lblOptimizeHandles.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeHandles.Location = new System.Drawing.Point(5, 64);
            this.lblOptimizeHandles.Name = "lblOptimizeHandles";
            this.lblOptimizeHandles.Size = new System.Drawing.Size(30, 10);
            this.lblOptimizeHandles.TabIndex = 3;
            this.lblOptimizeHandles.Text = "Handles";
            // 
            // lblOptimizeThreads
            // 
            this.lblOptimizeThreads.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeThreads.Location = new System.Drawing.Point(5, 78);
            this.lblOptimizeThreads.Name = "lblOptimizeThreads";
            this.lblOptimizeThreads.Size = new System.Drawing.Size(30, 10);
            this.lblOptimizeThreads.TabIndex = 2;
            this.lblOptimizeThreads.Text = "Threads";
            // 
            // lblOptimizeCompany
            // 
            this.lblOptimizeCompany.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeCompany.Location = new System.Drawing.Point(5, 50);
            this.lblOptimizeCompany.Name = "lblOptimizeCompany";
            this.lblOptimizeCompany.Size = new System.Drawing.Size(37, 10);
            this.lblOptimizeCompany.TabIndex = 1;
            this.lblOptimizeCompany.Text = "Company";
            // 
            // lblOptimizeProcessId
            // 
            this.lblOptimizeProcessId.Appearance.Font = new System.Drawing.Font("Arial", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeProcessId.Location = new System.Drawing.Point(5, 22);
            this.lblOptimizeProcessId.Name = "lblOptimizeProcessId";
            this.lblOptimizeProcessId.Size = new System.Drawing.Size(37, 10);
            this.lblOptimizeProcessId.TabIndex = 0;
            this.lblOptimizeProcessId.Text = "ProcessID";
            // 
            // gvOptimizeProcess
            // 
            this.gvOptimizeProcess.AllowUserToAddRows = false;
            this.gvOptimizeProcess.AllowUserToDeleteRows = false;
            this.gvOptimizeProcess.AllowUserToOrderColumns = true;
            this.gvOptimizeProcess.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.gvOptimizeProcess.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gvOptimizeProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gvOptimizeProcess.BackgroundColor = System.Drawing.Color.White;
            this.gvOptimizeProcess.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gvOptimizeProcess.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gvOptimizeProcess.ColumnHeadersHeight = 17;
            this.gvOptimizeProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gvOptimizeProcess.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_OptimizeWindow_Process_InfoTypeImage,
            this.col_OptimizeWindow_Process_InfoType,
            this.colIcon,
            this.col_OptimizeWindow_Process_ProcessName,
            this.colCpu,
            this.colMemory,
            this.colSigned,
            this.colDescription,
            this.colPath});
            this.gvOptimizeProcess.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gvOptimizeProcess.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.gvOptimizeProcess.Location = new System.Drawing.Point(3, 16);
            this.gvOptimizeProcess.MultiSelect = false;
            this.gvOptimizeProcess.Name = "gvOptimizeProcess";
            this.gvOptimizeProcess.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gvOptimizeProcess.RowHeadersVisible = false;
            this.gvOptimizeProcess.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.gvOptimizeProcess.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.gvOptimizeProcess.RowTemplate.Height = 15;
            this.gvOptimizeProcess.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvOptimizeProcess.Size = new System.Drawing.Size(548, 124);
            this.gvOptimizeProcess.TabIndex = 19;
            this.gvOptimizeProcess.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gvOptimizeProcess_ColumnHeaderMouseClick);
            this.gvOptimizeProcess.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gvOptimizeProcess_DataBindingComplete);
            this.gvOptimizeProcess.SelectionChanged += new System.EventHandler(this.gvOptimizeProcess_SelectionChanged);
            // 
            // col_OptimizeWindow_Process_InfoTypeImage
            // 
            this.col_OptimizeWindow_Process_InfoTypeImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.col_OptimizeWindow_Process_InfoTypeImage.FillWeight = 3.40096F;
            this.col_OptimizeWindow_Process_InfoTypeImage.HeaderText = "";
            this.col_OptimizeWindow_Process_InfoTypeImage.Name = "col_OptimizeWindow_Process_InfoTypeImage";
            this.col_OptimizeWindow_Process_InfoTypeImage.ReadOnly = true;
            this.col_OptimizeWindow_Process_InfoTypeImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.col_OptimizeWindow_Process_InfoTypeImage.Width = 15;
            // 
            // col_OptimizeWindow_Process_InfoType
            // 
            this.col_OptimizeWindow_Process_InfoType.DataPropertyName = "InfoType";
            this.col_OptimizeWindow_Process_InfoType.HeaderText = "InfoType";
            this.col_OptimizeWindow_Process_InfoType.Name = "col_OptimizeWindow_Process_InfoType";
            this.col_OptimizeWindow_Process_InfoType.Visible = false;
            // 
            // colIcon
            // 
            this.colIcon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colIcon.DataPropertyName = "Image";
            this.colIcon.FillWeight = 3.572702F;
            this.colIcon.HeaderText = "";
            this.colIcon.Name = "colIcon";
            this.colIcon.Width = 16;
            // 
            // col_OptimizeWindow_Process_ProcessName
            // 
            this.col_OptimizeWindow_Process_ProcessName.DataPropertyName = "ProcessName";
            this.col_OptimizeWindow_Process_ProcessName.FillWeight = 7.042321F;
            this.col_OptimizeWindow_Process_ProcessName.HeaderText = "Process Name";
            this.col_OptimizeWindow_Process_ProcessName.Name = "col_OptimizeWindow_Process_ProcessName";
            this.col_OptimizeWindow_Process_ProcessName.ReadOnly = true;
            this.col_OptimizeWindow_Process_ProcessName.Width = 125;
            // 
            // colCpu
            // 
            this.colCpu.DataPropertyName = "CpuPercent";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCpu.DefaultCellStyle = dataGridViewCellStyle6;
            this.colCpu.FillWeight = 1.877953F;
            this.colCpu.HeaderText = "CPU";
            this.colCpu.Name = "colCpu";
            this.colCpu.ReadOnly = true;
            this.colCpu.Width = 34;
            // 
            // colMemory
            // 
            this.colMemory.DataPropertyName = "FormattedMemory";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colMemory.DefaultCellStyle = dataGridViewCellStyle7;
            this.colMemory.FillWeight = 5.382864F;
            this.colMemory.HeaderText = "Memory";
            this.colMemory.Name = "colMemory";
            this.colMemory.ReadOnly = true;
            this.colMemory.Width = 96;
            // 
            // colSigned
            // 
            this.colSigned.DataPropertyName = "StartTime";
            this.colSigned.FillWeight = 6.280009F;
            this.colSigned.HeaderText = "Start Time";
            this.colSigned.Name = "colSigned";
            this.colSigned.ReadOnly = true;
            this.colSigned.Width = 111;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.FillWeight = 4.694881F;
            this.colDescription.HeaderText = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 84;
            // 
            // colPath
            // 
            this.colPath.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPath.DataPropertyName = "Path";
            this.colPath.FillWeight = 11.7372F;
            this.colPath.HeaderText = "Path";
            this.colPath.Name = "colPath";
            this.colPath.ReadOnly = true;
            // 
            // btnOptimizeOptimizeAll
            // 
            this.btnOptimizeOptimizeAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptimizeOptimizeAll.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeOptimizeAll.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeOptimizeAll.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeOptimizeAll.Location = new System.Drawing.Point(3, 146);
            this.btnOptimizeOptimizeAll.Name = "btnOptimizeOptimizeAll";
            this.btnOptimizeOptimizeAll.Size = new System.Drawing.Size(104, 27);
            this.btnOptimizeOptimizeAll.TabIndex = 87;
            this.btnOptimizeOptimizeAll.Text = "Optimize All";
            this.btnOptimizeOptimizeAll.Click += new System.EventHandler(this.btnOptimizeOptimizeAll_Click);
            // 
            // tpPerformance
            // 
            this.tpPerformance.Controls.Add(this.groupOptimizeMemoryUsage);
            this.tpPerformance.Controls.Add(this.groupOptimizeCpuUsage);
            this.tpPerformance.Enabled = true;
            this.tpPerformance.Name = "tpPerformance";
            this.tpPerformance.Size = new System.Drawing.Size(552, 279);
            this.tpPerformance.Text = "Performance";
            // 
            // groupOptimizeMemoryUsage
            // 
            this.groupOptimizeMemoryUsage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupOptimizeMemoryUsage.Controls.Add(this.lblOptimizeVMemoryUsage);
            this.groupOptimizeMemoryUsage.Controls.Add(this.smdbOptimizeVMemory);
            this.groupOptimizeMemoryUsage.Controls.Add(this.lblOptimizePMemoryUsage);
            this.groupOptimizeMemoryUsage.Controls.Add(this.smdbOptimizePMemory);
            this.groupOptimizeMemoryUsage.Controls.Add(this.smdcOptimizeMemory);
            this.groupOptimizeMemoryUsage.Location = new System.Drawing.Point(3, 140);
            this.groupOptimizeMemoryUsage.Name = "groupOptimizeMemoryUsage";
            this.groupOptimizeMemoryUsage.Size = new System.Drawing.Size(546, 137);
            this.groupOptimizeMemoryUsage.TabIndex = 33;
            this.groupOptimizeMemoryUsage.Text = "Memory Usage";
            // 
            // lblOptimizeVMemoryUsage
            // 
            this.lblOptimizeVMemoryUsage.AutoSize = true;
            this.lblOptimizeVMemoryUsage.BackColor = System.Drawing.Color.Black;
            this.lblOptimizeVMemoryUsage.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeVMemoryUsage.ForeColor = System.Drawing.Color.White;
            this.lblOptimizeVMemoryUsage.Location = new System.Drawing.Point(161, 49);
            this.lblOptimizeVMemoryUsage.Name = "lblOptimizeVMemoryUsage";
            this.lblOptimizeVMemoryUsage.Size = new System.Drawing.Size(48, 12);
            this.lblOptimizeVMemoryUsage.TabIndex = 34;
            this.lblOptimizeVMemoryUsage.Text = "VM Usage";
            // 
            // smdbOptimizeVMemory
            // 
            this.smdbOptimizeVMemory.BackColor = System.Drawing.Color.Silver;
            this.smdbOptimizeVMemory.BarColor = System.Drawing.Color.Blue;
            this.smdbOptimizeVMemory.Location = new System.Drawing.Point(5, 47);
            this.smdbOptimizeVMemory.Name = "smdbOptimizeVMemory";
            this.smdbOptimizeVMemory.Size = new System.Drawing.Size(150, 16);
            this.smdbOptimizeVMemory.TabIndex = 33;
            this.smdbOptimizeVMemory.Value = 0;
            // 
            // lblOptimizePMemoryUsage
            // 
            this.lblOptimizePMemoryUsage.AutoSize = true;
            this.lblOptimizePMemoryUsage.BackColor = System.Drawing.Color.Black;
            this.lblOptimizePMemoryUsage.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizePMemoryUsage.ForeColor = System.Drawing.Color.White;
            this.lblOptimizePMemoryUsage.Location = new System.Drawing.Point(161, 27);
            this.lblOptimizePMemoryUsage.Name = "lblOptimizePMemoryUsage";
            this.lblOptimizePMemoryUsage.Size = new System.Drawing.Size(48, 12);
            this.lblOptimizePMemoryUsage.TabIndex = 32;
            this.lblOptimizePMemoryUsage.Text = "PM Usage";
            // 
            // smdbOptimizePMemory
            // 
            this.smdbOptimizePMemory.BackColor = System.Drawing.Color.Silver;
            this.smdbOptimizePMemory.BarColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.smdbOptimizePMemory.Location = new System.Drawing.Point(5, 25);
            this.smdbOptimizePMemory.Name = "smdbOptimizePMemory";
            this.smdbOptimizePMemory.Size = new System.Drawing.Size(150, 16);
            this.smdbOptimizePMemory.TabIndex = 30;
            this.smdbOptimizePMemory.Value = 0;
            // 
            // smdcOptimizeMemory
            // 
            this.smdcOptimizeMemory.BackColor = System.Drawing.Color.Black;
            this.smdcOptimizeMemory.ChartType = Win.Controls.ChartType.Line;
            this.smdcOptimizeMemory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.smdcOptimizeMemory.GridColor = System.Drawing.Color.Green;
            this.smdcOptimizeMemory.GridPixels = 10;
            this.smdcOptimizeMemory.InitialHeight = 100;
            this.smdcOptimizeMemory.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.smdcOptimizeMemory.Location = new System.Drawing.Point(2, 22);
            this.smdcOptimizeMemory.Name = "smdcOptimizeMemory";
            this.smdcOptimizeMemory.Size = new System.Drawing.Size(542, 113);
            this.smdcOptimizeMemory.TabIndex = 31;
            // 
            // groupOptimizeCpuUsage
            // 
            this.groupOptimizeCpuUsage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupOptimizeCpuUsage.Controls.Add(this.lblOptimizeCpuUsage);
            this.groupOptimizeCpuUsage.Controls.Add(this.smdbOptimizeCpu);
            this.groupOptimizeCpuUsage.Controls.Add(this.smdcOptimizeCpu);
            this.groupOptimizeCpuUsage.Location = new System.Drawing.Point(3, 3);
            this.groupOptimizeCpuUsage.Name = "groupOptimizeCpuUsage";
            this.groupOptimizeCpuUsage.Size = new System.Drawing.Size(546, 134);
            this.groupOptimizeCpuUsage.TabIndex = 1;
            this.groupOptimizeCpuUsage.Text = "CPU Usage";
            // 
            // lblOptimizeCpuUsage
            // 
            this.lblOptimizeCpuUsage.AutoSize = true;
            this.lblOptimizeCpuUsage.BackColor = System.Drawing.Color.Black;
            this.lblOptimizeCpuUsage.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeCpuUsage.ForeColor = System.Drawing.Color.White;
            this.lblOptimizeCpuUsage.Location = new System.Drawing.Point(211, 27);
            this.lblOptimizeCpuUsage.Name = "lblOptimizeCpuUsage";
            this.lblOptimizeCpuUsage.Size = new System.Drawing.Size(49, 12);
            this.lblOptimizeCpuUsage.TabIndex = 32;
            this.lblOptimizeCpuUsage.Text = "CpuUsage";
            // 
            // smdbOptimizeCpu
            // 
            this.smdbOptimizeCpu.BackColor = System.Drawing.Color.Silver;
            this.smdbOptimizeCpu.BarColor = System.Drawing.Color.GreenYellow;
            this.smdbOptimizeCpu.Location = new System.Drawing.Point(5, 25);
            this.smdbOptimizeCpu.Name = "smdbOptimizeCpu";
            this.smdbOptimizeCpu.Size = new System.Drawing.Size(200, 16);
            this.smdbOptimizeCpu.TabIndex = 30;
            this.smdbOptimizeCpu.Value = 0;
            // 
            // smdcOptimizeCpu
            // 
            this.smdcOptimizeCpu.BackColor = System.Drawing.Color.Black;
            this.smdcOptimizeCpu.ChartType = Win.Controls.ChartType.Line;
            this.smdcOptimizeCpu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.smdcOptimizeCpu.GridColor = System.Drawing.Color.Green;
            this.smdcOptimizeCpu.GridPixels = 10;
            this.smdcOptimizeCpu.InitialHeight = 100;
            this.smdcOptimizeCpu.LineColor = System.Drawing.Color.GreenYellow;
            this.smdcOptimizeCpu.Location = new System.Drawing.Point(2, 22);
            this.smdcOptimizeCpu.Name = "smdcOptimizeCpu";
            this.smdcOptimizeCpu.Size = new System.Drawing.Size(542, 110);
            this.smdcOptimizeCpu.TabIndex = 31;
            // 
            // pnlOptimizeBrowseObjectManager
            // 
            this.pnlOptimizeBrowseObjectManager.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeBrowseObjectManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeBrowseObjectManager.Caption = "Browse Helper Object Manager";
            this.pnlOptimizeBrowseObjectManager.Controls.Add(this.lvOptimizeBrowseObjectManager);
            this.pnlOptimizeBrowseObjectManager.Controls.Add(this.btnOptimizeBrowseObjectManagerEnableAll);
            this.pnlOptimizeBrowseObjectManager.Controls.Add(this.lblOptimizeBrowserObjectManagerItemSelected);
            this.pnlOptimizeBrowseObjectManager.Controls.Add(this.btnOptimizeBrowseObjectManagerDisableAll);
            this.pnlOptimizeBrowseObjectManager.Description = "Enable or disable browser helper objects to improve web browsing";
            this.pnlOptimizeBrowseObjectManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeBrowseObjectManager.HideLogo = true;
            this.pnlOptimizeBrowseObjectManager.Icon = global::Win.Properties.Resources.browser_object_64x64;
            this.pnlOptimizeBrowseObjectManager.IsIconOnLeft = true;
            this.pnlOptimizeBrowseObjectManager.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeBrowseObjectManager.Name = "pnlOptimizeBrowseObjectManager";
            this.pnlOptimizeBrowseObjectManager.NavBarImage = global::Win.Properties.Resources.browser_nav_bar;
            this.pnlOptimizeBrowseObjectManager.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeBrowseObjectManager.TabIndex = 22;
            // 
            // lvOptimizeBrowseObjectManager
            // 
            this.lvOptimizeBrowseObjectManager.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvOptimizeBrowseObjectManager.BackgroundImageTiled = true;
            this.lvOptimizeBrowseObjectManager.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvOptimizeBrowseObjectManager.CheckBoxes = true;
            this.lvOptimizeBrowseObjectManager.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.lvOptimizeBrowseObjectManager.ContextMenuStrip = this.cmnuOptimizeBrowseObjectManager;
            this.lvOptimizeBrowseObjectManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvOptimizeBrowseObjectManager.FullRowSelect = true;
            this.lvOptimizeBrowseObjectManager.GridLines = true;
            this.lvOptimizeBrowseObjectManager.HideSelection = false;
            this.lvOptimizeBrowseObjectManager.Location = new System.Drawing.Point(179, 80);
            this.lvOptimizeBrowseObjectManager.MultiSelect = false;
            this.lvOptimizeBrowseObjectManager.Name = "lvOptimizeBrowseObjectManager";
            this.lvOptimizeBrowseObjectManager.Size = new System.Drawing.Size(560, 282);
            this.lvOptimizeBrowseObjectManager.SmallImageList = this.ilOptimizeBrowseObjectManager;
            this.lvOptimizeBrowseObjectManager.TabIndex = 5;
            this.lvOptimizeBrowseObjectManager.UseCompatibleStateImageBehavior = false;
            this.lvOptimizeBrowseObjectManager.View = System.Windows.Forms.View.Details;
            this.lvOptimizeBrowseObjectManager.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvOptimizeBrowseObjectManager_ItemChecked);
            this.lvOptimizeBrowseObjectManager.SelectedIndexChanged += new System.EventHandler(this.lvOptimizeBrowseObjectManager_SelectedIndexChanged);
            this.lvOptimizeBrowseObjectManager.DoubleClick += new System.EventHandler(this.lvOptimizeBrowseObjectManager_DoubleClick);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Name";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Path";
            this.columnHeader5.Width = 200;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Description";
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Company";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Size";
            this.columnHeader8.Width = 40;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Product Name";
            this.columnHeader9.Width = 100;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Product Version";
            this.columnHeader10.Width = 100;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Date Modified";
            this.columnHeader11.Width = 100;
            // 
            // cmnuOptimizeBrowseObjectManager
            // 
            this.cmnuOptimizeBrowseObjectManager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOptimizeBrowseObjectManagerSelectAll,
            this.tsmiOptimizeBrowseObjectManagerSelectNone,
            this.tsmiOptimizeBrowseObjectManagerInvertSelection,
            this.tsmiOptimizeBrowseObjectManagerSeparator,
            this.tsmiOptimizeBrowseObjectManagerProperties});
            this.cmnuOptimizeBrowseObjectManager.Name = "cmnuDiskShortcutFixer";
            this.cmnuOptimizeBrowseObjectManager.Size = new System.Drawing.Size(156, 98);
            this.cmnuOptimizeBrowseObjectManager.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmnuOptimizeBrowseObjectManager_ItemClicked);
            // 
            // tsmiOptimizeBrowseObjectManagerSelectAll
            // 
            this.tsmiOptimizeBrowseObjectManagerSelectAll.Name = "tsmiOptimizeBrowseObjectManagerSelectAll";
            this.tsmiOptimizeBrowseObjectManagerSelectAll.Size = new System.Drawing.Size(155, 22);
            this.tsmiOptimizeBrowseObjectManagerSelectAll.Text = "Select All";
            // 
            // tsmiOptimizeBrowseObjectManagerSelectNone
            // 
            this.tsmiOptimizeBrowseObjectManagerSelectNone.Name = "tsmiOptimizeBrowseObjectManagerSelectNone";
            this.tsmiOptimizeBrowseObjectManagerSelectNone.Size = new System.Drawing.Size(155, 22);
            this.tsmiOptimizeBrowseObjectManagerSelectNone.Text = "Select None";
            // 
            // tsmiOptimizeBrowseObjectManagerInvertSelection
            // 
            this.tsmiOptimizeBrowseObjectManagerInvertSelection.Name = "tsmiOptimizeBrowseObjectManagerInvertSelection";
            this.tsmiOptimizeBrowseObjectManagerInvertSelection.Size = new System.Drawing.Size(155, 22);
            this.tsmiOptimizeBrowseObjectManagerInvertSelection.Text = "Invert Selection";
            // 
            // tsmiOptimizeBrowseObjectManagerSeparator
            // 
            this.tsmiOptimizeBrowseObjectManagerSeparator.Name = "tsmiOptimizeBrowseObjectManagerSeparator";
            this.tsmiOptimizeBrowseObjectManagerSeparator.Size = new System.Drawing.Size(152, 6);
            // 
            // tsmiOptimizeBrowseObjectManagerProperties
            // 
            this.tsmiOptimizeBrowseObjectManagerProperties.Enabled = false;
            this.tsmiOptimizeBrowseObjectManagerProperties.Image = global::Win.Properties.Resources.properties_16x16;
            this.tsmiOptimizeBrowseObjectManagerProperties.Name = "tsmiOptimizeBrowseObjectManagerProperties";
            this.tsmiOptimizeBrowseObjectManagerProperties.Size = new System.Drawing.Size(155, 22);
            this.tsmiOptimizeBrowseObjectManagerProperties.Text = "Properties";
            // 
            // ilOptimizeBrowseObjectManager
            // 
            this.ilOptimizeBrowseObjectManager.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilOptimizeBrowseObjectManager.ImageStream")));
            this.ilOptimizeBrowseObjectManager.TransparentColor = System.Drawing.Color.Transparent;
            this.ilOptimizeBrowseObjectManager.Images.SetKeyName(0, "shortcut_16x16.png");
            this.ilOptimizeBrowseObjectManager.Images.SetKeyName(1, "folder_16x16.png");
            // 
            // btnOptimizeBrowseObjectManagerEnableAll
            // 
            this.btnOptimizeBrowseObjectManagerEnableAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeBrowseObjectManagerEnableAll.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeBrowseObjectManagerEnableAll.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeBrowseObjectManagerEnableAll.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeBrowseObjectManagerEnableAll.Location = new System.Drawing.Point(615, 377);
            this.btnOptimizeBrowseObjectManagerEnableAll.Name = "btnOptimizeBrowseObjectManagerEnableAll";
            this.btnOptimizeBrowseObjectManagerEnableAll.Size = new System.Drawing.Size(108, 27);
            this.btnOptimizeBrowseObjectManagerEnableAll.TabIndex = 87;
            this.btnOptimizeBrowseObjectManagerEnableAll.Text = "Enable All";
            this.btnOptimizeBrowseObjectManagerEnableAll.Click += new System.EventHandler(this.btnOptimizeBrowseObjectManagerEnableAll_Click);
            // 
            // lblOptimizeBrowserObjectManagerItemSelected
            // 
            this.lblOptimizeBrowserObjectManagerItemSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeBrowserObjectManagerItemSelected.AutoSize = true;
            this.lblOptimizeBrowserObjectManagerItemSelected.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeBrowserObjectManagerItemSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeBrowserObjectManagerItemSelected.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeBrowserObjectManagerItemSelected.Location = new System.Drawing.Point(189, 60);
            this.lblOptimizeBrowserObjectManagerItemSelected.Name = "lblOptimizeBrowserObjectManagerItemSelected";
            this.lblOptimizeBrowserObjectManagerItemSelected.Size = new System.Drawing.Size(120, 13);
            this.lblOptimizeBrowserObjectManagerItemSelected.TabIndex = 21;
            this.lblOptimizeBrowserObjectManagerItemSelected.Tag = "{0} of {1} items selected";
            this.lblOptimizeBrowserObjectManagerItemSelected.Text = "{0} of {1} items selected";
            // 
            // btnOptimizeBrowseObjectManagerDisableAll
            // 
            this.btnOptimizeBrowseObjectManagerDisableAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeBrowseObjectManagerDisableAll.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeBrowseObjectManagerDisableAll.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeBrowseObjectManagerDisableAll.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeBrowseObjectManagerDisableAll.Location = new System.Drawing.Point(501, 377);
            this.btnOptimizeBrowseObjectManagerDisableAll.Name = "btnOptimizeBrowseObjectManagerDisableAll";
            this.btnOptimizeBrowseObjectManagerDisableAll.Size = new System.Drawing.Size(108, 27);
            this.btnOptimizeBrowseObjectManagerDisableAll.TabIndex = 86;
            this.btnOptimizeBrowseObjectManagerDisableAll.Text = "Disable All";
            this.btnOptimizeBrowseObjectManagerDisableAll.Click += new System.EventHandler(this.btnOptimizeBrowseObjectManagerDisableAll_Click);
            // 
            // pnlOptimizeRegistryDefragmenter
            // 
            this.pnlOptimizeRegistryDefragmenter.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlOptimizeRegistryDefragmenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlOptimizeRegistryDefragmenter.Caption = "Registry Defragmenter";
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.lblOptimizeRegistryDefragmenterTotalNewHiveSize);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.lblOptimizeRegistryDefragmenterTotalOldHiveSize);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.lblOptimizeRegistryDefragmenterTotal);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.lvOptimizeRegistryDefragmenter);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.btnOptimizeRegistryDefragmenterOptimize);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.pbOptimizeRegistryDefragmenter);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.lblOptimizeRegistryDefragmenterStatus);
            this.pnlOptimizeRegistryDefragmenter.Controls.Add(this.btnOptimizeRegistryDefragmenterAnalyze);
            this.pnlOptimizeRegistryDefragmenter.Description = "Reduce the amount of RAM the registry takes up";
            this.pnlOptimizeRegistryDefragmenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOptimizeRegistryDefragmenter.HideLogo = true;
            this.pnlOptimizeRegistryDefragmenter.Icon = global::Win.Properties.Resources.registry_defragmenter_64x64;
            this.pnlOptimizeRegistryDefragmenter.IsIconOnLeft = true;
            this.pnlOptimizeRegistryDefragmenter.Location = new System.Drawing.Point(0, 0);
            this.pnlOptimizeRegistryDefragmenter.Name = "pnlOptimizeRegistryDefragmenter";
            this.pnlOptimizeRegistryDefragmenter.NavBarImage = global::Win.Properties.Resources.registry_nav_bar;
            this.pnlOptimizeRegistryDefragmenter.Size = new System.Drawing.Size(741, 416);
            this.pnlOptimizeRegistryDefragmenter.TabIndex = 88;
            // 
            // lblOptimizeRegistryDefragmenterTotalNewHiveSize
            // 
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.AutoSize = true;
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Location = new System.Drawing.Point(635, 348);
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Name = "lblOptimizeRegistryDefragmenterTotalNewHiveSize";
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Size = new System.Drawing.Size(31, 13);
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.TabIndex = 26;
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Tag = "";
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Text = "Total";
            this.lblOptimizeRegistryDefragmenterTotalNewHiveSize.Visible = false;
            // 
            // lblOptimizeRegistryDefragmenterTotalOldHiveSize
            // 
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.AutoSize = true;
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Location = new System.Drawing.Point(535, 348);
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Name = "lblOptimizeRegistryDefragmenterTotalOldHiveSize";
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Size = new System.Drawing.Size(31, 13);
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.TabIndex = 25;
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Tag = "";
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Text = "Total";
            this.lblOptimizeRegistryDefragmenterTotalOldHiveSize.Visible = false;
            // 
            // lblOptimizeRegistryDefragmenterTotal
            // 
            this.lblOptimizeRegistryDefragmenterTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeRegistryDefragmenterTotal.AutoSize = true;
            this.lblOptimizeRegistryDefragmenterTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeRegistryDefragmenterTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeRegistryDefragmenterTotal.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeRegistryDefragmenterTotal.Location = new System.Drawing.Point(189, 348);
            this.lblOptimizeRegistryDefragmenterTotal.Name = "lblOptimizeRegistryDefragmenterTotal";
            this.lblOptimizeRegistryDefragmenterTotal.Size = new System.Drawing.Size(31, 13);
            this.lblOptimizeRegistryDefragmenterTotal.TabIndex = 24;
            this.lblOptimizeRegistryDefragmenterTotal.Tag = "";
            this.lblOptimizeRegistryDefragmenterTotal.Text = "Total";
            this.lblOptimizeRegistryDefragmenterTotal.Visible = false;
            // 
            // lvOptimizeRegistryDefragmenter
            // 
            this.lvOptimizeRegistryDefragmenter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvOptimizeRegistryDefragmenter.BackgroundImageTiled = true;
            this.lvOptimizeRegistryDefragmenter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvOptimizeRegistryDefragmenter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colOptimizeRegistryDefragmenterHivePath,
            this.colOptimizeRegistryDefragmenterOldHiveSize,
            this.colOptimizeRegistryDefragmenterNewHiveSize});
            this.lvOptimizeRegistryDefragmenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvOptimizeRegistryDefragmenter.FullRowSelect = true;
            this.lvOptimizeRegistryDefragmenter.GridLines = true;
            this.lvOptimizeRegistryDefragmenter.HideSelection = false;
            this.lvOptimizeRegistryDefragmenter.Location = new System.Drawing.Point(179, 104);
            this.lvOptimizeRegistryDefragmenter.MultiSelect = false;
            this.lvOptimizeRegistryDefragmenter.Name = "lvOptimizeRegistryDefragmenter";
            this.lvOptimizeRegistryDefragmenter.Size = new System.Drawing.Size(560, 241);
            this.lvOptimizeRegistryDefragmenter.TabIndex = 23;
            this.lvOptimizeRegistryDefragmenter.UseCompatibleStateImageBehavior = false;
            this.lvOptimizeRegistryDefragmenter.View = System.Windows.Forms.View.Details;
            // 
            // colOptimizeRegistryDefragmenterHivePath
            // 
            this.colOptimizeRegistryDefragmenterHivePath.Text = "Hive Path";
            this.colOptimizeRegistryDefragmenterHivePath.Width = 354;
            // 
            // colOptimizeRegistryDefragmenterOldHiveSize
            // 
            this.colOptimizeRegistryDefragmenterOldHiveSize.Text = "Old Hive Size";
            this.colOptimizeRegistryDefragmenterOldHiveSize.Width = 98;
            // 
            // colOptimizeRegistryDefragmenterNewHiveSize
            // 
            this.colOptimizeRegistryDefragmenterNewHiveSize.Text = "New Hive Size";
            this.colOptimizeRegistryDefragmenterNewHiveSize.Width = 100;
            // 
            // btnOptimizeRegistryDefragmenterOptimize
            // 
            this.btnOptimizeRegistryDefragmenterOptimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeRegistryDefragmenterOptimize.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryDefragmenterOptimize.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeRegistryDefragmenterOptimize.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeRegistryDefragmenterOptimize.Location = new System.Drawing.Point(615, 377);
            this.btnOptimizeRegistryDefragmenterOptimize.Name = "btnOptimizeRegistryDefragmenterOptimize";
            this.btnOptimizeRegistryDefragmenterOptimize.Size = new System.Drawing.Size(108, 27);
            this.btnOptimizeRegistryDefragmenterOptimize.TabIndex = 28;
            this.btnOptimizeRegistryDefragmenterOptimize.Text = "Optimize";
            this.btnOptimizeRegistryDefragmenterOptimize.ClickButtonArea += new CButtonLib.CButton.ClickButtonAreaEventHandler(this.btnOptimizeRegistryDefragmenterOptimize_ClickButtonArea);
            // 
            // pbOptimizeRegistryDefragmenter
            // 
            this.pbOptimizeRegistryDefragmenter.Location = new System.Drawing.Point(184, 76);
            this.pbOptimizeRegistryDefragmenter.Name = "pbOptimizeRegistryDefragmenter";
            this.pbOptimizeRegistryDefragmenter.Size = new System.Drawing.Size(552, 23);
            this.pbOptimizeRegistryDefragmenter.TabIndex = 22;
            // 
            // lblOptimizeRegistryDefragmenterStatus
            // 
            this.lblOptimizeRegistryDefragmenterStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOptimizeRegistryDefragmenterStatus.AutoSize = true;
            this.lblOptimizeRegistryDefragmenterStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblOptimizeRegistryDefragmenterStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblOptimizeRegistryDefragmenterStatus.ForeColor = System.Drawing.Color.Black;
            this.lblOptimizeRegistryDefragmenterStatus.Location = new System.Drawing.Point(189, 60);
            this.lblOptimizeRegistryDefragmenterStatus.Name = "lblOptimizeRegistryDefragmenterStatus";
            this.lblOptimizeRegistryDefragmenterStatus.Size = new System.Drawing.Size(37, 13);
            this.lblOptimizeRegistryDefragmenterStatus.TabIndex = 21;
            this.lblOptimizeRegistryDefragmenterStatus.Tag = "";
            this.lblOptimizeRegistryDefragmenterStatus.Text = "Status";
            // 
            // btnOptimizeRegistryDefragmenterAnalyze
            // 
            this.btnOptimizeRegistryDefragmenterAnalyze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOptimizeRegistryDefragmenterAnalyze.BackColor = System.Drawing.Color.Transparent;
            this.btnOptimizeRegistryDefragmenterAnalyze.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnOptimizeRegistryDefragmenterAnalyze.ForeColor = System.Drawing.Color.White;
            this.btnOptimizeRegistryDefragmenterAnalyze.Location = new System.Drawing.Point(501, 377);
            this.btnOptimizeRegistryDefragmenterAnalyze.Name = "btnOptimizeRegistryDefragmenterAnalyze";
            this.btnOptimizeRegistryDefragmenterAnalyze.Size = new System.Drawing.Size(108, 27);
            this.btnOptimizeRegistryDefragmenterAnalyze.TabIndex = 27;
            this.btnOptimizeRegistryDefragmenterAnalyze.Text = "Analize";
            this.btnOptimizeRegistryDefragmenterAnalyze.ClickButtonArea += new CButtonLib.CButton.ClickButtonAreaEventHandler(this.btnOptimizeRegistryDefragmenterAnalyze_ClickButtonArea);
            // 
            // cmnuOptimizeStartupManager
            // 
            this.cmnuOptimizeStartupManager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOptimizeStartupManagerEdit,
            this.tsmiOptimizeStartupManagerDelete,
            this.tsmiOptimizeStartupManagerView,
            this.tsmiOptimizeStartupManagerRun});
            this.cmnuOptimizeStartupManager.Name = "cmnuDiskShortcutFixer";
            this.cmnuOptimizeStartupManager.Size = new System.Drawing.Size(108, 92);
            this.cmnuOptimizeStartupManager.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmnuOptimizeStartupManager_ItemClicked);
            // 
            // tsmiOptimizeStartupManagerEdit
            // 
            this.tsmiOptimizeStartupManagerEdit.Image = global::Win.Properties.Resources.edit_16x16;
            this.tsmiOptimizeStartupManagerEdit.Name = "tsmiOptimizeStartupManagerEdit";
            this.tsmiOptimizeStartupManagerEdit.Size = new System.Drawing.Size(107, 22);
            this.tsmiOptimizeStartupManagerEdit.Text = "Edit";
            // 
            // tsmiOptimizeStartupManagerDelete
            // 
            this.tsmiOptimizeStartupManagerDelete.Image = global::Win.Properties.Resources.delete_16x16;
            this.tsmiOptimizeStartupManagerDelete.Name = "tsmiOptimizeStartupManagerDelete";
            this.tsmiOptimizeStartupManagerDelete.Size = new System.Drawing.Size(107, 22);
            this.tsmiOptimizeStartupManagerDelete.Text = "Delete";
            // 
            // tsmiOptimizeStartupManagerView
            // 
            this.tsmiOptimizeStartupManagerView.Image = global::Win.Properties.Resources.view_16x16;
            this.tsmiOptimizeStartupManagerView.Name = "tsmiOptimizeStartupManagerView";
            this.tsmiOptimizeStartupManagerView.Size = new System.Drawing.Size(107, 22);
            this.tsmiOptimizeStartupManagerView.Text = "View";
            // 
            // tsmiOptimizeStartupManagerRun
            // 
            this.tsmiOptimizeStartupManagerRun.Image = global::Win.Properties.Resources.run_16x16;
            this.tsmiOptimizeStartupManagerRun.Name = "tsmiOptimizeStartupManagerRun";
            this.tsmiOptimizeStartupManagerRun.Size = new System.Drawing.Size(107, 22);
            this.tsmiOptimizeStartupManagerRun.Text = "Run";
            // 
            // tvaHomeStep2Problem
            // 
            this.tvaHomeStep2Problem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvaHomeStep2Problem.BackColor = System.Drawing.SystemColors.Window;
            this.tvaHomeStep2Problem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvaHomeStep2Problem.Columns.Add(this.treeColumn12);
            this.tvaHomeStep2Problem.Columns.Add(this.treeColumn22);
            this.tvaHomeStep2Problem.Columns.Add(this.treeColumn32);
            this.tvaHomeStep2Problem.DefaultToolTipProvider = null;
            this.tvaHomeStep2Problem.DragDropMarkColor = System.Drawing.Color.Black;
            this.tvaHomeStep2Problem.FullRowSelect = true;
            this.tvaHomeStep2Problem.GridLineStyle = Common_Tools.TreeViewAdv.Tree.GridLineStyle.Horizontal;
            this.tvaHomeStep2Problem.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tvaHomeStep2Problem.LoadOnDemand = true;
            this.tvaHomeStep2Problem.Location = new System.Drawing.Point(179, 132);
            this.tvaHomeStep2Problem.Model = null;
            this.tvaHomeStep2Problem.Name = "tvaHomeStep2Problem";
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeCheckBox2);
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeIcon2);
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeSection2);
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeProblem2);
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeLocation2);
            this.tvaHomeStep2Problem.NodeControls.Add(this.nodeValueName2);
            this.tvaHomeStep2Problem.SelectedNode = null;
            this.tvaHomeStep2Problem.SelectionMode = Common_Tools.TreeViewAdv.Tree.TreeSelectionMode.Multi;
            this.tvaHomeStep2Problem.Size = new System.Drawing.Size(560, 229);
            this.tvaHomeStep2Problem.TabIndex = 0;
            this.tvaHomeStep2Problem.UseColumns = true;
            this.tvaHomeStep2Problem.NodeMouseDoubleClick += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeNodeAdvMouseEventArgs>(this.treeViewAdvResults_NodeMouseDoubleClick);
            this.tvaHomeStep2Problem.ColumnClicked += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeColumnEventArgs>(this.treeViewAdvResults_ColumnClicked);
            this.tvaHomeStep2Problem.Expanded += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeViewAdvEventArgs>(this.treeViewAdvResults_Expanded);
            // 
            // treeColumn12
            // 
            this.treeColumn12.Header = "Problem";
            this.treeColumn12.Sortable = true;
            this.treeColumn12.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn12.TooltipText = null;
            this.treeColumn12.Width = 75;
            // 
            // treeColumn22
            // 
            this.treeColumn22.Header = "Location";
            this.treeColumn22.Sortable = true;
            this.treeColumn22.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn22.TooltipText = null;
            this.treeColumn22.Width = 175;
            // 
            // treeColumn32
            // 
            this.treeColumn32.Header = "Details";
            this.treeColumn32.Sortable = true;
            this.treeColumn32.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn32.TooltipText = null;
            this.treeColumn32.Width = 175;
            // 
            // nodeCheckBox2
            // 
            this.nodeCheckBox2.DataPropertyName = "Checked";
            this.nodeCheckBox2.LeftMargin = 0;
            this.nodeCheckBox2.ParentColumn = this.treeColumn12;
            // 
            // nodeIcon2
            // 
            this.nodeIcon2.DataPropertyName = "Img";
            this.nodeIcon2.LeftMargin = 1;
            this.nodeIcon2.ParentColumn = this.treeColumn12;
            this.nodeIcon2.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeSection2
            // 
            this.nodeSection2.DataPropertyName = "SectionName";
            this.nodeSection2.IncrementalSearchEnabled = true;
            this.nodeSection2.LeftMargin = 3;
            this.nodeSection2.ParentColumn = this.treeColumn12;
            this.nodeSection2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeProblem2
            // 
            this.nodeProblem2.DataPropertyName = "Problem";
            this.nodeProblem2.IncrementalSearchEnabled = true;
            this.nodeProblem2.LeftMargin = 3;
            this.nodeProblem2.ParentColumn = this.treeColumn12;
            this.nodeProblem2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeLocation2
            // 
            this.nodeLocation2.DataPropertyName = "RegKeyPath";
            this.nodeLocation2.IncrementalSearchEnabled = true;
            this.nodeLocation2.LeftMargin = 3;
            this.nodeLocation2.ParentColumn = this.treeColumn22;
            this.nodeLocation2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeValueName2
            // 
            this.nodeValueName2.DataPropertyName = "ValueName";
            this.nodeValueName2.IncrementalSearchEnabled = true;
            this.nodeValueName2.LeftMargin = 3;
            this.nodeValueName2.ParentColumn = this.treeColumn32;
            this.nodeValueName2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // tvaHomeStep3Problem
            // 
            this.tvaHomeStep3Problem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvaHomeStep3Problem.BackColor = System.Drawing.SystemColors.Window;
            this.tvaHomeStep3Problem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvaHomeStep3Problem.Columns.Add(this.treeColumn13);
            this.tvaHomeStep3Problem.Columns.Add(this.treeColumn23);
            this.tvaHomeStep3Problem.Columns.Add(this.treeColumn33);
            this.tvaHomeStep3Problem.DefaultToolTipProvider = null;
            this.tvaHomeStep3Problem.DragDropMarkColor = System.Drawing.Color.Black;
            this.tvaHomeStep3Problem.FullRowSelect = true;
            this.tvaHomeStep3Problem.GridLineStyle = Common_Tools.TreeViewAdv.Tree.GridLineStyle.Horizontal;
            this.tvaHomeStep3Problem.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tvaHomeStep3Problem.LoadOnDemand = true;
            this.tvaHomeStep3Problem.Location = new System.Drawing.Point(179, 55);
            this.tvaHomeStep3Problem.Model = null;
            this.tvaHomeStep3Problem.Name = "tvaHomeStep3Problem";
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeCheckBox3);
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeIcon3);
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeSection3);
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeProblem3);
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeLocation3);
            this.tvaHomeStep3Problem.NodeControls.Add(this.nodeValueName3);
            this.tvaHomeStep3Problem.SelectedNode = null;
            this.tvaHomeStep3Problem.SelectionMode = Common_Tools.TreeViewAdv.Tree.TreeSelectionMode.Multi;
            this.tvaHomeStep3Problem.Size = new System.Drawing.Size(560, 307);
            this.tvaHomeStep3Problem.TabIndex = 0;
            this.tvaHomeStep3Problem.UseColumns = true;
            this.tvaHomeStep3Problem.NodeMouseDoubleClick += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeNodeAdvMouseEventArgs>(this.treeViewAdvResults_NodeMouseDoubleClick);
            this.tvaHomeStep3Problem.ColumnClicked += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeColumnEventArgs>(this.treeViewAdvResults_ColumnClicked);
            this.tvaHomeStep3Problem.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tvaHomeStep3Problem_KeyUp);
            this.tvaHomeStep3Problem.NodeMouseClick += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeNodeAdvMouseEventArgs>(this.tvaHomeStep3Problem_NodeMouseClick);
            this.tvaHomeStep3Problem.Expanded += new System.EventHandler<Common_Tools.TreeViewAdv.Tree.TreeViewAdvEventArgs>(this.treeViewAdvResults_Expanded);
            // 
            // treeColumn13
            // 
            this.treeColumn13.Header = "Problem";
            this.treeColumn13.Sortable = true;
            this.treeColumn13.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn13.TooltipText = null;
            this.treeColumn13.Width = 75;
            // 
            // treeColumn23
            // 
            this.treeColumn23.Header = "Location";
            this.treeColumn23.Sortable = true;
            this.treeColumn23.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn23.TooltipText = null;
            this.treeColumn23.Width = 175;
            // 
            // treeColumn33
            // 
            this.treeColumn33.Header = "Details";
            this.treeColumn33.Sortable = true;
            this.treeColumn33.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn33.TooltipText = null;
            this.treeColumn33.Width = 175;
            // 
            // nodeCheckBox3
            // 
            this.nodeCheckBox3.DataPropertyName = "Checked";
            this.nodeCheckBox3.EditEnabled = true;
            this.nodeCheckBox3.LeftMargin = 0;
            this.nodeCheckBox3.ParentColumn = this.treeColumn13;
            // 
            // nodeIcon3
            // 
            this.nodeIcon3.DataPropertyName = "Img";
            this.nodeIcon3.LeftMargin = 1;
            this.nodeIcon3.ParentColumn = this.treeColumn13;
            this.nodeIcon3.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeSection3
            // 
            this.nodeSection3.DataPropertyName = "SectionName";
            this.nodeSection3.IncrementalSearchEnabled = true;
            this.nodeSection3.LeftMargin = 3;
            this.nodeSection3.ParentColumn = this.treeColumn13;
            this.nodeSection3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeProblem3
            // 
            this.nodeProblem3.DataPropertyName = "Problem";
            this.nodeProblem3.IncrementalSearchEnabled = true;
            this.nodeProblem3.LeftMargin = 3;
            this.nodeProblem3.ParentColumn = this.treeColumn13;
            this.nodeProblem3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeLocation3
            // 
            this.nodeLocation3.DataPropertyName = "RegKeyPath";
            this.nodeLocation3.IncrementalSearchEnabled = true;
            this.nodeLocation3.LeftMargin = 3;
            this.nodeLocation3.ParentColumn = this.treeColumn23;
            this.nodeLocation3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeValueName3
            // 
            this.nodeValueName3.DataPropertyName = "ValueName";
            this.nodeValueName3.IncrementalSearchEnabled = true;
            this.nodeValueName3.LeftMargin = 3;
            this.nodeValueName3.ParentColumn = this.treeColumn33;
            this.nodeValueName3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // pnlHome
            // 
            this.pnlHome.BackColor = System.Drawing.Color.White;
            this.pnlHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHome.Controls.Add(this.pnlHomeMain);
            this.pnlHome.Controls.Add(this.pnlHomeStep3);
            this.pnlHome.Controls.Add(this.pnlHomeStep2);
            this.pnlHome.Location = new System.Drawing.Point(8, 73);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(741, 416);
            this.pnlHome.TabIndex = 0;
            // 
            // pnlHomeMain
            // 
            this.pnlHomeMain.BackgroundImage = global::Win.Properties.Resources.panel_background_main_left;
            this.pnlHomeMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHomeMain.Caption = null;
            this.pnlHomeMain.Controls.Add(this.btnHomeStartupManager);
            this.pnlHomeMain.Controls.Add(this.btnHomeClearTempraryFiles);
            this.pnlHomeMain.Controls.Add(this.btnHomeRegistryManager);
            //this.pnlHomeMain.Controls.Add(this.btnHomeTitle2);
            //this.pnlHomeMain.Controls.Add(this.btnHomeTitle1);
            this.pnlHomeMain.Controls.Add(this.btnHomeStartScan);
            this.pnlHomeMain.Controls.Add(this.btnHomeDiskCleaner);
            //this.pnlHomeMain.Controls.Add(this.btnHomeTitle3);
            this.pnlHomeMain.Description = null;
            this.pnlHomeMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeMain.HideLogo = false;
            this.pnlHomeMain.Icon = null;
            this.pnlHomeMain.IsIconOnLeft = true;
            this.pnlHomeMain.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeMain.Name = "pnlHomeMain";
            this.pnlHomeMain.NavBarImage = global::Win.Properties.Resources.nav_bar_home;
            this.pnlHomeMain.SeparatorCount = 1;
            this.pnlHomeMain.Size = new System.Drawing.Size(741, 416);
            this.pnlHomeMain.TabIndex = 16;
            this.pnlHomeMain.RegisterClick += new System.EventHandler(this.btnRegister_Click);
            this.pnlHomeMain.PushToTalkClick += new System.EventHandler(this.btnPushToTalkClick_Click);
            // 
            // btnHomeStartupManager
            // 
            this.btnHomeStartupManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnHomeStartupManager.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStartupManager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnHomeStartupManager.Description = "Speed up Windows boot time by disabling unnecessary items.";
            this.btnHomeStartupManager.Icon = global::Win.Properties.Resources.startup_64x64;
            this.btnHomeStartupManager.Location = new System.Drawing.Point(188, 240);
            this.btnHomeStartupManager.Name = "btnHomeStartupManager";
            this.btnHomeStartupManager.Size = new System.Drawing.Size(285, 108);
            this.btnHomeStartupManager.TabIndex = 87;
            this.btnHomeStartupManager.Title = "Startup Manager";
            this.btnHomeStartupManager.ButtonClick += new System.EventHandler(this.btnOptimizeStartupManager_Click);
            // 
            // btnHomeClearTempraryFiles
            // 
            this.btnHomeClearTempraryFiles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnHomeClearTempraryFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeClearTempraryFiles.Description = "Remove Windows temporary files to free up disk space.";
            this.btnHomeClearTempraryFiles.Icon = global::Win.Properties.Resources.clear_temp_64x64;
            this.btnHomeClearTempraryFiles.Location = new System.Drawing.Point(473, 240);
            this.btnHomeClearTempraryFiles.Name = "btnHomeClearTempraryFiles";
            this.btnHomeClearTempraryFiles.Size = new System.Drawing.Size(285, 108);
            this.btnHomeClearTempraryFiles.TabIndex = 86;
            this.btnHomeClearTempraryFiles.Title = "Clear Temporary Files";
            this.btnHomeClearTempraryFiles.ButtonClick += new System.EventHandler(this.btnDiskClearTempraryFiles_Click);
            // 
            // btnHomeRegistryManager
            // 
            this.btnHomeRegistryManager.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnHomeRegistryManager.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeRegistryManager.Description = "Find and repair Windows registry problems.";
            this.btnHomeRegistryManager.Icon = global::Win.Properties.Resources.registry_64x64;
            this.btnHomeRegistryManager.Location = new System.Drawing.Point(473, 92);
            this.btnHomeRegistryManager.Name = "btnHomeRegistryManager";
            this.btnHomeRegistryManager.Size = new System.Drawing.Size(285, 108);
            this.btnHomeRegistryManager.TabIndex = 85;
            this.btnHomeRegistryManager.Title = "Registry Cleaner";
            this.btnHomeRegistryManager.ButtonClick += new System.EventHandler(this.btnOptimizeRegistryManager_Click);
            // 
            // btnHomeTitle2
            // 
            //this.btnHomeTitle2.BackColor = System.Drawing.Color.Transparent;
            //this.btnHomeTitle2.Font = new System.Drawing.Font("Segoe UI Semibold", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            //this.btnHomeTitle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            //this.btnHomeTitle2.Location = new System.Drawing.Point(10, 100);
            //this.btnHomeTitle2.Name = "btnHomeTitle2";
            //this.btnHomeTitle2.Size = new System.Drawing.Size(163, 23);
            //this.btnHomeTitle2.TabIndex = 83;
            //this.btnHomeTitle2.Text = "Start Scan";
            //this.btnHomeTitle2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnHomeTitle1
            // 
            //this.btnHomeTitle1.BackColor = System.Drawing.Color.Transparent;
            //this.btnHomeTitle1.Font = new System.Drawing.Font("Segoe UI", 14.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            //this.btnHomeTitle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            //this.btnHomeTitle1.Location = new System.Drawing.Point(10, 75);
            //this.btnHomeTitle1.Name = "btnHomeTitle1";
            //this.btnHomeTitle1.Size = new System.Drawing.Size(163, 23);
            //this.btnHomeTitle1.TabIndex = 82;
            //this.btnHomeTitle1.Text = "Click the";
            //this.btnHomeTitle1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnHomeStartScan
            // 
            this.btnHomeStartScan.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnHomeStartScan.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStartScan.Caption = "";
            this.btnHomeStartScan.CaptionColor = System.Drawing.Color.Black;
            this.btnHomeStartScan.CaptionColorActive = System.Drawing.Color.Black;
            this.btnHomeStartScan.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnHomeStartScan.Image = global::Win.Properties.Resources.start_scan_button_normal1;
            this.btnHomeStartScan.ImageActive = null;
            this.btnHomeStartScan.ImageDisabled = global::Win.Properties.Resources.start_scan_button_normal1;
            this.btnHomeStartScan.ImageNormal = global::Win.Properties.Resources.start_scan_button_normal1;
            this.btnHomeStartScan.ImageRollover = global::Win.Properties.Resources.start_scan_button_rollover1;
            this.btnHomeStartScan.IsActive = false;
            this.btnHomeStartScan.Location = new System.Drawing.Point(12, 125);
            this.btnHomeStartScan.Name = "btnHomeStartScan";
            this.btnHomeStartScan.Size = new System.Drawing.Size(163, 141);
            this.btnHomeStartScan.TabIndex = 73;
            this.btnHomeStartScan.TabStop = false;
            this.btnHomeStartScan.Click += new System.EventHandler(this.btnHomeStartScan_Click);
            // 
            // btnHomeDiskCleaner
            // 
            this.btnHomeDiskCleaner.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnHomeDiskCleaner.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeDiskCleaner.Description = "Free up disk space by removing unnecessary files.";
            this.btnHomeDiskCleaner.Icon = global::Win.Properties.Resources.disc_cleaner_64x64;
            this.btnHomeDiskCleaner.Location = new System.Drawing.Point(188, 92);
            this.btnHomeDiskCleaner.Name = "btnHomeDiskCleaner";
            this.btnHomeDiskCleaner.Size = new System.Drawing.Size(285, 108);
            this.btnHomeDiskCleaner.TabIndex = 81;
            this.btnHomeDiskCleaner.Title = "Disk Cleaner";
            this.btnHomeDiskCleaner.ButtonClick += new System.EventHandler(this.btnHomeDiskCleaner_Click);
            // 
            // btnHomeTitle3
            // 
            //this.btnHomeTitle3.BackColor = System.Drawing.Color.Transparent;
            //this.btnHomeTitle3.Font = new System.Drawing.Font("Segoe UI", 14.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            //this.btnHomeTitle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            //this.btnHomeTitle3.Location = new System.Drawing.Point(10, 125);
            //this.btnHomeTitle3.Name = "btnHomeTitle3";
            //this.btnHomeTitle3.Size = new System.Drawing.Size(163, 50);
            //this.btnHomeTitle3.TabIndex = 84;
            //this.btnHomeTitle3.Text = "to scan your computer for problems";
            //this.btnHomeTitle3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlHomeStep3
            // 
            this.pnlHomeStep3.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlHomeStep3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHomeStep3.Caption = "Overall Scan Results";
            this.pnlHomeStep3.Controls.Add(this.btnHomeStep3Cancel);
            this.pnlHomeStep3.Controls.Add(this.btnHomeStep3FixAll);
            this.pnlHomeStep3.Controls.Add(this.lblHomeStep3DetectedItems);
            this.pnlHomeStep3.Controls.Add(this.osgHomeStep3OverallScanResult);
            this.pnlHomeStep3.Controls.Add(this.pnlHomeStep4Fixing);
            this.pnlHomeStep3.Controls.Add(this.tvaHomeStep3Problem);
            this.pnlHomeStep3.Description = null;
            this.pnlHomeStep3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeStep3.HideLogo = true;
            this.pnlHomeStep3.Icon = global::Win.Properties.Resources.overall_scan_results_64x64;
            this.pnlHomeStep3.IsIconOnLeft = true;
            this.pnlHomeStep3.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeStep3.Name = "pnlHomeStep3";
            this.pnlHomeStep3.NavBarImage = global::Win.Properties.Resources.results_nav_bar;
            this.pnlHomeStep3.Size = new System.Drawing.Size(741, 416);
            this.pnlHomeStep3.TabIndex = 27;
            // 
            // btnHomeStep3Cancel
            // 
            this.btnHomeStep3Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHomeStep3Cancel.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStep3Cancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeStep3Cancel.ForeColor = System.Drawing.Color.White;
            this.btnHomeStep3Cancel.Location = new System.Drawing.Point(635, 377);
            this.btnHomeStep3Cancel.Name = "btnHomeStep3Cancel";
            this.btnHomeStep3Cancel.Size = new System.Drawing.Size(88, 27);
            this.btnHomeStep3Cancel.TabIndex = 85;
            this.btnHomeStep3Cancel.Text = "Cancel";
            this.btnHomeStep3Cancel.Click += new System.EventHandler(this.btnHomeStep3Cancel_Click);
            // 
            // btnHomeStep3FixAll
            // 
            this.btnHomeStep3FixAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnHomeStep3FixAll.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStep3FixAll.Caption = "";
            this.btnHomeStep3FixAll.CaptionColor = System.Drawing.Color.Black;
            this.btnHomeStep3FixAll.CaptionColorActive = System.Drawing.Color.Black;
            this.btnHomeStep3FixAll.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnHomeStep3FixAll.Image = global::Win.Properties.Resources.fix_all_normal;
            this.btnHomeStep3FixAll.ImageActive = null;
            this.btnHomeStep3FixAll.ImageDisabled = global::Win.Properties.Resources.fix_all_disabled;
            this.btnHomeStep3FixAll.ImageNormal = global::Win.Properties.Resources.fix_all_normal;
            this.btnHomeStep3FixAll.ImageRollover = global::Win.Properties.Resources.fix_all_rollover;
            this.btnHomeStep3FixAll.IsActive = false;
            this.btnHomeStep3FixAll.Location = new System.Drawing.Point(303, 371);
            this.btnHomeStep3FixAll.Name = "btnHomeStep3FixAll";
            this.btnHomeStep3FixAll.Size = new System.Drawing.Size(135, 39);
            this.btnHomeStep3FixAll.TabIndex = 76;
            this.btnHomeStep3FixAll.TabStop = false;
            this.btnHomeStep3FixAll.Click += new System.EventHandler(this.btnHomeStep3FixAll_Click);
            // 
            // lblHomeStep3DetectedItems
            // 
            this.lblHomeStep3DetectedItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHomeStep3DetectedItems.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeStep3DetectedItems.BackgroundImage = global::Win.Properties.Resources.detected_items;
            this.lblHomeStep3DetectedItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblHomeStep3DetectedItems.Count = 10;
            this.lblHomeStep3DetectedItems.Location = new System.Drawing.Point(14, 368);
            this.lblHomeStep3DetectedItems.Name = "lblHomeStep3DetectedItems";
            this.lblHomeStep3DetectedItems.Size = new System.Drawing.Size(145, 45);
            this.lblHomeStep3DetectedItems.TabIndex = 84;
            // 
            // osgHomeStep3OverallScanResult
            // 
            this.osgHomeStep3OverallScanResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.osgHomeStep3OverallScanResult.BackColor = System.Drawing.Color.Transparent;
            this.osgHomeStep3OverallScanResult.Location = new System.Drawing.Point(179, 55);
            this.osgHomeStep3OverallScanResult.Name = "osgHomeStep3OverallScanResult";
            this.osgHomeStep3OverallScanResult.Size = new System.Drawing.Size(560, 307);
            this.osgHomeStep3OverallScanResult.TabIndex = 54;
            this.osgHomeStep3OverallScanResult.Edit += new System.EventHandler(this.osgHomeStep3OverallScanResult_Edit);
            this.osgHomeStep3OverallScanResult.Action += new System.EventHandler(this.osgHomeStep3OverallScanResult_Action);
            // 
            // pnlHomeStep4Fixing
            // 
            this.pnlHomeStep4Fixing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHomeStep4Fixing.BackColor = System.Drawing.Color.Transparent;
            this.pnlHomeStep4Fixing.Controls.Add(this.pbHomeStep4Progress);
            this.pnlHomeStep4Fixing.Controls.Add(this.lblHomeStep4ProgressValue);
            this.pnlHomeStep4Fixing.Controls.Add(this.lblHomeStep4ProgressTitle);
            this.pnlHomeStep4Fixing.Controls.Add(this.lblHomeStep4CurrentFixedObject);
            this.pnlHomeStep4Fixing.Controls.Add(this.lblHomeStep4CurrentFixer);
            this.pnlHomeStep4Fixing.Location = new System.Drawing.Point(179, 55);
            this.pnlHomeStep4Fixing.Name = "pnlHomeStep4Fixing";
            this.pnlHomeStep4Fixing.Size = new System.Drawing.Size(560, 307);
            this.pnlHomeStep4Fixing.TabIndex = 57;
            // 
            // pbHomeStep4Progress
            // 
            this.pbHomeStep4Progress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbHomeStep4Progress.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pbHomeStep4Progress.Location = new System.Drawing.Point(17, 143);
            this.pbHomeStep4Progress.Name = "pbHomeStep4Progress";
            this.pbHomeStep4Progress.Size = new System.Drawing.Size(526, 24);
            this.pbHomeStep4Progress.TabIndex = 54;
            // 
            // lblHomeStep4ProgressValue
            // 
            this.lblHomeStep4ProgressValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHomeStep4ProgressValue.Appearance.Font = new System.Drawing.Font("Arial", 13.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblHomeStep4ProgressValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep4ProgressValue.Location = new System.Drawing.Point(305, 187);
            this.lblHomeStep4ProgressValue.Name = "lblHomeStep4ProgressValue";
            this.lblHomeStep4ProgressValue.Size = new System.Drawing.Size(17, 16);
            this.lblHomeStep4ProgressValue.TabIndex = 53;
            this.lblHomeStep4ProgressValue.Text = "0%";
            // 
            // lblHomeStep4ProgressTitle
            // 
            this.lblHomeStep4ProgressTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHomeStep4ProgressTitle.Appearance.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblHomeStep4ProgressTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep4ProgressTitle.Location = new System.Drawing.Point(230, 189);
            this.lblHomeStep4ProgressTitle.Name = "lblHomeStep4ProgressTitle";
            this.lblHomeStep4ProgressTitle.Size = new System.Drawing.Size(70, 12);
            this.lblHomeStep4ProgressTitle.TabIndex = 52;
            this.lblHomeStep4ProgressTitle.Text = "PROGRESS ...";
            // 
            // lblHomeStep4CurrentFixedObject
            // 
            this.lblHomeStep4CurrentFixedObject.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHomeStep4CurrentFixedObject.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeStep4CurrentFixedObject.Appearance.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblHomeStep4CurrentFixedObject.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep4CurrentFixedObject.Location = new System.Drawing.Point(17, 121);
            this.lblHomeStep4CurrentFixedObject.Name = "lblHomeStep4CurrentFixedObject";
            this.lblHomeStep4CurrentFixedObject.Size = new System.Drawing.Size(394, 15);
            this.lblHomeStep4CurrentFixedObject.TabIndex = 50;
            this.lblHomeStep4CurrentFixedObject.Text = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\DataAccess\\RootBinder";
            // 
            // lblHomeStep4CurrentFixer
            // 
            this.lblHomeStep4CurrentFixer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHomeStep4CurrentFixer.Appearance.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblHomeStep4CurrentFixer.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep4CurrentFixer.Location = new System.Drawing.Point(17, 102);
            this.lblHomeStep4CurrentFixer.Name = "lblHomeStep4CurrentFixer";
            this.lblHomeStep4CurrentFixer.Size = new System.Drawing.Size(49, 12);
            this.lblHomeStep4CurrentFixer.TabIndex = 51;
            this.lblHomeStep4CurrentFixer.Text = "Cleaning :";
            // 
            // pnlHomeStep2
            // 
            this.pnlHomeStep2.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlHomeStep2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHomeStep2.Caption = null;
            this.pnlHomeStep2.Controls.Add(this.tvaHomeStep2Problem);
            this.pnlHomeStep2.Controls.Add(this.btnHomeStep2Cancel);
            this.pnlHomeStep2.Controls.Add(this.sucHomeStep2Privacy);
            this.pnlHomeStep2.Controls.Add(this.sucHomeStep2Registry);
            this.pnlHomeStep2.Controls.Add(this.sucHomeStep2Process);
            this.pnlHomeStep2.Controls.Add(this.lblHomeStep2ProgressValue);
            this.pnlHomeStep2.Controls.Add(this.pbHomeStep2Progress);
            this.pnlHomeStep2.Controls.Add(this.lblHomeStep2ProgressTitle);
            this.pnlHomeStep2.Controls.Add(this.lblHomeStep2CurrentScannedObject);
            this.pnlHomeStep2.Controls.Add(this.lblHomeStep2CurrentScanner);
            this.pnlHomeStep2.Controls.Add(this.btnHomeStep2Stop);
            this.pnlHomeStep2.Controls.Add(this.lblHomeStep2DetectedItems);
            this.pnlHomeStep2.Description = null;
            this.pnlHomeStep2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeStep2.HideLogo = false;
            this.pnlHomeStep2.Icon = null;
            this.pnlHomeStep2.IsIconOnLeft = false;
            this.pnlHomeStep2.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeStep2.Name = "pnlHomeStep2";
            this.pnlHomeStep2.NavBarImage = global::Win.Properties.Resources.results_nav_bar;
            this.pnlHomeStep2.Size = new System.Drawing.Size(741, 416);
            this.pnlHomeStep2.TabIndex = 17;
            // 
            // btnHomeStep2Cancel
            // 
            this.btnHomeStep2Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHomeStep2Cancel.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStep2Cancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeStep2Cancel.ForeColor = System.Drawing.Color.White;
            this.btnHomeStep2Cancel.Location = new System.Drawing.Point(635, 375);
            this.btnHomeStep2Cancel.Name = "btnHomeStep2Cancel";
            this.btnHomeStep2Cancel.Size = new System.Drawing.Size(88, 27);
            this.btnHomeStep2Cancel.TabIndex = 86;
            this.btnHomeStep2Cancel.Text = "Cancel";
            this.btnHomeStep2Cancel.Click += new System.EventHandler(this.btnHomeStep2Cancel_Click);
            // 
            // sucHomeStep2Privacy
            // 
            this.sucHomeStep2Privacy.BackColor = System.Drawing.Color.Transparent;
            this.sucHomeStep2Privacy.BackgroundImage = global::Win.Properties.Resources.privacy_not_scanned1;
            this.sucHomeStep2Privacy.Caption = "Privacy";
            this.sucHomeStep2Privacy.Description = "Not scanned";
            this.sucHomeStep2Privacy.ImageIsNotScanned = global::Win.Properties.Resources.privacy_not_scanned1;
            this.sucHomeStep2Privacy.ImageScanning = global::Win.Properties.Resources.privacy_complete1;
            this.sucHomeStep2Privacy.IsScanned = false;
            this.sucHomeStep2Privacy.Location = new System.Drawing.Point(40, 261);
            this.sucHomeStep2Privacy.Name = "sucHomeStep2Privacy";
            this.sucHomeStep2Privacy.Size = new System.Drawing.Size(100, 100);
            this.sucHomeStep2Privacy.TabIndex = 90;
            // 
            // sucHomeStep2Registry
            // 
            this.sucHomeStep2Registry.BackColor = System.Drawing.Color.Transparent;
            this.sucHomeStep2Registry.BackgroundImage = global::Win.Properties.Resources.registry_not_scanned1;
            this.sucHomeStep2Registry.Caption = "Registry";
            this.sucHomeStep2Registry.Description = "Not scanned";
            this.sucHomeStep2Registry.ImageIsNotScanned = global::Win.Properties.Resources.registry_not_scanned1;
            this.sucHomeStep2Registry.ImageScanning = global::Win.Properties.Resources.registry_complete1;
            this.sucHomeStep2Registry.IsScanned = false;
            this.sucHomeStep2Registry.Location = new System.Drawing.Point(40, 53);
            this.sucHomeStep2Registry.Name = "sucHomeStep2Registry";
            this.sucHomeStep2Registry.Size = new System.Drawing.Size(100, 100);
            this.sucHomeStep2Registry.TabIndex = 88;
            // 
            // sucHomeStep2Process
            // 
            this.sucHomeStep2Process.BackColor = System.Drawing.Color.Transparent;
            this.sucHomeStep2Process.BackgroundImage = global::Win.Properties.Resources.process_not_scanned1;
            this.sucHomeStep2Process.Caption = "Process";
            this.sucHomeStep2Process.Description = "Not scanned";
            this.sucHomeStep2Process.ImageIsNotScanned = global::Win.Properties.Resources.process_not_scanned1;
            this.sucHomeStep2Process.ImageScanning = global::Win.Properties.Resources.process_complete1;
            this.sucHomeStep2Process.IsScanned = false;
            this.sucHomeStep2Process.Location = new System.Drawing.Point(40, 151);
            this.sucHomeStep2Process.Name = "sucHomeStep2Process";
            this.sucHomeStep2Process.Size = new System.Drawing.Size(100, 100);
            this.sucHomeStep2Process.TabIndex = 89;
            // 
            // lblHomeStep2ProgressValue
            // 
            this.lblHomeStep2ProgressValue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblHomeStep2ProgressValue.Appearance.Font = new System.Drawing.Font("Arial", 13.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblHomeStep2ProgressValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep2ProgressValue.Location = new System.Drawing.Point(474, 140);
            this.lblHomeStep2ProgressValue.Name = "lblHomeStep2ProgressValue";
            this.lblHomeStep2ProgressValue.Size = new System.Drawing.Size(17, 16);
            this.lblHomeStep2ProgressValue.TabIndex = 44;
            this.lblHomeStep2ProgressValue.Text = "0%";
            // 
            // pbHomeStep2Progress
            // 
            this.pbHomeStep2Progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbHomeStep2Progress.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pbHomeStep2Progress.Location = new System.Drawing.Point(205, 96);
            this.pbHomeStep2Progress.Name = "pbHomeStep2Progress";
            this.pbHomeStep2Progress.Size = new System.Drawing.Size(446, 24);
            this.pbHomeStep2Progress.TabIndex = 45;
            // 
            // lblHomeStep2ProgressTitle
            // 
            this.lblHomeStep2ProgressTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblHomeStep2ProgressTitle.Appearance.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblHomeStep2ProgressTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep2ProgressTitle.Location = new System.Drawing.Point(399, 142);
            this.lblHomeStep2ProgressTitle.Name = "lblHomeStep2ProgressTitle";
            this.lblHomeStep2ProgressTitle.Size = new System.Drawing.Size(70, 12);
            this.lblHomeStep2ProgressTitle.TabIndex = 43;
            this.lblHomeStep2ProgressTitle.Text = "PROGRESS ...";
            // 
            // lblHomeStep2CurrentScannedObject
            // 
            this.lblHomeStep2CurrentScannedObject.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeStep2CurrentScannedObject.Appearance.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblHomeStep2CurrentScannedObject.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep2CurrentScannedObject.Location = new System.Drawing.Point(205, 74);
            this.lblHomeStep2CurrentScannedObject.Name = "lblHomeStep2CurrentScannedObject";
            this.lblHomeStep2CurrentScannedObject.Size = new System.Drawing.Size(394, 15);
            this.lblHomeStep2CurrentScannedObject.TabIndex = 18;
            this.lblHomeStep2CurrentScannedObject.Text = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\DataAccess\\RootBinder";
            // 
            // lblHomeStep2CurrentScanner
            // 
            this.lblHomeStep2CurrentScanner.Appearance.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblHomeStep2CurrentScanner.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblHomeStep2CurrentScanner.Location = new System.Drawing.Point(205, 55);
            this.lblHomeStep2CurrentScanner.Name = "lblHomeStep2CurrentScanner";
            this.lblHomeStep2CurrentScanner.Size = new System.Drawing.Size(145, 12);
            this.lblHomeStep2CurrentScanner.TabIndex = 22;
            this.lblHomeStep2CurrentScanner.Text = "Scanning File/Path Reference";
            // 
            // btnHomeStep2Stop
            // 
            this.btnHomeStep2Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHomeStep2Stop.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeStep2Stop.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeStep2Stop.ForeColor = System.Drawing.Color.White;
            this.btnHomeStep2Stop.Location = new System.Drawing.Point(657, 95);
            this.btnHomeStep2Stop.Name = "btnHomeStep2Stop";
            this.btnHomeStep2Stop.Size = new System.Drawing.Size(66, 27);
            this.btnHomeStep2Stop.TabIndex = 87;
            this.btnHomeStep2Stop.Text = "Stop";
            this.btnHomeStep2Stop.Click += new System.EventHandler(this.btnHomeStep2Stop_Click);
            // 
            // lblHomeStep2DetectedItems
            // 
            this.lblHomeStep2DetectedItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHomeStep2DetectedItems.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeStep2DetectedItems.BackgroundImage = global::Win.Properties.Resources.detected_items;
            this.lblHomeStep2DetectedItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblHomeStep2DetectedItems.Count = 10;
            this.lblHomeStep2DetectedItems.Location = new System.Drawing.Point(298, 368);
            this.lblHomeStep2DetectedItems.Name = "lblHomeStep2DetectedItems";
            this.lblHomeStep2DetectedItems.Size = new System.Drawing.Size(145, 45);
            this.lblHomeStep2DetectedItems.TabIndex = 85;
            // 
            // pnlHomeInfo
            // 
            this.pnlHomeInfo.BackgroundImage = global::Win.Properties.Resources.panel_background_left;
            this.pnlHomeInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHomeInfo.Caption = "Info";
            this.pnlHomeInfo.Controls.Add(this.groupHomeInfoUpdates);
            this.pnlHomeInfo.Controls.Add(this.lblHomeInfoVersion);
            this.pnlHomeInfo.Controls.Add(this.groupHomeInfoRegistration);
            this.pnlHomeInfo.Description = "Updates and registration";
            this.pnlHomeInfo.HideLogo = false;
            this.pnlHomeInfo.Icon = global::Win.Properties.Resources.logo_64x64;
            this.pnlHomeInfo.IsIconOnLeft = true;
            this.pnlHomeInfo.Location = new System.Drawing.Point(8, 73);
            this.pnlHomeInfo.Name = "pnlHomeInfo";
            this.pnlHomeInfo.NavBarImage = global::Win.Properties.Resources.info_nav_bar;
            this.pnlHomeInfo.Size = new System.Drawing.Size(741, 416);
            this.pnlHomeInfo.TabIndex = 40;
            // 
            // groupHomeInfoUpdates
            // 
            this.groupHomeInfoUpdates.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupHomeInfoUpdates.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupHomeInfoUpdates.Appearance.Options.UseBackColor = true;
            this.groupHomeInfoUpdates.Controls.Add(this.cbHomeInfoAutocheckForUpdates);
            this.groupHomeInfoUpdates.Controls.Add(this.lblHomeIntoCheckForUpdatesProgress);
            this.groupHomeInfoUpdates.Controls.Add(this.lblHomeIntoCheckForUpdatesTotalSize);
            this.groupHomeInfoUpdates.Controls.Add(this.pbHomeIntoCheckForUpdatesProgress);
            this.groupHomeInfoUpdates.Controls.Add(this.lblHomeInfoCheckForUpdatesLatDateTime);
            this.groupHomeInfoUpdates.Controls.Add(this.btnHomeIntoCheckForUpdatesCancel);
            this.groupHomeInfoUpdates.Controls.Add(this.btnHomeIntoCheckForUpdates);
            this.groupHomeInfoUpdates.Location = new System.Drawing.Point(179, 55);
            this.groupHomeInfoUpdates.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupHomeInfoUpdates.Name = "groupHomeInfoUpdates";
            this.groupHomeInfoUpdates.Size = new System.Drawing.Size(560, 154);
            this.groupHomeInfoUpdates.TabIndex = 62;
            this.groupHomeInfoUpdates.Text = "Updates";
            // 
            // cbHomeInfoAutocheckForUpdates
            // 
            this.cbHomeInfoAutocheckForUpdates.Location = new System.Drawing.Point(7, 92);
            this.cbHomeInfoAutocheckForUpdates.Name = "cbHomeInfoAutocheckForUpdates";
            this.cbHomeInfoAutocheckForUpdates.Size = new System.Drawing.Size(255, 19);
            this.cbHomeInfoAutocheckForUpdates.TabIndex = 73;
            this.cbHomeInfoAutocheckForUpdates.CheckedChanged += new System.EventHandler(this.cbHomeInfoAutocheckForUpdates_CheckedChanged);
            // 
            // lblHomeIntoCheckForUpdatesProgress
            // 
            this.lblHomeIntoCheckForUpdatesProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHomeIntoCheckForUpdatesProgress.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeIntoCheckForUpdatesProgress.Location = new System.Drawing.Point(-1122, 67);
            this.lblHomeIntoCheckForUpdatesProgress.Name = "lblHomeIntoCheckForUpdatesProgress";
            this.lblHomeIntoCheckForUpdatesProgress.Size = new System.Drawing.Size(46, 13);
            this.lblHomeIntoCheckForUpdatesProgress.TabIndex = 68;
            this.lblHomeIntoCheckForUpdatesProgress.Text = "Progress:";
            this.lblHomeIntoCheckForUpdatesProgress.Visible = false;
            // 
            // lblHomeIntoCheckForUpdatesTotalSize
            // 
            this.lblHomeIntoCheckForUpdatesTotalSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHomeIntoCheckForUpdatesTotalSize.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeIntoCheckForUpdatesTotalSize.Location = new System.Drawing.Point(-1110, 67);
            this.lblHomeIntoCheckForUpdatesTotalSize.Name = "lblHomeIntoCheckForUpdatesTotalSize";
            this.lblHomeIntoCheckForUpdatesTotalSize.Size = new System.Drawing.Size(49, 13);
            this.lblHomeIntoCheckForUpdatesTotalSize.TabIndex = 67;
            this.lblHomeIntoCheckForUpdatesTotalSize.Text = "Total size:";
            this.lblHomeIntoCheckForUpdatesTotalSize.Visible = false;
            // 
            // pbHomeIntoCheckForUpdatesProgress
            // 
            this.pbHomeIntoCheckForUpdatesProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbHomeIntoCheckForUpdatesProgress.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pbHomeIntoCheckForUpdatesProgress.Location = new System.Drawing.Point(172, 34);
            this.pbHomeIntoCheckForUpdatesProgress.Name = "pbHomeIntoCheckForUpdatesProgress";
            this.pbHomeIntoCheckForUpdatesProgress.Size = new System.Drawing.Size(286, 24);
            this.pbHomeIntoCheckForUpdatesProgress.TabIndex = 65;
            this.pbHomeIntoCheckForUpdatesProgress.Value = 30;
            this.pbHomeIntoCheckForUpdatesProgress.Visible = false;
            // 
            // lblHomeInfoCheckForUpdatesLatDateTime
            // 
            this.lblHomeInfoCheckForUpdatesLatDateTime.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeInfoCheckForUpdatesLatDateTime.Location = new System.Drawing.Point(11, 67);
            this.lblHomeInfoCheckForUpdatesLatDateTime.Name = "lblHomeInfoCheckForUpdatesLatDateTime";
            this.lblHomeInfoCheckForUpdatesLatDateTime.Size = new System.Drawing.Size(124, 13);
            this.lblHomeInfoCheckForUpdatesLatDateTime.TabIndex = 62;
            this.lblHomeInfoCheckForUpdatesLatDateTime.Text = "Last check for updates in:";
            // 
            // btnHomeIntoCheckForUpdatesCancel
            // 
            this.btnHomeIntoCheckForUpdatesCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHomeIntoCheckForUpdatesCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeIntoCheckForUpdatesCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeIntoCheckForUpdatesCancel.ForeColor = System.Drawing.Color.White;
            this.btnHomeIntoCheckForUpdatesCancel.Location = new System.Drawing.Point(464, 33);
            this.btnHomeIntoCheckForUpdatesCancel.Name = "btnHomeIntoCheckForUpdatesCancel";
            this.btnHomeIntoCheckForUpdatesCancel.Size = new System.Drawing.Size(88, 27);
            this.btnHomeIntoCheckForUpdatesCancel.TabIndex = 79;
            this.btnHomeIntoCheckForUpdatesCancel.Text = "Cancel";
            this.btnHomeIntoCheckForUpdatesCancel.Click += new System.EventHandler(this.btnHomeIntoCheckForUpdatesCancel_Click);
            // 
            // btnHomeIntoCheckForUpdates
            // 
            this.btnHomeIntoCheckForUpdates.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeIntoCheckForUpdates.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeIntoCheckForUpdates.ForeColor = System.Drawing.Color.White;
            this.btnHomeIntoCheckForUpdates.Location = new System.Drawing.Point(9, 33);
            this.btnHomeIntoCheckForUpdates.Name = "btnHomeIntoCheckForUpdates";
            this.btnHomeIntoCheckForUpdates.Size = new System.Drawing.Size(157, 27);
            this.btnHomeIntoCheckForUpdates.TabIndex = 80;
            this.btnHomeIntoCheckForUpdates.Text = "Check for Updates";
            this.btnHomeIntoCheckForUpdates.Click += new System.EventHandler(this.btnHomeInfoCheckForUpdates_Click);
            // 
            // lblHomeInfoVersion
            // 
            this.lblHomeInfoVersion.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblHomeInfoVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeInfoVersion.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblHomeInfoVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeInfoVersion.Location = new System.Drawing.Point(0, 319);
            this.lblHomeInfoVersion.Name = "lblHomeInfoVersion";
            this.lblHomeInfoVersion.Size = new System.Drawing.Size(175, 23);
            this.lblHomeInfoVersion.TabIndex = 84;
            this.lblHomeInfoVersion.Text = "Version: 2.5";
            this.lblHomeInfoVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupHomeInfoRegistration
            // 
            this.groupHomeInfoRegistration.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupHomeInfoRegistration.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupHomeInfoRegistration.Appearance.Options.UseBackColor = true;
            this.groupHomeInfoRegistration.Controls.Add(this.btnHomeIntoBuyNow);
            this.groupHomeInfoRegistration.Controls.Add(this.btnHomeInfoLicenseKeyOk);
            this.groupHomeInfoRegistration.Controls.Add(this.lblHomeInfoLicenseStatusValue);
            this.groupHomeInfoRegistration.Controls.Add(this.lblHomeInfoLicenseStatusTitle);
            this.groupHomeInfoRegistration.Controls.Add(this.txtHomeInfoLicenseKey);
            this.groupHomeInfoRegistration.Controls.Add(this.lblHomeInfoLicenseKey);
            this.groupHomeInfoRegistration.Location = new System.Drawing.Point(179, 211);
            this.groupHomeInfoRegistration.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupHomeInfoRegistration.Name = "groupHomeInfoRegistration";
            this.groupHomeInfoRegistration.Size = new System.Drawing.Size(560, 151);
            this.groupHomeInfoRegistration.TabIndex = 63;
            this.groupHomeInfoRegistration.Text = "Registration";
            // 
            // btnHomeIntoBuyNow
            // 
            this.btnHomeIntoBuyNow.BackColor = System.Drawing.Color.Transparent;
            this.btnHomeIntoBuyNow.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnHomeIntoBuyNow.ForeColor = System.Drawing.Color.White;
            this.btnHomeIntoBuyNow.Location = new System.Drawing.Point(9, 33);
            this.btnHomeIntoBuyNow.Name = "btnHomeIntoBuyNow";
            this.btnHomeIntoBuyNow.Size = new System.Drawing.Size(88, 27);
            this.btnHomeIntoBuyNow.TabIndex = 80;
            this.btnHomeIntoBuyNow.Text = "Buy Now";
            this.btnHomeIntoBuyNow.Click += new System.EventHandler(this.btnHomeIntoBuyNow_Click);
            // 
            // btnHomeInfoLicenseKeyOk
            // 
            this.btnHomeInfoLicenseKeyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHomeInfoLicenseKeyOk.Caption = "";
            this.btnHomeInfoLicenseKeyOk.CaptionColor = System.Drawing.Color.Black;
            this.btnHomeInfoLicenseKeyOk.CaptionColorActive = System.Drawing.Color.Black;
            this.btnHomeInfoLicenseKeyOk.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnHomeInfoLicenseKeyOk.Image = global::Win.Properties.Resources.OK_normal;
            this.btnHomeInfoLicenseKeyOk.ImageActive = null;
            this.btnHomeInfoLicenseKeyOk.ImageDisabled = global::Win.Properties.Resources.OK_normal;
            this.btnHomeInfoLicenseKeyOk.ImageNormal = global::Win.Properties.Resources.OK_normal;
            this.btnHomeInfoLicenseKeyOk.ImageRollover = global::Win.Properties.Resources.OK_rollover;
            this.btnHomeInfoLicenseKeyOk.IsActive = false;
            this.btnHomeInfoLicenseKeyOk.Location = new System.Drawing.Point(467, 107);
            this.btnHomeInfoLicenseKeyOk.Name = "btnHomeInfoLicenseKeyOk";
            this.btnHomeInfoLicenseKeyOk.Size = new System.Drawing.Size(85, 39);
            this.btnHomeInfoLicenseKeyOk.TabIndex = 74;
            this.btnHomeInfoLicenseKeyOk.TabStop = false;
            this.btnHomeInfoLicenseKeyOk.Click += new System.EventHandler(this.btnHomeInfoLicenseKeyOk_Click);
            // 
            // lblHomeInfoLicenseStatusValue
            // 
            this.lblHomeInfoLicenseStatusValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeInfoLicenseStatusValue.Location = new System.Drawing.Point(106, 116);
            this.lblHomeInfoLicenseStatusValue.Name = "lblHomeInfoLicenseStatusValue";
            this.lblHomeInfoLicenseStatusValue.Size = new System.Drawing.Size(60, 13);
            this.lblHomeInfoLicenseStatusValue.TabIndex = 65;
            this.lblHomeInfoLicenseStatusValue.Text = "Status Value";
            // 
            // lblHomeInfoLicenseStatusTitle
            // 
            this.lblHomeInfoLicenseStatusTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeInfoLicenseStatusTitle.Location = new System.Drawing.Point(11, 116);
            this.lblHomeInfoLicenseStatusTitle.Name = "lblHomeInfoLicenseStatusTitle";
            this.lblHomeInfoLicenseStatusTitle.Size = new System.Drawing.Size(69, 13);
            this.lblHomeInfoLicenseStatusTitle.TabIndex = 64;
            this.lblHomeInfoLicenseStatusTitle.Text = "License Status";
            // 
            // txtHomeInfoLicenseKey
            // 
            this.txtHomeInfoLicenseKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHomeInfoLicenseKey.Location = new System.Drawing.Point(106, 83);
            this.txtHomeInfoLicenseKey.Name = "txtHomeInfoLicenseKey";
            this.txtHomeInfoLicenseKey.Size = new System.Drawing.Size(446, 20);
            this.txtHomeInfoLicenseKey.TabIndex = 62;
            // 
            // lblHomeInfoLicenseKey
            // 
            this.lblHomeInfoLicenseKey.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.lblHomeInfoLicenseKey.Location = new System.Drawing.Point(11, 86);
            this.lblHomeInfoLicenseKey.Name = "lblHomeInfoLicenseKey";
            this.lblHomeInfoLicenseKey.Size = new System.Drawing.Size(56, 13);
            this.lblHomeInfoLicenseKey.TabIndex = 61;
            this.lblHomeInfoLicenseKey.Text = "License Key";
            // 
            // tmrOneClickScan
            // 
            this.tmrOneClickScan.Interval = 300;
            this.tmrOneClickScan.Tick += new System.EventHandler(this.tmrOneClickScan_Tick);
            // 
            // tmrOneClickFix
            // 
            this.tmrOneClickFix.Tick += new System.EventHandler(this.tmrOneClickFix_Tick);
            // 
            // niMain
            // 
            this.niMain.ContextMenuStrip = this.cmnuTray;
            this.niMain.DoubleClick += new System.EventHandler(this.niMain_DoubleClick);
            // 
            // cmnuTray
            // 
            this.cmnuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.cmnuTray.Name = "cmnuTray";
            this.cmnuTray.Size = new System.Drawing.Size(93, 26);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Win.Properties.Resources.exit_16x16;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // ilOptimizeStartupManager
            // 
            this.ilOptimizeStartupManager.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilOptimizeStartupManager.ImageStream")));
            this.ilOptimizeStartupManager.TransparentColor = System.Drawing.Color.Transparent;
            this.ilOptimizeStartupManager.Images.SetKeyName(0, "User");
            this.ilOptimizeStartupManager.Images.SetKeyName(1, "Users");
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.BackColor = System.Drawing.Color.Transparent;
            this.btnInfo.Caption = "";
            this.btnInfo.CaptionColor = System.Drawing.Color.Black;
            this.btnInfo.CaptionColorActive = System.Drawing.Color.Black;
            this.btnInfo.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnInfo.Image = global::Win.Properties.Resources.info_button_normal;
            this.btnInfo.ImageActive = null;
            this.btnInfo.ImageDisabled = global::Win.Properties.Resources.info_button_normal;
            this.btnInfo.ImageNormal = global::Win.Properties.Resources.info_button_normal;
            this.btnInfo.ImageRollover = global::Win.Properties.Resources.info_button_rollover;
            this.btnInfo.IsActive = false;
            this.btnInfo.Location = new System.Drawing.Point(680, 43);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(34, 27);
            this.btnInfo.TabIndex = 79;
            this.btnInfo.TabStop = false;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.BackColor = System.Drawing.Color.Transparent;
            this.btnHelp.Caption = "";
            this.btnHelp.CaptionColor = System.Drawing.Color.Black;
            this.btnHelp.CaptionColorActive = System.Drawing.Color.Black;
            this.btnHelp.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
            this.btnHelp.Image = global::Win.Properties.Resources.help_button_normal;
            this.btnHelp.ImageActive = null;
            this.btnHelp.ImageDisabled = global::Win.Properties.Resources.help_button_normal;
            this.btnHelp.ImageNormal = global::Win.Properties.Resources.help_button_normal;
            this.btnHelp.ImageRollover = global::Win.Properties.Resources.help_button_rollover;
            this.btnHelp.IsActive = false;
            this.btnHelp.Location = new System.Drawing.Point(714, 43);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(34, 27);
            this.btnHelp.TabIndex = 80;
            this.btnHelp.TabStop = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // cbMain
            // 
            this.cbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMain.BackColor = System.Drawing.Color.Transparent;
            this.cbMain.BackgroundImage = global::Win.Properties.Resources.buttons_normal;
            this.cbMain.Location = new System.Drawing.Point(597, 0);
            this.cbMain.Name = "cbMain";
            this.cbMain.Size = new System.Drawing.Size(105, 32);
            this.cbMain.TabIndex = 81;
            this.cbMain.Maximize += new System.EventHandler(this.cbMain_Maximize);
            this.cbMain.Close += new System.EventHandler(this.cbMain_Close);
            this.cbMain.Minimize += new System.EventHandler(this.cbMain_Minimize);
            // 
            // tabMain
            // 
            this.tabMain.BackColor = System.Drawing.Color.Transparent;
            this.tabMain.Location = new System.Drawing.Point(8, 40);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(746, 33);
            this.tabMain.TabIndex = 82;
            this.tabMain.TabPageDisk = this.pnlDisk;
            this.tabMain.TabPageHome = this.pnlHome;
            this.tabMain.TabPageInfo = this.pnlHomeInfo;
            this.tabMain.TabPageOptimize = this.pnlOptimize;
            this.tabMain.TabPageSelected = this.pnlHome;
            this.tabMain.TabPageSettings = this.pnlSettings;
            this.tabMain.TabHeaderClick += new System.EventHandler(this.tabMain_Click);
            // 
            // pnlNetworkSecurityStatus
            // 
            this.pnlNetworkSecurityStatus.BackColor = System.Drawing.Color.Transparent;
            this.pnlNetworkSecurityStatus.BackgroundImage = global::Win.Properties.Resources.red;
            this.pnlNetworkSecurityStatus.Location = new System.Drawing.Point(640, 41);
            this.pnlNetworkSecurityStatus.Name = "pnlNetworkSecurityStatus";
            this.pnlNetworkSecurityStatus.Size = new System.Drawing.Size(32, 32);
            this.pnlNetworkSecurityStatus.TabIndex = 83;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(187)))), ((int)(((byte)(235)))));
            this.BackgroundImage = global::Win.Properties.Resources.Main_menu_back;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ClientSize = new System.Drawing.Size(760, 500);
            //this.Controls.Add(this.pnlNetworkSecurityStatus);
            this.Controls.Add(this.pnlHome);
            this.Controls.Add(this.pnlOptimize);
            this.Controls.Add(this.pnlDisk);
            this.Controls.Add(this.pnlHomeInfo);
            this.Controls.Add(this.pnlSettings);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.cbMain);
            this.Controls.Add(this.tabMain);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(760, 500);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(187)))), ((int)(((byte)(235)))));
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseUp);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettingsMain.ResumeLayout(false);
            this.pnlSettingsPrivacySettings.ResumeLayout(false);
            this.pnlSettingsGeneralSettings.ResumeLayout(false);
            this.pnlSettingsGeneralSettingsInside.ResumeLayout(false);
            this.pnlSettingsGeneralSettingsInside.PerformLayout();
            this.pnlSettingsScanSchedule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbSettingsSchedule)).EndInit();
            this.pnlSettingsRegistrySettings.ResumeLayout(false);
            this.pnlSettingsRegistrySettingsInside.ResumeLayout(false);
            this.pnlSettingsRegistrySettingsInside.PerformLayout();
            this.pnlDisk.ResumeLayout(false);
            this.pnlDiskMain.ResumeLayout(false);
            this.pnlDiskRestorePointManager.ResumeLayout(false);
            this.pnlDiskRestorePointManager.PerformLayout();
            this.pnlDiskDiskCleaner.ResumeLayout(false);
            this.cmnuDiskDiskCleaner.ResumeLayout(false);
            this.pnlDiskShortcutFixer.ResumeLayout(false);
            this.pnlDiskShortcutFixer.PerformLayout();
            this.cmnuDiskShortcutFixer.ResumeLayout(false);
            this.pnlDiskUninstallManager.ResumeLayout(false);
            this.pnlDiskUninstallManager.PerformLayout();
            this.pnlOptimize.ResumeLayout(false);
            this.pnlOptimizeMain.ResumeLayout(false);
            this.pnlOptimizeStartupManager.ResumeLayout(false);
            this.pnlOptimizeOptimizeWindows.ResumeLayout(false);
            this.pnlOptimizeOptimizeWindowsInside.ResumeLayout(false);
            this.pnlOptimizeOptimizeWindowsInside.PerformLayout();
            this.pnlOptimizeRegistryManager.ResumeLayout(false);
            this.pnlOptimizeRegistryManager.PerformLayout();
            this.cmnuOptimizeRegistryManager.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnOptimizeRegistryManagerFixAll)).EndInit();
            this.pnlOptimizeProcessManager.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabOptimizeProcessManager)).EndInit();
            this.tabOptimizeProcessManager.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeProcessDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOptimizeProcessImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOptimizeProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeMemoryUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOptimizeCpuUsage)).EndInit();
            this.pnlOptimizeBrowseObjectManager.ResumeLayout(false);
            this.pnlOptimizeBrowseObjectManager.PerformLayout();
            this.cmnuOptimizeBrowseObjectManager.ResumeLayout(false);
            this.pnlOptimizeRegistryDefragmenter.ResumeLayout(false);
            this.pnlOptimizeRegistryDefragmenter.PerformLayout();
            this.cmnuOptimizeStartupManager.ResumeLayout(false);
            this.pnlHome.ResumeLayout(false);
            this.pnlHomeMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeStartScan)).EndInit();
            this.pnlHomeStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeStep3FixAll)).EndInit();
            this.pnlHomeStep4Fixing.ResumeLayout(false);
            this.pnlHomeStep4Fixing.PerformLayout();
            this.pnlHomeStep2.ResumeLayout(false);
            this.pnlHomeStep2.PerformLayout();
            this.pnlHomeInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeInfoUpdates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeInfoRegistration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHomeInfoLicenseKeyOk)).EndInit();
            this.cmnuTray.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHelp)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
		private System.Windows.Forms.DataGridViewTextBoxColumn colTargetPath;
		private System.Windows.Forms.DataGridViewTextBoxColumn colDescriptionName;
		private System.Windows.Forms.DataGridViewTextBoxColumn colDriveLetter;
		private System.Windows.Forms.DataGridViewTextBoxColumn colDriveSize;
		private System.Windows.Forms.Timer tmrMain;
		private DoublePanel pnlHome;
		private DoublePanel pnlOptimize;
		private MainPanel pnlOptimizeMain;
		private CommonPanel pnlOptimizeOptimizeWindows;
		private CommonPanel pnlOptimizeStartupManager;
        private CommonPanel pnlOptimizeProcessManager;
        private DevExpress.XtraTab.XtraTabControl tabOptimizeProcessManager;
        private DevExpress.XtraTab.XtraTabPage tpProcesses;
        private DevExpress.XtraTab.XtraTabPage tpPerformance;
		private DevExpress.XtraEditors.GroupControl groupOptimizeCpuUsage;
		private System.Windows.Forms.DataGridView gvOptimizeProcess;
        private DevExpress.XtraEditors.GroupControl groupOptimizeProcessDetails;
        private DevExpress.XtraEditors.LabelControl lblOptimizePriority;
        private DevExpress.XtraEditors.LabelControl lblOptimizeHandles;
        private DevExpress.XtraEditors.LabelControl lblOptimizeThreads;
        private DevExpress.XtraEditors.LabelControl lblOptimizeCompany;
		private DevExpress.XtraEditors.LabelControl lblOptimizeProcessId;
		private DoublePanel pnlDisk;
		private MainPanel pnlDiskMain;
		private CommonPanel pnlDiskRestorePointManager;
		private CommonPanel pnlDiskDiskCleaner;
		private System.Windows.Forms.FolderBrowserDialog fbdDiskFolders;
        private DoublePanel pnlSettings;
		private MainPanel pnlSettingsMain;
		private CommonPanel pnlSettingsScanSchedule;
		private CommonPanel pnlSettingsRegistrySettings;
		private CommonPanel pnlSettingsPrivacySettings;
		private CommonPanel pnlSettingsGeneralSettings;
		private System.Windows.Forms.CheckBox cbSettingsAutoStartFix;
		private System.Windows.Forms.CheckBox cbSettingsCreateRestorePointBeforeFixing;
		private System.Windows.Forms.CheckBox cbSettingsAutoStartScan;
		private System.Windows.Forms.CheckBox cbSettingsAutoStartApp;
		private System.Windows.Forms.CheckBox cbSettingsMinimizeToTray;
		private System.Windows.Forms.CheckBox cbSettingsDisplayExitActions;
		private DevExpress.XtraEditors.ListBoxControl lbSettingsSchedule;

		private Common_Tools.TreeViewAdv.Tree.TreeViewAdv tvaSettingsPrivacySettings;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn14;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn24;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox nodeCheckBox4;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon4;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeSection4;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeProblem4;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeLocation4;

		private CommonPanel pnlOptimizeBrowseObjectManager;
		private System.Windows.Forms.Label lblOptimizeBrowserObjectManagerItemSelected;
        private System.Windows.Forms.Label lblOptimizeImage2Label;
        private System.Windows.Forms.PictureBox pbOptimizeImage2;
        private System.Windows.Forms.Label lblOptimizeImage1Label;
		private System.Windows.Forms.PictureBox pbOptimizeImage1;
        private System.Windows.Forms.Label lblOptimizeImage3Label;
		private System.Windows.Forms.PictureBox pbOptimizeImage3;
		private System.Windows.Forms.PictureBox pbOptimizeProcessImage;
		private MainPanel pnlHomeMain;
		private CommonPanel pnlHomeStep2;
        private DevExpress.XtraEditors.LabelControl lblHomeStep2CurrentScanner;
		private DevExpress.XtraEditors.LabelControl lblHomeStep2CurrentScannedObject;
		private CommonPanel pnlHomeStep3;
		private CommonPanel pnlOptimizeRegistryManager;
        private DevExpress.XtraEditors.LabelControl labelControl3;
		private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblOptimizePriorityValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeHandlesValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeThreadsValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeCompanyValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeProcessIdValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeVirtualMemorySizeValue;
        private DevExpress.XtraEditors.LabelControl lblOptimizeVirtualMemorySize;
        private DevExpress.XtraEditors.LabelControl lblOptimizeTotalProcessorTimeValue;
		private DevExpress.XtraEditors.LabelControl lblOptimizeTotalProcessorTime;
		private Controls.SystemMonitorDataBar smdbOptimizeCpu;
		private Controls.SystemMonitorDataChart smdcOptimizeCpu;
		private System.Windows.Forms.Label lblOptimizeCpuUsage;
		private DevExpress.XtraEditors.GroupControl groupOptimizeMemoryUsage;
		private System.Windows.Forms.Label lblOptimizePMemoryUsage;
		private Controls.SystemMonitorDataBar smdbOptimizePMemory;
		private Controls.SystemMonitorDataChart smdcOptimizeMemory;
		private System.Windows.Forms.Label lblOptimizeVMemoryUsage;
		private Controls.SystemMonitorDataBar smdbOptimizeVMemory;
		private System.Windows.Forms.Label lblOptimizeSelectedProcessCpuUsage;
		private Controls.SystemMonitorDataBar smdbOptimizeSelectedProcessCpu;
		private Controls.SystemMonitorDataChartAdv smdcOptimizeSelectedProcess;
		private System.Windows.Forms.Label lblOptimizeSelectedProcessPMemoryUsage;
		private Controls.SystemMonitorDataBar smdbOptimizeSelectedProcessPMemory;
		private System.Windows.Forms.Label lblOptimizeImage4Label;
		private System.Windows.Forms.PictureBox pbOptimizeImage4;
		private System.Windows.Forms.ImageList imageListTreeView;
		private System.Windows.Forms.ContextMenuStrip cmnuOptimizeRegistryManager;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerSelectAll;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerSelectNone;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerInvertSelection;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerExcludeSelected;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerViewinRegEdit;
		private System.Windows.Forms.TreeView treeView1;
		private Common_Tools.TreeViewAdv.Tree.TreeViewAdv tvaOptimizeRegistryManager;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn1;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn2;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox nodeCheckBox;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeSection;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeProblem;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeLocation;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeValueName;
		private Common_Tools.TreeViewAdv.Tree.TreeViewAdv tvaHomeStep2Problem;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn12;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn22;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn32;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox nodeCheckBox2;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon2;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeSection2;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeProblem2;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeLocation2;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeValueName2;
		private Common_Tools.TreeViewAdv.Tree.TreeViewAdv tvaHomeStep3Problem;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn13;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn23;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn33;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox nodeCheckBox3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeSection3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeProblem3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeLocation3;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeValueName3;
		private DevExpress.XtraEditors.LabelControl lblHomeStep2ProgressValue;
		private DevExpress.XtraEditors.LabelControl lblHomeStep2ProgressTitle;
		private System.Windows.Forms.ProgressBar pbHomeStep2Progress;
		private System.Windows.Forms.Timer tmrOneClickScan;
		private Controls.OverallScanGridControl osgHomeStep3OverallScanResult;
		private DoublePanel pnlHomeStep4Fixing;
		private System.Windows.Forms.ProgressBar pbHomeStep4Progress;
		private DevExpress.XtraEditors.LabelControl lblHomeStep4ProgressValue;
		private DevExpress.XtraEditors.LabelControl lblHomeStep4ProgressTitle;
		private DevExpress.XtraEditors.LabelControl lblHomeStep4CurrentFixedObject;
		private DevExpress.XtraEditors.LabelControl lblHomeStep4CurrentFixer;
		private System.Windows.Forms.Timer tmrOneClickFix;
		private CommonPanel pnlHomeInfo;
		private DevExpress.XtraEditors.GroupControl groupHomeInfoUpdates;
		private DevExpress.XtraEditors.GroupControl groupHomeInfoRegistration;
		private DevExpress.XtraEditors.TextEdit txtHomeInfoLicenseKey;
		private DevExpress.XtraEditors.LabelControl lblHomeInfoLicenseKey;
		private DevExpress.XtraEditors.LabelControl lblHomeInfoCheckForUpdatesLatDateTime;
		private DevExpress.XtraEditors.LabelControl lblHomeInfoLicenseStatusValue;
		private DevExpress.XtraEditors.LabelControl lblHomeInfoLicenseStatusTitle;
		private DevExpress.XtraEditors.LabelControl lblHomeIntoCheckForUpdatesProgress;
		private DevExpress.XtraEditors.LabelControl lblHomeIntoCheckForUpdatesTotalSize;
		private System.Windows.Forms.ProgressBar pbHomeIntoCheckForUpdatesProgress;
		private System.Windows.Forms.CheckBox cbSettingsHistoryList;
		private System.Windows.Forms.CheckBox cbSettingsSharedDLLs;
		private System.Windows.Forms.CheckBox cbSettingsHelpFiles;
		private System.Windows.Forms.CheckBox cbSettingsSoundEvents;
		private System.Windows.Forms.CheckBox cbSettingsApplicationInfo;
		private System.Windows.Forms.CheckBox cbSettingsActiveXCOM;
		private System.Windows.Forms.CheckBox cbSettingsStartup;
		private System.Windows.Forms.CheckBox cbSettingsSystemDrivers;
		private System.Windows.Forms.CheckBox cbSettingsSoftwareSettings;
		private System.Windows.Forms.CheckBox cbSettingsProgramLocations;
		private System.Windows.Forms.CheckBox cbSettingsWindowsFonts;
		private System.Windows.Forms.NotifyIcon niMain;
		private System.Windows.Forms.ContextMenuStrip cmnuTray;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private CommonPanel pnlDiskShortcutFixer;
		private System.Windows.Forms.ListView lvDiskShortcutFixerFiles;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ImageList ilDiskShortcutFixer;
		private DevExpress.XtraEditors.LabelControl lblDiskShortcutFixerStatus;
		private System.Windows.Forms.ContextMenuStrip cmnuDiskShortcutFixer;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerSelectAll;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerSelectNone;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerInvertSelection;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerDelete;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerChangeTarget;
		private System.Windows.Forms.ToolStripSeparator tsmiDiskShortcutFixerSeparator1;
		private System.Windows.Forms.ToolStripSeparator tsmiDiskShortcutFixerSeparator2;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerProperties;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskShortcutFixerOpenFolder;
		private System.Windows.Forms.FolderBrowserDialog fbdDiskShortcutFixer;
		private System.Windows.Forms.OpenFileDialog ofdDiskShortcutFixer;
		private System.Windows.Forms.ListView lvOptimizeBrowseObjectManager;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ImageList ilOptimizeBrowseObjectManager;
		private System.Windows.Forms.ContextMenuStrip cmnuOptimizeBrowseObjectManager;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeBrowseObjectManagerSelectAll;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeBrowseObjectManagerSelectNone;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeBrowseObjectManagerInvertSelection;
		private System.Windows.Forms.ToolStripSeparator tsmiOptimizeBrowseObjectManagerSeparator;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeBrowseObjectManagerProperties;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private Common_Tools.TreeViewAdv.Tree.TreeViewAdv tvaOptimizeStartupManager;
		private System.Windows.Forms.ImageList ilOptimizeStartupManager;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn15;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn25;
		private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn35;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon15;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxSection5;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxItem5;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxPath5;
		private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxArgs5;
		private System.Windows.Forms.ContextMenuStrip cmnuOptimizeStartupManager;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeStartupManagerEdit;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeStartupManagerDelete;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeStartupManagerView;
		private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeStartupManagerRun;
		private CommonPanel pnlDiskUninstallManager;
		private System.Windows.Forms.ListView lvDiskUninstallManager;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerProgram;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerPublisher;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerEstimatedSize;
		private System.Windows.Forms.TextBox txtDiskUninstallManagerSearchFilter;
		private System.Windows.Forms.ImageList ilDiskUninstallManager;
		private PictureBoxButton btnHomeInfoLicenseKeyOk;
		private System.Windows.Forms.ListView lvDiskDiskCleaner;
		private System.Windows.Forms.ColumnHeader columnHeader15;
		private System.Windows.Forms.ColumnHeader columnHeader16;
		private System.Windows.Forms.ColumnHeader columnHeader17;
		private Win.DiskCleaner.Misc.FileInfoCtrl fileInfoCtrl1;
		private DevExpress.XtraEditors.CheckEdit cbHomeInfoAutocheckForUpdates;
		private System.Windows.Forms.ContextMenuStrip cmnuDiskDiskCleaner;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskDiskCleanerSelectAll;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskDiskCleanerSelectNone;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskDiskCleanerInvertSelection;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskDiskCleanerProperties;
		private System.Windows.Forms.ToolStripMenuItem tsmiDiskDiskCleanerOpenFile;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerInstallLocation;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerVersion;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerContact;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerHelpLink;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerInstallSource;
		private System.Windows.Forms.ColumnHeader colDiskUninstallManagerUrlInfoAbout;
		private System.Windows.Forms.ListView lvDiskRestorePoint;
		private System.Windows.Forms.ColumnHeader colDiskRestorePointNumber;
		private System.Windows.Forms.ColumnHeader colDiskRestorePointDateTime;
		private System.Windows.Forms.ColumnHeader colDiskRestorePointDescription;
		private System.Windows.Forms.ColumnHeader colDiskRestorePointType;
		private System.Windows.Forms.Label lblDistRestorePointDescription;
		private DevExpress.XtraEditors.TextEdit txtDistRestorePointDescription;
        private System.Windows.Forms.CheckBox cbOptimizeClearPageFileOnReboot;
        private System.Windows.Forms.CheckBox cbOptimizeEnableDesktopSearch;
        private System.Windows.Forms.CheckBox cbOptimizeEnablePrefetch;
        private System.Windows.Forms.CheckBox cbOptimizeSendErrorsToMicrosoft;
		private System.Windows.Forms.CheckBox cbOptimizeAllowHybernation;
        private System.Windows.Forms.DataGridViewImageColumn col_OptimizeWindow_Process_InfoTypeImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_OptimizeWindow_Process_InfoType;
        private System.Windows.Forms.DataGridViewImageColumn colIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_OptimizeWindow_Process_ProcessName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCpu;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMemory;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSigned;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPath;
		private PictureBoxButton btnHomeStartScan;
		private PictureBoxButton btnInfo;
		private PictureBoxButton btnHelp;
		private PictureBoxButton btnHomeStep3FixAll;
		private DoublePanel pnlOptimizeOptimizeWindowsInside;
		private PictureBoxButton btnOptimizeRegistryManagerFixAll;
		private DoublePanel pnlSettingsGeneralSettingsInside;
		private DoublePanel pnlSettingsRegistrySettingsInside;
		private MonolitForm.ControlBoxControl cbMain;
		private MainButton btnHomeDiskCleaner;
		private MainButton btnOptimizeProcessManager;
		private MainButton btnOptimizeRegistryManager;
		private MainButton btnOptimizeStartupManager;
		private MainButton btnOptimizeOptimizeWindows;
		private MainButton btnOptimizeBrowserObjectManager;
		private MainButton btnDiskShortcutFixer;
		private MainButton btnDiskDiskCleaner;
		private MainButton btnDiskRestorePointManager;
		private MainButton btnDiskClearTempraryFiles;
		private MainButton btnDiskClearRecentHistory;
		private MainButton btnDiskUninstallManager;
		private MainButton btnSettingsGeneralSettings;
		private MainButton btnSettingsScanSchedule;
		private MainButton btnSettingsRegistrySettings;
		private MainButton btnSettingsPrivacySettings;
		private DetectedItemsLabel lblHomeStep3DetectedItems;
		private DetectedItemsLabel lblHomeStep2DetectedItems;
		private System.Windows.Forms.Label lblHomeInfoVersion;
		private RegularButton btnHomeIntoCheckForUpdatesCancel;
		private RegularButton btnHomeIntoBuyNow;
		private RegularButton btnHomeIntoCheckForUpdates;
		private RegularButton btnHomeStep3Cancel;
		private RegularButton btnHomeStep2Cancel;
		private RegularButton btnHomeStep2Stop;
		private RegularButton btnOptimizeOptimizeAll;
		private RegularButton btnOptimizeProperties;
		private RegularButton btnOptimizeOpenFolder;
		private RegularButton btnOptimizeEndProcess;
		private RegularButton btnOptimizeBrowseObjectManagerEnableAll;
		private RegularButton btnOptimizeBrowseObjectManagerDisableAll;
		private RegularButton btnOptimizeStartupManagerAdd;
		private RegularButton btnOptimizeStartupManagerEdit;
		private RegularButton btnOptimizeStartupManagerRefresh;
		private RegularButton btnOptimizeStartupManagerRun;
		private RegularButton btnOptimizeStartupManagerView;
		private RegularButton btnOptimizeStartupManagerDelete;
		private RegularButton btnOptimizeRegistryManagerScan;
		private RegularButton btnOptimizeRegistryManagerIgnoreList;
		private RegularButton btnDiskRemoveRestorePoint;
		private RegularButton btnDiskRestoreRestorePoint;
		private RegularButton btnDiskCreateRestorePoint;
		private RegularButton btnDiskDiskCleanerOptions;
		private RegularButton btnDiskDiskCleanerProperties;
		private RegularButton btnDiskDiskCleanerOpenFile;
		private RegularButton btnDiskDiskCleanerCleanFiles;
		private RegularButton btnDiskDiskCleanerScanDisks;
		private RegularButton btnDiskShortcutFixerDeleteSelected;
		private RegularButton btnDiskShortcutFixerScan;
		private RegularButton btnDiskUninstallManagerUninstall;
		private RegularButton btnDiskUninstallManagerRemoveEntry;
		private RegularButton btnSettingsScheduleDelete;
		private RegularButton btnSettingsScheduleEdit;
		private RegularButton btnSettingsScheduleNew;
		private RegularButton btnSettingsPrivacySettingsCancel;
		private RegularButton btnSettingsPrivacySettingsApply;
		private TabUserControl tabMain;
		private ScanningUserControl sucHomeStep2Registry;
		private ScanningUserControl sucHomeStep2Privacy;
		private ScanningUserControl sucHomeStep2Process;
		private DevExpress.XtraEditors.LabelControl lblDiskUninstallManagerSearchFilter;
        //private System.Windows.Forms.Label btnHomeTitle3;
        //private System.Windows.Forms.Label btnHomeTitle2;
        //private System.Windows.Forms.Label btnHomeTitle1;
		private MainButton btnHomeRegistryManager;
		private MainButton btnHomeStartupManager;
		private MainButton btnHomeClearTempraryFiles;
		private MainButton btnOptimizeRegistryDefragmenter;
		private CommonPanel pnlOptimizeRegistryDefragmenter;
		private System.Windows.Forms.ListView lvOptimizeRegistryDefragmenter;
		private System.Windows.Forms.ColumnHeader colOptimizeRegistryDefragmenterHivePath;
		private System.Windows.Forms.ColumnHeader colOptimizeRegistryDefragmenterOldHiveSize;
		private System.Windows.Forms.ColumnHeader colOptimizeRegistryDefragmenterNewHiveSize;
		private RegularButton btnOptimizeRegistryDefragmenterOptimize;
		private System.Windows.Forms.Label lblOptimizeRegistryDefragmenterStatus;
		private RegularButton btnOptimizeRegistryDefragmenterAnalyze;
		private System.Windows.Forms.ProgressBar pbOptimizeRegistryDefragmenter;
		private System.Windows.Forms.Label lblOptimizeRegistryDefragmenterTotalNewHiveSize;
		private System.Windows.Forms.Label lblOptimizeRegistryDefragmenterTotalOldHiveSize;
		private System.Windows.Forms.Label lblOptimizeRegistryDefragmenterTotal;
		private DoublePanel pnlNetworkSecurityStatus;
	}
}