﻿using System;
using System.Collections.Generic;
using Common;
using Microsoft.Win32;

namespace Win.Scanners
{
	public class ApplicationPaths : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.ApplicationPaths; }
		}

		/// <summary>
		/// Verifies programs in App Paths
		/// </summary>
		public static void Scan()
		{
			try
			{

				//////Main.Logger.WriteLine("Checking for invalid installer folders");
				ScanInstallFolders();

				//////Main.Logger.WriteLine("Checking for invalid application paths");
				ScanAppPaths();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		private static void ScanInstallFolders()
		{
			try
			{
				RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\Folders");

				if (regKey == null)
					return;

				foreach (string strFolder in regKey.GetValueNames())
				{
					ScanDlg.CurrentScannedObject = strFolder;

					if (!RegistryHelper.DirExists(strFolder))
						ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey.Name, strFolder);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		private static void ScanAppPaths()
		{
			try
			{
				RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\App Paths");

				if (regKey == null)
					return;

				foreach (string strSubKey in regKey.GetSubKeyNames())
				{
					RegistryKey regKey2 = regKey.OpenSubKey(strSubKey);

					if (regKey2 != null)
					{
						ScanDlg.CurrentScannedObject = regKey2.ToString();

						if (Convert.ToInt32(regKey2.GetValue("BlockOnTSNonInstallMode")) == 1)
							continue;

						string strAppPath = regKey2.GetValue("") as string;
						string strAppDir = regKey2.GetValue("Path") as string;

						if (string.IsNullOrEmpty(strAppPath))
						{
							ScanDlg.StoreInvalidKey(Strings.InvalidRegKey, regKey2.ToString());
							continue;
						}

						if (!string.IsNullOrEmpty(strAppDir))
						{
							if (RegistryHelper.SearchPath(strAppPath, strAppDir))
								continue;
							else if (RegistryHelper.SearchPath(strSubKey, strAppDir))
								continue;
						}
						else
						{
							if (RegistryHelper.FileExists(strAppPath))
								continue;
							else if (RegistryHelper.FileExists(strAppPath))
								continue;
						}

						ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey2.Name);
					}
				}

				regKey.Close();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
	}
}
