﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace Win.Scanners
{
	public class RecentDocs : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.RecentDocs; }
		}

		public static void Scan()
		{
			ScanMUICache();
			ScanExplorerDocs();
		}

		/// <summary>
		/// Checks MUI Cache for invalid file references (XP Only)
		/// </summary>
		private static void ScanMUICache()
		{
			try
			{
				using (RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\ShellNoRoam\MUICache"))
				{
					if (regKey == null)
						return;

					foreach (string valueName in regKey.GetValueNames())
					{
						if (valueName.StartsWith("@") || valueName == "LangID")
							continue;

						ScanDlg.CurrentScannedObject = valueName;

						if (!RegistryHelper.FileExists(valueName))
							ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey.Name, valueName);
					}
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Recurses through the recent documents registry keys for invalid files
		/// </summary>
		private static void ScanExplorerDocs()
		{
			try
			{
				string strRecentDocs = Environment.GetFolderPath(Environment.SpecialFolder.Recent);

				using (RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RecentDocs"))
				{
					if (regKey == null)
						return;

					//////Main.Logger.WriteLine("Cleaning invalid references in " + regKey.Name);

					foreach (string strSubKey in regKey.GetSubKeyNames())
					{
						RegistryKey subKey = regKey.OpenSubKey(strSubKey);

						if (subKey == null)
							continue;

						foreach (string strValueName in subKey.GetValueNames())
						{
							// Ignore MRUListEx and others
							if (!Regex.IsMatch(strValueName, "[0-9]"))
								continue;

							string strFileName = ExtractUnicodeStringFromBinary(subKey.GetValue(strValueName));

							ScanDlg.CurrentScannedObject = strFileName;

							// See if file exists in Recent Docs folder
							if (!string.IsNullOrEmpty(strFileName))
								if (!RegistryHelper.FileExists(string.Format("{0}\\{1}.lnk", strRecentDocs, strFileName)))
									ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey.ToString(), strValueName);
						}
					}
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Converts registry value to filename
		/// </summary>
		/// <param name="keyObj">Value from registry key</param>
		private static string ExtractUnicodeStringFromBinary(object keyObj)
		{
			string Value = keyObj.ToString();    //get object value 
			string strType = keyObj.GetType().Name;  //get object type

			if (strType == "Byte[]")
			{
				Value = "";
				byte[] Bytes = (byte[])keyObj;
				//this seems crude but cannot find a way to 'cast' a Unicode string to byte[]
				//even in case where we know the beginning format is Unicode
				//so do it the hard way

				char[] chars = Encoding.Unicode.GetChars(Bytes);
				foreach (char bt in chars)
				{
					if (bt != 0)
					{
						Value = Value + bt; //construct string one at a time
					}
					else
					{
						break;  //apparently found 0,0 (end of string)
					}
				}
			}
			return Value;
		}
	}
}
