﻿using System;
using System.Collections.Generic;
using Common;
using Microsoft.Win32;

namespace Win.Scanners
{
	public class ApplicationInfo : ScannerBase
	{
		public override string ScannerName
		{
			get { return Strings.ApplicationInfo; }
		}

		/// <summary>
		/// Verifies installed programs in add/remove list
		/// </summary>
		public static void Scan()
		{
			try
			{
				//////Main.Logger.WriteLine("Verifying programs in Add/Remove list");

				using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"))
				{
					if (regKey == null)
						return;

					foreach (string strProgName in regKey.GetSubKeyNames())
					{
						using (RegistryKey regKey2 = regKey.OpenSubKey(strProgName))
						{
							if (regKey2 != null)
							{
								ScanDlg.CurrentScannedObject = regKey2.ToString();

								UninstallManager.ProgramInfo progInfo = new Win.UninstallManager.ProgramInfo(regKey2);

								if (regKey2.ValueCount <= 0)
								{
									ScanDlg.StoreInvalidKey(Strings.InvalidRegKey, regKey2.ToString());
									continue;
								}

								if (progInfo.WindowsInstaller)
									continue;

								if (string.IsNullOrEmpty(progInfo.DisplayName) && (!progInfo.Uninstallable))
								{
									ScanDlg.StoreInvalidKey(Strings.InvalidRegKey, regKey2.ToString());
									continue;
								}

								// Check display icon
								if (!string.IsNullOrEmpty(progInfo.DisplayIcon))
									if (!RegistryHelper.IconExists(progInfo.DisplayIcon))
										ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey2.ToString(), "DisplayIcon");

								// Check install location 
								if (!string.IsNullOrEmpty(progInfo.InstallLocation))
									if ((!RegistryHelper.DirExists(progInfo.InstallLocation)) && (!RegistryHelper.FileExists(progInfo.InstallLocation)))
										ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey2.ToString(), "InstallLocation");

								// Check install source 
								if (!string.IsNullOrEmpty(progInfo.InstallSource))
									if ((!RegistryHelper.DirExists(progInfo.InstallSource)) && (!RegistryHelper.FileExists(progInfo.InstallSource)))
										ScanDlg.StoreInvalidKey(Strings.InvalidFile, regKey2.ToString(), "InstallSource");

								// Check ARP Cache
								if (progInfo.SlowCache)
								{
									if (!string.IsNullOrEmpty(progInfo.FileName))
										if (!RegistryHelper.FileExists(progInfo.FileName))
											ScanDlg.StoreInvalidKey(Strings.InvalidRegKey, progInfo.SlowInfoCacheRegKey);
								}
							}
						}
					}
				}

				//////Main.Logger.WriteLine("Verifying registry entries in Add/Remove Cache");

				checkARPCache(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\App Management\ARPCache\"));
				checkARPCache(Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\App Management\ARPCache\"));
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Do a cross-reference check on ARP Cache keys
		/// </summary>
		/// <param name="regKey"></param>
		private static void checkARPCache(RegistryKey regKey)
		{
			try
			{
				if (regKey == null)
					return;

				foreach (string subKey in regKey.GetSubKeyNames())
				{
					if (Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + subKey) == null)
						ScanDlg.StoreInvalidKey(Strings.ObsoleteRegKey, string.Format("{0}/{1}", regKey.Name, subKey));
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
	}
}
