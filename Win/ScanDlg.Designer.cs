﻿namespace Win
{
    partial class ScanDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanDlg));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblScanning = new System.Windows.Forms.Label();
			this.btnStop = new Win.Controls.RegularButton();
			this.lblProblems = new System.Windows.Forms.Label();
			this.textBoxSubKey = new System.Windows.Forms.TextBox();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.timerUpdate = new System.Windows.Forms.Timer(this.components);
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblScanning);
			this.groupBox1.Controls.Add(this.btnStop);
			this.groupBox1.Controls.Add(this.lblProblems);
			this.groupBox1.Controls.Add(this.textBoxSubKey);
			this.groupBox1.Controls.Add(this.progressBar);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// lblScanning
			// 
			resources.ApplyResources(this.lblScanning, "lblScanning");
			this.lblScanning.Name = "lblScanning";
			// 
			// btnStop
			// 
			resources.ApplyResources(this.btnStop, "btnStop");
			this.btnStop.BackColor = System.Drawing.Color.Transparent;
			this.btnStop.ForeColor = System.Drawing.Color.White;
			this.btnStop.Name = "btnStop";
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// lblProblems
			// 
			resources.ApplyResources(this.lblProblems, "lblProblems");
			this.lblProblems.Name = "lblProblems";
			// 
			// textBoxSubKey
			// 
			resources.ApplyResources(this.textBoxSubKey, "textBoxSubKey");
			this.textBoxSubKey.Name = "textBoxSubKey";
			this.textBoxSubKey.ReadOnly = true;
			// 
			// progressBar
			// 
			resources.ApplyResources(this.progressBar, "progressBar");
			this.progressBar.Name = "progressBar";
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "ActiveXComObjects");
			this.imageList.Images.SetKeyName(1, "ApplicationInfo");
			this.imageList.Images.SetKeyName(2, "SystemDrivers");
			this.imageList.Images.SetKeyName(3, "WindowsFonts");
			this.imageList.Images.SetKeyName(4, "WindowsHelpFiles");
			this.imageList.Images.SetKeyName(5, "RecentDocs");
			this.imageList.Images.SetKeyName(6, "ApplicationPaths");
			this.imageList.Images.SetKeyName(7, "SharedDLLs");
			this.imageList.Images.SetKeyName(8, "ApplicationSettings");
			this.imageList.Images.SetKeyName(9, "WindowsSounds");
			this.imageList.Images.SetKeyName(10, "StartupFiles");
			// 
			// timerUpdate
			// 
			this.timerUpdate.Enabled = true;
			this.timerUpdate.Interval = 10;
			this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
			// 
			// ScanDlg
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.Controls.Add(this.groupBox1);
			this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScanDlg";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScanDlg_FormClosing);
			this.Shown += new System.EventHandler(this.ScanDlg_Shown);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBoxSubKey;
		private System.Windows.Forms.Label lblProblems;
        private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.Timer timerUpdate;
		private Controls.RegularButton btnStop;
		private System.Windows.Forms.Label lblScanning;
		private System.Windows.Forms.ProgressBar progressBar;
    }
}