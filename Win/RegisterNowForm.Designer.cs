﻿namespace Win
{
	partial class RegisterNowForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbLogo = new System.Windows.Forms.PictureBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.btnRegisterNow = new Win.Controls.PictureBoxButton();
			this.lblDescription = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnRegisterNow)).BeginInit();
			this.SuspendLayout();
			// 
			// pbLogo
			// 
			this.pbLogo.BackColor = System.Drawing.Color.Transparent;
			this.pbLogo.BackgroundImage = global::Win.Properties.Resources.need_register_icon;
			this.pbLogo.Location = new System.Drawing.Point(12, 12);
			this.pbLogo.Name = "pbLogo";
			this.pbLogo.Size = new System.Drawing.Size(65, 65);
			this.pbLogo.TabIndex = 1;
			this.pbLogo.TabStop = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTitle.BackColor = System.Drawing.Color.Transparent;
			this.lblTitle.Font = new System.Drawing.Font("Arial", 14.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.lblTitle.Location = new System.Drawing.Point(93, 12);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(341, 63);
			this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Knitrix PC Optimizer has determined that your PC needs to be optimized.";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnRegisterNow
			// 
			this.btnRegisterNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnRegisterNow.BackColor = System.Drawing.Color.Transparent;
			this.btnRegisterNow.Caption = "";
			this.btnRegisterNow.CaptionColor = System.Drawing.Color.Black;
			this.btnRegisterNow.CaptionColorActive = System.Drawing.Color.Black;
			this.btnRegisterNow.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
			this.btnRegisterNow.Image = global::Win.Properties.Resources.register_now_normal1;
			this.btnRegisterNow.ImageActive = null;
			this.btnRegisterNow.ImageDisabled = global::Win.Properties.Resources.register_now_inactive1;
			this.btnRegisterNow.ImageNormal = global::Win.Properties.Resources.register_now_normal1;
			this.btnRegisterNow.ImageRollover = global::Win.Properties.Resources.register_now_rollover1;
			this.btnRegisterNow.IsActive = false;
			this.btnRegisterNow.Location = new System.Drawing.Point(36, 204);
			this.btnRegisterNow.Name = "btnRegisterNow";
			this.btnRegisterNow.Size = new System.Drawing.Size(175, 39);
			this.btnRegisterNow.TabIndex = 3;
			this.btnRegisterNow.TabStop = false;
			this.btnRegisterNow.Click += new System.EventHandler(this.btnRegisterNow_Click);
			// 
			// lblDescription
			// 
			this.lblDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblDescription.Font = new System.Drawing.Font("Arial", 11.25F);
			this.lblDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.lblDescription.Location = new System.Drawing.Point(94, 75);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(340, 126);
			this.lblDescription.TabIndex = 6;
			this.lblDescription.Text = "To fix the XXX selected items, you will need to register the software.";
			// 
			// RegisterNowForm
			// 
			this.Appearance.BackColor = System.Drawing.Color.White;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
			this.BackgroundImageStore = global::Win.Properties.Resources.need_register_back;
			this.ClientSize = new System.Drawing.Size(450, 281);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.btnRegisterNow);
			this.Controls.Add(this.pbLogo);
			this.Controls.Add(this.lblDescription);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RegisterNowForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Registration Required";
			this.Load += new System.EventHandler(this.RegisterNowForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnRegisterNow)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.PictureBox pbLogo;
		private Controls.PictureBoxButton btnRegisterNow;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label lblDescription;
	}
}