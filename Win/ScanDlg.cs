﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using Common;
using Common_Tools.TreeViewAdv.Tree;
using Data;
using Data.Registry;
using Win.Misc;
using Win.Scanners;
using Microsoft.Win32;
using Common.Xml;
using System.Security.Permissions;
using Win.Scanners;

namespace Win
{
	public partial class ScanDlg : Form
	{
		public delegate void ScanDelegate();

		private Thread threadMain;//, threadScan;

		private static string _currentObject = string.Empty;
		private static int _objectsScanned = 0;

		public static string CurrentScannedObject
		{
			get
			{
				return _currentObject;
			}
			set
			{
				ScanDlg._currentObject = RegistryHelper.PrefixRegPath(value);
				ScanDlg._objectsScanned++;
			}
		}

		public static string CurrentSection
		{
			get;
			set;
		}

		private static ScannerBase currentScanner;

		public static BadRegKeyArray arrBadRegistryKeys = new BadRegKeyArray();

		public ScanDlg(int nSectionCount)
		{
			InitializeComponent();

			Icon = Properties.Resources.logo2;
			// Set the section count so it can be accessed later
			ScanDlg.arrBadRegistryKeys.Clear(nSectionCount);
		}


		private void ScanDlg_Shown(object sender, EventArgs e)
		{
			// Setup progress bar
			this.progressBar.Value = 0;
			this.progressBar.Minimum = 0;
			this.progressBar.Maximum = ScanDlg.arrBadRegistryKeys.SectionCount;

			// Append 0 to problem label
			this.lblProblems.Text = string.Format("{0} 0", this.lblProblems.Text);

			// Starts scanning registry on seperate thread
			this.threadMain = new Thread(new ThreadStart(StartScanning));
			this.threadMain.Start();
		}

		/// <summary>
		/// Begins scanning for errors in the registry
		/// </summary>
		private void StartScanning()
		{
			// Get start time of scan
			DateTime dateTimeStart = DateTime.Now;

			// Begin Critical Region
			Thread.BeginCriticalRegion();

			// Begin scanning
			try
			{
				////Main.Logger.WriteLine("Started scan at " + DateTime.Now.ToString());
				////Main.Logger.WriteLine();

				if (DataContext.RegistryScanSettings.ApplicationInfo)
					this.StartScanner(new ApplicationInfo());

				if (DataContext.RegistryScanSettings.ProgramLocations)
					this.StartScanner(new ApplicationPaths());

				if (DataContext.RegistryScanSettings.SoftwareSettings)
					this.StartScanner(new ApplicationSettings());

				if (DataContext.RegistryScanSettings.Startup)
					this.StartScanner(new StartupFiles());

				if (DataContext.RegistryScanSettings.SystemDrivers)
					this.StartScanner(new SystemDrivers());

				if (DataContext.RegistryScanSettings.SharedDLLs)
					this.StartScanner(new SharedDLLs());

				if (DataContext.RegistryScanSettings.HelpFiles)
					this.StartScanner(new WindowsHelpFiles());

				if (DataContext.RegistryScanSettings.SoundEvents)
					this.StartScanner(new WindowsSounds());

				if (DataContext.RegistryScanSettings.HistoryList)
					this.StartScanner(new RecentDocs());

				if (DataContext.RegistryScanSettings.WindowsFonts)
					this.StartScanner(new WindowsFonts());

				if (DataContext.RegistryScanSettings.ActiveXCOM)
					this.StartScanner(new ActivexComObjects());

				this.DialogResult = DialogResult.OK;
			}
			catch (ThreadAbortException tae)
			{
				CLogger.WriteLog(ELogLevel.WARN, tae.ToString());
				//if (this.threadScan.IsAlive)
				//    this.threadScan.Abort();

				this.DialogResult = DialogResult.Abort;
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			finally
			{
				TimeSpan ts = DateTime.Now.Subtract(dateTimeStart);
			}

			// End Critical Region
			Thread.EndCriticalRegion();

			// Dialog will be closed automatically
		}

		/// <summary>
		/// Starts a scanner
		/// </summary>
		public void StartScanner(ScannerBase scannerName)
		{
			currentScanner = scannerName;

			System.Reflection.MethodInfo mi = scannerName.GetType().GetMethod("Scan", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			ScanDelegate objScan = (ScanDelegate)Delegate.CreateDelegate(typeof(ScanDelegate), mi);

			////Main.Logger.WriteLine("Starting scanning: " + scannerName.ScannerName);

			// Update section name
			scannerName.RootNode.SectionName = scannerName.ScannerName;
			scannerName.RootNode.Img = this.imageList.Images[scannerName.GetType().Name];
			ScanDlg.CurrentSection = scannerName.ScannerName;

			//// Start scanning
			//this.threadScan = new Thread(new ThreadStart(objScan));
			//this.threadScan.Start();
			//this.threadScan.Join();

			try
			{
				// Start scanning
				objScan.Invoke();
			}
			finally
			{
				// Wait 50ms
				Thread.Sleep(50);
				if (scannerName.RootNode.Nodes.Count > 0)
				{
					ScanDlg.arrBadRegistryKeys.Add(scannerName.RootNode);
				}
			}

			////Main.Logger.WriteLine("Finished scanning: " + scannerName.ScannerName);
			////Main.Logger.WriteLine();

			this.progressBar.Value ++;
		}

		/// <summary>
		/// <para>Stores an invalid registry key to array list</para>
		/// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
		/// </summary>
		/// <param name="Problem">Reason its invalid</param>
		/// <param name="Path">The path to registry key (including registry hive)</param>
		/// <returns>True if it was added</returns>
		public static bool StoreInvalidKey(string Problem, string Path)
		{
			return StoreInvalidKey(Problem, Path, "");
		}

		/// <summary>
		/// <para>Stores an invalid registry key to array list</para>
		/// <para>Use IsOnIgnoreList to check for ignored registry keys and paths</para>
		/// </summary>
		/// <param name="problem">Reason its invalid</param>
		/// <param name="regPath">The path to registry key (including registry hive)</param>
		/// <param name="valueName">Value name (leave blank if theres none)</param>
		/// <returns>True if it was added. Otherwise, false.</returns>
		public static bool StoreInvalidKey(string problem, string regPath, string valueName)
		{
			string baseKey, subKey;

			// Check for null parameters
			if (string.IsNullOrEmpty(problem) || string.IsNullOrEmpty(regPath))
				return false;

			// Make sure registry key exists
			if (!RegistryHelper.RegKeyExists(regPath))
				return false;

			// Parse registry key to base and subkey
			if (!RegistryHelper.ParseRegKeyPath(regPath, out baseKey, out subKey))
				return false;

			// Check for ignored registry path
			if (RegistryHelper.IsOnIgnoreList(regPath))
				return false;

			// If value name is specified, see if it exists
			if (!string.IsNullOrEmpty(valueName))
				if (!RegistryHelper.ValueNameExists(baseKey, subKey, valueName))
					return false;

			try
			{
				// Throws exception if user doesnt have permission
				RegistryPermission regPermission = new RegistryPermission(RegistryPermissionAccess.AllAccess, regPath);
				regPermission.Demand();
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				return false;
			}

			ScanDlg.currentScanner.RootNode.Nodes.Add(new BadRegistryKey(problem, baseKey, subKey, valueName));

			ScanDlg.arrBadRegistryKeys.Problems++;

			//if (!string.IsNullOrEmpty(valueName))
			////Main.Logger.WriteLine(string.Format("Bad Registry Value Found! Problem: \"{0}\" Path: \"{1}\" Value Name: \"{2}\"", problem, regPath, valueName));
			//else
			////Main.Logger.WriteLine(string.Format("Bad Registry Key Found! Problem: \"{0}\" Path: \"{1}\"", problem, regPath));

			return true;
		}

		private void ScanDlg_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.DialogResult != DialogResult.OK)
			{
				if (e.CloseReason == CloseReason.UserClosing)
				{
					if (MessageBox.Show(this, Properties.Resources.scanDlgExit, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						e.Cancel = true;
					else
						this.threadMain.Abort();
				}
			}
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Abort;
			this.Close();
		}

		private void timerUpdate_Tick(object sender, EventArgs e)
		{
			System.Resources.ResourceManager rm = new System.Resources.ResourceManager(typeof(ScanDlg));

			this.lblScanning.Text = Properties.Resources.scanDlgProgressBarScanning + ScanDlg.CurrentSection;
			this.lblProblems.Text = string.Format("{0} {1}", rm.GetString("lblProblems.Text"), ScanDlg.arrBadRegistryKeys.Problems);
			this.textBoxSubKey.Text = ScanDlg.CurrentScannedObject;
		}
	}
}
