﻿namespace Win.Controls
{
	partial class OverallScanRowControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbImage = new System.Windows.Forms.PictureBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblDetectedCount = new System.Windows.Forms.Label();
			this.lblSelectedCount = new System.Windows.Forms.Label();
			this.lblEdit = new System.Windows.Forms.Label();
			this.btnAction = new Win.Controls.ScanResultButton();
			((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
			this.SuspendLayout();
			// 
			// pbImage
			// 
			this.pbImage.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.pbImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pbImage.Location = new System.Drawing.Point(0, 0);
			this.pbImage.Name = "pbImage";
			this.pbImage.Size = new System.Drawing.Size(48, 48);
			this.pbImage.TabIndex = 0;
			this.pbImage.TabStop = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblTitle.BackColor = System.Drawing.Color.Transparent;
			this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblTitle.Location = new System.Drawing.Point(51, 3);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(102, 24);
			this.lblTitle.TabIndex = 1;
			this.lblTitle.Text = "Title";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDescription
			// 
			this.lblDescription.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblDescription.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblDescription.Location = new System.Drawing.Point(51, 27);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(151, 13);
			this.lblDescription.TabIndex = 2;
			this.lblDescription.Text = "Description";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDetectedCount
			// 
			this.lblDetectedCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblDetectedCount.BackColor = System.Drawing.Color.Transparent;
			this.lblDetectedCount.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblDetectedCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblDetectedCount.Location = new System.Drawing.Point(193, 12);
			this.lblDetectedCount.Name = "lblDetectedCount";
			this.lblDetectedCount.Size = new System.Drawing.Size(85, 24);
			this.lblDetectedCount.TabIndex = 3;
			this.lblDetectedCount.Text = "41 Items";
			this.lblDetectedCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSelectedCount
			// 
			this.lblSelectedCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblSelectedCount.BackColor = System.Drawing.Color.Transparent;
			this.lblSelectedCount.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblSelectedCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblSelectedCount.Location = new System.Drawing.Point(284, 0);
			this.lblSelectedCount.Name = "lblSelectedCount";
			this.lblSelectedCount.Size = new System.Drawing.Size(84, 24);
			this.lblSelectedCount.TabIndex = 4;
			this.lblSelectedCount.Text = "40 Items";
			this.lblSelectedCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblEdit
			// 
			this.lblEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblEdit.BackColor = System.Drawing.Color.Transparent;
			this.lblEdit.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
			this.lblEdit.Location = new System.Drawing.Point(284, 24);
			this.lblEdit.Name = "lblEdit";
			this.lblEdit.Size = new System.Drawing.Size(84, 24);
			this.lblEdit.TabIndex = 11;
			this.lblEdit.Text = "Edit";
			this.lblEdit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblEdit.Click += new System.EventHandler(this.lblEdit_Click);
			// 
			// btnAction
			// 
			this.btnAction.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnAction.BackColor = System.Drawing.Color.Transparent;
			this.btnAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnAction.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.btnAction.ForeColor = System.Drawing.Color.White;
			this.btnAction.Location = new System.Drawing.Point(380, 11);
			this.btnAction.Name = "btnAction";
			this.btnAction.Size = new System.Drawing.Size(127, 27);
			this.btnAction.TabIndex = 79;
			this.btnAction.Text = "scanResultButton1";
			this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
			// 
			// OverallScanRowControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Controls.Add(this.pbImage);
			this.Controls.Add(this.lblEdit);
			this.Controls.Add(this.lblSelectedCount);
			this.Controls.Add(this.lblDetectedCount);
			this.Controls.Add(this.lblDescription);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.btnAction);
			this.Name = "OverallScanRowControl";
			this.Size = new System.Drawing.Size(510, 48);
			((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox pbImage;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Label lblDetectedCount;
		private System.Windows.Forms.Label lblSelectedCount;
		private System.Windows.Forms.Label lblEdit;
		private ScanResultButton btnAction;
	}
}
