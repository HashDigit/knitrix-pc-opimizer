﻿namespace Win.Controls
{
	partial class ScanningUserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblCaption = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.piMain = new ProgressControls.ProgressIndicator();
			this.SuspendLayout();
			// 
			// lblCaption
			// 
			this.lblCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblCaption.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblCaption.Location = new System.Drawing.Point(0, 57);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(100, 23);
			this.lblCaption.TabIndex = 0;
			this.lblCaption.Text = "Caption";
			this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDescription
			// 
			this.lblDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblDescription.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold);
			this.lblDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
			this.lblDescription.Location = new System.Drawing.Point(0, 74);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(100, 23);
			this.lblDescription.TabIndex = 1;
			this.lblDescription.Text = "Not scanned";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// piMain
			// 
			this.piMain.CircleColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.piMain.Location = new System.Drawing.Point(61, 4);
			this.piMain.Name = "piMain";
			this.piMain.Percentage = 0F;
			this.piMain.Size = new System.Drawing.Size(32, 32);
			this.piMain.TabIndex = 56;
			this.piMain.Text = "progressIndicator1";
			// 
			// ScanningUserControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.lblCaption);
			this.Controls.Add(this.piMain);
			this.Controls.Add(this.lblDescription);
			this.Name = "ScanningUserControl";
			this.Size = new System.Drawing.Size(100, 100);
			this.ResumeLayout(false);

		}

		#endregion

		public ProgressControls.ProgressIndicator piMain;
		private System.Windows.Forms.Label lblCaption;
		private System.Windows.Forms.Label lblDescription;
	}
}
