﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Data;

namespace Win.Controls
{
	public partial class OverallScanGridControl : UserControl
	{
		public event EventHandler Edit;
		public event EventHandler Action;

		public OverallScanRowControl PrivacyFilesUserControl
		{
			get
			{
				return osrPrivacyFiles;
			}
		}

		public OverallScanRowControl MalwareProcessesUserControl
		{
			get
			{
				return osrMalwareProcesses;
			}
		}

		public OverallScanRowControl StartupProcessesUserControl
		{
			get
			{
				return osrStartupProcesses;
			}
		}

		public OverallScanRowControl ActiveProcessesUserControl
		{
			get
			{
				return osrActiveProcesses;
			}
		}

		public OverallScanRowControl RegistryProblemsUserControl
		{
			get
			{
				return osrRegistryProblems;
			}
		}

		public OverallScanGridControl()
		{
			InitializeComponent();

			osrPrivacyFiles.Edit += new EventHandler(osr_Edit);
			osrPrivacyFiles.Action += new EventHandler(osr_Action);

			osrMalwareProcesses.Edit += new EventHandler(osr_Edit);
			osrMalwareProcesses.Action += new EventHandler(osr_Action);

			osrStartupProcesses.Edit += new EventHandler(osr_Edit);
			osrStartupProcesses.Action += new EventHandler(osr_Action);

			osrActiveProcesses.Edit += new EventHandler(osr_Edit);
			osrActiveProcesses.Action += new EventHandler(osr_Action);
			
			osrRegistryProblems.Edit += new EventHandler(osr_Edit);
			osrRegistryProblems.Action += new EventHandler(osr_Action);
		}

		/// <summary>
		/// Init control with Overall Scan result data
		/// </summary>
		/// <param name="scannedItems"></param>
		public void Init()
		{
			osrPrivacyFiles.DetectedCount = DataContext.OneClickScanState.PrivacyFilesItemsToFixCount;
			osrPrivacyFiles.SelectedCount = DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
			osrPrivacyFiles.Tag = OneClickScanOverallAreaEnum.PrivacyFiles;

			osrMalwareProcesses.DetectedCount = DataContext.OneClickScanState.MalwareProcessesItemsToFixCount;
			osrMalwareProcesses.SelectedCount = DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
			osrMalwareProcesses.Tag = OneClickScanOverallAreaEnum.MalwareProcesses;

			osrStartupProcesses.DetectedCount = DataContext.OneClickScanState.StartupProcessesItemsToFixCount;
			osrStartupProcesses.SelectedCount = DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
			osrStartupProcesses.Tag = OneClickScanOverallAreaEnum.StartupProcesses;

			osrActiveProcesses.DetectedCount = DataContext.OneClickScanState.ActiveProcessesItemsToFixCount;
			osrActiveProcesses.SelectedCount = DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
			osrActiveProcesses.Tag = OneClickScanOverallAreaEnum.ActiveProcesses;

			osrRegistryProblems.DetectedCount = DataContext.OneClickScanState.RegistryProblemsItemsToFixCount;
			osrRegistryProblems.SelectedCount = DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;
			osrRegistryProblems.Tag = OneClickScanOverallAreaEnum.RegistryProblems;
		}

		private void OverallScanGridControlControl_Load(object sender, System.EventArgs e)
		{
			InitGui();
		}

		private void osr_Edit(object sender, EventArgs e)
		{
			if (Edit != null)
				Edit(sender, e);
		}

		private void osr_Action(object sender, EventArgs e)
		{
			if (Action != null)
				Action(sender, e);
		}

		private void InitGui()
		{
			osrPrivacyFiles.Title = "Privacy Files";
			osrPrivacyFiles.Description = "Delete Sensitive Files";
			osrPrivacyFiles.OneClickScanOverallArea = OneClickScanOverallAreaEnum.PrivacyFiles;

			//osrMalwareProcesses.Title = "Malware Processes";
			osrMalwareProcesses.Title = "Malware Process";
			osrMalwareProcesses.Description = "Remove Malicious Items";
			osrMalwareProcesses.OneClickScanOverallArea = OneClickScanOverallAreaEnum.MalwareProcesses;

			osrStartupProcesses.Title = "Startup Items";
			osrStartupProcesses.Description = "Improve Startup Speed";
			osrStartupProcesses.OneClickScanOverallArea = OneClickScanOverallAreaEnum.StartupProcesses;

			osrActiveProcesses.Title = "Active Processes";
			osrActiveProcesses.Description = "Optimize Performance";
			osrActiveProcesses.OneClickScanOverallArea = OneClickScanOverallAreaEnum.ActiveProcesses;

			osrRegistryProblems.Title = "Registry Problems";
			osrRegistryProblems.Description = "Clean System Registry";
			osrRegistryProblems.OneClickScanOverallArea = OneClickScanOverallAreaEnum.RegistryProblems;
		}

	}
}
