﻿using System.Drawing;

namespace Win.Controls
{
	partial class OverallScanGridControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OverallScanGridControl));
			this.osrRegistryProblems = new Win.Controls.OverallScanRowControl();
			this.osrActiveProcesses = new Win.Controls.OverallScanRowControl();
			this.osrStartupProcesses = new Win.Controls.OverallScanRowControl();
			this.osrMalwareProcesses = new Win.Controls.OverallScanRowControl();
			this.osrPrivacyFiles = new Win.Controls.OverallScanRowControl();
			this.lblDetected = new System.Windows.Forms.Label();
			this.lblSelected = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// osrRegistryProblems
			// 
			this.osrRegistryProblems.ActionCaption = "Clean Registry";
			this.osrRegistryProblems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.osrRegistryProblems.BackColor = System.Drawing.Color.Transparent;
			this.osrRegistryProblems.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("osrRegistryProblems.BackgroundImage")));
			this.osrRegistryProblems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.osrRegistryProblems.Description = "Clean System Registry";
			this.osrRegistryProblems.DetectedCount = 0;
			this.osrRegistryProblems.Icon = global::Win.Properties.Resources.registry_64x64;
			this.osrRegistryProblems.Location = new System.Drawing.Point(0, 213);
			this.osrRegistryProblems.Name = "osrRegistryProblems";
			this.osrRegistryProblems.OneClickScanOverallArea = Data.OneClickScanOverallAreaEnum.PrivacyFiles;
			this.osrRegistryProblems.SelectedCount = 0;
			this.osrRegistryProblems.Size = new System.Drawing.Size(510, 48);
			this.osrRegistryProblems.TabIndex = 4;
			this.osrRegistryProblems.Title = "Registry Problems";
			// 
			// osrActiveProcesses
			// 
			this.osrActiveProcesses.ActionCaption = "Optimize Memory";
			this.osrActiveProcesses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.osrActiveProcesses.BackColor = System.Drawing.Color.Transparent;
			this.osrActiveProcesses.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("osrActiveProcesses.BackgroundImage")));
			this.osrActiveProcesses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.osrActiveProcesses.Description = "Optimize Performance";
			this.osrActiveProcesses.DetectedCount = 0;
			this.osrActiveProcesses.Icon = global::Win.Properties.Resources.active_processes_64x64;
			this.osrActiveProcesses.Location = new System.Drawing.Point(0, 165);
			this.osrActiveProcesses.Name = "osrActiveProcesses";
			this.osrActiveProcesses.OneClickScanOverallArea = Data.OneClickScanOverallAreaEnum.PrivacyFiles;
			this.osrActiveProcesses.SelectedCount = 0;
			this.osrActiveProcesses.Size = new System.Drawing.Size(510, 48);
			this.osrActiveProcesses.TabIndex = 3;
			this.osrActiveProcesses.Title = "Active Processes";
			// 
			// osrStartupProcesses
			// 
			this.osrStartupProcesses.ActionCaption = "Optimize Startup";
			this.osrStartupProcesses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.osrStartupProcesses.BackColor = System.Drawing.Color.Transparent;
			this.osrStartupProcesses.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("osrStartupProcesses.BackgroundImage")));
			this.osrStartupProcesses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.osrStartupProcesses.Description = "Improve Startup Speed";
			this.osrStartupProcesses.DetectedCount = 0;
			this.osrStartupProcesses.Icon = global::Win.Properties.Resources.startup_64x64;
			this.osrStartupProcesses.Location = new System.Drawing.Point(0, 117);
			this.osrStartupProcesses.Name = "osrStartupProcesses";
			this.osrStartupProcesses.OneClickScanOverallArea = Data.OneClickScanOverallAreaEnum.PrivacyFiles;
			this.osrStartupProcesses.SelectedCount = 0;
			this.osrStartupProcesses.Size = new System.Drawing.Size(510, 48);
			this.osrStartupProcesses.TabIndex = 2;
			this.osrStartupProcesses.Title = "Startup Processes";
			// 
			// osrMalwareProcesses
			// 
			this.osrMalwareProcesses.ActionCaption = "Remove Malware";
			this.osrMalwareProcesses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.osrMalwareProcesses.BackColor = System.Drawing.Color.Transparent;
			this.osrMalwareProcesses.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("osrMalwareProcesses.BackgroundImage")));
			this.osrMalwareProcesses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.osrMalwareProcesses.Description = "Remove Threads";
			this.osrMalwareProcesses.DetectedCount = 0;
			this.osrMalwareProcesses.Icon = global::Win.Properties.Resources.malware_64x64;
			this.osrMalwareProcesses.Location = new System.Drawing.Point(0, 69);
			this.osrMalwareProcesses.Name = "osrMalwareProcesses";
			this.osrMalwareProcesses.OneClickScanOverallArea = Data.OneClickScanOverallAreaEnum.PrivacyFiles;
			this.osrMalwareProcesses.SelectedCount = 0;
			this.osrMalwareProcesses.Size = new System.Drawing.Size(510, 48);
			this.osrMalwareProcesses.TabIndex = 1;
			this.osrMalwareProcesses.Title = "Malware Processes";
			// 
			// osrPrivacyFiles
			// 
			this.osrPrivacyFiles.ActionCaption = "Clean Privacy Files";
			this.osrPrivacyFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.osrPrivacyFiles.BackColor = System.Drawing.Color.Transparent;
			this.osrPrivacyFiles.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("osrPrivacyFiles.BackgroundImage")));
			this.osrPrivacyFiles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.osrPrivacyFiles.Description = "Remove Sensitive Files";
			this.osrPrivacyFiles.DetectedCount = 0;
			this.osrPrivacyFiles.Icon = global::Win.Properties.Resources.privacy_64x64;
			this.osrPrivacyFiles.Location = new System.Drawing.Point(0, 21);
			this.osrPrivacyFiles.Name = "osrPrivacyFiles";
			this.osrPrivacyFiles.OneClickScanOverallArea = Data.OneClickScanOverallAreaEnum.PrivacyFiles;
			this.osrPrivacyFiles.SelectedCount = 0;
			this.osrPrivacyFiles.Size = new System.Drawing.Size(510, 48);
			this.osrPrivacyFiles.TabIndex = 0;
			this.osrPrivacyFiles.Title = "Privacy Files";
			// 
			// lblDetected
			// 
			this.lblDetected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblDetected.BackColor = System.Drawing.Color.Transparent;
			this.lblDetected.Font = new System.Drawing.Font("Segoe UI Semibold", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblDetected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblDetected.Location = new System.Drawing.Point(193, 0);
			this.lblDetected.Name = "lblDetected";
			this.lblDetected.Size = new System.Drawing.Size(85, 21);
			this.lblDetected.TabIndex = 5;
			this.lblDetected.Text = "Detected";
			this.lblDetected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSelected
			// 
			this.lblSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblSelected.BackColor = System.Drawing.Color.Transparent;
			this.lblSelected.Font = new System.Drawing.Font("Segoe UI Semibold", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblSelected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.lblSelected.Location = new System.Drawing.Point(284, 0);
			this.lblSelected.Name = "lblSelected";
			this.lblSelected.Size = new System.Drawing.Size(85, 21);
			this.lblSelected.TabIndex = 6;
			this.lblSelected.Text = "Selected";
			this.lblSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// OverallScanGridControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.lblSelected);
			this.Controls.Add(this.lblDetected);
			this.Controls.Add(this.osrRegistryProblems);
			this.Controls.Add(this.osrActiveProcesses);
			this.Controls.Add(this.osrStartupProcesses);
			this.Controls.Add(this.osrMalwareProcesses);
			this.Controls.Add(this.osrPrivacyFiles);
			this.Name = "OverallScanGridControl";
			this.Size = new System.Drawing.Size(510, 260);
			this.Load += new System.EventHandler(this.OverallScanGridControlControl_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private OverallScanRowControl osrPrivacyFiles;
		private OverallScanRowControl osrMalwareProcesses;
		private OverallScanRowControl osrStartupProcesses;
		private OverallScanRowControl osrActiveProcesses;
		private OverallScanRowControl osrRegistryProblems;
		private System.Windows.Forms.Label lblDetected;
		private System.Windows.Forms.Label lblSelected;
	}
}
