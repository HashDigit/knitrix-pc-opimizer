﻿namespace Win.Controls
{
    partial class NavigationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pbBack = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pbBack)).BeginInit();
			this.SuspendLayout();
			// 
			// pbBack
			// 
			this.pbBack.Image = global::Win.Properties.Resources.back_normal;
			this.pbBack.Location = new System.Drawing.Point(0, 0);
			this.pbBack.Name = "pbBack";
			this.pbBack.Size = new System.Drawing.Size(32, 32);
			this.pbBack.TabIndex = 24;
			this.pbBack.TabStop = false;
			this.pbBack.Click += new System.EventHandler(this.pbBack_Click);
			this.pbBack.MouseEnter += new System.EventHandler(this.pb_MouseEnter);
			this.pbBack.MouseLeave += new System.EventHandler(this.pb_MouseLeave);
			// 
			// NavigationControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.pbBack);
			this.Name = "NavigationControl";
			this.Size = new System.Drawing.Size(32, 32);
			((System.ComponentModel.ISupportInitialize)(this.pbBack)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.PictureBox pbBack;
    }
}
