﻿namespace Win.Controls
{
	partial class MainButton
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnMain = new Win.Controls.PictureBoxButton();
			((System.ComponentModel.ISupportInitialize)(this.btnMain)).BeginInit();
			this.SuspendLayout();
			// 
			// btnMain
			// 
			this.btnMain.Caption = "";
			this.btnMain.CaptionColor = System.Drawing.Color.Black;
			this.btnMain.CaptionColorActive = System.Drawing.Color.Black;
			this.btnMain.CaptionFont = new System.Drawing.Font("Tahoma", 8F);
			this.btnMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMain.Image = global::Win.Properties.Resources.square_btn_normal;
			this.btnMain.ImageActive = null;
			this.btnMain.ImageDisabled = global::Win.Properties.Resources.square_btn_normal;
			this.btnMain.ImageNormal = global::Win.Properties.Resources.square_btn_normal;
			this.btnMain.ImageRollover = global::Win.Properties.Resources.square_btn_rollover;
			this.btnMain.IsActive = false;
			this.btnMain.Location = new System.Drawing.Point(0, 0);
			this.btnMain.Name = "btnMain";
			this.btnMain.Size = new System.Drawing.Size(285, 108);
			this.btnMain.TabIndex = 0;
			this.btnMain.TabStop = false;
			this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
			this.btnMain.Paint += new System.Windows.Forms.PaintEventHandler(this.btnMain_Paint);
			// 
			// MainButton
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.btnMain);
			this.Name = "MainButton";
			this.Size = new System.Drawing.Size(285, 108);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainButton_Paint);
			((System.ComponentModel.ISupportInitialize)(this.btnMain)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		public PictureBoxButton btnMain;

	}
}
