﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class DetectedItemsLabel : UserControl
	{
		public int Count
		{
			get
			{
				return _count;
			}
			set
			{
				_count = value;
				lblValue.Text = _count < 75 ? "Negligible" : _count.ToString();
			}
		}
		private int _count;

		public DetectedItemsLabel()
		{
			InitializeComponent();
		}
	}
}
