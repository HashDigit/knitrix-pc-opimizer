﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class ScanningUserControl : UserControl
	{
		public bool IsScanned
		{
			get
			{
				return _isScanned;
			}
			set
			{
				_isScanned = value;
				base.BackgroundImage = _isScanned ? _imageScanning : _imageIsNotScanned;
				this.Invalidate();
				if (_isScanned)
				{
					lblCaption.ForeColor = Color.FromArgb(162, 0, 0);
					lblDescription.ForeColor = Color.FromArgb(80, 80, 80);
				}
				else
				{
					lblCaption.ForeColor = Color.FromArgb(81, 0, 0);
					lblDescription.ForeColor = Color.FromArgb(40, 40, 40);
					lblDescription.Text = "Not scanned";
				}
			}
		}
		private bool _isScanned;
		
		public Image ImageScanning
		{
			get
			{
				return _imageScanning;
			}
			set
			{
				_imageScanning = value;
				base.BackgroundImage = _isScanned ? _imageScanning : _imageIsNotScanned;
				this.Invalidate();
			}
		}
		private Image _imageScanning;

		public Image ImageIsNotScanned
		{
			get
			{
				return _imageIsNotScanned;
			}
			set
			{
				_imageIsNotScanned = value;
				base.BackgroundImage = _isScanned ? _imageScanning : _imageIsNotScanned;
				this.Invalidate();
			}
		}
		private Image _imageIsNotScanned;

		public string Caption
		{
			get
			{
				return lblCaption.Text;
			}
			set
			{
				lblCaption.Text = value;
			}
		}

		public string Description
		{
			get
			{
				return lblDescription.Text;
			}
			set
			{
				lblDescription.Text = value;
			}
		}
		public ScanningUserControl()
		{
			InitializeComponent();
		}
	}
}
