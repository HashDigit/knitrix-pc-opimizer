﻿namespace Win.Controls
{
	partial class TabUserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbbInfo = new Win.Controls.PictureBoxButton();
			this.pbbSettings = new Win.Controls.PictureBoxButton();
			this.pbbOptimize = new Win.Controls.PictureBoxButton();
			this.pbbDisk = new Win.Controls.PictureBoxButton();
			this.pbbHome = new Win.Controls.PictureBoxButton();
			((System.ComponentModel.ISupportInitialize)(this.pbbInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbSettings)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbOptimize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbDisk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbHome)).BeginInit();
			this.SuspendLayout();
			// 
			// pbbInfo
			// 
			this.pbbInfo.Caption = "Info";
			this.pbbInfo.CaptionColor = System.Drawing.Color.White;
			this.pbbInfo.CaptionColorActive = System.Drawing.Color.White;
			this.pbbInfo.CaptionFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
			this.pbbInfo.Image = global::Win.Properties.Resources.tab_normal;
			this.pbbInfo.ImageActive = global::Win.Properties.Resources.tab_active;
			this.pbbInfo.ImageDisabled = null;
			this.pbbInfo.ImageNormal = global::Win.Properties.Resources.tab_normal;
			this.pbbInfo.ImageRollover = global::Win.Properties.Resources.tab_rollover;
			this.pbbInfo.IsActive = false;
			this.pbbInfo.Location = new System.Drawing.Point(452, 1);
			this.pbbInfo.Name = "pbbInfo";
			this.pbbInfo.Size = new System.Drawing.Size(112, 33);
			this.pbbInfo.TabIndex = 4;
			this.pbbInfo.TabStop = false;
			this.pbbInfo.Visible = false;
			// 
			// pbbSettings
			// 
			this.pbbSettings.Caption = "Settings";
			this.pbbSettings.CaptionColor = System.Drawing.Color.White;
			this.pbbSettings.CaptionColorActive = System.Drawing.Color.White;
			this.pbbSettings.CaptionFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
			this.pbbSettings.Image = global::Win.Properties.Resources.tab_normal;
			this.pbbSettings.ImageActive = global::Win.Properties.Resources.tab_active;
			this.pbbSettings.ImageDisabled = null;
			this.pbbSettings.ImageNormal = global::Win.Properties.Resources.tab_normal;
			this.pbbSettings.ImageRollover = global::Win.Properties.Resources.tab_rollover;
			this.pbbSettings.IsActive = false;
			this.pbbSettings.Location = new System.Drawing.Point(340, 1);
			this.pbbSettings.Name = "pbbSettings";
			this.pbbSettings.Size = new System.Drawing.Size(112, 33);
			this.pbbSettings.TabIndex = 3;
			this.pbbSettings.TabStop = false;
			this.pbbSettings.Click += new System.EventHandler(this.pbb_Click);
			// 
			// pbbOptimize
			// 
			this.pbbOptimize.Caption = "Optimize";
			this.pbbOptimize.CaptionColor = System.Drawing.Color.White;
			this.pbbOptimize.CaptionColorActive = System.Drawing.Color.White;
			this.pbbOptimize.CaptionFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
			this.pbbOptimize.Image = global::Win.Properties.Resources.tab_normal;
			this.pbbOptimize.ImageActive = global::Win.Properties.Resources.tab_active;
			this.pbbOptimize.ImageDisabled = null;
			this.pbbOptimize.ImageNormal = global::Win.Properties.Resources.tab_normal;
			this.pbbOptimize.ImageRollover = global::Win.Properties.Resources.tab_rollover;
			this.pbbOptimize.IsActive = false;
			this.pbbOptimize.Location = new System.Drawing.Point(116, 1);
			this.pbbOptimize.Name = "pbbOptimize";
			this.pbbOptimize.Size = new System.Drawing.Size(112, 33);
			this.pbbOptimize.TabIndex = 1;
			this.pbbOptimize.TabStop = false;
			this.pbbOptimize.Click += new System.EventHandler(this.pbb_Click);
			// 
			// pbbDisk
			// 
			this.pbbDisk.Caption = "Disk";
			this.pbbDisk.CaptionColor = System.Drawing.Color.White;
			this.pbbDisk.CaptionColorActive = System.Drawing.Color.White;
			this.pbbDisk.CaptionFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
			this.pbbDisk.Image = global::Win.Properties.Resources.tab_normal;
			this.pbbDisk.ImageActive = global::Win.Properties.Resources.tab_active;
			this.pbbDisk.ImageDisabled = null;
			this.pbbDisk.ImageNormal = global::Win.Properties.Resources.tab_normal;
			this.pbbDisk.ImageRollover = global::Win.Properties.Resources.tab_rollover;
			this.pbbDisk.IsActive = false;
			this.pbbDisk.Location = new System.Drawing.Point(228, 1);
			this.pbbDisk.Name = "pbbDisk";
			this.pbbDisk.Size = new System.Drawing.Size(112, 33);
			this.pbbDisk.TabIndex = 2;
			this.pbbDisk.TabStop = false;
			this.pbbDisk.Click += new System.EventHandler(this.pbb_Click);
			// 
			// pbbHome
			// 
			this.pbbHome.Caption = "Home";
			this.pbbHome.CaptionColor = System.Drawing.Color.White;
			this.pbbHome.CaptionColorActive = System.Drawing.Color.White;
			this.pbbHome.CaptionFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
			this.pbbHome.Image = global::Win.Properties.Resources.tab_active;
			this.pbbHome.ImageActive = global::Win.Properties.Resources.tab_active;
			this.pbbHome.ImageDisabled = null;
			this.pbbHome.ImageNormal = global::Win.Properties.Resources.tab_normal;
			this.pbbHome.ImageRollover = global::Win.Properties.Resources.tab_rollover;
			this.pbbHome.IsActive = true;
			this.pbbHome.Location = new System.Drawing.Point(4, 1);
			this.pbbHome.Name = "pbbHome";
			this.pbbHome.Size = new System.Drawing.Size(112, 33);
			this.pbbHome.TabIndex = 0;
			this.pbbHome.TabStop = false;
			this.pbbHome.Click += new System.EventHandler(this.pbb_Click);
			// 
			// TabUserControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.pbbInfo);
			this.Controls.Add(this.pbbSettings);
			this.Controls.Add(this.pbbOptimize);
			this.Controls.Add(this.pbbDisk);
			this.Controls.Add(this.pbbHome);
			this.Name = "TabUserControl";
			this.Size = new System.Drawing.Size(746, 33);
			((System.ComponentModel.ISupportInitialize)(this.pbbInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbSettings)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbOptimize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbDisk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbbHome)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		public PictureBoxButton pbbInfo;
		public PictureBoxButton pbbHome;
		public PictureBoxButton pbbOptimize;
		public PictureBoxButton pbbDisk;
		public PictureBoxButton pbbSettings;


	}
}
