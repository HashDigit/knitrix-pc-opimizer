using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Data;

namespace Win.Controls
{
	public class MainPanel : CommonPanel
	{
		public event EventHandler PushToTalkClick;
		public event EventHandler RegisterClick;

		/// <summary>
		/// Number of horizontal lines on main area that are separate buttons
		/// </summary>
		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		Category("Appearance")]
		public int SeparatorCount { get; set; }

		public MainPanel()
		{
			InitializeComponent();
			SetNavigationVisibility(false);
		}

		private RegisterButton btnRegister;
		private RegisterButton btnPushToTalk;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnRegister = new Win.Controls.RegisterButton();
			this.btnPushToTalk = new Win.Controls.RegisterButton();
			this.SuspendLayout();
			// 
			// btnRegister
			// 
			this.btnRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnRegister.BackColor = System.Drawing.Color.Transparent;
			this.btnRegister.Location = new System.Drawing.Point(592, 377);
			this.btnRegister.Name = "btnRegister";
			this.btnRegister.Size = new System.Drawing.Size(119, 27);
			this.btnRegister.TabIndex = 72;
			this.btnRegister.TabStop = false;
			this.btnRegister.Text = "Register";
			this.btnRegister.Visible = Business.License.Instance.LicenseState != LicenseStateEnum.Registered;
			this.btnRegister.Click += new System.EventHandler(btnRegister_Click);
			// 
			// btnPushToTalk
			// 
			this.btnPushToTalk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnPushToTalk.BackColor = System.Drawing.Color.Transparent;
			this.btnPushToTalk.Location = new System.Drawing.Point(412, 377);
			this.btnPushToTalk.Name = "btnPushToTalk";
			this.btnPushToTalk.Size = new System.Drawing.Size(150, 27);
			this.btnPushToTalk.TabIndex = 72;
			this.btnPushToTalk.TabStop = false;
			this.btnPushToTalk.Text = "Live Chat";
			this.btnPushToTalk.Click += new System.EventHandler(btnPushToTalk_Click);
			//
			//
			//
			this.Controls.Add(btnRegister);
			//this.Controls.Add(btnPushToTalk);
			this.ResumeLayout(false);
		}

		private void btnPushToTalk_Click(object sender, EventArgs e)
		{
			if (PushToTalkClick != null)
				PushToTalkClick(sender, e);
		}

		private void btnRegister_Click(object sender, System.EventArgs e)
		{
			if (RegisterClick != null)
				RegisterClick(sender, e);
		}

		public void RefreshBtnRegister()
		{
			btnRegister.Text = "Register";
			btnRegister.Visible = Business.License.Instance.LicenseState != LicenseStateEnum.Registered;
		}
	}
}
