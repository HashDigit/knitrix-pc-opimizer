﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Win.Properties;

namespace Win.Controls
{
	public partial class PCHealthProgressBar : UserControl
	{
		/// <summary>
		/// Кол-во делителей для отображения
		/// </summary>
		private int _value = -1;

		public Color CaptionColor { get; set; }
		public Font CaptionFont { get; set; }
		public Color TextColor { get; set; }
		public Font TextFont { get; set; }
		private string _text { get; set; }

		public PCHealthProgressBar()
		{
			InitializeComponent();

			CaptionColor = Color.WhiteSmoke;
			CaptionFont = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Pixel);
			TextColor = Color.White;
			TextFont = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Pixel);

            lblDescription.Visible = false;

			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
			//SetState(-1);
		}

		/// <summary>
		/// Set background image and text using issueCount
		/// -1 - unrated, >=300 - low, 75-299 - medium, 0-74 - high
		/// </summary>
		public void SetState(int issueCount)
		{
			if (issueCount == -1)
			{
				_text = "UNRATED";
				TextColor = Color.LightGray;
				lblDescription.Text = "Scan Required";
				SetValue(issueCount);
				//pbMain.MainColor = Color.LightGray;
			}
			else if (issueCount >= 300)
			{
				_text = "LOW";
				TextColor = Color.Red;
				lblDescription.Text = "Your PC should be optimized";
			}
			else if (issueCount >= 75)
			{
				_text = "MEDIUM";
				TextColor = Color.Orange;
				lblDescription.Text = "Your PC can be optimized";
			}
			else if (issueCount >= 0)
			{
				_text = "HIGH";
				TextColor = Color.LightGreen;
				lblDescription.Text = "Your PC is healthy";
			}

			if (issueCount > -1)
			{
				var y = f(issueCount);
				SetValue((int) (y*100));
				//pbMain.MainColor = Color.FromArgb(255, 100 + (int)((255 - 100) * y), 255 + (int)((100 - 255) * y), 100);
				lblDescription.Text += string.Format("\n\rFound {0} problems", issueCount);
			}

			this.Invalidate();
		}

		private void SetValue(int value)
		{
			_value = value < 0 ? -1 : value / 10;
			/*
			Image itemImage = null;
			switch (_value)
			{
				case 0:
					itemImage = Properties.Resources.progressbar_9;
					break;
				case 1:
					itemImage = Properties.Resources.progressbar_8;
					break;
				case 2:
					itemImage = Properties.Resources.progressbar_8;
					break;
				case 3:
					itemImage = Properties.Resources.progressbar_7;
					break;
				case 4:
					itemImage = Properties.Resources.progressbar_6;
					break;
				case 5:
					itemImage = Properties.Resources.progressbar_5;
					break;
				case 6:
					itemImage = Properties.Resources.progressbar_4;
					break;
				case 7:
					itemImage = Properties.Resources.progressbar_3;
					break;
				case 8:
					itemImage = Properties.Resources.progressbar_2;
					break;
				case 9:
				default:
					itemImage = Properties.Resources.progressbar_1;
					break;
			}
			if (0 <= _value && value >= 0)
				pnlItem0.BackgroundImage = itemImage;
			else
				pnlItem0.BackgroundImage = null;
			if (1 <= _value)
				pnlItem1.BackgroundImage = itemImage;
			else
				pnlItem1.BackgroundImage = null;
			if (2 <= _value)
				pnlItem2.BackgroundImage = itemImage;
			else
				pnlItem2.BackgroundImage = null;
			if (3 <= _value)
				pnlItem3.BackgroundImage = itemImage;
			else
				pnlItem3.BackgroundImage = null;
			if (4 <= _value)
				pnlItem4.BackgroundImage = itemImage;
			else
				pnlItem4.BackgroundImage = null;
			if (5 <= _value)
				pnlItem5.BackgroundImage = itemImage;
			else
				pnlItem5.BackgroundImage = null;
			if (6 <= _value)
				pnlItem6.BackgroundImage = itemImage;
			else
				pnlItem6.BackgroundImage = null;
			if (7 <= _value)
				pnlItem7.BackgroundImage = itemImage;
			else
				pnlItem7.BackgroundImage = null;
			if (8 <= _value)
				pnlItem8.BackgroundImage = itemImage;
			else
				pnlItem8.BackgroundImage = null;
			if (9 <= _value)
				pnlItem9.BackgroundImage = itemImage;
			else
				pnlItem9.BackgroundImage = null;
			*/
		}

		/// <summary>
		/// Формула f = -1/(x/alfa + 1) + 1, где alfa = 100
		/// f(0) = 0
		/// f(00) = 1
		/// f(10) = 0,1
		/// f(100) = 0,5
		/// f(500) = 0,83
		/// </summary>
		/// <param name="issueCount"></param>
		/// <returns></returns>
		private double f(int issueCount)
		{
			int alfa = 100;
			return -1 / ((double)issueCount / alfa + 1) + 1;
		}

		private void PCHealthProgressBar_Paint(object sender, PaintEventArgs e)
		{
			//e.Graphics.DrawImage(Resources.progressbar_disabled, 176, 0);

			Image itemImage = null;
			switch (_value)
			{
				case 0:
					itemImage = Properties.Resources.progressbar_9;
					break;
				case 1:
					itemImage = Properties.Resources.progressbar_8;
					break;
				case 2:
					itemImage = Properties.Resources.progressbar_8;
					break;
				case 3:
					itemImage = Properties.Resources.progressbar_7;
					break;
				case 4:
					itemImage = Properties.Resources.progressbar_6;
					break;
				case 5:
					itemImage = Properties.Resources.progressbar_5;
					break;
				case 6:
					itemImage = Properties.Resources.progressbar_4;
					break;
				case 7:
					itemImage = Properties.Resources.progressbar_3;
					break;
				case 8:
					itemImage = Properties.Resources.progressbar_2;
					break;
				case 9:
				default:
					itemImage = Properties.Resources.progressbar_1;
					break;
			}
			for (int i = 0; i <= _value; i++)
			{
                //e.Graphics.DrawImage(itemImage,
                //    new Rectangle(182 + itemImage.Width * i, 5, itemImage.Width, itemImage.Height),
                //    0, 0, itemImage.Width, itemImage.Height, GraphicsUnit.Pixel);
			}

			//e.Graphics.DrawString("HEALTH:", CaptionFont, new SolidBrush(CaptionColor), 286, 7);
			//e.Graphics.DrawString(_text, TextFont, new SolidBrush(TextColor), 336, 7);
		}
	}
}
