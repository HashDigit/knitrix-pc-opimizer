﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class NavigationControl : UserControl
	{
		public event EventHandler Back;
		public event EventHandler Forward;

		public NavigationControl()
		{
			InitializeComponent();
		}

		//public bool IsBackEnabled
		//{
		//    get
		//    {
		//        return pbBack.Enabled;
		//    }
		//    set
		//    {
		//        pbBack.Enabled = value;
		//        if (!pbBack.Enabled)
		//            pbBack.Image = Properties.Resources.back_disabled;
		//    }
		//}

		//public bool IsForwardEnabled
		//{
		//    get
		//    {
		//        return pbForward.Enabled;
		//    }
		//    set
		//    {
		//        pbForward.Enabled = value;
		//        if (!pbForward.Enabled)
		//            pbForward.Image = Properties.Resources.front_disabled;
		//    }
		//}

		private void pbBack_Click(object sender, EventArgs e)
		{
			if (Back != null)
				Back(this, e);
		}
		
		private void pb_MouseEnter(object sender, EventArgs e)
		{
			if (sender == pbBack && pbBack.Enabled)
			{
				pbBack.Image = Properties.Resources.back_rollover;
			}
		}

		private void pb_MouseLeave(object sender, EventArgs e)
		{
			if (sender == pbBack && pbBack.Enabled)
			{
				pbBack.Image = Properties.Resources.back_normal;
			}
		}
	}
}
