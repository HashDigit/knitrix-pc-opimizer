﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Data;
using Win.Properties;

namespace Win.Controls
{
	public partial class OverallScanRowControl : UserControl
	{
		public event EventHandler Edit;
		public event EventHandler Action;

		public string Title 
		{ 
			get
			{
				return lblTitle.Text;
			}
			set
			{
				lblTitle.Text = value;
			}
		}

		public string Description
		{
			get
			{
				return lblDescription.Text;
			}
			set
			{
				lblDescription.Text = value;
			}
		}

		public string ActionCaption
		{
			get
			{
				return btnAction.Text;
			}
			set
			{
				btnAction.Text = value;
			}
		}

		public Image Icon
		{
			get
			{
				return pbImage.BackgroundImage;
			}
			set
			{
				pbImage.BackgroundImage = value;
			}
		}

		public OneClickScanOverallAreaEnum OneClickScanOverallArea
		{
			get
			{
				return _oneClickScanOverallArea;
			}
			set
			{
				_oneClickScanOverallArea = value;
				//SetImage();
				btnAction.Enabled = (_selectedCount > 0);
				//SetBtnActionImage(true);
			}
		}
		private OneClickScanOverallAreaEnum _oneClickScanOverallArea;

		public int DetectedCount
		{
			get
			{
				return _detectedCount;
			}
			set
			{
				_detectedCount = value;
				lblEdit.Enabled = (_detectedCount > 0);
				btnAction.Enabled = (_selectedCount > 0);
				lblDetectedCount.Text = string.Format("{0} Items", _detectedCount);
			}
		}
		private int _detectedCount;

		public int SelectedCount
		{
			get
			{
				return _selectedCount;
			}
			set
			{
				_selectedCount = value;
				btnAction.Enabled = (_selectedCount > 0);
				//SetBtnActionImage(true);
				lblSelectedCount.Text = string.Format("{0} Items", _selectedCount);
			}
		}
		private int _selectedCount;

		public OverallScanRowControl()
		{
			InitializeComponent();
		}
		
		/// <summary>
		/// User clicked on "Edit" link
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lblEdit_Click(object sender, EventArgs e)
		{
			if (_detectedCount > 0 && Edit != null)
				Edit(Tag, new EventArgs());
		}

		/// <summary>
		/// User clicked on "Action" button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnAction_Click(object sender, EventArgs e)
		{
			if (_selectedCount > 0 && Action != null)
				Action(Tag, new EventArgs());
		}
		/*
		private void btnAction_MouseEnter(object sender, EventArgs e)
		{
			SetBtnActionImage(false);
		}

		private void btnAction_MouseLeave(object sender, EventArgs e)
		{
			SetBtnActionImage(true);
		}

		/// <summary>
		/// Set Action button image: normal, rollover, disabled
		/// </summary>
		/// <param name="isNormal"></param>
		private void SetBtnActionImage(bool isNormal)
		{
			switch (OneClickScanOverallArea)
			{
				case OneClickScanOverallAreaEnum.PrivacyFiles:
					btnAction.BackgroundImage = _selectedCount > 0 ?
						(isNormal ? Resources.protect_privacy_normal : Resources.protect_privacy_rollover) :
						Resources.protect_privacy_disabled;
					break;
				case OneClickScanOverallAreaEnum.MalwareProcesses:
					btnAction.BackgroundImage = _selectedCount > 0 ?
						(isNormal ? Resources.remove_malware_normal: Resources.remove_malware_rollover) :
						Resources.remove_malware_disabled;
					break;
				case OneClickScanOverallAreaEnum.StartupProcesses:
					btnAction.BackgroundImage = _selectedCount > 0 ?
						(isNormal ? Resources.optimize_startup_normal : Resources.optimize_startup_rollover) :
						Resources.optimize_startup_disabled;
					break;
				case OneClickScanOverallAreaEnum.ActiveProcesses:
					btnAction.BackgroundImage = _selectedCount > 0 ?
						(isNormal ? Resources.optimize_speed_normal : Resources.optimize_speed_rollover) :
						Resources.optimize_speed_disabled;
					break;
				case OneClickScanOverallAreaEnum.RegistryProblems:
					btnAction.BackgroundImage = _selectedCount > 0 ?
						(isNormal ? Resources.clean_registry_normal : Resources.clean_registry_rollover) :
						Resources.clean_registry_disabled;
					break;
			}
		}
		*/

		/*
		/// <summary>
		/// Set main image
		/// </summary>
		/// <param name="isNormal"></param>
		private void SetImage()
		{
			switch (OneClickScanOverallArea)
			{
				case OneClickScanOverallAreaEnum.PrivacyFiles:
					pbImage.BackgroundImage = Properties.Resources.privacy_icon1;
					break;
				case OneClickScanOverallAreaEnum.MalwareProcesses:
					pbImage.BackgroundImage = Properties.Resources.malware_icon1;
					break;
				case OneClickScanOverallAreaEnum.StartupProcesses:
					pbImage.BackgroundImage = Properties.Resources.startup_icon1;
					break;
				case OneClickScanOverallAreaEnum.ActiveProcesses:
					pbImage.BackgroundImage = Properties.Resources.active_processes_icon;
					break;
				case OneClickScanOverallAreaEnum.RegistryProblems:
					pbImage.BackgroundImage = Properties.Resources.registry_icon1;
					break;
			}
		}
		*/
	}
}
