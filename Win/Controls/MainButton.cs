﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class MainButton : UserControl
	{
		public event EventHandler ButtonClick;

		public string Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
				this.Invalidate();
			}
		}
		public string _title;
		private Color _titleColor;
		private Font _titleFont;

		public string Description { get; set; }
		private Color _descriptionColor;
		private Font _descriptionFont;

		public Image Icon { get; set; }

		public MainButton()
		{
			InitializeComponent();

			_titleColor = Color.FromArgb(162, 0, 0);
			_titleFont = new Font("Segoe UI", 13F, FontStyle.Bold, GraphicsUnit.Pixel);
			_descriptionColor = Color.FromArgb(80, 80, 80);
			_descriptionFont = new Font("Segoe UI Semibold", 11.25F, GraphicsUnit.Pixel);

			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
		}

		private void btnMain_Click(object sender, EventArgs e)
		{
			if (ButtonClick != null)
				ButtonClick(sender, e);
		}

		private void btnMain_Paint(object sender, PaintEventArgs e)
		{
			if (Icon != null)
				//e.Graphics.DrawImage(Icon, (btnMain.Width - Icon.Width) / 2, (btnMain.Height - Icon.Height) / 2, Icon.Width, Icon.Height);
				e.Graphics.DrawImage(Icon, 12, 10, Icon.Width, Icon.Height);
			//e.Graphics.DrawString(Title, ButtonCaptionFont, new SolidBrush(ButtonCaptionColor), 16, 80);
			var sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			e.Graphics.DrawString(Title, _titleFont, new SolidBrush(_titleColor), new RectangleF(80, 7, 170, 40), sf);
			e.Graphics.DrawString(Description, _descriptionFont, new SolidBrush(_descriptionColor), new RectangleF(80, 35, 170, 62), sf);
		}

		private void MainButton_Paint(object sender, PaintEventArgs e)
		{
		}
	}
}
