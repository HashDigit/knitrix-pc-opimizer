using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using Data;

namespace Win.Controls
{
	/// <summary>
	/// Summary description for CommonPanel.
	/// </summary>
	public class CommonPanel : DoublePanel
	{
		/// <summary>
		/// Navigation bar
		/// </summary>
		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		Category("Appearance")]
		public Image NavBarImage { get; set; }

		/// <summary>
		/// Image with title and description
		/// </summary>
		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		Category("Appearance")]
		public Image Icon
		{
			get
			{
				return _icon;
			}
			set
			{
				_icon = value;
				Invalidate();
			}
		}
		private Image _icon { get; set; }

		/// <summary>
		/// Image with title and description
		/// </summary>
		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		Category("Appearance")]
		public bool IsIconOnLeft
		{
			get
			{
				return _isIconOnLeft;
			}
			set
			{
				_isIconOnLeft = value;
				Invalidate();
			}
		}
		private bool _isIconOnLeft { get; set; }

		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		Category("Appearance")]
		public bool HideLogo { get; set; }

		public string Caption
		{
			get
			{
				return _caption;
			}
			set
			{
				_caption = value;
				Invalidate();
			}
		}
		private string _caption { get; set; }

		private Font _captionFont { get; set; }
		private Color _captionColor { get; set; }

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
				Invalidate();
			}
		}
		private string _description { get; set; }

		private Font _descriptionFont { get; set; }
		private Color _descriptionColor { get; set; }

		private PCHealthProgressBar pchpbHealth;

		#region Constructor and initialization
		public CommonPanel()
		{
			InitializeComponent();

			//_captionFont = new Font("Segoe UI", 9.75F, FontStyle.Bold);
			_captionFont = new Font("Segoe UI Semibold", 12F, GraphicsUnit.Pixel);
			_captionColor = Color.WhiteSmoke;

			_descriptionFont = new Font("Segoe UI", 10.25F, GraphicsUnit.Pixel);
			_descriptionColor = Color.LightGray;
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Navigation = new Win.Controls.NavigationControl();
			this.pchpbHealth = new Win.Controls.PCHealthProgressBar();
			this.SuspendLayout();
			// 
			// Navigation
			// 
			this.Navigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.Navigation.BackColor = System.Drawing.Color.Transparent;
			this.Navigation.Location = new System.Drawing.Point(695, 9);
			this.Navigation.Name = "Navigation";
			this.Navigation.Size = new System.Drawing.Size(32, 32);
			this.Navigation.TabIndex = 20;
			// 
			// pchpbHealth
			// 
			this.pchpbHealth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pchpbHealth.BackColor = System.Drawing.Color.Transparent;
			this.pchpbHealth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.pchpbHealth.Location = new System.Drawing.Point(288, 11);
			this.pchpbHealth.Name = "pchpbHealth";
			this.pchpbHealth.Size = new System.Drawing.Size(393, 31);
			this.pchpbHealth.TabIndex = 78;
			// 
			// 
			// 
			this.Controls.Add(this.Navigation);
			this.Controls.Add(this.pchpbHealth);
			this.ResumeLayout(false);
		}

		[Browsable(true),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
		DefaultValue(60),
		Category("Appearance")]
		public NavigationControl Navigation { get; set; }

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
		}
		#endregion Constructor and initialization

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			base.OnPaintBackground(pevent);
			
			// draw navigation bar
			if (NavBarImage != null)
				pevent.Graphics.DrawImage(NavBarImage, 11, 7, NavBarImage.Width, NavBarImage.Height);
			// draw title and description
			var titleBackgroundImage = global::Win.Properties.Resources.large_plate;
			if (Icon != null)
			{
				
				pevent.Graphics.DrawImage(titleBackgroundImage, IsIconOnLeft ? 40 : 598,
										  (Height - titleBackgroundImage.Height) / 2,
										  titleBackgroundImage.Width, titleBackgroundImage.Height);
				pevent.Graphics.DrawImage(Icon, IsIconOnLeft ? 60 : 623,
										  (Height - titleBackgroundImage.Height) / 2 + 10, Icon.Width,
										  Icon.Height);
			}

			if (!string.IsNullOrEmpty(Caption))
			{
				var sf = new StringFormat();
				sf.Alignment = StringAlignment.Center;
				pevent.Graphics.DrawString(Caption, _captionFont, new SolidBrush(_captionColor), new Rectangle(IsIconOnLeft ? 45 : 603, Height / 2 - 30, titleBackgroundImage.Width - 10, 45), sf);
			}

			if (!string.IsNullOrEmpty(Description))
			{
				var sf = new StringFormat();
				sf.Alignment = StringAlignment.Center;
				pevent.Graphics.DrawString(Description, _descriptionFont, new SolidBrush(_descriptionColor), new Rectangle(IsIconOnLeft ? 50 : 608, Height / 2 + 10, titleBackgroundImage.Width - 20, 90), sf);
			}
			
			// draw Application logo
			if (!HideLogo)
			{
				var logo = Properties.Resources.logo_text;
				//pevent.Graphics.DrawImage(logo, 20, Height - 46, logo.Width, logo.Height);
			}
		}

		public void SetNavigationVisibility(bool value)
		{
			Navigation.Visible = value;
			//pchpbHealth.Location = Navigation.Visible ? new Point(288, 11) : new Point(332, 11);
		}

		public void SetState(int issueCount)
		{
			//pchpbHealth.SetState(issueCount);
		}
	}
}