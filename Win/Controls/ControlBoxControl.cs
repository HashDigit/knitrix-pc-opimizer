using System;
using System.Windows.Forms;

namespace MonolitForm
{
	public class ControlBoxControl : Panel
	{
		private enum State
		{
			None = 0, // default
			Min,
			Max,
			Close,
		}

		public event EventHandler Minimize;
		public event EventHandler Maximize;
		public event EventHandler Close;
        

		/// <summary>
		/// Button mouse currently over
		/// </summary>
		private State _state;

		public ControlBoxControl()
		{
			this.BackgroundImage = Win.Properties.Resources.buttons_normal;
			this.DoubleBuffered = true;
			this.MouseClick += new MouseEventHandler(this.ControlBoxControl_MouseClick);
			this.MouseMove += new MouseEventHandler(ControlBoxControl_MouseMove);
			this.MouseLeave += new EventHandler(ControlBoxControl_MouseLeave);
		}

		void ControlBoxControl_MouseLeave(object sender, System.EventArgs e)
		{
			if (_state != State.None)
				BackgroundImage = Win.Properties.Resources.buttons_normal;
			_state = State.None;
		}

		private void ControlBoxControl_MouseMove(object sender, MouseEventArgs e)
		{
			// 8,6 -> 31,21 Min
			// 33,6 -> 56,21 Max
			// 58,6 -> 98,21 Close
			if (e.X >= 8 && e.Y >= 6 &&
				e.X <= 31 && e.Y <= 21)
			{
				if (_state != State.Min)
					BackgroundImage = Win.Properties.Resources.min_rollover;
				_state = State.Min;
			}
			else if (e.X >= 33 && e.Y >= 6 &&
				e.X <= 56 && e.Y <= 21)
			{
				if (_state != State.Max)
					BackgroundImage = Win.Properties.Resources.max_rollover;
				_state = State.Max;
			}
			else if (e.X >= 58 && e.Y >= 6 &&
				e.X <= 98 && e.Y <= 21)
			{
				if (_state != State.Close)
					BackgroundImage = Win.Properties.Resources.close_rollover;
				_state = State.Close;
			}
			else
			{
				if (_state != State.None)
					BackgroundImage = Win.Properties.Resources.buttons_normal;
				_state = State.None;
			}
		}

		private void ControlBoxControl_MouseClick(object sender, MouseEventArgs e)
		{
			// 8,6 -> 31,21 Min
			// 33,6 -> 56,21 Max
			// 58,6 -> 98,21 Close
			if (e.X >= 8 && e.Y >= 6 &&
				e.X <= 31 && e.Y <= 21)
			{
				if (Minimize != null)
					Minimize(sender, e);
			}
			else if (e.X >= 33 && e.Y >= 6 &&
				e.X <= 56 && e.Y <= 21)
			{
				if (Maximize != null)
					Maximize(sender, e);
			}
			else if (e.X >= 58 && e.Y >= 6 &&
				e.X <= 98 && e.Y <= 21)
			{
				if (Close != null)
					Close(sender, e);
			}
		}
	}
}