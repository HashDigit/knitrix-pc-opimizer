﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class PictureBoxButton : PictureBox
	{
		/// <summary>
		/// Is Mouse cursor over button?
		/// </summary>
		private bool _isOver;
		public PictureBoxButton()
		{
			InitializeComponent();

			base.Image = ImageNormal;

			Caption = "";
			CaptionFont = new Font("Tahoma", 10F, GraphicsUnit.Pixel);
			CaptionColor = Color.Black;
			CaptionColorActive = Color.Black;
		}

		public bool IsActive
		{
			get
			{
				return _isActive;
			}
			set
			{
				_isActive = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}
		private bool _isActive;

		public Image ImageNormal
		{
			get
			{
				return _imageNormal;
			}
			set
			{
				_imageNormal = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}
		private Image _imageNormal;

		public Image ImageRollover
		{
			get
			{
				return _imageRollover;
			}
			set
			{
				_imageRollover = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}
		private Image _imageRollover;

		public Image ImageDisabled
		{
			get
			{
				return _imageDisabled;
			}
			set
			{
				_imageDisabled = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}
		private Image _imageDisabled;

		public Image ImageActive
		{
			get
			{
				return _imageActive;
			}
			set
			{
				_imageActive = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}
		private Image _imageActive;

		public new Image Image
		{
			get
			{
				return base.Image;
			}
			set
			{
				// do nothing, setting is fraud for Visual Studio Designer
			}
		}

		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}

		public string Caption
		{
			get
			{
				return _caption;
			}
			set
			{
				_caption = value;
				this.Invalidate();
			}
		}
		private string _caption;

		public Font CaptionFont
		{
			get
			{
				return _captionFont;
			}
			set
			{
				_captionFont = value;
				this.Invalidate();
			}

		}
		private Font _captionFont;

		public Color CaptionColor
		{
			get
			{
				return _captionColor;
			}
			set
			{
				_captionColor = value;
				this.Invalidate();
			}

		}
		private Color _captionColor;

		public Color CaptionColorActive
		{
			get
			{
				return _captionColorActive;
			}
			set
			{
				_captionColorActive = value;
				this.Invalidate();
			}

		}
		private Color _captionColorActive;

		private void PictureBoxButton_MouseEnter(object sender, EventArgs e)
		{
			_isOver = true;
			if (Enabled)
			{
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}

		private void PictureBoxButton_MouseLeave(object sender, EventArgs e)
		{
			_isOver = false;
			if (Enabled)
			{
				base.Image = IsActive ? ImageActive : (base.Enabled ? (_isOver ? ImageRollover : ImageNormal) : ImageDisabled);
				this.Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			var size = e.Graphics.MeasureString(Caption, CaptionFont);
			if (!string.IsNullOrEmpty(Caption))
				e.Graphics.DrawString(Caption, CaptionFont, 
					IsActive ? new SolidBrush(CaptionColorActive) : new SolidBrush(CaptionColor), 
					(Width - size.Width) / 2, (Height - size.Height) / 2 + 1);
		}
	}
}
