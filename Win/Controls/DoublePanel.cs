using System.Windows.Forms;

namespace Win.Controls
{
    public class DoublePanel : Panel
    {
        public DoublePanel()
        {
            //ControlStyle is enum of system.windows.forms
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
        }
    }
}