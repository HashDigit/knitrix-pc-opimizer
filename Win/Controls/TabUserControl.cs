﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Win.Controls
{
	public partial class TabUserControl : UserControl
	{
		public event EventHandler TabHeaderClick;

		public Panel TabPageHome
		{
			get
			{
				return pbbHome.Tag as Panel;
			}
			set
			{
				pbbHome.Tag = value;
			}
		}

		public Panel TabPageDisk
		{
			get
			{
				return pbbDisk.Tag as Panel;
			}
			set
			{
				pbbDisk.Tag = value;
			}
		}

		public Panel TabPageOptimize
		{
			get
			{
				return pbbOptimize.Tag as Panel;
			}
			set
			{
				pbbOptimize.Tag = value;
			}
		}

		public Panel TabPageSettings
		{
			get
			{
				return pbbSettings.Tag as Panel;
			}
			set
			{
				pbbSettings.Tag = value;
			}
		}

		public Panel TabPageInfo
		{
			get
			{
				return pbbInfo.Tag as Panel;
			}
			set
			{
				pbbInfo.Tag = value;
			}
		}

		public Panel TabPageSelected
		{
			get
			{
				return _tabPageSelected;
			}
			set
			{
				this.SuspendLayout();
				foreach (var tabPageHeader in _pbbTabPageHeaders)
				{
					if (tabPageHeader.Tag != null && tabPageHeader.Tag == value)
					{
						(tabPageHeader.Tag as Panel).BringToFront();
						_tabPageSelected = tabPageHeader.Tag as Panel;
					}
					tabPageHeader.IsActive = tabPageHeader.Tag != null && tabPageHeader.Tag == value;
				}
				this.ResumeLayout(false);

				//this.SuspendLayout();
				//foreach (var tabPageHeader in _pbbTabPageHeaders)
				//{
				//    if (tabPageHeader.Tag != null)
				//        if (tabPageHeader.Tag == value)
				//        {
				//            (tabPageHeader.Tag as Panel).Visible = true;
				//            _tabPageSelected = tabPageHeader.Tag as Panel;
				//        }
				//        else
				//        {
				//            (tabPageHeader.Tag as Panel).Visible = false;
				//        }
				//    tabPageHeader.IsActive = tabPageHeader.Tag != null && tabPageHeader.Tag == value;
				//}
				//this.ResumeLayout(false);
			}
		}
		private Panel _tabPageSelected;

		private List<PictureBoxButton> _pbbTabPageHeaders;

		public TabUserControl()
		{
			InitializeComponent();

			_pbbTabPageHeaders = new List<PictureBoxButton>();
			_pbbTabPageHeaders.Add(pbbHome);
			_pbbTabPageHeaders.Add(pbbDisk);
			_pbbTabPageHeaders.Add(pbbOptimize);
			_pbbTabPageHeaders.Add(pbbSettings);
			_pbbTabPageHeaders.Add(pbbInfo);

			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
		}

		private void pbb_Click(object sender, EventArgs e)
		{
			TabPageSelected = (sender as PictureBoxButton).Tag as Panel;

			// fire Click event
			if (this.TabHeaderClick != null)
				this.TabHeaderClick(this, new EventArgs());
		}
	}
}
