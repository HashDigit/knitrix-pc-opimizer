﻿namespace Win.Controls
{
	partial class RegularButton
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			CButtonLib.DesignerRectTracker designerRectTracker1 = new CButtonLib.DesignerRectTracker();
			CButtonLib.cBlendItems cBlendItems1 = new CButtonLib.cBlendItems();
			CButtonLib.DesignerRectTracker designerRectTracker2 = new CButtonLib.DesignerRectTracker();
			this.SuspendLayout();
			// 
			// cButton1
			// 
			this.BackColor = System.Drawing.Color.Transparent;
			this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
			designerRectTracker1.IsActive = false;
			this.CenterPtTracker = designerRectTracker1;
			cBlendItems1.iColor = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(191)))), ((int)(((byte)(193))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(23)))), ((int)(((byte)(24))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))))};
			cBlendItems1.iPoint = new float[] {
        0F,
        0.5F,
        1F};
			this.ColorFillBlend = cBlendItems1;
			this.ColorFillSolid = System.Drawing.SystemColors.Control;
			this.Corners.All = ((short)(4));
			this.Corners.LowerLeft = ((short)(4));
			this.Corners.LowerRight = ((short)(4));
			this.Corners.UpperLeft = ((short)(4));
			this.Corners.UpperRight = ((short)(4));
			this.FillType = CButtonLib.CButton.eFillType.GradientLinear;
			this.FillTypeLinear = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.FocalPoints.CenterPtX = 0.5258621F;
			this.FocalPoints.CenterPtY = 0.6F;
			this.FocalPoints.FocusPtX = 0F;
			this.FocalPoints.FocusPtY = 0F;
			designerRectTracker2.IsActive = false;
			this.FocusPtTracker = designerRectTracker2;
			this.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.Image = null;
			this.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ImageIndex = 0;
			this.ImageSize = new System.Drawing.Size(16, 16);
			this.Location = new System.Drawing.Point(243, 118);
			this.Name = "cButton1";
			this.Shape = CButtonLib.CButton.eShape.Rectangle;
			this.SideImage = null;
			this.SideImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.SideImageSize = new System.Drawing.Size(48, 48);
			this.Size = new System.Drawing.Size(116, 27);
			this.TabIndex = 79;
			this.Text = "Sample Text";
			this.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
			this.TextMargin = new System.Windows.Forms.Padding(2);
			this.TextShadow = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
			this.TextSmoothingMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
			this.UseMnemonic = true;
			this.ResumeLayout(false);
		}

		#endregion
	}
}
