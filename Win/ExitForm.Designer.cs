﻿namespace Win
{
	partial class ExitForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblText = new System.Windows.Forms.Label();
			this.cbDontAskAgain = new System.Windows.Forms.CheckBox();
			this.pbLogo = new System.Windows.Forms.PictureBox();
			this.btnExit = new Win.Controls.RegularButton();
			this.btnMinimize = new Win.Controls.RegularButton();
			this.btnCancel = new Win.Controls.RegularButton();
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
			this.SuspendLayout();
			// 
			// lblText
			// 
			this.lblText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblText.AutoSize = true;
			this.lblText.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.lblText.Location = new System.Drawing.Point(56, 22);
			this.lblText.Name = "lblText";
			this.lblText.Size = new System.Drawing.Size(398, 14);
			this.lblText.TabIndex = 0;
			this.lblText.Text = "Would you like to close Knitrix PC Optimizer or minimize it to the system tray?";
			// 
			// cbDontAskAgain
			// 
			this.cbDontAskAgain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cbDontAskAgain.AutoSize = true;
			this.cbDontAskAgain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.cbDontAskAgain.Location = new System.Drawing.Point(12, 65);
			this.cbDontAskAgain.Name = "cbDontAskAgain";
			this.cbDontAskAgain.Size = new System.Drawing.Size(116, 17);
			this.cbDontAskAgain.TabIndex = 1;
			this.cbDontAskAgain.Text = "Don\'t ask me again";
			this.cbDontAskAgain.UseVisualStyleBackColor = true;
			// 
			// pbLogo
			// 
			this.pbLogo.Image = global::Win.Properties.Resources.Exit_32x32;
			this.pbLogo.Location = new System.Drawing.Point(12, 12);
			this.pbLogo.Name = "pbLogo";
			this.pbLogo.Size = new System.Drawing.Size(32, 32);
			this.pbLogo.TabIndex = 5;
			this.pbLogo.TabStop = false;
			// 
			// btnExit
			// 
			this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExit.BackColor = System.Drawing.Color.Transparent;
			this.btnExit.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnExit.ForeColor = System.Drawing.Color.White;
			this.btnExit.Location = new System.Drawing.Point(204, 55);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(72, 27);
			this.btnExit.TabIndex = 79;
			this.btnExit.Text = "Exit";
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// btnMinimize
			// 
			this.btnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnMinimize.BackColor = System.Drawing.Color.Transparent;
			this.btnMinimize.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnMinimize.ForeColor = System.Drawing.Color.White;
			this.btnMinimize.Location = new System.Drawing.Point(282, 55);
			this.btnMinimize.Name = "btnMinimize";
			this.btnMinimize.Size = new System.Drawing.Size(102, 27);
			this.btnMinimize.TabIndex = 80;
			this.btnMinimize.Text = "Minimize";
			this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI Symbol", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.btnCancel.ForeColor = System.Drawing.Color.White;
			this.btnCancel.Location = new System.Drawing.Point(390, 55);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(91, 27);
			this.btnCancel.TabIndex = 81;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// ExitForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(493, 94);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnMinimize);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.pbLogo);
			this.Controls.Add(this.cbDontAskAgain);
			this.Controls.Add(this.lblText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ExitForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Exit Options";
			((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblText;
		private System.Windows.Forms.CheckBox cbDontAskAgain;
		private System.Windows.Forms.PictureBox pbLogo;
		private Controls.RegularButton btnExit;
		private Controls.RegularButton btnMinimize;
		private Controls.RegularButton btnCancel;
	}
}