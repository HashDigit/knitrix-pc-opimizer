﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Business;
using Business.Processes;
using Business.Registry;
using Business.UninstallManager;
using Common;
using Common_Tools.TreeViewAdv.Tree;
using Core.Business;
using Data;
using Data.Registry;
using Data.UninstallManager;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using Win.DiskCleaner;
using Win.DiskCleaner.Misc;
using Win.Misc;
using Common.Xml;
using Win.Controls;
using Win.ScanSchedule;
using Win.ShortcutFixer;
using Win.StartupManager;
using License = Business.License;
using System.Net.NetworkInformation;
using System.Net;
using System.Management;
using System.Linq;
using Business.TaskScheduler;

namespace Win
{
	public partial class MainForm : BaseForm
	{
		/// <summary>
		/// Main user commands that user can send to application.
		/// 'IsEnable' property is show ability to execute this command now.
		/// </summary>
		private Dictionary<UserCommandTypeEnum, UserCommand> _userCommands;

		private int _tickIndex;
		private bool _forceClose;

		/// <summary>
		/// Used to show animation while One-Click Scanning
		/// </summary>
		private int _oneClickScanAnimationIndexer = 0;

		private MainDS _ds = new MainDS();

		private const int OPTIMIZE_STARTUP_MANAGER_IMAGE_INDEX = 1;
		private const int OPTIMIZE_PROCESS_IMAGE_INDEX = 0;
		private const int HOME_SCANED_ITEMS_IMAGE_INDEX = 0;
		private const int HOME_SCANED_ITEMS_ACTION_INDEX = 4;
		private const int OPTIMIZE_REGISTRY_ENTRIES_ACTION_INDEX = 3;

		/// <summary>
        /// The process selected in the grid on the "Optimize Windows"->"Process Manager"
		/// </summary>
		private MainDS.ProcessInfoRow _selectedProcessInfo;

		/// <summary>
        /// The current panel, which is open to the user.
		/// </summary>
		private DoublePanel _currentPanel;

		/// <summary>
        /// Model list of bad registers on page "Window Optimize"
		/// </summary>
		private TreeModel tmOptimizeRegistryManager = new TreeModel();

		/// <summary>
		/// Settings->PrivacySettings
        /// Directory lists of private data
		/// </summary>
		private TreeModel tmSettingsPrivacySettings = new TreeModel();

		/// <summary>
        /// Model list of bad registers on page "Home Step2"
		/// </summary>
		private TreeModel tmHomeStep2Problem = new TreeModel();

		/// <summary>
        /// Model Startup list item is
		/// </summary>
		private TreeModel tmOptimizeStartupManager = new TreeModel();

		private bool _disableCheckedChangedHandler; // flag that allows checkboxes initing without execution event handlers
		private bool _disableGvOptimizeProcessHandler;

		public MainForm()
		{
			InitializeComponent();
			Icon = Properties.Resources.logo2;
			niMain.Icon = Properties.Resources.logo2;
		}

		#region Event Handlers
		private void MainForm_Load(object sender, EventArgs e)
		{
			#region GUI
			// TEMP :: (AK) START приведение GUI в нужному при старте виду: вывод нужных панелей поверх
			BringToFront(pnlHomeMain);
			//navHome.Visible = false;

			BringToFront(pnlOptimizeMain);
			//navOptimize.Visible = false;

			BringToFront(pnlDiskMain);
			//navDisk.Visible = false;

			BringToFront(pnlSettingsMain);
			//navSettings.Visible = false;

			pnlHomeInfo.SendToBack();
			// TEMP :: (AK) END

			// headers
			//pnlOptimizeHeader.Top = 70;
			//pnlOptimizeHeader.BringToFront();
			//pnlDiskHeader.Top = 70;
			//pnlDiskHeader.BringToFront();
			//pnlSettingsHeader.Top = 70;
			//pnlSettingsHeader.BringToFront();

			// 
			gvOptimizeProcess.AutoGenerateColumns = false;
			tvaSettingsPrivacySettings.ExpandAll();
			treeView1.ExpandAll();

			smdcOptimizeSelectedProcess.Init(2);

			this.tvaOptimizeRegistryManager.Model = new SortedTreeModel(this.tmOptimizeRegistryManager);
			this.tvaHomeStep2Problem.Model = new SortedTreeModel(this.tmHomeStep2Problem);
			this.tvaSettingsPrivacySettings.Model = new SortedTreeModel(this.tmSettingsPrivacySettings);

			//this.tvaHomeStep3Problem.Model = new SortedTreeModel(this.tmHomeStep2Problem);
			//this.tvaHomeStep3Problem.Model = new SortedTreeModel(DataContext.OneClickScanState.RegistryItemsTreeModel);

			//lblOptimizeTitle.Text = Properties.Resources.OptimizeTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeDescription;
			//lblDiskTitle.Text = Properties.Resources.DiskTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskDescription;
			//lblSettingsTitle.Text = Properties.Resources.SettingsTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsDescription;
			lblDiskShortcutFixerStatus.Text = "";

			SetHealthState(-1);

			btnDiskCreateRestorePoint.Enabled = SystemRestorePointLogic.SysRestoreAvailable();

			pnlHomeInfo.Size = pnlHomeMain.Size;

			//btnHomeRegister.Visible = Context.License.IsTrial;

			// taskbar
			//this.ShowInTaskbar = !Options.Instance.IsAlwaysInTray; // (AK) slowly???

			InitUserCommands();

			//// skin // (AK) slowly??
			//SetSkinAction(Win.Options.Instance.DiskCleanerSettings.LookAndFeelSkinName,
			//    Win.Options.Instance.DiskCleanerSettings.LookAndFeelStyle,
			//    Win.Options.Instance.DiskCleanerSettings.LookAndFeelUseWindowsXPTheme,
			//    Win.Options.Instance.DiskCleanerSettings.SkinMenuIndex);
			pnlHomeStep2.SetNavigationVisibility(false);
			pnlHomeStep3.SetNavigationVisibility(false);
			pnlHomeStep3.Navigation.Back += new EventHandler(Navigation_Back);

			pnlOptimizeBrowseObjectManager.Navigation.Back += new EventHandler(Navigation_Back);
			pnlOptimizeOptimizeWindows.Navigation.Back += new EventHandler(Navigation_Back);
			pnlOptimizeProcessManager.Navigation.Back += new EventHandler(Navigation_Back);
			pnlOptimizeRegistryManager.Navigation.Back += new EventHandler(Navigation_Back);
			pnlOptimizeStartupManager.Navigation.Back += new EventHandler(Navigation_Back);
			pnlOptimizeRegistryDefragmenter.Navigation.Back += new EventHandler(Navigation_Back);

			pnlDiskDiskCleaner.Navigation.Back += new EventHandler(Navigation_Back);
			pnlDiskRestorePointManager.Navigation.Back += new EventHandler(Navigation_Back);
			pnlDiskShortcutFixer.Navigation.Back += new EventHandler(Navigation_Back);
			pnlDiskUninstallManager.Navigation.Back += new EventHandler(Navigation_Back);

			pnlSettingsGeneralSettings.Navigation.Back += new EventHandler(Navigation_Back);
			pnlSettingsPrivacySettings.Navigation.Back += new EventHandler(Navigation_Back);
			pnlSettingsRegistrySettings.Navigation.Back += new EventHandler(Navigation_Back);
			pnlSettingsScanSchedule.Navigation.Back += new EventHandler(Navigation_Back);

			pnlHomeInfo.Navigation.Back += new EventHandler(Navigation_Back);

			pnlHomeMain.RefreshBtnRegister();
			pnlOptimizeMain.RefreshBtnRegister();
			pnlDiskMain.RefreshBtnRegister();
			pnlSettingsMain.RefreshBtnRegister();

			// title and position
			this.Text = Application.ProductName;

			tmrMain.Start();

			#endregion GUI

			CheckForUpdateLogic.ProgressChanged += new DownloadProgressHandler(CheckForUpdateLogic_ProgressChanged);
			CheckForUpdateLogic.Aborted += new EventHandler(CheckForUpdateLogic_Aborted);
			CheckForUpdateLogic.Finished += new EventHandler(CheckForUpdateLogic_Finished);

			#region One-Click Scan
			OneClickScanLogic.ScanRegistryFinished += new EventHandler(OneClickScanLogic_ScanRegistryFinished);
			OneClickScanLogic.ScanProcessFinished += new EventHandler(OneClickScanLogic_ScanProcessFinished);
			OneClickScanLogic.ScanPrivacyFinished += new EventHandler(OneClickScanLogic_ScanPrivacyFinished);
			OneClickScanLogic.ScanFinished += new EventHandler(OneClickScanLogic_ScanFinished);
			OneClickScanLogic.ScanAborted += new EventHandler(OneClickScanLogic_ScanAborted);

			OneClickScanLogic.FixFinished += new EventHandler(OneClickScanLogic_FixFinished);
			OneClickScanLogic.FixAborted += new EventHandler(OneClickScanLogic_FixAborted);
			#endregion One-Click Scan

			SettingsRegistryScanSettingsRefresh();
			SettingsGeneralSettingsRefresh();
			SettingsPrivacySettingsRefresh();
			OptmizeOptimizeWindowsRefresh();

			//SetExpiredTrialGuiIfRequired();
			CheckIsInBlackList();

			if (Context.AutoStartScan)
			{
				// start One-Click scan
				StartScanAction();
			}
		}

		private void Navigation_Back(object sender, EventArgs e)
		{
			if (sender != pnlHomeStep3.Navigation)
			{
				(sender as NavigationControl).Parent.SendToBack();
			}
			else
			{
				osgHomeStep3OverallScanResult.BringToFront();
				pnlHomeStep3.SetNavigationVisibility(false);
			}
			//HomeBackAction();
		}

		private void MainForm_SizeChanged(object sender, EventArgs e)
		{
			//this.Visible = (this.WindowState != FormWindowState.Minimized);
		}

		private void niMain_DoubleClick(object sender, EventArgs e)
		{
			this.Visible = !this.Visible;
			niMain.Visible = !this.Visible;
			if (this.Visible && this.WindowState == FormWindowState.Minimized)
				this.WindowState = FormWindowState.Normal;

			if (this.Visible)
			{
				this.Activate();
				this.Focus();
				this.Select();
			}
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_forceClose)
				return;

			if (Options.Instance.GeneralSettings.DisplayExitActions)
			{
				if (e.CloseReason == CloseReason.UserClosing)
				{
					new ExitForm().ShowDialog();
					e.Cancel = true;
				}
			}
			else if (Options.Instance.GeneralSettings.MinimizeToTray)
			{
				if (e.CloseReason == CloseReason.UserClosing)
				{
					MinimizeToTray();
					e.Cancel = true;
				}
			}
		}

		private void tmrMain_Tick(object sender, EventArgs e)
		{
			//pbHomeMainImage.Image = _images[_tickIndex % _images.Count];
			_tickIndex++;

			if (_currentPanel == pnlOptimizeProcessManager)
			{
				// gvOptimizeProcess
				//var simpleDelegate = new MethodInvoker(UpdateProcessInfo);
				//simpleDelegate.BeginInvoke(null, null);
				//this.gvOptimizeProcess.SelectionChanged -= new System.EventHandler(this.gvOptimizeProcess_SelectionChanged);
				try
				{
					_disableGvOptimizeProcessHandler = true;
					var vOffset = gvOptimizeProcess.FirstDisplayedScrollingRowIndex;
					int selectedProcessId = 0;
					if (gvOptimizeProcess.SelectedRows.Count > 0)
						selectedProcessId = ((MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row).ProcessId;

					// TODO :: (AK) вызывать в другом потоке, чтобы оптимизировать производительность
					ProcessInfoLogic.ListInDataContext();
					//this.gvOptimizeProcess.SelectionChanged += new System.EventHandler(this.gvOptimizeProcess_SelectionChanged);
					if (gvOptimizeProcess.DataSource == null)
					{
						gvOptimizeProcess.DataSource = DataContext.MainDS.ProcessInfo;
						gvOptimizeProcess.Sort(col_OptimizeWindow_Process_ProcessName, ListSortDirection.Ascending);
					}
					//UpdateProcessInfo_ThreadSafe(vOffset);
					// restore grid position and selected row
					if (vOffset > -1)
						gvOptimizeProcess.FirstDisplayedScrollingRowIndex = vOffset;
					if (selectedProcessId != 0)
						foreach (DataGridViewRow row in gvOptimizeProcess.Rows)
						{
							if (selectedProcessId == ((MainDS.ProcessInfoRow)(row.DataBoundItem as DataRowView).Row).ProcessId)
							{
								row.Selected = true;
								break;
							}
						}
				}
				finally
				{
					_disableGvOptimizeProcessHandler = false;
				}

				// (Wilson) :: if no processes are unwanted/malware, then "Optimize All" button should be grayed out.
				btnOptimizeOptimizeAll.Enabled = DataContext.FoundUnwantedProcess;
				// (AK)
				//if (!btnOptimizeOptimizeAll.Enabled)
				//    btnOptimizeOptimizeAll.Image = Properties.Resources.optimize_inactive;

				// SystemMonitor
				var simpleDelegate = new MethodInvoker(UpdateSystemMonitorInfo);
				simpleDelegate.BeginInvoke(null, null);
			}
			GC.Collect();
		}

		private void btnHelp_Click(object sender, EventArgs e)
		{
			// deselect 
			tabMain.Select();
			HelpAction();
		}

		private void btnInfo_Click(object sender, EventArgs e)
		{
			// deselect 
			tabMain.Select();
			InfoAction();
		}

		private void btnOptimizeEndProcess_Click(object sender, EventArgs e)
		{
			OptimizeEndProcessAction();
		}

		private void btnOptimizeOpenFolder_Click(object sender, EventArgs e)
		{
			OptimizeOpenFolderAction();
		}

		private void btnOptimizeProperties_Click(object sender, EventArgs e)
		{
			OptimizePropertiesAction();
		}

		private void btnOptimizeRegistryManagerIgnoreList_Click(object sender, EventArgs e)
		{
			ShowIgnoreListManagerFormAction();
		}

		private void btnOptimizeRegistryManagerFixAll_Click(object sender, EventArgs e)
		{
			OptimizeRegistryManagerFixAll();
		}

		private void btnOptimizeOptimizeAll_Click(object sender, EventArgs e)
		{
			OptimizeWindowProcessManagerOptimizeAllAction();
		}

		private void tabMain_Click(object sender, EventArgs e)
		{
			if (tabMain.TabPageSelected == tabMain.TabPageHome)
				pnlHomeMain.BringToFront();
			if (tabMain.TabPageSelected == tabMain.TabPageDisk)
				pnlDiskMain.BringToFront();
			if (tabMain.TabPageSelected == tabMain.TabPageOptimize)
				pnlOptimizeMain.BringToFront();
			if (tabMain.TabPageSelected == tabMain.TabPageSettings)
				pnlSettingsMain.BringToFront();
			// control can be hidden by Info page, so move control to front
			pnlHomeInfo.SendToBack();
		}

		#region Optimize Windows -> Processes
		/// <summary>
		/// User selected process row on Processes grid on "Process Manager" tab
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void gvOptimizeProcess_SelectionChanged(object sender, EventArgs e)
		{
			if (_disableGvOptimizeProcessHandler)
				return;

			if (gvOptimizeProcess.SelectedRows.Count < 1)
				return;

			var selectedProcessInfo = (MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row;
			lblOptimizeProcessIdValue.Text = selectedProcessInfo.ProcessId.ToString();
			lblOptimizeCompanyValue.Text = selectedProcessInfo.Company;
			lblOptimizePriorityValue.Text = selectedProcessInfo.Priority.ToString();
			lblOptimizeHandlesValue.Text = selectedProcessInfo.Handles.ToString();
			lblOptimizeThreadsValue.Text = selectedProcessInfo.Threads.ToString();
			lblOptimizeTotalProcessorTimeValue.Text = selectedProcessInfo.FormattedTotalProcessorTime;
			lblOptimizeVirtualMemorySizeValue.Text = selectedProcessInfo.FormattedVirtualMemory.ToString();
			pbOptimizeProcessImage.Image = selectedProcessInfo.BigImage;
			groupOptimizeProcessDetails.Text = string.Format("Process: {0}", selectedProcessInfo.ProcessName);

			btnOptimizeProperties.Enabled = !string.IsNullOrEmpty(selectedProcessInfo.Path);
			btnOptimizeOpenFolder.Enabled = !string.IsNullOrEmpty(selectedProcessInfo.Path);
			btnOptimizeEndProcess.Enabled = !string.IsNullOrEmpty(selectedProcessInfo.Path);
			// (AK)
			/*
			if (string.IsNullOrEmpty(selectedProcessInfo.Path))
			{
				btnOptimizeProperties.Image = Properties.Resources.properties_disabled;
				btnOptimizeOpenFolder.Image = Properties.Resources.open_folder_disabled;
				btnOptimizeEndProcess.Image = Properties.Resources.end_process_disabled;
			}
			else
			{
				btnOptimizeProperties.Image = Properties.Resources.properties_normal;
				btnOptimizeOpenFolder.Image = Properties.Resources.open_normal;
				btnOptimizeEndProcess.Image = Properties.Resources.end_normal;
			}
			*/
			if (_selectedProcessInfo != null && _selectedProcessInfo.ProcessName != selectedProcessInfo.ProcessName)
				smdcOptimizeSelectedProcess.ClearChart();
			_selectedProcessInfo = selectedProcessInfo;
		}

		#region gvOptimizeProcess
		private void gvOptimizeProcess_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.ColumnIndex == 0) // InfoTypeImage column
			{
				if (gvOptimizeProcess.SortedColumn != col_OptimizeWindow_Process_InfoType)
					gvOptimizeProcess.Sort(col_OptimizeWindow_Process_InfoType, ListSortDirection.Ascending);
				else
					gvOptimizeProcess.Sort(col_OptimizeWindow_Process_InfoType,
										   gvOptimizeProcess.SortOrder == SortOrder.Ascending
											? ListSortDirection.Descending
											: ListSortDirection.Ascending);
			}
		}
		#endregion gvOptimizeProcess
		#endregion Optimize Windows -> Processes


		private void btnHomeDiskCleaner_Click(object sender, EventArgs e)
		{
			DiskCleanerAction();
		}

		private void btnRegister_Click(object sender, EventArgs e)
		{
			HomeRegisterAction();
		}

		private void btnPushToTalkClick_Click(object sender, EventArgs e)
		{
			PushToTalkAction();
		}
		#endregion Event Handlers

		#region Menu handlers
		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ExitAction();
		}
		#endregion Menu handlers

		#region Buttons Handlers

		private void btnOptimizeOptimizeWindows_Click(object sender, EventArgs e)
		{
			OptimizeWindowsAction();
		}

		private void btnOptimizeStartupManager_Click(object sender, EventArgs e)
		{
			StartupManagerAction();
		}

		private void btnOptimizeProcessManager_Click(object sender, EventArgs e)
		{
			ProcessManagerAction();
		}

		private void btnOptimizeBrowserObjectManager_Click(object sender, EventArgs e)
		{
			BrowserObjectManagerAction();
		}

		private void btnOptimizeRegistryManager_Click(object sender, EventArgs e)
		{
			RegistryManagerAction();
		}

		private void btnOptimizeRegistryDefragmenter_Click(object sender, EventArgs e)
		{
			RegistryDefragmenterAction();
		}

		private void btnOptimizeRegistryManagerScan_Click(object sender, EventArgs e)
		{
			ScanRegistryAction();
		}

		private void btnDiskRestorePointManager_Click(object sender, EventArgs e)
		{
			RestorePointManagerAction();
		}

		private void btnDiskClearTempraryFiles_Click(object sender, EventArgs e)
		{
			ClearTemporaryFilesAction();
		}

		private void btnDiskClearRecentHistory_Click(object sender, EventArgs e)
		{
			ClearRecentHistoryAction();
		}

		private void btnDiskDiskCleaner_Click(object sender, EventArgs e)
		{
			DiskCleanerAction();
		}

		private void btnSettingsGeneralSettings_Click(object sender, EventArgs e)
		{
			GeneralSettingsAction();
		}

		private void btnSettingsScanSchedule_Click(object sender, EventArgs e)
		{
			ScanScheduleAction();
		}

		private void btnSettingsRegistrySettings_Click(object sender, EventArgs e)
		{
			RegistrySettingsAction();
		}

		private void btnSettingsPrivacySettings_Click(object sender, EventArgs e)
		{
			PrivacySettingsAction();
		}

		private void btnHomeShortcutFixer_Click(object sender, EventArgs e)
		{
			ShortcutFixerAction();
		}

		private void btnDiskShortcutFixer_Click(object sender, EventArgs e)
		{
			ShortcutFixerAction();
		}

		private void btnDiskUninstallManager_Click(object sender, EventArgs e)
		{
			UninstallManagerAction();
		}

		#endregion Buttons Handlers

		#region Menu Actions
		private void ExitAction()
		{
			Application.Exit();
		}

		public void MinimizeToTray()
		{
			this.Visible = !this.Visible;
			niMain.Visible = !this.Visible;
			if (this.Visible && this.WindowState == FormWindowState.Minimized)
				this.WindowState = FormWindowState.Normal;
		}
		#endregion Menu Actions

		#region Main Actions

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Optimize Windows"
		/// </summary>
		private void OptimizeWindowsAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//lblOptimizeTitle.Text = Properties.Resources.OptimizeOptimizeWindowsTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeOptimizeWindowsDescription;
			//pnlOptimizeMainImage.BackgroundImage = Win.Properties.Resources.optimize_logo;

			BringToFront(pnlOptimizeOptimizeWindows);

			//pnlOptimizeHeader.Top = 3;
			//pnlOptimizeHeader.BorderWidth = 0;
			//pnlOptimizeHeader.BringToFront();

			//navOptimize.IsBackEnabled = true;
			//navOptimize.IsForwardEnabled = false;
			//navOptimize.BringToFront();
			//navOptimize.Visible = true;

			tabMain.TabPageSelected = pnlOptimize;

			// business
		}

		/// <summary>
		/// Пользователь нажал Back на панели "Optimize Windows"
		/// </summary>
		private void OptimizeBackAction()
		{
			//lblOptimizeTitle.Text = Properties.Resources.OptimizeTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeDescription;
			//pnlOptimizeMainImage.BackgroundImage = Win.Properties.Resources.tuneup_icon;
			//pnlOptimizeHeader.BorderWidth = 6;
			//pnlOptimizeHeader.Top = 70;

			//navOptimize.Visible = false;
			BringToFront(pnlOptimizeMain);

			//pnlOptimizeHeader.BringToFront();
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Startup Manager"
		/// </summary>
		private void StartupManagerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//pnlOptimizeHeader.BorderWidth = 0;
			//pnlOptimizeHeader.Top = 3;
			//pnlOptimizeHeader.BringToFront();

			//navOptimize.IsBackEnabled = true;
			//navOptimize.IsForwardEnabled = false;
			//navOptimize.Visible = true;
			//navOptimize.BringToFront();

			//lblOptimizeTitle.Text = Properties.Resources.OptimizeStartupManagerTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeStartupManagerDescription;
			//pnlOptimizeMainImage.BackgroundImage = Properties.Resources.startup_icon_without_text;

			// business
			FillTvaOptimizeStartupManager();

			BringToFront(pnlOptimizeStartupManager);

			tabMain.TabPageSelected = pnlOptimize;

		}


		/// <summary>
        /// Action presents when you click on "Process Manager"
		/// </summary>
		private void ProcessManagerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//pnlOptimizeHeader.BorderWidth = 0;
			//pnlOptimizeHeader.Top = 3;
			//pnlOptimizeHeader.BringToFront();

			//navOptimize.IsBackEnabled = true;
			//navOptimize.IsForwardEnabled = false;
			//navOptimize.Visible = true;
			//navOptimize.BringToFront();

			//lblOptimizeTitle.Text = Properties.Resources.OptimizeProcessManagerTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeProcessManagerDescription;
			//pnlOptimizeMainImage.BackgroundImage = Properties.Resources.process_manager_icon;

			BringToFront(pnlOptimizeProcessManager);

			tabMain.TabPageSelected = pnlOptimize;

			// business
		}

		/// <summary>
        /// Action presents when you click on "Browser Object Manager"
		/// </summary>
		private void BrowserObjectManagerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// business
			FillLvOptimizeBrowseObjectManager();

			// gui
			//pnlOptimizeHeader.BorderWidth = 0;
			//pnlOptimizeHeader.Top = 3;
			//pnlOptimizeHeader.BringToFront();

			//navOptimize.IsBackEnabled = true;
			//navOptimize.IsForwardEnabled = false;
			//navOptimize.Visible = true;
			//navOptimize.BringToFront();

			//lblOptimizeTitle.Text = Properties.Resources.OptimizeBrowserObjectManagerTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeBrowserObjectManagerDescription;
			//pnlOptimizeMainImage.BackgroundImage = Properties.Resources.browser_header_object_manager;

			BringToFront(pnlOptimizeBrowseObjectManager);

			tabMain.TabPageSelected = pnlOptimize;
		}

		/// <summary>
        /// Action presents when you click on "RegistryManager"
		/// </summary>
		private void RegistryManagerAction()
		{
			//if (License.Instance.ProductEdition != ProductEditionEnum.Pro || License.Instance.LicenseState != LicenseStateEnum.Registered)
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//pnlOptimizeHeader.BorderWidth = 0;
			//pnlOptimizeHeader.Top = 3;
			//pnlOptimizeHeader.BringToFront();

			//navOptimize.IsBackEnabled = true;
			//navOptimize.IsForwardEnabled = false;
			//navOptimize.Visible = true;
			//navOptimize.BringToFront();

			//lblOptimizeTitle.Text = Properties.Resources.OptimizeRegistryManagerTitle;
			//lblOptimizeDescription_ToDel.Text = Properties.Resources.OptimizeRegistryManagerDescription;
			//pnlOptimizeMainImage.BackgroundImage = Win.Properties.Resources.registry_icon_without_text;

			BringToFront(pnlOptimizeRegistryManager);

			tabMain.TabPageSelected = pnlOptimize;

			// business
		}

		/// <summary>
        /// Action presents when you click on "RegistryDefragmenter"
		/// </summary>
		private void RegistryDefragmenterAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}

			BringToFront(pnlOptimizeRegistryDefragmenter);

			tabMain.TabPageSelected = pnlOptimize;
		}

		/// <summary>
        ///Action presents when you click on "Restore Point Manager"
		/// </summary>
		private void RestorePointManagerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// business
			FillLvDiskRestorePointManager();

			// gui
			//navDisk.IsBackEnabled = true;
			//navDisk.IsForwardEnabled = false;
			//navDisk.Visible = true;

			//pnlDiskHeader.BorderWidth = 0;
			//pnlDiskHeader.Top = 3;
			//pnlDiskHeader.BringToFront();

			//lblDiskTitle.Text = Properties.Resources.DiskRestorePointManagerTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskRestorePointManagerDescription;
			//pnlDiskMainImage.BackgroundImage = Win.Properties.Resources.restore_point_logo;

			BringToFront(pnlDiskRestorePointManager);

			tabMain.TabPageSelected = pnlDisk;
		}

		/// <summary>
        /// The user clicks on the Back Panel "Disk"
		/// </summary>
		private void DiskBackAction()
		{
			//lblDiskTitle.Text = Properties.Resources.DiskTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskDescription;
			//pnlDiskMainImage.BackgroundImage = Win.Properties.Resources.disk_logo;
			//pnlDiskHeader.BorderWidth = 6;
			//pnlDiskHeader.Top = 70;

			//navDisk.Visible = false;
			BringToFront(pnlDiskMain);

			//pnlDiskHeader.BringToFront();
		}

		/// <summary>
        /// Action presents when you click on "Clear Temporary Files"
		/// </summary>
		private void ClearTemporaryFilesAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			if (MessageBox.Show(this, "Please confirm you wish to delete Windows temporary files to save disk space.", Application.ProductName, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
			{
				// business
				TemporaryFilesLogic.Clear();
				MessageBox.Show(this, "Your temporary files have been cleared.", Application.ProductName);
			}
		}

		/// <summary>
        /// Action presents when you click on "Clear Recent History"
		/// </summary>
		private void ClearRecentHistoryAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			if (MessageBox.Show(this, "Please confirm you wish to delete your Windows and Internet history information.", Application.ProductName, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
			{
				// business
				RecentHistoryLogic.Clear();
				MessageBox.Show(this, "Your recent files have been cleared.", Application.ProductName);
			}
		}

		/// <summary>
        /// Action presents when you click on "Duplicate Finder"
		/// </summary>
		private void DiskCleanerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//navDisk.IsBackEnabled = true;
			//navDisk.IsForwardEnabled = false;
			//navDisk.Visible = true;

			//pnlDiskHeader.BorderWidth = 0;
			//pnlDiskHeader.Top = 3;
			//pnlDiskHeader.BringToFront();

			//lblDiskTitle.Text = Properties.Resources.DiskDiskCleanerTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskDiskCleanerDescription;
			//pnlDiskMainImage.BackgroundImage = Win.Properties.Resources.disk_cleaner;

			BringToFront(pnlDiskDiskCleaner);

			tabMain.TabPageSelected = pnlDisk;

			// business
		}

		/// <summary>
        /// Action presents when you click on "General Settings"
		/// </summary>
		private void GeneralSettingsAction()
		{
			// gui
			//lblSettingsTitle.Text = Properties.Resources.SettingsGeneralSettingsTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsGeneralSettingsDescription;
			//pnlSettingsMainImage.BackgroundImage = Win.Properties.Resources.general_logo;

			BringToFront(pnlSettingsGeneralSettings);

			//pnlSettingsHeader.Top = 3;
			//pnlSettingsHeader.BorderWidth = 0;
			//pnlSettingsHeader.BringToFront();

			//navSettings.IsBackEnabled = true;
			//navSettings.IsForwardEnabled = false;
			//navSettings.Visible = true;
			//navSettings.BringToFront();

			tabMain.TabPageSelected = pnlSettings;

			// business
		}

		/// <summary>
        /// The user clicks on the Back Panel "Settings"
		/// </summary>
		private void SettingsBackAction()
		{
			//lblSettingsTitle.Text = Properties.Resources.SettingsTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsDescription;
			//pnlSettingsMainImage.BackgroundImage = Win.Properties.Resources.settings_logo;
			//pnlSettingsHeader.BorderWidth = 6;
			//pnlSettingsHeader.Top = 70;

			//navSettings.Visible = false;
			//BringToFront(pnlSettingsMain);

			//pnlSettingsHeader.BringToFront();
		}

		/// <summary>
        /// Action presents when you click on "Scan Schedule"
		/// </summary>
		private void ScanScheduleAction()
		{
			// gui
			//lblSettingsTitle.Text = Properties.Resources.SettingsScanScheduleTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsScanScheduleDescription;
			//pnlSettingsMainImage.BackgroundImage = Win.Properties.Resources.scan_schedule_logo;

			SettingsScanScheduleRefresh();

			BringToFront(pnlSettingsScanSchedule);

			//pnlSettingsHeader.Top = 3;
			//pnlSettingsHeader.BorderWidth = 0;
			//pnlSettingsHeader.BringToFront();

			//navSettings.IsBackEnabled = true;
			//navSettings.IsForwardEnabled = false;
			//navSettings.Visible = true;
			//navSettings.BringToFront();

			tabMain.TabPageSelected = pnlSettings;
		}

		/// <summary>
        /// Action presents when you click on "Registry Settings"
		/// </summary>
		private void RegistrySettingsAction()
		{
			// gui
			//lblSettingsTitle.Text = Properties.Resources.SettingsRegistrySettingsTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsRegistrySettingsDescription;
			//pnlSettingsMainImage.BackgroundImage = Win.Properties.Resources.registry_setting_logo;

			BringToFront(pnlSettingsRegistrySettings);

			//pnlSettingsHeader.Top = 3;
			//pnlSettingsHeader.BorderWidth = 0;
			//pnlSettingsHeader.BringToFront();

			//navSettings.IsBackEnabled = true;
			//navSettings.IsForwardEnabled = false;
			//navSettings.Visible = true;
			//navSettings.BringToFront();

			tabMain.TabPageSelected = pnlSettings;

			// business
		}

		/// <summary>
        /// Action presents when you click on "Privacy Settings"
		/// </summary>
		private void PrivacySettingsAction()
		{
			// gui
			//pnlSettingsHeader.BorderWidth = 0;
			//pnlSettingsHeader.Top = 3;
			//pnlSettingsHeader.BringToFront();

			//navSettings.IsBackEnabled = true;
			//navSettings.IsForwardEnabled = false;
			//navSettings.Visible = true;
			//navSettings.BringToFront();

			//lblSettingsTitle.Text = Properties.Resources.SettingsPrivacySettingsTitle;
			//lblSettingsDescription.Text = Properties.Resources.SettingsPrivacySettingsDescription;
			//pnlSettingsMainImage.BackgroundImage = Win.Properties.Resources.privacy_logo;

			BringToFront(pnlSettingsPrivacySettings);

			tabMain.TabPageSelected = pnlSettings;

			// business
		}

		/// <summary>
        /// The user clicks on the Back Panel "Overall Scan Results"
		/// </summary>
		private void HomeBackAction()
		{
			//lblHomeTitle.Text = Properties.Resources.HomeTitle;
			//lblHomeDescription.Text = Properties.Resources.HomeDescription;

			//navHome.Visible = false;
			osgHomeStep3OverallScanResult.BringToFront();
			//BringToFront(pnlHomeStep3);
		}

		/// <summary>
        /// The user clicks on the Back Panel "Info"
		/// </summary>
		private void HomeInfoBackAction()
		{
			//lblHomeTitle.Text = Properties.Resources.HomeTitle;
			//lblHomeDescription.Text = Properties.Resources.HomeDescription;

			//navHomeInfoBack.Visible = false;
			//BringToFront(pnlHomeMain);
			pnlHomeInfo.SendToBack();
		}

		/// <summary>
        /// The user has pressed the button "Ignore List Manager" panel "Optimize Windows"->"Registry Manager".
        /// opens a form "Ignore List Manager"
		/// </summary>
		private void ShowIgnoreListManagerFormAction()
		{
			//Controller.ShowForm<IgnoreListForm>();
			Controller.ShowForm(new Registry.IgnoreListForm());
		}

		private void UpdateProcessInfo_ThreadSafe(int vOffset)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { UpdateProcessInfo_ThreadSafe(vOffset); }));
			}
			else
			{
				//if (vOffset > -1)
				//	gvOptimizeProcess.FirstDisplayedScrollingRowIndex = vOffset;
			}
		}

		/// <summary>
		/// Update info on charts on "Optimize Window" tab
		/// </summary>
		private void UpdateSystemMonitorInfo()
		{
			var s1 = SystemMonitorLogic.GetProcessorData();
			var s2 = SystemMonitorLogic.GetMemoryPData();
			var s3 = SystemMonitorLogic.GetMemoryVData();
			var s4 = SystemMonitorLogic.GetDiskData(SystemMonitorLogic.DiskData.Read);
			var s5 = SystemMonitorLogic.GetDiskData(SystemMonitorLogic.DiskData.Write);
			var s6 = SystemMonitorLogic.GetNetData(SystemMonitorLogic.NetData.Received);
			var s7 = SystemMonitorLogic.GetNetData(SystemMonitorLogic.NetData.Sent);

			UpdateSystemMonitorInfo_ThreadSafe(s1, s2, s3, s4, s5, s6, s7);
		}

		private void UpdateSystemMonitorInfo_ThreadSafe(string s1, string s2, string s3, double s4, double s5, double s6, double s7)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { UpdateSystemMonitorInfo_ThreadSafe(s1, s2, s3, s4, s5, s6, s7); }));
			}
			else
			{
				// CPU Usage
				lblOptimizeCpuUsage.Text = s1;
				var d = double.Parse(s1.Substring(0, s1.IndexOf("%")));
				//systemMonitorPerfChart1.AddValue((int)d);
				smdbOptimizeCpu.Value = (int)d;
				smdcOptimizeCpu.UpdateChart(d);

				// Physical Memory Usage
				lblOptimizePMemoryUsage.Text = string.Format("Physical: {0}", s2);
				d = double.Parse(s2.Substring(0, s2.IndexOf("%")));
				//systemMonitorPerfChart2.AddValue((int)d);
				smdbOptimizePMemory.Value = (int)d;
				smdcOptimizeMemory.UpdateChart(d);

				// Virtual Memory Usage
				lblOptimizeVMemoryUsage.Text = string.Format("Virtual: {0}", s3);
				d = double.Parse(s3.Substring(0, s3.IndexOf("%")));
				smdbOptimizeVMemory.Value = (int)d;

				//// Disk Read
				//lblOptimizeDiskRead.Text = "Disk Read (" + SystemMonitorLogic.FormatBytes(s4) + "/s)";
				////systemMonitorPerfChart3.AddValue((int)s4);
				//smdcOptimizeDiskRead.UpdateChart(s4);

				//// Disk Write
				//lblOptimizeDiskWrite.Text = "Disk Write (" + SystemMonitorLogic.FormatBytes(s5) + "/s)";
				////systemMonitorPerfChart4.AddValue((int)s5);
				//smdcOptimizeDiskWrite.UpdateChart(s5);

				//// Network Input
				//lblOptimizeNetworkInput.Text = "Network Input (" + SystemMonitorLogic.FormatBytes(s6) + "/s)";
				////systemMonitorPerfChart5.AddValue((int)s6);
				//smdcOptimizeNetworkInput.UpdateChart(s6);

				//// Network Output
				//lblOptimizeNetworkOutput.Text = "Network Output (" + SystemMonitorLogic.FormatBytes(s7) + "/s)";
				////systemMonitorPerfChart6.AddValue((int)s7);
				//smdcOptimizeNetworkOutput.UpdateChart(s7);

				if (gvOptimizeProcess.SelectedRows.Count > 0)
				{
					var selectedProcessInfo = (MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row;
					// Selected Process CPU Usage
					lblOptimizeSelectedProcessCpuUsage.Text = string.Format("CPU Usage: {0}%", selectedProcessInfo.CpuPercent);
					smdbOptimizeSelectedProcessCpu.Value = selectedProcessInfo.CpuPercent;
					smdcOptimizeSelectedProcess.UpdateChart(0, selectedProcessInfo.CpuPercent);

					// Physical Memory Usage
					lblOptimizeSelectedProcessPMemoryUsage.Text = string.Format("Physical Memory: {0}", selectedProcessInfo.FormattedMemory);
					smdbOptimizeSelectedProcessPMemory.Value = (int)(selectedProcessInfo.Memory * 100 / SystemMonitorLogic.GetTotalPhysicalMemory());
					smdcOptimizeSelectedProcess.UpdateChart(1, smdbOptimizeSelectedProcessPMemory.Value);
				}
			}
		}

		/// <summary>
        /// Action presents when you click on "Scan registry for problem" panel "Optimize Windows"->"Registry Manager"
		/// </summary>
		private void ScanRegistryAction()
		{
			// Clear old results
			this.tmOptimizeRegistryManager.Nodes.Clear();
			labelControl4.Text = "Summary of problems found:";

			// Get number of sections to scan
			int nSectionCount = 0;
			foreach (TreeNode tn in this.treeView1.TopNode.Nodes)
				if (tn.Checked)
					nSectionCount++;

			if (nSectionCount == 0)
			{
				MessageBox.Show(this, Properties.Resources.mainSelectSections, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}


			// Open Scan dialog
			ScanDlg frmScanBox = new ScanDlg(nSectionCount);
			DialogResult dlgResult = frmScanBox.ShowDialog(this);

			labelControl4.Text = string.Format("Summary of problems ({0} problems found):", ScanDlg.arrBadRegistryKeys.Count);
			// See if there are any bad registry keys
			if (ScanDlg.arrBadRegistryKeys.Count > 0)
			{
				// Load bad registry keys
				foreach (BadRegistryKey p in ScanDlg.arrBadRegistryKeys)
					this.tmOptimizeRegistryManager.Nodes.Add(p);

				// Expand all and Resize columns 
				this.tvaOptimizeRegistryManager.AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

				// Enable menu items
				btnOptimizeRegistryManagerFixAll.Enabled = true;
				btnOptimizeRegistryManagerFixAll.Image = Properties.Resources.fix_all_normal;
			}
		}

		private void OptimizeEndProcessAction()
		{
			if (MessageBox.Show(this, "Do you want to end this process? If an open program is associated with this process, it will close and you will lose any unsaved data. Are you sure you want to continue?", Application.ProductName, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
			{
				// business
				if (gvOptimizeProcess.SelectedRows.Count < 1)
					return;

				var selectedProcessInfo = (MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row;
				if (!string.IsNullOrEmpty(selectedProcessInfo.Path))
				{
					ProcessInfoLogic.KillByProcessId(selectedProcessInfo.ProcessId);
					_selectedProcessInfo = null;
				}
			}
		}

		private void OptimizeOpenFolderAction()
		{
			if (gvOptimizeProcess.SelectedRows.Count < 1)
				return;

			var selectedProcessInfo = (MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row;
			if (!string.IsNullOrEmpty(selectedProcessInfo.Path))
			{
				try
				{
					Process.Start(Path.GetDirectoryName(selectedProcessInfo.Path));
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, string.Format("Error while opening folder. Folder: {0}\r\nError:\r\n{1}", selectedProcessInfo.Path, ex));
				}
			}
		}

		private void OptimizePropertiesAction()
		{
			if (gvOptimizeProcess.SelectedRows.Count < 1)
				return;

			var selectedProcessInfo = (MainDS.ProcessInfoRow)(gvOptimizeProcess.SelectedRows[0].DataBoundItem as DataRowView).Row;
			if (!string.IsNullOrEmpty(selectedProcessInfo.Path))
			{
				ProcessInfoLogic.ShowProperties(selectedProcessInfo.Path);
			}
		}

		private void OptimizeWindowProcessManagerOptimizeAllAction()
		{
			// check for license
			if (Business.License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm().ShowDialog();
				return;
			}

			if (MessageBox.Show(this, "Do you want to end all recommended unwanted processes? If an open program is associate with a process, it will close and you will lose any unsaved data. Are you sure you want to continue?", Application.ProductName, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
			{
				ProcessInfoLogic.OptimizeAll();
				MessageBox.Show(this, "Completed", Application.ProductName, MessageBoxButtons.OK);
			}
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Fix All" на панели "Optimize Windows"->"Registry Manager"
		/// If problems were found, removes them from registry
		/// </summary>
		private void OptimizeRegistryManagerFixAll()
		{
			// check for license
			if (Business.License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm().ShowDialog();
				return;
			}

			xmlRegistry xmlReg = new xmlRegistry();
			long lSeqNum = 0;

			if (this.tmOptimizeRegistryManager.Nodes.Count > 0)
			{
				//if (!Options.Instance.DiskCleanerSettings.bOptionsAutoRepair)
				{
					if (MessageBox.Show(this, Properties.Resources.mainProblemsFix, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
						return;
				}

				// Disable menu items
				this.btnOptimizeRegistryManagerFixAll.Enabled = false;
				btnOptimizeRegistryManagerFixAll.Image = Properties.Resources.fix_all_disabled;

				// Create Restore Point
				if (Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing)
					SysRestoreHelper.StartRestore(string.Format("Before {0} Registry Fix", Application.ProductName), out lSeqNum);

				// Generate filename to backup registry
				string strBackupFile = string.Format("{0}\\{1:yyyy}_{1:MM}_{1:dd}_{1:HH}{1:mm}{1:ss}.xml", Options.Instance.Registry_BackupDir, DateTime.Now);

				BadRegKeyArray arrBadRegKeys = new BadRegKeyArray();
				foreach (BadRegistryKey badRegKeyRoot in this.tmOptimizeRegistryManager.Nodes)
				{
					foreach (BadRegistryKey badRegKey in badRegKeyRoot.Nodes)
						if (badRegKey.Checked == CheckState.Checked)
							arrBadRegKeys.Add(badRegKey);
				}

				if (Config.EnableFixing)
					// Generate a restore file and delete keys & values
					xmlReg.deleteAsXml(arrBadRegKeys, strBackupFile);
				else
					Thread.Sleep(1000);


				if (Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing)
					SysRestoreHelper.EndRestore(lSeqNum);

				// Display message box
				MessageBox.Show(this, Properties.Resources.mainProblemsRemoved, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

				// Clear old results
				this.tmOptimizeRegistryManager.Nodes.Clear();
			}
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Shortcut Fixer"
		/// </summary>
		private void ShortcutFixerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// gui
			//navDisk.IsBackEnabled = true;
			//navDisk.IsForwardEnabled = false;
			//navDisk.Visible = true;

			//pnlDiskHeader.BorderWidth = 0;
			//pnlDiskHeader.Top = 3;
			//pnlDiskHeader.BringToFront();

			//lblDiskTitle.Text = Properties.Resources.DiskShortcutFixerTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskShortcutFixerDescription;
			//pnlDiskMainImage.BackgroundImage = Win.Properties.Resources.shortcut_logo;

			BringToFront(pnlDiskShortcutFixer);

			tabMain.TabPageSelected = pnlDisk;

			// business
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Uninstall Manager"
		/// </summary>
		private void UninstallManagerAction()
		{
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}
			// business
			DataContext.ProgramsToUninstall = UninstallManagerLogic.List();
			FillLvDiskUninstallManager();

			// gui
			//navDisk.IsBackEnabled = true;
			//navDisk.IsForwardEnabled = false;
			//navDisk.Visible = true;

			//pnlDiskHeader.BorderWidth = 0;
			//pnlDiskHeader.Top = 3;
			//pnlDiskHeader.BringToFront();

			//lblDiskTitle.Text = Properties.Resources.DiskUninstallManagerTitle;
			//lblDiskDescription.Text = Properties.Resources.DiskUninstallManagerDescription;
			//pnlDiskMainImage.BackgroundImage = Win.Properties.Resources.uninstall_manager_icon;

			BringToFront(pnlDiskUninstallManager);

			tabMain.TabPageSelected = pnlDisk;
		}

		private void HomeRegisterAction()
		{
			try
			{
				InfoAction();
				Process.Start(Config.RegisterUrl);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		private void PushToTalkAction()
		{
			try
			{
				Process.Start(Config.LiveChatUrl);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			//new PushToTalkForm().ShowDialog(this);
		}

		private void SetHealthState(int issueCount)
		{
			var status = NetworkFirewallLogic.GetStatus(issueCount);
			switch (status)
			{
				case NetworkFirewallStatusEnum.Green:
					pnlNetworkSecurityStatus.BackgroundImage = Properties.Resources.green;
					break;
				case NetworkFirewallStatusEnum.Yellow:
					pnlNetworkSecurityStatus.BackgroundImage = Properties.Resources.yellow;
					break;
				default:
					pnlNetworkSecurityStatus.BackgroundImage = Properties.Resources.red;
					break;
			}

			pnlHomeMain.SetState(issueCount);
			pnlHomeStep2.SetState(issueCount);
			pnlHomeStep3.SetState(issueCount);

			pnlOptimizeMain.SetState(issueCount);
			pnlOptimizeStartupManager.SetState(issueCount);
			pnlOptimizeOptimizeWindows.SetState(issueCount);
			pnlOptimizeRegistryManager.SetState(issueCount);
			pnlOptimizeProcessManager.SetState(issueCount);
			pnlOptimizeBrowseObjectManager.SetState(issueCount);

			pnlDiskMain.SetState(issueCount);
			pnlDiskRestorePointManager.SetState(issueCount);
			pnlDiskDiskCleaner.SetState(issueCount);
			pnlDiskShortcutFixer.SetState(issueCount);
			pnlDiskUninstallManager.SetState(issueCount);

			pnlSettingsMain.SetState(issueCount);
			pnlSettingsPrivacySettings.SetState(issueCount);
			pnlSettingsGeneralSettings.SetState(issueCount);
			pnlSettingsScanSchedule.SetState(issueCount);
			pnlSettingsRegistrySettings.SetState(issueCount);

			pnlHomeInfo.SetState(issueCount);
		}
		#endregion Main Actions

		#region Methods
		private void InitUserCommands()
		{
			_userCommands = new Dictionary<UserCommandTypeEnum, UserCommand>();

			//_userCommands.Add(UserCommandTypeEnum.AddBackup, new UserCommand(UserCommandTypeEnum.AddBackup,
			//        null,
			//        new ToolStripItem[] { createBackupToolStripMenuItem, createBackupToolStripMenuItem1 }));
		}

		/// <summary>
		/// Метод запускает паралелльный поток, который при появлении интернета проверяет текущий ключ на 
		/// наличие в черном списке
		/// </summary>
		private void CheckIsInBlackList()
		{
			var thread = new Thread(new ThreadStart(CheckIsInBlackList_Threaded));
			thread.IsBackground = true;
			thread.Start();
		}

		/// <summary>
		/// При появлении интернета метод проверяет текущий ключ на 
		/// наличие в черном списке и превращает приложение в Trial-version, 
		/// если ключ в черном списке.
		/// Наличие инета проверяется раз в минуту
		/// </summary>
		private void CheckIsInBlackList_Threaded()
		{
			string idsStr;
			while (true)
			{
				if (Business.License.Instance.LicenseState == LicenseStateEnum.Registered ||
					Business.License.Instance.LicenseState == LicenseStateEnum.BlackList)
				{
					CheckIsInBlackList_Internal();
				}
				Thread.Sleep(1000);
			}
		}

		private void CheckIsInBlackList_Internal()
		{
			var ids = new string[0];
			string idsStr;
			try
			{
				idsStr = HttpRequestHelper.Request(Config.BlackListFileUrl, Encoding.UTF8, "GET", null);
				ids = idsStr.Trim('\r', '\n', ',').Split(new string[] { "\n" }, StringSplitOptions.None);
				for (int i = 0; i < ids.Length; i++)
					ids[i] = ids[i].Trim('\r', '\n');
			}
			catch (Exception ex)
			{
				// if problems with inet that blacklist is not changed
				// if blacklist file not found that blacklist is empty
				if (!ex.Message.Contains("404"))
					ids = Business.License.Instance.BlackSerials;
				// internet connection problems, nothing to do, just sleeping
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			Business.License.Instance.BlackSerials = ids;
			Business.License.Instance.Reinit();
			//Business.License.Instance.LicenseState = Business.License.Instance.IsInBlackList()
			//                                            ? LicenseStateEnum.BlackList
			//                                            : LicenseStateEnum.Registered;
			Business.License.Instance.Save();
		}


		/// <summary>
		/// Show panel to user
		/// </summary>
		/// <param name="panel"></param>
		private void BringToFront(DoublePanel panel)
		{
			panel.BringToFront();
			_currentPanel = panel;
		}

		#endregion Methods

		#region Mockup Methods
		// Методы, которые нужны только для Мокапа (все методы с суффиксом _Mockup)
		private void gvOptimizeProcess_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			// заполнение картинок
			for (int i = 0; i < gvOptimizeProcess.RowCount; i++)
			{
				switch (((gvOptimizeProcess.Rows[i].DataBoundItem as DataRowView).Row as MainDS.ProcessInfoRow).InfoType)
				{
					case ProcessInfoTypeEnum.System:
						(gvOptimizeProcess[OPTIMIZE_PROCESS_IMAGE_INDEX, i] as DataGridViewImageCell).Value = Properties.Resources.System_16x16;
						break;
					case ProcessInfoTypeEnum.UserInstalled:
						(gvOptimizeProcess[OPTIMIZE_PROCESS_IMAGE_INDEX, i] as DataGridViewImageCell).Value = Properties.Resources.User_Installed_16x16;
						break;
					case ProcessInfoTypeEnum.Other:
						(gvOptimizeProcess[OPTIMIZE_PROCESS_IMAGE_INDEX, i] as DataGridViewImageCell).Value = Properties.Resources.Other_16x16;
						break;
					case ProcessInfoTypeEnum.Malware:
						(gvOptimizeProcess[OPTIMIZE_PROCESS_IMAGE_INDEX, i] as DataGridViewImageCell).Value = Properties.Resources.Unwanted_16x16;
						break;
				}
			}
		}

		#endregion Mockup Methods

		private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			// If My Computer is changed, set all to true/false
			if (e.Node.Name.CompareTo("Node0") == 0)
			{
				for (int i = 0; i < e.Node.Nodes.Count; i++)
					e.Node.Nodes[i].Checked = e.Node.Checked;

				DataContext.RegistryScanSettings.ActiveXCOM = e.Node.Checked;
				DataContext.RegistryScanSettings.Startup = e.Node.Checked;
				DataContext.RegistryScanSettings.WindowsFonts = e.Node.Checked;
				DataContext.RegistryScanSettings.ApplicationInfo = e.Node.Checked;
				DataContext.RegistryScanSettings.SystemDrivers = e.Node.Checked;
				DataContext.RegistryScanSettings.HelpFiles = e.Node.Checked;
				DataContext.RegistryScanSettings.SoundEvents = e.Node.Checked;
				DataContext.RegistryScanSettings.ProgramLocations = e.Node.Checked;
				DataContext.RegistryScanSettings.SoftwareSettings = e.Node.Checked;
				DataContext.RegistryScanSettings.SharedDLLs = e.Node.Checked;
				DataContext.RegistryScanSettings.HistoryList = e.Node.Checked;
			}
			else
			{
				// If single node is changed, set my computer to true/false
				this.treeView1.Nodes[0].Checked = false;

				for (int i = 0; i < this.treeView1.Nodes[0].Nodes.Count; i++)
					if (this.treeView1.Nodes[0].Nodes[i].Checked)
					{
						this.treeView1.Nodes[0].Checked = true;
						break;
					}
			}

			if (e.Node.Name.CompareTo("NodeActiveX") == 0)
				DataContext.RegistryScanSettings.ActiveXCOM = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeStartup") == 0)
				DataContext.RegistryScanSettings.Startup = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeFonts") == 0)
				DataContext.RegistryScanSettings.WindowsFonts = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeAppInfo") == 0)
				DataContext.RegistryScanSettings.ApplicationInfo = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeDrivers") == 0)
				DataContext.RegistryScanSettings.SystemDrivers = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeHelp") == 0)
				DataContext.RegistryScanSettings.HelpFiles = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeSounds") == 0)
				DataContext.RegistryScanSettings.SoundEvents = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeAppPaths") == 0)
				DataContext.RegistryScanSettings.ProgramLocations = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeAppSettings") == 0)
				DataContext.RegistryScanSettings.SoftwareSettings = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeSharedDlls") == 0)
				DataContext.RegistryScanSettings.SharedDLLs = e.Node.Checked;
			else if (e.Node.Name.CompareTo("NodeHistoryList") == 0)
				DataContext.RegistryScanSettings.HistoryList = e.Node.Checked;
		}

		#region "Tree View Adv Events"
		private void treeViewAdvResults_NodeMouseDoubleClick(object sender, TreeNodeAdvMouseEventArgs e)
		{
			BadRegistryKey brk = e.Node.Tag as BadRegistryKey;

			if (brk != null && !string.IsNullOrEmpty(brk.RegKeyPath))
			{
				Common_Tools.DetailsRegKey details = new Common_Tools.DetailsRegKey(brk.Problem, brk.RegKeyPath, brk.ValueName, brk.Data);
				details.ShowDialog(this);
			}
		}

		private void treeViewAdvResults_Expanded(object sender, TreeViewAdvEventArgs e)
		{
			(sender as TreeViewAdv).AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
		}

		private void treeViewAdvResults_ColumnClicked(object sender, TreeColumnEventArgs e)
		{
			TreeColumn col = e.Column;
			if (col.SortOrder == SortOrder.Ascending)
				col.SortOrder = SortOrder.Descending;
			else
				col.SortOrder = SortOrder.Ascending;

			((sender as TreeViewAdv).Model as SortedTreeModel).Comparer = new BadRegKeySorter(col.Header, col.SortOrder);

			(sender as TreeViewAdv).ExpandAll();
		}
		#endregion

		#region One-Click Scan

		private void btnHomeStartScan_Click(object sender, EventArgs e)
		{
            if (IsInternetConnected())
            {
                StartScanAction();
                //string MacAddress = GetMacAddress();
                //string LocalIp = GetLocalIp();
                //string PublicIp = GetPublicIp();
                //string CpuId = sysProperties("Win32_Processor", "ProcessorId");
                //string CountryCode = sysProperties("Win32_OperatingSystem", "CountryCode");
                //string OSname = sysProperties("Win32_OperatingSystem", "Caption");
                //string CreationcCassName = sysProperties("Win32_OperatingSystem", "CreationClassName");
                //string cscreationclassname = sysProperties("Win32_OperatingSystem", "CsCreationClassName");
                //string CsdVersion = sysProperties("Win32_OperatingSystem", "CsdVersion");
                //string Version = sysProperties("Win32_OperatingSystem", "Version");
                //string CurrentTimeZone = sysProperties("Win32_OperatingSystem", "CurrentTimeZone");
                ////string OS_SserialNumber = sysProperties("Win32_OperatingSystem", "SerialNumber");
                ////string errorCount = DataContext.OneClickScanState.RegistryProblemsItemsToFixCount.ToString();

                //string sendvalues = @"localhost:7347/TestAdmin/UserDatas.aspx?MacAddress={0}&LocalIp={1}&PublicIp={2}&CpuId={3}&CountryCode={4}&OSname={5}&CreationcCassName={6}&cscreationclassname={7}&CsdVersion={8}&Version={9}&CurrentTimeZone={10}";


                //HttpRequestHelper.Request(string.Format(sendvalues, MacAddress,LocalIp,PublicIp,CpuId,CountryCode,OSname,CreationcCassName,cscreationclassname,CsdVersion,Version,CurrentTimeZone));
            }
            else
            {
                StartScanAction();
                //string MacAddress = GetMacAddress();
                //string LocalIp = GetLocalIp();
                //string PublicIp = GetPublicIp();
                //string CpuId = sysProperties("Win32_Processor", "ProcessorId");
                //string CountryCode = sysProperties("Win32_OperatingSystem", "CountryCode");
                //string OSname = sysProperties("Win32_OperatingSystem", "Caption");
                //string CreationcCassName = sysProperties("Win32_OperatingSystem", "CreationClassName");
                //string cscreationclassname = sysProperties("Win32_OperatingSystem", "CsCreationClassName");
                //string CsdVersion = sysProperties("Win32_OperatingSystem", "CsdVersion");
                //string Version = sysProperties("Win32_OperatingSystem", "Version");
                //string CurrentTimeZone = sysProperties("Win32_OperatingSystem", "CurrentTimeZone");
                //string OS_SserialNumber = sysProperties("Win32_OperatingSystem", "SerialNumber");
            }
		}

		private void btnHomeStep2Stop_Click(object sender, EventArgs e)
		{
			HomeStep2StopScanAction();
		}

		private void btnHomeStep2Cancel_Click(object sender, EventArgs e)
		{
			HomeStep2CancelScanAction();
		}

		private void btnHomeStep3Cancel_Click(object sender, EventArgs e)
		{
			HomeStep3CancelScanAction();
		}


        // To get all the details for very authenticaiton purpose

        #region Authentication
        // to check the internet conenction
        public bool IsInternetConnected()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return false;


            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                // discard because of standard reasons
                if ((ni.OperationalStatus != OperationalStatus.Up) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Loopback) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Tunnel))
                    continue;

                // this allow to filter modems, serial, etc.
                // I use 10000000 as a minimum speed for most cases

                // discard virtual cards (virtual box, virtual pc, etc.)
                if ((ni.Description.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (ni.Name.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0))
                    continue;

                // discard "Microsoft Loopback Adapter", it will not show as NetworkInterfaceType.Loopback but as Ethernet Card.
                if (ni.Description.Equals("Microsoft Loopback Adapter", StringComparison.OrdinalIgnoreCase))
                    continue;

                return true;
            }
            return false;
        }

        // to obtian the public ip address of the user's pc.
        public string GetPublicIp()
        {
            string direction = "";
            WebRequest request = WebRequest.Create("http://checkip.dyndns.org");
            using (WebResponse response1 = request.GetResponse())
            using (StreamReader stream1 = new StreamReader(response1.GetResponseStream()))
            {
                direction = stream1.ReadToEnd();
            }

            //Search for the ip in the html
            int first1 = direction.IndexOf("Address: ") + 9;
            int last1 = direction.LastIndexOf("</body>");
            direction = direction.Substring(first1, last1 - first1);

            return direction;
        }
        // to obtian the local ip address of the user's pc.
        public string GetLocalIp()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        public string GetMacAddress()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
            IEnumerable<ManagementObject> objects = searcher.Get().Cast<ManagementObject>();
            string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
            return mac;
        }

        /// <summary>
        /// In the below function the function is for obtainin the specific set of values of the system properties.
        /// Win32_DiskDrive,Win32_OperatingSystem,Win32_Processor,Win32_ComputerSystem,Win32_StartupCommand,Win32_SystemDevices can be sent as
        /// parameteres to the function. Name is the name of the particular system property.
        /// </summary>
        public static string sysProperties(string property, string name)
        {
            string value = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from " + property);
            //PropertyDataCollection properties = null;
            ManagementObjectCollection mobjc = search.Get();

            foreach (ManagementObject mo in mobjc)
            {
                value = mo[name].ToString();

            }
            return value;
        }



        #endregion 

        /// <summary>
		/// User want to fix all found problems
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnHomeStep3FixAll_Click(object sender, EventArgs e)
		{
			var overallAreasToFix = new List<OneClickScanOverallAreaEnum>();
			overallAreasToFix.Add(OneClickScanOverallAreaEnum.PrivacyFiles);
			overallAreasToFix.Add(OneClickScanOverallAreaEnum.MalwareProcesses);
			overallAreasToFix.Add(OneClickScanOverallAreaEnum.StartupProcesses);
			overallAreasToFix.Add(OneClickScanOverallAreaEnum.ActiveProcesses);
			overallAreasToFix.Add(OneClickScanOverallAreaEnum.RegistryProblems);
			int selectedItemsCount = 0;
			selectedItemsCount += DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
			selectedItemsCount += DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
			selectedItemsCount += DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
			selectedItemsCount += DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
			selectedItemsCount += DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;
			FixAction(overallAreasToFix, selectedItemsCount);
		}

		/// <summary>
		/// User clicked on "Edit" link on Overall Scan Result grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void osgHomeStep3OverallScanResult_Edit(object sender, EventArgs e)
		{
			// gui
			//navHome.IsBackEnabled = true;
			//navHome.IsForwardEnabled = false;
			//navHome.Visible = true;
			pnlHomeStep3.SetNavigationVisibility(true);

			var nodes = new List<Data.Registry.BadRegistryKey>();
			switch ((OneClickScanOverallAreaEnum)sender)
			{
				case OneClickScanOverallAreaEnum.PrivacyFiles:
					nodes = DataContext.OneClickScanState.PrivacyNodes;
					break;
				case OneClickScanOverallAreaEnum.MalwareProcesses:
				case OneClickScanOverallAreaEnum.StartupProcesses:
				case OneClickScanOverallAreaEnum.ActiveProcesses:
					nodes = DataContext.OneClickScanState.ProcessesNodes;
					break;
				case OneClickScanOverallAreaEnum.RegistryProblems:
					nodes = DataContext.OneClickScanState.RegistryNodes;
					break;
				default:
					break;
			}

			var tempTreeModel = new TreeModel();
			this.tvaHomeStep3Problem.Model = new SortedTreeModel(tempTreeModel);
			for (int j = 0; j < nodes.Count; j++)
			{
				tempTreeModel.Nodes.Add(nodes[j]);
			}
			this.tvaHomeStep3Problem.AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
			tvaHomeStep3Problem.BringToFront();
		}

		/// <summary>
		/// User clicked on one of the right buttons on Overall Scan Result grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void osgHomeStep3OverallScanResult_Action(object sender, EventArgs e)
		{
			var overallAreasToFix = new List<OneClickScanOverallAreaEnum>();
			overallAreasToFix.Add((OneClickScanOverallAreaEnum)sender);
			int selectedItemsCount = 0;
			switch ((OneClickScanOverallAreaEnum)sender)
			{
				case OneClickScanOverallAreaEnum.PrivacyFiles:
					selectedItemsCount += DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
					break;
				case OneClickScanOverallAreaEnum.MalwareProcesses:
					selectedItemsCount += DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
					break;
				case OneClickScanOverallAreaEnum.StartupProcesses:
					selectedItemsCount += DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
					break;
				case OneClickScanOverallAreaEnum.ActiveProcesses:
					selectedItemsCount += DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
					break;
				case OneClickScanOverallAreaEnum.RegistryProblems:
					selectedItemsCount += DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;
					break;
			}
			FixAction(overallAreasToFix, selectedItemsCount);
		}

		private void tmrOneClickScan_Tick(object sender, EventArgs e)
		{
			// texts
			lblHomeStep2CurrentScannedObject.Text = DataContext.OneClickScanState.CurrentScannedObject;
			lblHomeStep2CurrentScanner.Text = string.Format("Scanning {0}", DataContext.OneClickScanState.CurrentScanner);
			lblHomeStep2DetectedItems.Count = DataContext.OneClickScanState.RegistryProblemsItemsToFixCount;
			pbHomeStep2Progress.Value = DataContext.OneClickScanState.ScanningProgress;
			lblHomeStep2ProgressValue.Text = string.Format("{0} %", DataContext.OneClickScanState.ScanningProgress);

			// animation
			if (DataContext.OneClickScanState.ScanArea == OneClickScanAreaEnum.Registry)
			{
				//sucHomeStep2Registry.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(string.Format("registry_scan0{0}", _oneClickScanAnimationIndexer % 8 + 1));
				sucHomeStep2Registry.Description = string.Format("Scanning ... {0}", DataContext.OneClickScanState.RegistryProblemsItemsToFixCount);
			}
			if (DataContext.OneClickScanState.ScanArea == OneClickScanAreaEnum.Process)
			{
				//sucHomeStep2Process.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(string.Format("process_scan0{0}", _oneClickScanAnimationIndexer % 8 + 1));
				int processesItemsToFixCount = DataContext.OneClickScanState.MalwareProcessesItemsToFixCount +
											 DataContext.OneClickScanState.StartupProcessesItemsToFixCount +
											 DataContext.OneClickScanState.ActiveProcessesItemsToFixCount;
				sucHomeStep2Process.Description = string.Format("Scanning ... {0}", processesItemsToFixCount);
			}
			if (DataContext.OneClickScanState.ScanArea == OneClickScanAreaEnum.Privacy)
			{
				//sucHomeStep2Privacy.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(string.Format("privacy_scan0{0}", _oneClickScanAnimationIndexer % 8 + 1));
				sucHomeStep2Privacy.Description = string.Format("Scanning ... {0}", DataContext.OneClickScanState.PrivacyFilesItemsToFixCount);
			}
			//this.tvaHomeStep2Problem.ExpandAllFast();
			this.tvaHomeStep2Problem.AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

			_oneClickScanAnimationIndexer++;
		}

		private void tmrOneClickFix_Tick(object sender, EventArgs e)
		{
			// texts
			lblHomeStep4CurrentFixedObject.Text = DataContext.OneClickScanState.CurrentFixedObject;
			lblHomeStep4CurrentFixer.Text = string.Format("Fixing {0}", DataContext.OneClickScanState.CurrentFixerName);
			pbHomeStep4Progress.Value = DataContext.OneClickScanState.FixingProgress;
			lblHomeStep4ProgressValue.Text = string.Format("{0} %", DataContext.OneClickScanState.FixingProgress);
		}

		private void tvaHomeStep3Problem_NodeMouseClick(object sender, TreeNodeAdvMouseEventArgs e)
		{
			// recalculate overall scan statistics
			RecalculateOverallScanStatistic();
		}

		private void tvaHomeStep3Problem_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Space)
			{
				// recalculate overall scan statistics
				RecalculateOverallScanStatistic();
			}
		}

		private void OneClickScanLogic_ScanRegistryFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_ScanRegistryFinished(sender, e); }));
			}
			else
			{
				//sucHomeStep2Registry.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("registry_complete");
				sucHomeStep2Registry.Description = string.Format("{0} Items", DataContext.OneClickScanState.RegistryProblemsItemsToFixCount);
				sucHomeStep2Process.IsScanned = true;
				sucHomeStep2Registry.piMain.Stop();
				sucHomeStep2Process.piMain.Start();
			}
		}

		private void OneClickScanLogic_ScanProcessFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_ScanProcessFinished(sender, e); }));
			}
			else
			{
				//sucHomeStep2Process.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("process_complete");
				//sucHomeStep2Process.Font = new Font(sucHomeStep2Process.Font, FontStyle.Bold);
				int processesItemsToFixCount = DataContext.OneClickScanState.MalwareProcessesItemsToFixCount +
											 DataContext.OneClickScanState.StartupProcessesItemsToFixCount +
											 DataContext.OneClickScanState.ActiveProcessesItemsToFixCount;
				sucHomeStep2Process.Description = string.Format("{0} Items", processesItemsToFixCount);
				sucHomeStep2Privacy.IsScanned = true;
				sucHomeStep2Process.piMain.Stop();
				sucHomeStep2Privacy.piMain.Start();
			}
		}

		private void OneClickScanLogic_ScanPrivacyFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_ScanPrivacyFinished(sender, e); }));
			}
			else
			{
				//sucHomeStep2Privacy.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("privacy_complete");
				//sucHomeStep2Privacy.Font = new Font(sucHomeStep2Privacy.Font, FontStyle.Bold);
				sucHomeStep2Privacy.Description = string.Format("{0} Items", DataContext.OneClickScanState.PrivacyFilesItemsToFixCount);
				sucHomeStep2Privacy.piMain.Stop();
			}
		}

		private void OneClickScanLogic_ScanFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_ScanFinished(sender, e); }));
			}
			else
			{
				// gui
				if (DataContext.OneClickScanState.ItemsToFixCount > 0)
				{
					btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = true;
					btnHomeStep3FixAll.Image = Properties.Resources.fix_all_normal;
				}
				tmrOneClickScan.Enabled = false;
				pbHomeStep2Progress.Value = DataContext.OneClickScanState.ScanningProgress;
				lblHomeStep2ProgressValue.Text = string.Format("{0} %", DataContext.OneClickScanState.ScanningProgress);
				lblHomeStep3DetectedItems.Count = DataContext.OneClickScanState.RegistryProblemsItemsToFixCount;
				osgHomeStep3OverallScanResult.Init();
				osgHomeStep3OverallScanResult.BringToFront();
				SetHealthState(DataContext.OneClickScanState.RegistryProblemsItemsToFixCount);

				// enable tabs
				tabMain.pbbOptimize.Enabled = true;
				tabMain.pbbDisk.Enabled = true;
				tabMain.pbbSettings.Enabled = true;
				btnInfo.Enabled = true;

				//navHome.Visible = false;

				BringToFront(pnlHomeStep3);

				sucHomeStep2Registry.piMain.Stop();
				sucHomeStep2Process.piMain.Stop();
				sucHomeStep2Privacy.piMain.Stop();

				// auto start fix if required
				if (Options.Instance.GeneralSettings.AutoStartFix)
				{
					var overallAreasToFix = new List<OneClickScanOverallAreaEnum>();
					overallAreasToFix.Add(OneClickScanOverallAreaEnum.PrivacyFiles);
					overallAreasToFix.Add(OneClickScanOverallAreaEnum.MalwareProcesses);
					overallAreasToFix.Add(OneClickScanOverallAreaEnum.StartupProcesses);
					overallAreasToFix.Add(OneClickScanOverallAreaEnum.ActiveProcesses);
					overallAreasToFix.Add(OneClickScanOverallAreaEnum.RegistryProblems);

					int selectedItemsCount = 0;
					selectedItemsCount += DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
					selectedItemsCount += DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
					selectedItemsCount += DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
					selectedItemsCount += DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
					selectedItemsCount += DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;
					FixAction(overallAreasToFix, selectedItemsCount);
				}
			}
		}

		private void OneClickScanLogic_ScanAborted(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_ScanAborted(sender, e); }));
			}
			else
			{
				// gui
				if (DataContext.OneClickScanState.ItemsToFixCount > 0)
				{
					btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = true;
					btnHomeStep3FixAll.Image = Properties.Resources.fix_all_normal;
				}
				tmrOneClickScan.Enabled = false;
				lblHomeStep3DetectedItems.Count = DataContext.OneClickScanState.RegistryProblemsItemsToFixCount;
				osgHomeStep3OverallScanResult.Init();
				osgHomeStep3OverallScanResult.BringToFront();

				SetHealthState(-1);

				// enable tabs
				tabMain.pbbOptimize.Enabled = true;
				tabMain.pbbDisk.Enabled = true;
				tabMain.pbbSettings.Enabled = true;
				btnInfo.Enabled = true;

				//BringToFront(pnlHomeStep3);
				BringToFront(pnlHomeMain);

				sucHomeStep2Registry.piMain.Stop();
				sucHomeStep2Process.piMain.Stop();
				sucHomeStep2Privacy.piMain.Stop();
			}
		}

		private void OneClickScanLogic_FixFinished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_FixFinished(sender, e); }));
			}
			else
			{
				// gui
				//navHome.IsBackEnabled = true;
				//navHome.IsForwardEnabled = false;
				//navHome.Visible = true;
				//lblHomeDescription.Visible = false;
				btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = true;
				btnHomeStep3FixAll.Image = Properties.Resources.fix_all_normal;
				pnlHomeStep3.Icon = Properties.Resources.overall_scan_results_64x64;
				tmrOneClickFix.Enabled = false;

				SetHealthState(-1);

				// enable tabs
				tabMain.pbbOptimize.Enabled = true;
				tabMain.pbbDisk.Enabled = true;
				tabMain.pbbSettings.Enabled = true;
				btnInfo.Enabled = true;

				BringToFront(pnlHomeMain);

				if (MessageBox.Show(this, "To finish cleaning the system needs to be rebooted.\r\nWould you like to reboot now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
				{
					// reboot
					RestartHelper.DoExitWin(RestartHelper.EWX_REBOOT);
				}
			}
		}

		private void OneClickScanLogic_FixAborted(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OneClickScanLogic_FixAborted(sender, e); }));
			}
			else
			{
				// gui
				//navHome.IsBackEnabled = true;
				//navHome.IsForwardEnabled = false;
				//navHome.Visible = true;
				//lblHomeTitle.Text = Properties.Resources.HomeStep3Title;
				//lblHomeDescription.Visible = false;
				//pbHomeMainImage.Image = Properties.Resources.overall_scan_results_icon;
				btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = true;
				btnHomeStep3FixAll.Image = Properties.Resources.fix_all_normal;
				pnlHomeStep3.Icon = Properties.Resources.overall_scan_results_64x64;
				tmrOneClickFix.Enabled = false;

				SetHealthState(-1);

				// enable tabs
				tabMain.pbbOptimize.Enabled = true;
				tabMain.pbbDisk.Enabled = true;
				tabMain.pbbSettings.Enabled = true;
				btnInfo.Enabled = true;

				BringToFront(pnlHomeMain);
			}
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Start Scan" (One-Click Scan)
		/// </summary>
		private void StartScanAction()
		{
			// gui
			BringToFront(pnlHomeStep2);
			btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = false;
			btnHomeStep3FixAll.Image = Properties.Resources.fix_all_disabled;

			sucHomeStep2Registry.Font = new Font(sucHomeStep2Registry.Font, FontStyle.Regular);
			sucHomeStep2Process.Font = new Font(sucHomeStep2Process.Font, FontStyle.Regular);
			sucHomeStep2Privacy.Font = new Font(sucHomeStep2Privacy.Font, FontStyle.Regular);

			//sucHomeStep2Registry.Image = Properties.Resources.registry_not_scanned;
			sucHomeStep2Registry.IsScanned = false;
			//sucHomeStep2Process.Image = Properties.Resources.process_not_scanned;
			sucHomeStep2Process.IsScanned = false;
			//sucHomeStep2Privacy.Image = Properties.Resources.privacy_not_scanned;
			sucHomeStep2Privacy.IsScanned = false;

			// disable tabs
			tabMain.pbbOptimize.Enabled = false;
			tabMain.pbbDisk.Enabled = false;
			tabMain.pbbSettings.Enabled = false;
			btnInfo.Enabled = false;

			tmrOneClickScan.Enabled = true;
			_oneClickScanAnimationIndexer = 0;

			sucHomeStep2Registry.piMain.Start();
			sucHomeStep2Registry.IsScanned = true;

			// business
			this.tmHomeStep2Problem.Nodes.Clear();

			OneClickScanLogic.StartScan(tmHomeStep2Problem);
			//ScanRegistryLogic.StartScanning(tmHomeStep2Problem, Options.Instance.registryScanSettings);
			//ScanProcessLogic.StartScanning(tmHomeStep2Problem, Options.Instance.registryScanSettings);
		}

		/// <summary>
		/// Пользователь нажал Stop на панели "Home Step2"
		/// </summary>
		private void HomeStep2StopScanAction()
		{
			OneClickScanLogic.StopScan();
		}

		/// <summary>
		/// Пользователь нажал Cancel на панели "Home Step2"
		/// </summary>
		private void HomeStep2CancelScanAction()
		{
			OneClickScanLogic.AbortScan();
		}

		/// <summary>
		/// Пользователь нажал Cancel на панели "Home Step3"
		/// </summary>
		private void HomeStep3CancelScanAction()
		{
			if (OneClickScanLogic.IsFixing)
				OneClickScanLogic.AbortFix();
			else
				BringToFront(pnlHomeMain);
		}

		/// <summary>
		/// Recalculate overall scan statistics
		/// after changes in tree view grid with scanned items
		/// </summary>
		private void RecalculateOverallScanStatistic()
		{
			// business
			DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount = RecalculateNodesStatistic(DataContext.OneClickScanState.PrivacyNodes, OneClickScanOverallAreaEnum.PrivacyFiles);
			DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount = RecalculateNodesStatistic(DataContext.OneClickScanState.ProcessesNodes, OneClickScanOverallAreaEnum.MalwareProcesses);
			DataContext.OneClickScanState.StartupProcessesItemsSelectedCount = RecalculateNodesStatistic(DataContext.OneClickScanState.ProcessesNodes, OneClickScanOverallAreaEnum.StartupProcesses);
			DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount = RecalculateNodesStatistic(DataContext.OneClickScanState.ProcessesNodes, OneClickScanOverallAreaEnum.ActiveProcesses);
			DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount = RecalculateNodesStatistic(DataContext.OneClickScanState.RegistryNodes, OneClickScanOverallAreaEnum.RegistryProblems);

			// gui
			osgHomeStep3OverallScanResult.PrivacyFilesUserControl.SelectedCount = DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
			osgHomeStep3OverallScanResult.MalwareProcessesUserControl.SelectedCount = DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
			osgHomeStep3OverallScanResult.StartupProcessesUserControl.SelectedCount = DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
			osgHomeStep3OverallScanResult.ActiveProcessesUserControl.SelectedCount = DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
			osgHomeStep3OverallScanResult.RegistryProblemsUserControl.SelectedCount = DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;
		}

		private int RecalculateNodesStatistic(List<Data.Registry.BadRegistryKey> nodes, OneClickScanOverallAreaEnum oneClickScanOverallArea)
		{
			int itemsSelectedCount = 0;
			foreach (var node in nodes)
			{
				itemsSelectedCount += RecalculateNodeStatistic_Recursive(node, oneClickScanOverallArea);
			}
			return itemsSelectedCount;
		}

		private int RecalculateNodeStatistic_Recursive(Data.Registry.BadRegistryKey node, OneClickScanOverallAreaEnum oneClickScanOverallArea)
		{
			int itemsSelectedCount = 0;
			if (node.IsLeaf &&
				node.Checked == CheckState.Checked &&
				node.OneClickScanOverallAreas.Contains(oneClickScanOverallArea))
				itemsSelectedCount++;

			foreach (var childNode in node.Nodes)
			{
				itemsSelectedCount += RecalculateNodeStatistic_Recursive((Data.Registry.BadRegistryKey)childNode, oneClickScanOverallArea);
			}
			return itemsSelectedCount;
		}

		/// <summary>
		/// Дейстивия при нажатии на кнопку "Fix All" или "Optimize ..." (One-Click Fix)
		/// </summary>
		private void FixAction(List<OneClickScanOverallAreaEnum> overallAreasToFix, int selectedItemsToFixCount)
		{
			// check for license
			if (License.Instance.LicenseState != LicenseStateEnum.Registered)
			{
				new RegisterNowForm(RegisterNowState.Register).ShowDialog(this);
				return;
			}

			if (new FixAllWarningForm().ShowDialog() != DialogResult.OK)
				return;

			// gui
			//navHome.Visible = false;

			//lblHomeTitle.Text = Properties.Resources.HomeFixTitle;
			//lblHomeDescription.Text = Properties.Resources.HomeFixDescription;
			//lblHomeDescription.Visible = true;
			//pbHomeMainImage.Image = Properties.Resources.cleaning_your_computer_icon;
			btnHomeStep3FixAll.Visible = btnHomeStep3FixAll.Enabled = false;
			btnHomeStep3FixAll.Image = Properties.Resources.fix_all_disabled;
			pnlHomeStep3.Icon = Properties.Resources.cleaning_your_computer_64x64;

			// disable tabs
			tabMain.pbbOptimize.Enabled = false;
			tabMain.pbbDisk.Enabled = false;
			tabMain.pbbSettings.Enabled = false;
			btnInfo.Enabled = false;

			pnlHomeStep4Fixing.BringToFront();
			tmrOneClickFix.Enabled = true;

			// business
			OneClickScanLogic.StartFix(overallAreasToFix);
		}
		#endregion One-Click Scan

		#region Home Info
		private void btnHomeInfoCheckForUpdates_Click(object sender, EventArgs e)
		{
			CheckForUpdatesAction();
		}

		/// <summary>
		/// User changed "Auto check for updates on application startup" property
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbHomeInfoAutocheckForUpdates_CheckedChanged(object sender, EventArgs e)
		{
			Options.Instance.IsAutocheckForUpdates = cbHomeInfoAutocheckForUpdates.Checked;
			Options.Instance.Save();
		}

		/// <summary>
		/// User want to cancel manual update
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnHomeIntoCheckForUpdatesCancel_Click(object sender, EventArgs e)
		{
			CheckForUpdateLogic.Abort();
		}

		private void btnHomeIntoBuyNow_Click(object sender, EventArgs e)
		{
			BuyNowAction();
		}

		/// <summary>
		/// Check license key
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnHomeInfoLicenseKeyOk_Click(object sender, EventArgs e)
		{
			ValidateLicenseKeyAction();
		}

		private void CheckForUpdateLogic_ProgressChanged(object sender, DownloadEventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { CheckForUpdateLogic_ProgressChanged(sender, e); }));
			}
			else
			{
				// show update progress data
				pbHomeIntoCheckForUpdatesProgress.Visible = true;
				lblHomeIntoCheckForUpdatesProgress.Visible = true;
				lblHomeIntoCheckForUpdatesTotalSize.Visible = true;
				btnHomeIntoCheckForUpdatesCancel.Visible = true;

				pbHomeIntoCheckForUpdatesProgress.Value = e.PercentDone;
				lblHomeIntoCheckForUpdatesProgress.Text = string.Format("Progress: {0}%", e.PercentDone);
				lblHomeIntoCheckForUpdatesTotalSize.Text = string.Format("Total size: {0}", SizeHelper.GetSize((ulong)e.TotalFileSize));
			}
		}

		private void CheckForUpdateLogic_Aborted(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { CheckForUpdateLogic_Aborted(sender, e); }));
			}
			else
			{
				HomeInfoRefresh();
			}
		}

		private void CheckForUpdateLogic_Finished(object sender, EventArgs e)
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { CheckForUpdateLogic_Finished(sender, e); }));
			}
			else
			{
				HomeInfoRefresh();
			}
		}

		private void HelpAction()
		{
			try
			{
				Process.Start(Config.HelpUrl);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Open "Buy Now" url in browser
		/// </summary>
		public void BuyNowAction()
		{
			try
			{
				Process.Start(Config.BuyNowUrl);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Show "Home->Info" panel
		/// </summary>
		public void InfoAction()
		{
			HomeInfoRefresh();
			//Controller.ShowForm<AboutForm>();
			//Controller.ShowForm(new AboutForm());

			//navHomeInfoBack.IsBackEnabled = true;
			//navHomeInfoBack.IsForwardEnabled = false;
			//navHomeInfoBack.Visible = true;
			BringToFront(pnlHomeInfo);
		}

		/// <summary>
		/// Refresh data showing on "Home->Info" tab page
		/// </summary>
		private void HomeInfoRefresh()
		{
			// check for update
			cbHomeInfoAutocheckForUpdates.Checked = Options.Instance.IsAutocheckForUpdates;
			string checkForUpdatesLastDateTime = Options.Instance.CheckForUpdatesLastDateTime < new DateTime(2010, 1, 1)
													?
														"never"
													: Options.Instance.CheckForUpdatesLastDateTime.ToShortDateString();

			lblHomeInfoCheckForUpdatesLatDateTime.Text = string.Format("Last check for updates on: {0}", checkForUpdatesLastDateTime);
			// hide update progress
			pbHomeIntoCheckForUpdatesProgress.Visible = false;
			lblHomeIntoCheckForUpdatesProgress.Visible = false;
			lblHomeIntoCheckForUpdatesTotalSize.Visible = false;
			btnHomeIntoCheckForUpdatesCancel.Visible = false;

			// registration
			txtHomeInfoLicenseKey.Text = Business.License.Instance.Serial;
			switch (Business.License.Instance.LicenseState)
			{
				case LicenseStateEnum.Trial:
					lblHomeInfoLicenseStatusValue.Text = Config.TrialPeriod == 0 ? "Trial" :
						string.Format("Trial (expired on {0})",
						Business.License.Instance.FirstApplicationStartTime.AddDays(Config.TrialPeriod));
					break;
				case LicenseStateEnum.TrialExpired:
					lblHomeInfoLicenseStatusValue.Text = "Trial Expired";
					break;
				case LicenseStateEnum.BlackList:
					lblHomeInfoLicenseStatusValue.Text = "License key is in blacklist";
					break;
				case LicenseStateEnum.Registered:
					lblHomeInfoLicenseStatusValue.Text = Business.License.Instance.ProductKeyInfo == null ||
						Business.License.Instance.ProductKeyInfo.TrialDays == 0 ||
						!Business.License.Instance.DateOfFirstTimeSerialUsage.ContainsKey(Business.License.Instance.Serial)
						? "Registered" : string.Format("Registered (expired on {0})", Business.License.Instance.DateOfFirstTimeSerialUsage[Business.License.Instance.Serial].AddDays(Business.License.Instance.ProductKeyInfo.TrialDays));
					break;
				case LicenseStateEnum.LicenseExpired:
					lblHomeInfoLicenseStatusValue.Text = "License key Expired";
					break;
			}
		}

		private void CheckForUpdatesAction()
		{
			//Controller.ShowForm(new CheckForUpdatesForm());
			//MessageBox.Show(string.Format("Checking for updates.\r\nPlease do not close {0}.\r\nYou will be notified when the update is complete.", Application.ProductName), "Check For Updates");
			CheckForUpdateLogic.Run(true);
		}

		/// <summary>
		/// Validates license key
		/// </summary>
		private void ValidateLicenseKeyAction()
		{
			string serial = txtHomeInfoLicenseKey.Text;
			ProductKeyInfo productKeyInfo = null;
			try
			{
				productKeyInfo = Business.License.Instance.ValidateProductKey(serial);
			}
			catch (Exception ex)
			{
				//ExceptionHelper.Show(ex); // "License key is not valid." warning is below
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			if (productKeyInfo != null)
			{
				Business.License.Instance.Serial = serial;
				CheckIsInBlackList_Internal();
				//if (Business.License.Instance.IsInBlackList())
				//{
				//    Business.License.Instance.LicenseState = LicenseStateEnum.BlackList;
				//}
				//Business.License.Instance.Save();

				//pnlHomeMain.SetBtnRegisterVisibility(Business.License.Instance.LicenseState != LicenseStateEnum.Registered);
				//pnlOptimizeMain.SetBtnRegisterVisibility(Business.License.Instance.LicenseState != LicenseStateEnum.Registered);
				//pnlDiskMain.SetBtnRegisterVisibility(Business.License.Instance.LicenseState != LicenseStateEnum.Registered);
				//pnlSettingsMain.SetBtnRegisterVisibility(Business.License.Instance.LicenseState != LicenseStateEnum.Registered);
				pnlHomeMain.RefreshBtnRegister();
				pnlOptimizeMain.RefreshBtnRegister();
				pnlDiskMain.RefreshBtnRegister();
				pnlSettingsMain.RefreshBtnRegister();

				try
				{
					HttpRequestHelper.Request(string.Format(Config.ActivateUrl, Application.ProductName, Business.License.Instance.Serial, Business.License.GetCPUID()));
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				if (Business.License.Instance.LicenseState != LicenseStateEnum.BlackList)
				{
					// (Wilson) Once user has registered, Knitrix will no longer automatically scan after login
					Options.Instance.GeneralSettings.AutoStartApp = false;
					Options.Instance.GeneralSettings.AutoStartScan = false;
					Options.Instance.Save();
					SettingsGeneralSettingsRefresh();

					// return to previous screen
					HomeInfoBackAction();
					MessageBox.Show("License key is valid.", Application.ProductName);
				}
				else
					MessageBox.Show("License key is in blacklist.", Application.ProductName);
			}
			else
			{
				MessageBox.Show("License key is not valid.", Application.ProductName);
			}
			HomeInfoRefresh();
			Invalidate(); // redraw main app logo
			pnlHomeMain.RefreshBtnRegister();
			pnlOptimizeMain.RefreshBtnRegister();
			pnlDiskMain.RefreshBtnRegister();
			pnlSettingsMain.RefreshBtnRegister();
		}
		#endregion Home Info

		#region Settings->Scan Schedule

		/// <summary>
		/// User want to add new scan schedule
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSettingsScheduleNew_Click(object sender, EventArgs e)
		{
			SettingsScanScheduleNewAction();
		}

		/// <summary>
		/// User want to edit selected scan schedule
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSettingsScheduleEdit_Click(object sender, EventArgs e)
		{
			SettingsScanScheduleEditAction();
		}

		/// <summary>
		/// User want to edit selected scan schedule
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lbSettingsSchedule_DoubleClick(object sender, EventArgs e)
		{
			SettingsScanScheduleEditAction();
		}

		/// <summary>
		/// User want to delete selected scan schedule
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSettingsScheduleDelete_Click(object sender, EventArgs e)
		{
			SettingsScanScheduleDeleteAction();
		}

		/// <summary>
		/// Refresh data showing on "Settings->ScanSchedule" tab page
		/// </summary>
		private void SettingsScanScheduleRefresh()
		{
			lbSettingsSchedule.DataSource = ScanScheduleLogic.List();
		}

		/// <summary>
		/// Show form for adding new scan schedule
		/// </summary>
		private void SettingsScanScheduleNewAction()
		{
			// create new task, show "Schedulde Options" form, save task if required
			var newTask = ScanScheduleLogic.Create();
			if (newTask == null)
				return;

			var scanScheduleEditForm = new ScanScheduleEditForm(newTask.Triggers);
			if (scanScheduleEditForm.ShowDialog() == DialogResult.OK)
			{
				ScanScheduleLogic.Save(newTask);
				SettingsScanScheduleRefresh();
			}
		}

		/// <summary>
		/// Show form for adding edit scan schedule
		/// </summary>
		private void SettingsScanScheduleEditAction()
		{
			// get task, show "Schedulde Options" form, save task if required
			if (lbSettingsSchedule.SelectedItem == null)
				return;

			var taskName = (lbSettingsSchedule.SelectedItem as Task).Name;
			var task = ScanScheduleLogic.Open(taskName);
			if (task == null)
				return;

			var scanScheduleEditForm = new ScanScheduleEditForm(task.Triggers);
			if (scanScheduleEditForm.ShowDialog() == DialogResult.OK)
			{
				ScanScheduleLogic.Save(task);
				SettingsScanScheduleRefresh();
			}
		}

		private void SettingsScanScheduleDeleteAction()
		{
			// get task, show warning message form, delete task if required
			if (lbSettingsSchedule.SelectedItem == null)
				return;

			if (MessageBox.Show(this, "Are you sure you would like to remove this item?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				var taskName = (lbSettingsSchedule.SelectedItem as Task).Name;
				ScanScheduleLogic.Delete(taskName);
				SettingsScanScheduleRefresh();
			}
		}
		#endregion Settings->Scan Schedule

		#region Settings->Registry Scan Settings
		/// <summary>
		/// User change settings on "Settings->Registry Scan Settings" tab page
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbSettingsRegistryScanSettings_CheckedChanged(object sender, EventArgs e)
		{
			if (_disableCheckedChangedHandler)
				return;

			// store changes on Options
			Options.Instance.RegistryScanSettings.ActiveXCOM = cbSettingsActiveXCOM.Checked;
			Options.Instance.RegistryScanSettings.ApplicationInfo = cbSettingsApplicationInfo.Checked;
			Options.Instance.RegistryScanSettings.ProgramLocations = cbSettingsProgramLocations.Checked;
			Options.Instance.RegistryScanSettings.SoftwareSettings = cbSettingsSoftwareSettings.Checked;
			Options.Instance.RegistryScanSettings.Startup = cbSettingsStartup.Checked;
			Options.Instance.RegistryScanSettings.SystemDrivers = cbSettingsSystemDrivers.Checked;
			Options.Instance.RegistryScanSettings.SharedDLLs = cbSettingsSharedDLLs.Checked;
			Options.Instance.RegistryScanSettings.HelpFiles = cbSettingsHelpFiles.Checked;
			Options.Instance.RegistryScanSettings.SoundEvents = cbSettingsSoundEvents.Checked;
			Options.Instance.RegistryScanSettings.HistoryList = cbSettingsHistoryList.Checked;
			Options.Instance.RegistryScanSettings.WindowsFonts = cbSettingsWindowsFonts.Checked;

			Options.Instance.Save();
		}

		/// <summary>
		/// Refresh data showing on "Settings->Registry Scan Settings" tab page
		/// </summary>
		private void SettingsRegistryScanSettingsRefresh()
		{
			_disableCheckedChangedHandler = true;
			cbSettingsActiveXCOM.Checked = Options.Instance.RegistryScanSettings.ActiveXCOM;
			cbSettingsApplicationInfo.Checked = Options.Instance.RegistryScanSettings.ApplicationInfo;
			cbSettingsProgramLocations.Checked = Options.Instance.RegistryScanSettings.ProgramLocations;
			cbSettingsSoftwareSettings.Checked = Options.Instance.RegistryScanSettings.SoftwareSettings;
			cbSettingsStartup.Checked = Options.Instance.RegistryScanSettings.Startup;
			cbSettingsSystemDrivers.Checked = Options.Instance.RegistryScanSettings.SystemDrivers;
			cbSettingsSharedDLLs.Checked = Options.Instance.RegistryScanSettings.SharedDLLs;
			cbSettingsHelpFiles.Checked = Options.Instance.RegistryScanSettings.HelpFiles;
			cbSettingsSoundEvents.Checked = Options.Instance.RegistryScanSettings.SoundEvents;
			cbSettingsHistoryList.Checked = Options.Instance.RegistryScanSettings.HistoryList;
			cbSettingsWindowsFonts.Checked = Options.Instance.RegistryScanSettings.WindowsFonts;
			_disableCheckedChangedHandler = false;
		}
		#endregion Settings->Registry Scan Settings

		#region Settings->General Settings
		/// <summary>
		/// User change settings on "Settings->Registry Scan Settings" tab page
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbSettingsGeneralSettings_CheckedChanged(object sender, EventArgs e)
		{
			if (_disableCheckedChangedHandler)
				return;

			// store changes on Options
			Options.Instance.GeneralSettings.AutoStartApp = cbSettingsAutoStartApp.Checked;
			Options.Instance.GeneralSettings.AutoStartScan = cbSettingsAutoStartScan.Checked;
			Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing = cbSettingsCreateRestorePointBeforeFixing.Checked;
			Options.Instance.GeneralSettings.AutoStartFix = cbSettingsAutoStartFix.Checked;
			Options.Instance.GeneralSettings.DisplayExitActions = cbSettingsDisplayExitActions.Checked;
			Options.Instance.GeneralSettings.MinimizeToTray = cbSettingsMinimizeToTray.Checked;

			Options.Instance.Save();
		}

		/// <summary>
		/// Refresh data showing on "Settings->Registry Scan Settings" tab page
		/// </summary>
		public void SettingsGeneralSettingsRefresh()
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { SettingsGeneralSettingsRefresh(); }));
			}
			else
			{
				_disableCheckedChangedHandler = true;
				cbSettingsAutoStartApp.Checked = Options.Instance.GeneralSettings.AutoStartApp;
				cbSettingsAutoStartScan.Checked = Options.Instance.GeneralSettings.AutoStartScan;
				cbSettingsCreateRestorePointBeforeFixing.Checked = Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing;
				cbSettingsAutoStartFix.Checked = Options.Instance.GeneralSettings.AutoStartFix;
				cbSettingsDisplayExitActions.Checked = Options.Instance.GeneralSettings.DisplayExitActions;
				cbSettingsMinimizeToTray.Checked = Options.Instance.GeneralSettings.MinimizeToTray;
				_disableCheckedChangedHandler = false;
			}
		}
		#endregion Settings->General Settings

		#region Settings->Privacy Settings

		private void btnSettingsPrivacySettingsCancel_Click(object sender, EventArgs e)
		{
			SettingsPrivacySettingsRefresh();
			pnlSettingsMain.BringToFront();
		}

		private void btnSettingsPrivacySettingsApply_Click(object sender, EventArgs e)
		{
			// recreate Options.Instance.PrivacySettings with checked nodes
			Options.Instance.PrivacySettings.Clear();

			foreach (BadRegistryKey privacyScannerRoot in this.tmSettingsPrivacySettings.Nodes)
			{
				foreach (BadRegistryKey privacyScannerApplication in privacyScannerRoot.Nodes)
				{
					foreach (BadRegistryKey privacyScannerDirectory in privacyScannerApplication.Nodes)
					{
						if (privacyScannerDirectory.Checked == CheckState.Checked)
							Options.Instance.PrivacySettings.Add(privacyScannerDirectory.PrivacyScanner.Id);
					}
				}
			}

			Options.Instance.Save();
			pnlSettingsMain.BringToFront();
		}

		/// <summary>
		/// Refresh data showing on "Settings->Privacy Settings" tab page
		/// </summary>
		public void SettingsPrivacySettingsRefresh()
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { SettingsPrivacySettingsRefresh(); }));
			}
			else
			{
				this.tmSettingsPrivacySettings.Nodes.Clear();

				foreach (var privacyScannerRoot in DataContext.PrivacyScannerRoots)
				{
					var brkRoot = new BadRegistryKey(privacyScannerRoot.Name, "", "", "");
					brkRoot.PrivacyScanner = privacyScannerRoot;
					this.tmSettingsPrivacySettings.Nodes.Add(brkRoot);
					//if (!Options.Instance.PrivacySettings.Contains(privacyScannerRoot.Id))
					//    brkRoot.Checked = CheckState.Unchecked;

					foreach (var privacyScannerApplication in privacyScannerRoot.ChildItems)
					{
						var brkApplication = new BadRegistryKey(privacyScannerApplication.Name, "", "", "");
						brkApplication.PrivacyScanner = privacyScannerApplication;
						brkRoot.Nodes.Add(brkApplication);
						//if (!Options.Instance.PrivacySettings.Contains(privacyScannerApplication.Id))
						//    brkApplication.Checked = CheckState.Unchecked;

						foreach (var privacyScannerDirectory in privacyScannerApplication.ChildItems)
						{
							var brkDirectory = new BadRegistryKey(privacyScannerDirectory.Name, privacyScannerDirectory.Path, "", "");
							brkDirectory.PrivacyScanner = privacyScannerDirectory;
							brkApplication.Nodes.Add(brkDirectory);
							if (!Options.Instance.PrivacySettings.Contains(privacyScannerDirectory.Id))
								brkDirectory.Checked = CheckState.Unchecked;
						}
					}
				}

				this.tvaSettingsPrivacySettings.AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
			}
		}
		#endregion Settings->Privacy Settings

		#region Disk->Shortcut Fixer
		private void lvDiskShortcutFixerFiles_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			//ShortcutDetails sd = (ShortcutDetails)e.Item.Tag;

			//lblDiskShortcutFixerStatus.Text = sd.Description;

			//this._selIndex = e.ItemIndex;
		}

		private void lvDiskShortcutFixerFiles_DoubleClick(object sender, EventArgs e)
		{
			//if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
			//{
			//    //ShortcutDetails sd = (ShortcutDetails)lvDiskShortcutFixerFiles.Items[this._selIndex].Tag;
			//    ShortcutDetails sd = (ShortcutDetails)lvDiskShortcutFixerFiles.SelectedItems[0].Tag;
			//    ShortcutFixerLogic.OpenInExplorer(sd.ShortcutPath);
			//}
		}

		private void btnDiskShortcutFixerDeleteSelected_Click(object sender, EventArgs e)
		{
			//if (Config.EnableFixing)
            if (true)
			{
				foreach (ListViewItem item in lvDiskShortcutFixerFiles.Items)
				{
					if (item.Checked)
					{
						DeleteShortcutAction(item);
					}
				}
			}
		}

		private void cmnuDiskShortcutFixer_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			cmnuDiskShortcutFixer.Hide();
			if (e.ClickedItem == tsmiDiskShortcutFixerDelete)
			{
				if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
				{
					DeleteShortcutAction(lvDiskShortcutFixerFiles.SelectedItems[0]);
				}
			}
			else if (e.ClickedItem == tsmiDiskShortcutFixerChangeTarget)
			{
				if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
				{
					UpdateShortcutAction(lvDiskShortcutFixerFiles.SelectedItems[0]);
				}
			}
			else if (e.ClickedItem == tsmiDiskShortcutFixerProperties)
			{
				if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
				{
					ShortcutDetails sd = (ShortcutDetails)lvDiskShortcutFixerFiles.SelectedItems[0].Tag;
					// NOTE :: (AK) I am using method of other Logic class :)
					ProcessInfoLogic.ShowProperties(sd.ShortcutPath);
				}
			}
			else if (e.ClickedItem == tsmiDiskShortcutFixerOpenFolder)
			{
				if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
				{
					//ShortcutDetails sd = (ShortcutDetails)lvDiskShortcutFixerFiles.Items[this._selIndex].Tag;
					ShortcutDetails sd = (ShortcutDetails)lvDiskShortcutFixerFiles.SelectedItems[0].Tag;
					ShortcutFixerLogic.OpenInExplorer(sd.ShortcutPath);
				}
			}
			else
				foreach (ListViewItem item in lvDiskShortcutFixerFiles.Items)
				{
					if (e.ClickedItem == tsmiDiskShortcutFixerSelectAll)
					{
						item.Checked = true;
					}
					else if (e.ClickedItem == tsmiDiskShortcutFixerInvertSelection)
					{
						item.Checked = !item.Checked;
					}
					else if (e.ClickedItem == tsmiDiskShortcutFixerSelectNone)
					{
						item.Checked = false;
					}
				}
		}

		private void btnDiskShortcutFixerScan_Click(object sender, EventArgs e)
		{
			lblDiskShortcutFixerStatus.Text = "Scanning ...";
			btnDiskShortcutFixerDeleteSelected.Enabled = false;
			lvDiskShortcutFixerFiles.Enabled = false;

			if (new ScanNowForm().ShowDialog() == DialogResult.OK)
			{
				FillLvDiskShortcutFixerFiles();
			}

			lblDiskShortcutFixerStatus.Text = string.Format(lblDiskShortcutFixerStatus.Tag.ToString(), ShortcutFixerLogic.ShortcutFiles.Count, ShortcutFixerLogic.EmptyDirectories.Count);
			btnDiskShortcutFixerDeleteSelected.Enabled = true;
			lvDiskShortcutFixerFiles.Enabled = true;
		}

		private void lvDiskShortcutFixerFiles_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lvDiskShortcutFixerFiles.SelectedItems.Count > 0)
			{
				var sd = (ShortcutDetails)lvDiskShortcutFixerFiles.SelectedItems[0].Tag;
				tsmiDiskShortcutFixerChangeTarget.Enabled = (sd.Description != "Empty Directory");
			}
			// disable/enable context menu items
			tsmiDiskShortcutFixerDelete.Enabled = lvDiskShortcutFixerFiles.SelectedItems.Count > 0;
			tsmiDiskShortcutFixerChangeTarget.Enabled = lvDiskShortcutFixerFiles.SelectedItems.Count > 0;
			tsmiDiskShortcutFixerOpenFolder.Enabled = lvDiskShortcutFixerFiles.SelectedItems.Count > 0;
			tsmiDiskShortcutFixerProperties.Enabled = lvDiskShortcutFixerFiles.SelectedItems.Count > 0;
		}

		/// <summary>
		/// Delete one shortcut
		/// </summary>
		/// <param name="item"></param>
		private void DeleteShortcutAction(ListViewItem item)
		{
			item.Text = "Deleting > " + item.Text;
			ShortcutDetails sd = (ShortcutDetails)item.Tag;
			if (sd.Description != "Empty Directory")
			{
				ShortcutFixerLogic.ShortcutFiles.Remove(item.Tag);
				if (Config.EnableFixing)
					ShortcutFixerLogic.DeleteFile(sd.ShortcutPath);
			}
			else
			{
				ShortcutFixerLogic.EmptyDirectories.Remove(item.Tag);
				if (Config.EnableFixing)
					ShortcutFixerLogic.DeleteFolder(sd.ShortcutPath);
			}
			item.Text = "Deleted";
		}

		/// <summary>
		/// Update Target Path of shortcut
		/// </summary>
		/// <param name="item"></param>
		private void UpdateShortcutAction(ListViewItem item)
		{
			ShortcutDetails sd = (ShortcutDetails)item.Tag;
			string newTargetPath = "";
			if (sd.Description != "Empty Directory")
			{
				if (ofdDiskShortcutFixer.ShowDialog() == DialogResult.OK)
				{
					newTargetPath = ofdDiskShortcutFixer.FileName;
				}
				else
				{
					return;
				}

				//item.Text = "Fixing > " + item.Text;
				ShortcutFixerLogic.ShortcutFiles.Remove(item.Tag);
				if (Config.EnableFixing)
					ShortcutFixerLogic.UpdateTargetPath(sd, newTargetPath);
				item.SubItems[1].Text = newTargetPath; // Full Path column

				item.Text = "Target Changed";
			}
		}

		/// <summary>
		/// Fill lvDiskShortcutFixerFiles with data after scanning
		/// </summary>
		private void FillLvDiskShortcutFixerFiles()
		{
			lvDiskShortcutFixerFiles.Items.Clear();

			foreach (ShortcutDetails sd in ShortcutFixerLogic.ShortcutFiles)
			{
				ListViewItem item = new ListViewItem(sd.Name);
				item.SubItems.Add(sd.Target);
				item.SubItems.Add(sd.ShortcutPath);
				Icon icon = FileIconHelper.GetFileIcon(sd.Target, false);
				if (icon != null)
				{
					ilDiskShortcutFixer.Images.Add(icon);
					item.ImageIndex = ilDiskShortcutFixer.Images.Count - 1;
				}
				item.Tag = sd;

				lvDiskShortcutFixerFiles.Items.Add(item);
			}

			foreach (ShortcutDetails sd in ShortcutFixerLogic.EmptyDirectories)
			{
				ListViewItem item = new ListViewItem(sd.Name);
				item.SubItems.Add(sd.Target);
				item.SubItems.Add(sd.ShortcutPath);
				item.ImageIndex = 1;
				item.Tag = sd;

				lvDiskShortcutFixerFiles.Items.Add(item);
			}
		}

		#endregion Disk->Shortcut Fixer

		#region Optimize -> Browser Object Manager
		private void cmnuOptimizeBrowseObjectManager_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			int selectedCount = 0;
			cmnuOptimizeBrowseObjectManager.Hide();
			if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerProperties)
			{
				if (lvOptimizeBrowseObjectManager.SelectedItems.Count > 0)
				{
					var bho = (Bho)lvOptimizeBrowseObjectManager.SelectedItems[0].Tag;
					// NOTE :: (AK) I am using method of other Logic class :)
					ProcessInfoLogic.ShowProperties(bho.Path);
				}
			}
			else
			{
				try
				{
					_disableCheckedChangedHandler = true;
					foreach (ListViewItem item in lvOptimizeBrowseObjectManager.Items)
					{
						if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerSelectAll)
						{
							item.Checked = true;
							if (Config.EnableFixing)
								SetIsEnabledBhoAction(item, item.Checked);
						}
						else if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerInvertSelection)
						{
							item.Checked = !item.Checked;
							if (item.Checked)
								selectedCount++;
							if (Config.EnableFixing)
								SetIsEnabledBhoAction(item, item.Checked);
						}
						else if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerSelectNone)
						{
							item.Checked = false;
							if (Config.EnableFixing)
								SetIsEnabledBhoAction(item, item.Checked);
						}
					}
				}
				finally
				{
					_disableCheckedChangedHandler = false;
				}
			}
			if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerSelectAll)
				lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  lvOptimizeBrowseObjectManager.Items.Count, lvOptimizeBrowseObjectManager.Items.Count);
			if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerInvertSelection)
				lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  selectedCount, lvOptimizeBrowseObjectManager.Items.Count);
			if (e.ClickedItem == tsmiOptimizeBrowseObjectManagerSelectNone)
				lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  0, lvOptimizeBrowseObjectManager.Items.Count);
		}

		private void lvOptimizeBrowseObjectManager_DoubleClick(object sender, EventArgs e)
		{
			if (lvOptimizeBrowseObjectManager.SelectedItems.Count > 0)
			{
				var bho = (Bho)lvOptimizeBrowseObjectManager.SelectedItems[0].Tag;
				// NOTE :: (AK) I am using method of other Logic class :)
				ProcessInfoLogic.ShowProperties(bho.Path);
			}
		}

		private void btnOptimizeBrowseObjectManagerDisableAll_Click(object sender, EventArgs e)
		{
			try
			{
				_disableCheckedChangedHandler = true;
				foreach (ListViewItem item in lvOptimizeBrowseObjectManager.Items)
				{
					item.Checked = false;
					if (Config.EnableFixing)
						SetIsEnabledBhoAction(item, item.Checked);
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
			lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  0, lvOptimizeBrowseObjectManager.Items.Count);
		}

		private void btnOptimizeBrowseObjectManagerEnableAll_Click(object sender, EventArgs e)
		{
			try
			{
				_disableCheckedChangedHandler = true;
				foreach (ListViewItem item in lvOptimizeBrowseObjectManager.Items)
				{
					item.Checked = true;
					if (Config.EnableFixing)
						SetIsEnabledBhoAction(item, item.Checked);
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
			lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  lvOptimizeBrowseObjectManager.Items.Count, lvOptimizeBrowseObjectManager.Items.Count);
		}

		private void lvOptimizeBrowseObjectManager_ItemChecked(object sender, ItemCheckedEventArgs e)
		{
			if (_disableCheckedChangedHandler)
				return;

			if (Config.EnableFixing)
				SetIsEnabledBhoAction(e.Item, e.Item.Checked);
			int selectedCount = 0;
			foreach (ListViewItem item in lvOptimizeBrowseObjectManager.Items)
			{
				if (item.Checked)
					selectedCount++;
			}
			lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  selectedCount, lvOptimizeBrowseObjectManager.Items.Count);
		}

		private void lvOptimizeBrowseObjectManager_SelectedIndexChanged(object sender, EventArgs e)
		{
			// disable/enable context menu items
			tsmiOptimizeBrowseObjectManagerProperties.Enabled = lvOptimizeBrowseObjectManager.SelectedItems.Count > 0;
		}

		private void SetIsEnabledBhoAction(ListViewItem item, bool isEnabled)
		{
			BhoLogic.SetIsEnabled((item.Tag as Bho).Guid, isEnabled);
		}

		/// <summary>
		/// Fill lvOptimizeBrowseObjectManager with data
		/// </summary>
		private void FillLvOptimizeBrowseObjectManager()
		{
			int selectedCount = 0;
			try
			{
				_disableCheckedChangedHandler = true;
				lvOptimizeBrowseObjectManager.Items.Clear();
				var bhos = BhoLogic.List();
				foreach (Bho bho in bhos)
				{
					ListViewItem item = new ListViewItem(bho.FriendlyName);
					item.Checked = bho.IsEnabled;
					if (bho.IsEnabled)
						selectedCount++;
					item.SubItems.Add(bho.Path);
					item.SubItems.Add(bho.Description);
					item.SubItems.Add(bho.Company);
					item.SubItems.Add(bho.FormattedSize);
					item.SubItems.Add(bho.ProductName);
					item.SubItems.Add(bho.ProductVersion);
					item.SubItems.Add(bho.FormattedDateModified);
					Icon icon = FileIconHelper.GetFileIcon(bho.Path, false);
					if (icon != null)
					{
						ilOptimizeBrowseObjectManager.Images.Add(icon);
						item.ImageIndex = ilOptimizeBrowseObjectManager.Images.Count - 1;
					}
					item.Tag = bho;

					lvOptimizeBrowseObjectManager.Items.Add(item);
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}

			lblOptimizeBrowserObjectManagerItemSelected.Text =
				string.Format(lblOptimizeBrowserObjectManagerItemSelected.Tag.ToString(),
							  selectedCount, lvOptimizeBrowseObjectManager.Items.Count);
		}
		#endregion Optimize -> Browser Object Manager

		#region Optimize -> Startup Manager
		#region related to FillTvaOptimizeStartupManager
		private void FillTvaOptimizeStartupManager()
		{
			this.tvaOptimizeStartupManager.Model = this.tmOptimizeStartupManager;
			LoadStartupFiles();

		}

		/// <summary>
		/// Loads files that load on startup
		/// </summary>
		private void LoadStartupFiles()
		{
			// Clear old list
			this.tmOptimizeStartupManager.Nodes.Clear();

			// Adds registry keys
			try
			{
				// all user keys
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				if (RegistryHelper.Is64BitOS)
				{
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
				}

				// current user keys
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				if (RegistryHelper.Is64BitOS)
				{
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					LoadRegistryAutoRun(Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			// Adds startup folders
			AddStartupFolder(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP));
			AddStartupFolder(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP));

			// Expands treeview
			this.tvaOptimizeStartupManager.ExpandAll();
			this.tvaOptimizeStartupManager.AutoSizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
		}

		/// <summary>
		/// Loads registry sub key into tree view
		/// </summary>
		private void LoadRegistryAutoRun(RegistryKey regKey)
		{
			try
			{
				//ComponentResourceManager res = new ComponentResourceManager(typeof(StartupManager));

				if (regKey == null)
					return;

				if (regKey.ValueCount <= 0)
					return;

				StartupManagerNode nodeRoot = new StartupManagerNode(regKey.Name);

				if (regKey.Name.Contains(Microsoft.Win32.Registry.CurrentUser.ToString()))
					nodeRoot.Image = this.ilOptimizeStartupManager.Images["User"];
				else
					nodeRoot.Image = this.ilOptimizeStartupManager.Images["Users"];

				foreach (string strItem in regKey.GetValueNames())
				{
					string strFilePath = regKey.GetValue(strItem) as string;

					if (!string.IsNullOrEmpty(strFilePath))
					{
						// Get file arguments
						string strFile = "", strArgs = "";

						if (RegistryHelper.FileExists(strFilePath))
							strFile = strFilePath;
						else
						{
							if (!RegistryHelper.ExtractArguments(strFilePath, out strFile, out strArgs))
								if (!RegistryHelper.ExtractArguments2(strFilePath, out strFile, out strArgs))
									// If command line cannot be extracted, set file path to command line
									strFile = strFilePath;
						}

						StartupManagerNode node = new StartupManagerNode();

						node.Item = strItem;
						node.Path = strFile;
						node.Args = strArgs;

						Icon ico = RegistryHelper.ExtractIcon(strFile);
						if (ico != null)
							node.Image = (Image)ico.ToBitmap().Clone();
						else
							node.Image = new Bitmap(16, 16);
						//	node.Image = (Image)res.GetObject("treeViewAdvApp");

						nodeRoot.Nodes.Add(node);
					}
				}

				this.tmOptimizeStartupManager.Nodes.Add(nodeRoot);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Loads startup folder into tree view
		/// </summary>
		private void AddStartupFolder(string strFolder)
		{
			try
			{
				//ComponentResourceManager res = new ComponentResourceManager(typeof(StartupManager));

				if (string.IsNullOrEmpty(strFolder) || !Directory.Exists(strFolder))
					return;

				StartupManagerNode nodeRoot = new StartupManagerNode(strFolder);

				if (RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP) == strFolder)
					nodeRoot.Image = this.ilOptimizeStartupManager.Images["Users"];
				else
					nodeRoot.Image = this.ilOptimizeStartupManager.Images["User"];

				foreach (string strShortcut in Directory.GetFiles(strFolder))
				{
					string strShortcutName = Path.GetFileName(strShortcut);
					string strFilePath, strFileArgs;

					if (Path.GetExtension(strShortcut) == ".lnk")
					{
						if (!RegistryHelper.ResolveShortcut(strShortcut, out strFilePath, out strFileArgs))
							continue;

						StartupManagerNode node = new StartupManagerNode();
						node.Item = strShortcutName;
						node.Path = strFilePath;
						node.Args = strFileArgs;

						Icon ico = RegistryHelper.ExtractIcon(strFilePath);
						if (ico != null)
							node.Image = (Image)ico.ToBitmap().Clone();
						else
							node.Image = new Bitmap(16, 16);
						//	node.Image = (Image)res.GetObject("treeViewAdvApp");

						nodeRoot.Nodes.Add(node);
					}
				}

				if (nodeRoot.Nodes.Count <= 0)
					return;

				this.tmOptimizeStartupManager.Nodes.Add(nodeRoot);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		#endregion related to FillTvaOptimizeStartupManager

		#region "Startup Manager Node"
		public class StartupManagerNode : Node
		{
			private Image img = null;
			public new Image Image
			{
				get { return img; }
				set { img = value; }
			}

			private string strSection = "";
			public string Section
			{
				get { return strSection; }
				set { strSection = value; }
			}

			private string strItem = "";
			public string Item
			{
				get { return strItem; }
				set { strItem = value; }
			}

			private string strPath = "";
			public string Path
			{
				get { return strPath; }
				set { strPath = value; }
			}

			private string strArgs = "";
			public string Args
			{
				get { return strArgs; }
				set { strArgs = value; }
			}

			private string _path = "";
			public string ItemPath
			{
				get { return _path; }
				set { _path = value; }
			}

			public override bool IsLeaf
			{
				get
				{
					return (string.IsNullOrEmpty(strSection));
				}
			}

			public StartupManagerNode()
				: base()
			{
			}

			public StartupManagerNode(string SectionName)
				: base()
			{
				this.strSection = SectionName;
			}
		}
		#endregion

		private void btnOptimizeStartupManagerAdd_Click(object sender, EventArgs e)
		{
			var nrv = new NewRunItemForm();
			if (nrv.ShowDialog(this) == DialogResult.OK)
				this.LoadStartupFiles();
		}

		private void btnOptimizeStartupManagerEdit_Click(object sender, EventArgs e)
		{
			EditStartupItem();
		}

		private void btnOptimizeStartupManagerDelete_Click(object sender, EventArgs e)
		{
			DeleteStartupItem();
		}

		private void btnOptimizeStartupManagerView_Click(object sender, EventArgs e)
		{
			ViewStartupItem();
		}

		private void btnOptimizeStartupManagerRun_Click(object sender, EventArgs e)
		{
			RunStartupItem();
		}

		private void btnOptimizeStartupManagerRefresh_Click(object sender, EventArgs e)
		{
			LoadStartupFiles();
		}

		private void tvaOptimizeStartupManager_DoubleClick(object sender, EventArgs e)
		{
			ViewStartupItem();
		}

		private void cmnuOptimizeStartupManager_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			cmnuOptimizeStartupManager.Hide();
			if (e.ClickedItem == tsmiOptimizeStartupManagerEdit)
			{
				EditStartupItem();
			}
			else if (e.ClickedItem == tsmiOptimizeStartupManagerDelete)
			{
				DeleteStartupItem();
			}
			else if (e.ClickedItem == tsmiOptimizeStartupManagerView)
			{
				ViewStartupItem();
			}
			else if (e.ClickedItem == tsmiOptimizeStartupManagerRun)
			{
				RunStartupItem();
			}
		}

		private void tvaOptimizeStartupManager_SelectionChanged(object sender, EventArgs e)
		{
			// attach/detach context menu if tree view contains selected items
			// disable/enable buttons
			if (this.tvaOptimizeStartupManager.SelectedNodes.Count > 0 &&
				(this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).IsLeaf)
			{
				tvaOptimizeStartupManager.ContextMenuStrip = cmnuOptimizeStartupManager;
				btnOptimizeStartupManagerEdit.Enabled = true;
				btnOptimizeStartupManagerDelete.Enabled = true;
				btnOptimizeStartupManagerView.Enabled = true;
				btnOptimizeStartupManagerRun.Enabled = true;
			}
			else
			{
				tvaOptimizeStartupManager.ContextMenuStrip = null;
				btnOptimizeStartupManagerEdit.Enabled = false;
				btnOptimizeStartupManagerDelete.Enabled = false;
				btnOptimizeStartupManagerView.Enabled = false;
				btnOptimizeStartupManagerRun.Enabled = false;
			}
		}

		/// <summary>
		/// Show EditRunItemForm for curenlty selected startup item
		/// </summary>
		private void EditStartupItem()
		{
			if (this.tvaOptimizeStartupManager.SelectedNodes.Count > 0)
			{
				StartupManagerNode node = this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode;

				if (!node.IsLeaf)
					return;

				string strSection = (node.Parent as StartupManagerNode).Section;

				var frmEditRunItem = new EditRunItemForm(node.Item, strSection, node.Path, node.Args);
				if (frmEditRunItem.ShowDialog(this) == DialogResult.OK)
					LoadStartupFiles();
			}
		}

		/// <summary>
		/// Delete currently selected Startup item
		/// </summary>
		private void DeleteStartupItem()
		{
			if (this.tvaOptimizeStartupManager.SelectedNodes.Count > 0)
			{
				StartupManagerNode node = this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode;
				if (!node.IsLeaf)
					return;

				if (MessageBox.Show(this, Properties.Resources.smRemove, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{

					bool bFailed = false;

					string strSection = (node.Parent as StartupManagerNode).Section;

					if (Directory.Exists(strSection))
					{
						// Startup folder
						string strPath = Path.Combine(strSection, node.Item);

						try
						{
							if (File.Exists(strPath))
								File.Delete(strPath);
						}
						catch (Exception ex)
						{
							MessageBox.Show(this, ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
							bFailed = true;
						}
					}
					else
					{
						// Registry key
						string strMainKey = strSection.Substring(0, strSection.IndexOf('\\'));
						string strSubKey = strSection.Substring(strSection.IndexOf('\\') + 1);
						RegistryKey rk = RegistryHelper.RegOpenKey(strMainKey, strSubKey);

						try
						{
							if (rk != null)
								rk.DeleteValue(node.Item);
						}
						catch (Exception ex)
						{
							MessageBox.Show(this, ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
							bFailed = true;
						}

						rk.Close();
					}

					if (!bFailed)
						MessageBox.Show(this, Properties.Resources.smRemoved, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

					LoadStartupFiles();
				}
			}
		}

		/// <summary>
		/// Show currently selected startup item on tvaOptimizeStartupManager
		/// in RegEdit or in Explorer
		/// </summary>
		private void ViewStartupItem()
		{
			if (this.tvaOptimizeStartupManager.SelectedNodes.Count > 0)
			{
				if (!(this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).IsLeaf)
					return;

				string strItem = (this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).Item;
				string strPath = (this.tvaOptimizeStartupManager.SelectedNode.Parent.Tag as StartupManagerNode).Section;

				if (strPath.StartsWith("HKEY"))
				{
					RegEditGo.GoTo(strPath, strItem);
				}
				else
				{
					Process.Start("explorer.exe", strPath);
				}
			}
		}

		/// <summary>
		/// Run currently selected startup item
		/// </summary>
		private void RunStartupItem()
		{
			if (this.tvaOptimizeStartupManager.SelectedNodes.Count > 0)
			{
				if (!(this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).IsLeaf)
					return;

				if (MessageBox.Show(this, Properties.Resources.smRun, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					string strFilepath = (this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).Path;
					string strFileArgs = (this.tvaOptimizeStartupManager.SelectedNode.Tag as StartupManagerNode).Args;

					// File path cannot be empty
					if (string.IsNullOrEmpty(strFilepath))
						return;

					Process.Start(strFilepath, strFileArgs);

					//MessageBox.Show(this, string.Format("{0}: {1}", Properties.Resources.smRun, strFilepath), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		#endregion Optimize -> Startup Manager

		#region Optimize -> Registry Manager

		private void cmnuOptimizeRegistryManager_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			cmnuOptimizeRegistryManager.Hide();
			if (e.ClickedItem == tsmiOptimizeRegistryManagerSelectAll)
			{
				foreach (BadRegistryKey brkRoot in this.tmOptimizeRegistryManager.Nodes)
				{
					brkRoot.Checked = CheckState.Checked;
					foreach (BadRegistryKey brk in brkRoot.Nodes)
						brk.Checked = CheckState.Checked;
				}

				this.tvaOptimizeRegistryManager.Refresh();
			}
			else if (e.ClickedItem == tsmiOptimizeRegistryManagerSelectNone)
			{
				foreach (BadRegistryKey brkRoot in this.tmOptimizeRegistryManager.Nodes)
				{
					brkRoot.Checked = CheckState.Unchecked;
					foreach (BadRegistryKey brk in brkRoot.Nodes)
						brk.Checked = CheckState.Unchecked;
				}

				this.tvaOptimizeRegistryManager.Refresh();
			}
			else if (e.ClickedItem == tsmiOptimizeRegistryManagerInvertSelection)
			{
				foreach (BadRegistryKey brkRoot in this.tmOptimizeRegistryManager.Nodes)
				{
					foreach (BadRegistryKey brk in brkRoot.Nodes)
						brk.Checked = ((brk.Checked == CheckState.Checked) ? (CheckState.Unchecked) : (CheckState.Checked));
				}

				this.tvaOptimizeRegistryManager.Refresh();
			}
			else if (e.ClickedItem == tsmiOptimizeRegistryManagerExcludeSelected)
			{
				if (this.tvaOptimizeRegistryManager.SelectedNodes.Count > 0)
				{
					for (int i = 0; i < this.tvaOptimizeRegistryManager.SelectedNodes.Count; i++)
					{
						BadRegistryKey brk = this.tvaOptimizeRegistryManager.SelectedNodes[i].Tag as BadRegistryKey;

						if (!string.IsNullOrEmpty(brk.RegKeyPath))
						{
							Options.Instance.RegistryExcludeArray.Add(
								new Data.Registry.RegistryExcludeItem(brk.RegKeyPath, null, null));
							brk.Checked = CheckState.Unchecked;
						}
					}

					MessageBox.Show(this, Properties.Resources.mainAddExcludeEntry, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (e.ClickedItem == tsmiOptimizeRegistryManagerViewinRegEdit)
			{
				if (this.tvaOptimizeRegistryManager.SelectedNodes.Count > 0)
				{
					BadRegistryKey brk = this.tvaOptimizeRegistryManager.SelectedNode.Tag as BadRegistryKey;
					string strSubKey = brk.RegKeyPath;
					string strValueName = brk.ValueName;

					RegEditGo.GoTo(strSubKey, strValueName);
				}
			}
		}

		private void tvaOptimizeRegistryManager_SelectionChanged(object sender, EventArgs e)
		{
			// enable/disable context menu items
			tsmiOptimizeRegistryManagerExcludeSelected.Enabled = tvaOptimizeRegistryManager.SelectedNodes.Count > 0;
			tsmiOptimizeRegistryManagerViewinRegEdit.Enabled = tvaOptimizeRegistryManager.SelectedNodes.Count > 0;
		}
		#endregion Optimize -> Registry Manager

		#region Optimize -> Registry Defragmenter
		public bool _optimizeRegistryDefragmenterIsAnalyzed = false;

		private void btnOptimizeRegistryDefragmenterAnalyze_ClickButtonArea(object Sender, MouseEventArgs e)
		{
			OptimizeRegistryDefragmenterAnalyzeAction();
		}

		private void btnOptimizeRegistryDefragmenterOptimize_ClickButtonArea(object Sender, MouseEventArgs e)
		{
			if (!_optimizeRegistryDefragmenterIsAnalyzed)
				OptimizeRegistryDefragmenterAnalyzeAction();
			OptimizeRegistryDefragmenterOptimizeAction();
		}

		private void OptimizeRegistryDefragmenterAnalyzeAction()
		{
			if (_optimizeRegistryDefragmenterIsAnalyzed)
				return;

			Thread threadCurrent;
			string strMsg =
				"You must close running programs before optimizing the registry.\n" +
				"Please save your work and close any running programs now.";

			if (MessageBox.Show(this, strMsg, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) !=
				DialogResult.OK)
				return;
			try
			{
				this.Invoke(new MethodInvoker(delegate() { Cursor = Cursors.WaitCursor; }));

				OptimizeRegistryDefragmenterSetButtonsEnabled(false);

				#region Seeking for Hives
				// Create restore point if program is started for 1st time
				//if (Properties.Settings.Default.LastStartDateTime == DateTime.MinValue)
				{
					long restoreSeqNum = 0;

					if (SysRestore.StartRestore(string.Format("Before {0} First Run", Application.ProductName), SysRestore.RestoreType.FirstRun, out restoreSeqNum) == 0)
						SysRestore.EndRestore(restoreSeqNum);
				}

				// Enumerate through hivelist
				RegistryKey rkHives = null;
				using (rkHives = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\hivelist"))
				{
					if (rkHives == null)
						throw new ApplicationException("Unable to open hivelist... this can be a problem!");

					foreach (string strValueName in rkHives.GetValueNames())
					{
						var strHivePath = rkHives.GetValue(strValueName) as string;
						if (!string.IsNullOrEmpty(strValueName) && !string.IsNullOrEmpty(strHivePath))
							Context.Hives.Add(new Hive(strValueName, strHivePath));
					}
				}
				// Update list view
				//OptimizeRegistryDefragmenterRefreshGui();
				#endregion Seeking for Hives

				if (Context.Hives.Count > 0)
				{
					this.pbOptimizeRegistryDefragmenter.Maximum = Context.Hives.Count;

					var index = 0;
					long totalOldHiveSize = 0;
					long totalNewHiveSize = 0;
					foreach (Hive hive in Context.Hives)
					{
						index++;
						// Analyze Hive
						threadCurrent = new Thread(new ThreadStart(hive.AnalyzeHive));
						threadCurrent.Start();
						threadCurrent.Join();

						// Update list view
						//OptimizeRegistryDefragmenterRefreshGui();
						var lvi = new ListViewItem(new string[] { hive.HivePath, 
							new FileSizeFormatProvider().Format("fsu", hive.OldHiveSize, null), 
							new FileSizeFormatProvider().Format("fsu", hive.NewHiveSize, null) });
						totalOldHiveSize += hive.OldHiveSize;
						totalNewHiveSize += hive.NewHiveSize;
						//hive.OldHiveSize.ToString("{0:fsu}", new FileSizeFormatProvider()), 
						//hive.NewHiveSize.ToString("{0:fsu}", new FileSizeFormatProvider()) });
						lvOptimizeRegistryDefragmenter.Items.Add(lvi);

						// Update progress bar
						this.pbOptimizeRegistryDefragmenter.Value = index;
						this.pbOptimizeRegistryDefragmenter.Invalidate();
						this.lblOptimizeRegistryDefragmenterStatus.Text = string.Format(@"Analyzed: {0}/{1}", this.pbOptimizeRegistryDefragmenter.Value, Context.Hives.Count);
					}
					lblOptimizeRegistryDefragmenterTotalOldHiveSize.Text = new FileSizeFormatProvider().Format("fsu", totalOldHiveSize, null);
					lblOptimizeRegistryDefragmenterTotalNewHiveSize.Text = new FileSizeFormatProvider().Format("fsu", totalNewHiveSize, null);
					lblOptimizeRegistryDefragmenterTotal.Visible = true;
					lblOptimizeRegistryDefragmenterTotalOldHiveSize.Visible = true;
					lblOptimizeRegistryDefragmenterTotalNewHiveSize.Visible = true;
				}
				_optimizeRegistryDefragmenterIsAnalyzed = true;
			}
			finally
			{
				OptimizeRegistryDefragmenterSetButtonsEnabled(true);
				this.Invoke(new MethodInvoker(delegate() { Cursor = Cursors.Default; }));
			}

			//// Save current date
			//try
			//{
			//    Properties.Settings.Default.LastScanDateTime = DateTime.Now.Date;
			//    Properties.Settings.Default.Save();
			//}
			//catch (Exception ex)
			//{
			//    CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving Settings\n{0}", ex.ToString()));
			//}
		}

		private void OptimizeRegistryDefragmenterOptimizeAction()
		{
			long lSeqNum = 0;
			Thread threadCurrent;

			if (MessageBox.Show(this, "Are you sure you want to compact your registry?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				return;

			var hasError = false;
			try
			{
				this.Invoke(new MethodInvoker(delegate() { Cursor = Cursors.WaitCursor; }));
				this.pbOptimizeRegistryDefragmenter.Maximum = Context.Hives.Count;
				this.lblOptimizeRegistryDefragmenterStatus.Text = string.Format(@"Optimized: 0/{0}", Context.Hives.Count);

				SysRestore.StartRestore(string.Format("Before {0} Optimization", Application.ProductName), SysRestore.RestoreType.Restore, out lSeqNum);

				OptimizeRegistryDefragmenterSetButtonsEnabled(false);

				var index = 0;
				foreach (Hive h in Context.Hives)
				{
					index++;

					//threadCurrent = new Thread(new ThreadStart(h.CompactHive));
					//threadCurrent.Start();
					//threadCurrent.Join();

					try
					{
						h.CompactHive();
					}
					catch (Win32Exception ex)
					{
						CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
						hasError = true;
					}

					this.pbOptimizeRegistryDefragmenter.Value = index;
					this.pbOptimizeRegistryDefragmenter.Invalidate();
					this.lblOptimizeRegistryDefragmenterStatus.Text = string.Format(@"Optimized: {0}/{1}", this.pbOptimizeRegistryDefragmenter.Value, Context.Hives.Count);
				}

				SysRestore.EndRestore(lSeqNum);
			}
			finally
			{
				OptimizeRegistryDefragmenterSetButtonsEnabled(true);
				this.Invoke(new MethodInvoker(delegate() { Cursor = Cursors.Default; }));
			}

			// Show messagebox and close program
			var message = "Successfully compacted the Windows Registry. ";
			if (hasError)
				message += "Several Hives were not optimized.";
			MessageBox.Show(this, message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
			this._forceClose = true;

			//if (MessageBox.Show(this, "You must restart your computer before the new setting will take effect. Do you want to restart your computer now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			MessageBox.Show(this, "Windows will now restart", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
			// Restart computer
			PInvoke.ExitWindowsEx(0x02, PInvoke.MajorOperatingSystem | PInvoke.MinorReconfig | PInvoke.FlagPlanned);

			this.Close();
		}

		private void OptimizeRegistryDefragmenterSetButtonsEnabled(bool bValue)
		{
			this.btnOptimizeRegistryDefragmenterOptimize.Enabled = bValue;
			this.tabMain.pbbHome.Enabled = bValue;
			this.tabMain.pbbDisk.Enabled = bValue;
			this.tabMain.pbbOptimize.Enabled = bValue;
			this.tabMain.pbbSettings.Enabled = bValue;
			//this._allowFormClosed = bValue;
		}

		/// <summary>
		/// Show actual data on form
		/// </summary>
		public void OptimizeRegistryDefragmenterRefreshGui()
		{
			//if (_isClosing)
			//    return;

			// thread safe
			if (this.InvokeRequired)
			{
				try
				{
					this.Invoke(new MethodInvoker(delegate() { OptimizeRegistryDefragmenterRefreshGui(); }));
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.ERROR, ex.ToString());
				}
			}
			else
			{
				try
				{
					//_disableHandleProcessing = true;
					lvOptimizeRegistryDefragmenter.PerformLayout();
					lvOptimizeRegistryDefragmenter.Items.Clear();
					foreach (Hive hive in Context.Hives)
					{
						var lvi = new ListViewItem(new string[] { hive.HivePath, 
							new FileSizeFormatProvider().Format("fsu", hive.OldHiveSize, null), 
							new FileSizeFormatProvider().Format("fsu", hive.NewHiveSize, null) });
						//hive.OldHiveSize.ToString("{0:fsu}", new FileSizeFormatProvider()), 
						//hive.NewHiveSize.ToString("{0:fsu}", new FileSizeFormatProvider()) });
						lvOptimizeRegistryDefragmenter.Items.Add(lvi);
					}
					lvOptimizeRegistryDefragmenter.ResumeLayout();
					//gcHive.DataSource = Context.Hives;
					//gcHive.RefreshDataSource();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}
				finally
				{
					//_disableHandleProcessing = false;
				}
			}
		}
		#endregion Optimize -> Registry Defragmenter

		#region Disk -> Uninstall Manager
		private void btnDiskUninstallManagerRemoveEntry_Click(object sender, EventArgs e)
		{
			RemoveProgramFromRegistry();
		}

		private void btnDiskUninstallManagerUninstall_Click(object sender, EventArgs e)
		{
			UninstallProgram();
		}

		/// <summary>
		/// Fill lvDiskUninstallManager with data
		/// </summary>
		private void FillLvDiskUninstallManager()
		{
			// Clear listview
			this.lvDiskUninstallManager.Items.Clear();
			this.btnDiskUninstallManagerUninstall.Enabled = false;
			this.btnDiskUninstallManagerRemoveEntry.Enabled = false;

			// Turn textbox into regex pattern
			Regex regex = new Regex("", RegexOptions.IgnoreCase);

			if (this.txtDiskUninstallManagerSearchFilter.ForeColor != SystemColors.GrayText)
			{
				StringBuilder result = new StringBuilder();
				foreach (string str in this.txtDiskUninstallManagerSearchFilter.Text.Split(' '))
				{
					result.Append(Regex.Escape(str));
					result.Append(".*");
				}

				regex = new Regex(result.ToString(), RegexOptions.IgnoreCase);
			}

			/*
			// Get the program info list
			using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"))
			{
				if (regKey != null)
				{
					foreach (string strSubKeyName in regKey.GetSubKeyNames())
					{
						using (RegistryKey subKey = regKey.OpenSubKey(strSubKeyName))
						{
							if (subKey != null)
								listProgInfo.Add(new ProgramInfo(subKey));
						}
					}
				}
			}

			// (x64 registry keys)
			if (RegistryHelper.Is64BitOS)
			{
				using (RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall"))
				{
					if (regKey != null)
					{
						foreach (string strSubKeyName in regKey.GetSubKeyNames())
						{
							using (RegistryKey subKey = regKey.OpenSubKey(strSubKeyName))
							{
								if (subKey != null)
									listProgInfo.Add(new ProgramInfo(subKey));
							}
						}
					}
				}
			}
			*/


			// Populate list view
			ilDiskUninstallManager.Images.Clear();
			foreach (ProgramInfo progInfo in DataContext.ProgramsToUninstall)
			{
				ListViewItem lvi = new ListViewItem();

				// Display Name
				if (!string.IsNullOrEmpty(progInfo.DisplayName))
					lvi.Text = progInfo.DisplayName;
				else if (!string.IsNullOrEmpty(progInfo.QuietDisplayName))
					lvi.Text = progInfo.QuietDisplayName;
				else
					lvi.Text = progInfo.Key;

				// Publisher
				lvi.SubItems.Add(((!string.IsNullOrEmpty(progInfo.DisplayName)) ? (progInfo.Publisher) : ("")));

				// Estimated Size
				if (progInfo.InstallSize > 0)
					lvi.SubItems.Add(RegistryHelper.ConvertSizeToString((uint)progInfo.InstallSize));
				else if (progInfo.EstimatedSize > 0)
					lvi.SubItems.Add(RegistryHelper.ConvertSizeToString(progInfo.EstimatedSize * 1024));
				else
					lvi.SubItems.Add("");
				lvi.SubItems.Add(progInfo.InstallLocation);
				lvi.SubItems.Add(progInfo.DisplayVersion);
				lvi.SubItems.Add(progInfo.Contact);
				lvi.SubItems.Add(progInfo.HelpLink);
				lvi.SubItems.Add(progInfo.InstallSource);
				lvi.SubItems.Add(progInfo.URLInfoAbout);

				// Uninstaller Icon
				if (progInfo.Icon != null)
				{
					ilDiskUninstallManager.Images.Add(progInfo.Icon);
					lvi.ImageIndex = ilDiskUninstallManager.Images.Count - 1;
				}

				if ((!string.IsNullOrEmpty(progInfo.DisplayName))
					&& (string.IsNullOrEmpty(progInfo.ParentKeyName))
					&& (!progInfo.SystemComponent))
				{
					//if (progInfo.Uninstallable)
					//    lvi.ImageKey = "OK";
					//else
					//    lvi.ImageKey = "ERROR";

					// Add program info to tag
					lvi.Tag = progInfo;

					if (regex.IsMatch(lvi.Text))
						this.lvDiskUninstallManager.Items.Add(lvi);
				}
			}
			if (this.lvDiskUninstallManager.Items.Count > 0)
				this.lvDiskUninstallManager.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

			//var thread = new Thread(new ParameterizedThreadStart(FillLvDiskUninstallManager_FillIconsThreaded));
			//thread.IsBackground = true;
			//thread.Start(listProgInfo);
		}

		//private void FillLvDiskUninstallManager_FillIconsThreaded(object obj)
		//{
		//    List<ProgramInfo> listProgInfo = (List<ProgramInfo>)obj;
		//    for (int i = 0; i < listProgInfo.Count; i++)
		//    {
		//        // Uninstaller Icon
		//        try
		//        {
		//            Icon icon = Icon.ExtractAssociatedIcon(listProgInfo[i].DisplayIcon.Trim('0', ','));
		//            if (icon != null)
		//            {
		//                ilDiskUninstallManager.Images.Add(icon);
		//                if (this.InvokeRequired)
		//                {
		//                    this.Invoke(new MethodInvoker(delegate() { this.lvDiskUninstallManager.Items[i].ImageIndex = ilDiskUninstallManager.Images.Count - 1; }));
		//                }
		//            }
		//        }
		//        catch (Exception ex)
		//        {
		//            CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
		//        }
		//    }
		//}

		private void txtDiskUninstallManagerSearchFilter_TextChanged(object sender, EventArgs e)
		{
			if (_disableCheckedChangedHandler)
				return;
			FillLvDiskUninstallManager();
		}

		private void txtDiskUninstallManagerSearchFilter_Enter(object sender, EventArgs e)
		{
			try
			{
				_disableCheckedChangedHandler = true;
				if (this.txtDiskUninstallManagerSearchFilter.ForeColor == SystemColors.GrayText)
				{
					this.txtDiskUninstallManagerSearchFilter.ForeColor = SystemColors.WindowText;
					this.txtDiskUninstallManagerSearchFilter.Text = "";
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}

		private void txtDiskUninstallManagerSearchFilter_Leave(object sender, EventArgs e)
		{
			try
			{
				_disableCheckedChangedHandler = true;
				if (this.txtDiskUninstallManagerSearchFilter.Text.Trim() == "")
				{
					this.txtDiskUninstallManagerSearchFilter.ForeColor = SystemColors.GrayText;
					this.txtDiskUninstallManagerSearchFilter.Text = Properties.Resources.umSearchText;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}

		private void UninstallProgram()
		{
			if (this.lvDiskUninstallManager.SelectedItems.Count > 0)
			{
				var numberOfAppsBeforeUninstall = DataContext.ProgramsToUninstall.Count;
				ListViewItem lvi = this.lvDiskUninstallManager.SelectedItems[0];
				ProgramInfo progInfo = lvi.Tag as ProgramInfo;

				if (MessageBox.Show(this, Properties.Resources.umUninstall, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					var success = progInfo.Uninstall();
					DataContext.ProgramsToUninstall = UninstallManagerLogic.List();
					var numberOfAppsAfterUninstall = DataContext.ProgramsToUninstall.Count;

					MessageBox.Show(success && numberOfAppsAfterUninstall < numberOfAppsBeforeUninstall ? "Program was sucessfully uninstalled" : "Program was not uninstalled",
						Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

					FillLvDiskUninstallManager();
				}
			}
		}

		private void RemoveProgramFromRegistry()
		{
			if (this.lvDiskUninstallManager.SelectedItems.Count > 0)
			{
				ListViewItem lvi = this.lvDiskUninstallManager.SelectedItems[0];
				ProgramInfo progInfo = lvi.Tag as ProgramInfo;

				if (MessageBox.Show(this, Properties.Resources.umForceUninstall, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					progInfo.RemoveFromRegistry();
					DataContext.ProgramsToUninstall = UninstallManagerLogic.List();
					FillLvDiskUninstallManager();
				}
			}
		}


		private void lvDiskUninstallManager_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			if (lvDiskUninstallManager.Tag == null || e.Column != (int)lvDiskUninstallManager.Tag)
			{
				lvDiskUninstallManager.Tag = e.Column;
				this.lvDiskUninstallManager.Sorting = SortOrder.Ascending;
			}
			else
			{
				if (this.lvDiskUninstallManager.Sorting == SortOrder.Ascending)
					this.lvDiskUninstallManager.Sorting = SortOrder.Descending;
				else
					this.lvDiskUninstallManager.Sorting = SortOrder.Ascending;
			}

			this.lvDiskUninstallManager.ListViewItemSorter = new ListViewItemComparer(e.Column, this.lvDiskUninstallManager.Sorting);
			this.lvDiskUninstallManager.Sort();
		}

		private void lvDiskUninstallManager_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.lvDiskUninstallManager.SelectedItems.Count > 0)
			{
				ListViewItem lvi = this.lvDiskUninstallManager.SelectedItems[0];
				ProgramInfo progInfo = lvi.Tag as ProgramInfo;

				this.btnDiskUninstallManagerUninstall.Enabled = progInfo.Uninstallable;
				this.btnDiskUninstallManagerRemoveEntry.Enabled = true;
			}
			else
			{
				this.btnDiskUninstallManagerUninstall.Enabled = false;
				this.btnDiskUninstallManagerRemoveEntry.Enabled = false;
			}
		}
		#endregion Disk -> Uninstall Manager

		#region Disk -> Disk Cleaner

		private void btnDiskDiskCleanerScanDisks_Click(object sender, EventArgs e)
		{
			DiskDiskCleanerScanDisksAction();
		}

		private void btnDiskDiskCleanerCleanFiles_Click(object sender, EventArgs e)
		{
			DiskDiskCleanerCleanFilesAction();
		}

		private void btnDiskDiskCleanerOpenFile_Click(object sender, EventArgs e)
		{
			DiskDiskCleanerOpenFileAction();
		}

		private void btnDiskDiskCleanerProperties_Click(object sender, EventArgs e)
		{
			DiskDiskCleanerPropertiesAction();
		}

		private void btnDiskDiskCleanerOptions_Click(object sender, EventArgs e)
		{
			OptionsForm options = new OptionsForm();
			options.ShowDialog(this);
		}

		private void lvDiskDiskCleaner_SelectedIndexChanged(object sender, EventArgs e)
		{
			// disable/enable context menu items
			tsmiDiskDiskCleanerOpenFile.Enabled = lvDiskDiskCleaner.SelectedItems.Count > 0;
			tsmiDiskDiskCleanerProperties.Enabled = lvDiskDiskCleaner.SelectedItems.Count > 0;
			btnDiskDiskCleanerOpenFile.Enabled = lvDiskDiskCleaner.SelectedItems.Count > 0;
			btnDiskDiskCleanerProperties.Enabled = lvDiskDiskCleaner.SelectedItems.Count > 0;

			// update detail info
			if (this.lvDiskDiskCleaner.SelectedItems.Count > 0)
			{
				fileInfoCtrl1.UpdateInfo(this.lvDiskDiskCleaner.SelectedItems[0].Tag as FileInfo);
			}
			else
			{
				fileInfoCtrl1.ResetInfo();
			}
		}

		private void cmnuDiskDiskCleaner_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			int selectedCount = 0;
			cmnuDiskDiskCleaner.Hide();
			if (e.ClickedItem == tsmiDiskDiskCleanerSelectAll)
			{
				DiskDiskCleanerSelectAllAction();
			}
			else if (e.ClickedItem == tsmiDiskDiskCleanerSelectNone)
			{
				DiskDiskCleanerSelectNoneAction();
			}
			else if (e.ClickedItem == tsmiDiskDiskCleanerInvertSelection)
			{
				DiskDiskCleanerInvertSelectionAction();
			}
			else if (e.ClickedItem == tsmiDiskDiskCleanerOpenFile)
			{
				DiskDiskCleanerOpenFileAction();
			}
			else if (e.ClickedItem == tsmiDiskDiskCleanerProperties)
			{
				DiskDiskCleanerPropertiesAction();
			}

			//if (e.ClickedItem == tsmiDiskDiskCleanerSelectAll)
			//    lblDiskDiskCleanerItemSelected.Text =
			//    string.Format(lblDiskDiskCleanerItemSelected.Tag.ToString(),
			//                  lvDiskDiskCleaner.Items.Count, lvDiskDiskCleaner.Items.Count);
			//if (e.ClickedItem == tsmiDiskDiskCleanerInvertSelection)
			//    lblDiskDiskCleanerItemSelected.Text =
			//    string.Format(lblDiskDiskCleanerItemSelected.Tag.ToString(),
			//                  selectedCount, lvDiskDiskCleaner.Items.Count);
			//if (e.ClickedItem == tsmiDiskDiskCleanerSelectNone)
			//    lblDiskDiskCleanerItemSelected.Text =
			//    string.Format(lblDiskDiskCleanerItemSelected.Tag.ToString(),
			//                  0, lvDiskDiskCleaner.Items.Count);
		}

		private void DiskDiskCleanerScanDisksAction()
		{
			try
			{
				_disableCheckedChangedHandler = true;
				this.fileInfoCtrl1.ResetInfo();
				this.lvDiskDiskCleaner.Items.Clear();

				var selDrives = new List<DriveInfo>();
				foreach (ListViewItem lvi in Options.Instance.DiskCleanerSettings.diskDrives)
				{
					if (lvi.Checked)
						selDrives.Add(lvi.Tag as DriveInfo);
				}

				if (selDrives.Count == 0)
				{
					MessageBox.Show(this, "No drives selected", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				AnalyzeForm analyze = new AnalyzeForm(selDrives);
				DialogResult dlgResult = analyze.ShowDialog(this);

				/*
				if (dlgResult == System.Windows.Forms.DialogResult.OK)
				{
					this.notifyIcon1.ShowBalloonTip(6000, "Disk Cleaner", "Finished analyzing hard drive(s)", ToolTipIcon.Info);
				}
				else
				{
					this.notifyIcon1.ShowBalloonTip(6000, "Disk Cleaner", "Aborted analyzing hard drive(s)", ToolTipIcon.Info);
				}
				*/

				foreach (FileInfo fileInfo in AnalyzeForm.fileList)
				{
					string fileName = fileInfo.Name;
					string filePath = fileInfo.DirectoryName;
					string fileSize = Utils.ConvertSizeToString(fileInfo.Length);

					ListViewItem listViewItem = new ListViewItem(new string[] { fileName, filePath, fileSize });
					listViewItem.Checked = true;
					listViewItem.Tag = fileInfo;
					this.lvDiskDiskCleaner.Items.Add(listViewItem);
				}

				this.lvDiskDiskCleaner.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

				if (Options.Instance.DiskCleanerSettings.autoClean)
					btnDiskDiskCleanerCleanFiles_Click(this, new EventArgs());
				else
				{
					this.btnDiskDiskCleanerCleanFiles.Enabled = true;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}

		private void DiskDiskCleanerCleanFilesAction()
		{
			try
			{
				_disableCheckedChangedHandler = false;

				if (this.lvDiskDiskCleaner.Items.Count > 0)
				{
					if (!Options.Instance.DiskCleanerSettings.autoClean)
						if (MessageBox.Show(this, "Are you sure you want to remove these files?", Application.ProductName,
											MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
							return;

					long lSeqNum = 0;
					SysRestore.StartRestore("Before Disk Cleaner Cleaning", out lSeqNum);

					foreach (ListViewItem lvi in this.lvDiskDiskCleaner.Items)
					{
						if (!Config.EnableFixing)
							continue;

						if (!lvi.Checked)
							continue;

						try
						{
							FileInfo fileInfo = lvi.Tag as FileInfo;

							// Make sure file exists
							if (!fileInfo.Exists)
								continue;

							if (Options.Instance.DiskCleanerSettings.removeMode == 0)
							{
								// Remove permanately
								fileInfo.Delete();
							}
							else if (Options.Instance.DiskCleanerSettings.removeMode == 1)
							{
								// Recycle file
								Utils.SendFileToRecycleBin(fileInfo.FullName);
							}
							else
							{
								// Move file to specified directory
								if (!Directory.Exists(Options.Instance.DiskCleanerSettings.moveFolder))
									Directory.CreateDirectory(Options.Instance.DiskCleanerSettings.moveFolder);

								File.Move(fileInfo.FullName,
										  string.Format(@"{0}\{1}", Options.Instance.DiskCleanerSettings.moveFolder, fileInfo.Name));
							}
						}
						catch (Exception ex)
						{
							CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while disk cleaning.\r\n{0}", ex));
						}
					}

					if (lSeqNum != 0)
					{
						SysRestore.EndRestore(lSeqNum);
					}

					MessageBox.Show(this, "Successfully cleaned files from disk", Application.ProductName, MessageBoxButtons.OK,
									MessageBoxIcon.Information);

					// Clear checked problems
					//this.lvDiskDiskCleaner.Items.Clear();
					int count = this.lvDiskDiskCleaner.Items.Count;
					for (int i = count - 1; i >= 0; i--)
						if (this.lvDiskDiskCleaner.Items[i].Checked)
							this.lvDiskDiskCleaner.Items.RemoveAt(i);
					AnalyzeForm.fileList.Clear();
					this.fileInfoCtrl1.ResetInfo();

					// Disable clean disk
					this.btnDiskDiskCleanerCleanFiles.Enabled = false;
					this.btnDiskDiskCleanerOpenFile.Enabled = false;
					this.btnDiskDiskCleanerProperties.Enabled = false;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = true;
			}
		}

		private void DiskDiskCleanerOpenFileAction()
		{
			if (this.lvDiskDiskCleaner.SelectedItems.Count > 0)
			{
				FileInfo fileInfo = this.lvDiskDiskCleaner.SelectedItems[0].Tag as FileInfo;

				var procStartInfo = new ProcessStartInfo(fileInfo.FullName);
				procStartInfo.ErrorDialog = true;
				procStartInfo.ErrorDialogParentHandle = this.Handle;
				Process.Start(procStartInfo);
			}
		}

		private void DiskDiskCleanerPropertiesAction()
		{
			if (this.lvDiskDiskCleaner.SelectedItems.Count > 0)
			{
				FileInfo fileInfo = this.lvDiskDiskCleaner.SelectedItems[0].Tag as FileInfo;

				Utils.ShowFileProperties(fileInfo.FullName);
			}
		}

		private void DiskDiskCleanerSelectAllAction()
		{
			try
			{
				_disableCheckedChangedHandler = true;
				if (this.lvDiskDiskCleaner.Items.Count > 0)
				{
					foreach (ListViewItem lvi in this.lvDiskDiskCleaner.Items)
						lvi.Checked = true;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}

		private void DiskDiskCleanerSelectNoneAction()
		{
			try
			{
				_disableCheckedChangedHandler = true;
				if (this.lvDiskDiskCleaner.Items.Count > 0)
				{
					foreach (ListViewItem lvi in this.lvDiskDiskCleaner.Items)
						lvi.Checked = false;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}

		private void DiskDiskCleanerInvertSelectionAction()
		{
			try
			{
				_disableCheckedChangedHandler = true;
				if (this.lvDiskDiskCleaner.Items.Count > 0)
				{
					foreach (ListViewItem lvi in this.lvDiskDiskCleaner.Items)
						lvi.Checked = !lvi.Checked;
				}
			}
			finally
			{
				_disableCheckedChangedHandler = false;
			}
		}
		#endregion Disk -> Disk Cleaner

		#region Disk -> RestorePoint Manager

		private void btnDiskCreateRestorePoint_Click(object sender, EventArgs e)
		{
			DiskCreateRestorePointAction();
		}

		private void btnDiskRestoreRestorePoint_Click(object sender, EventArgs e)
		{
			DiskRestoreRestorePointAction();
		}

		private void btnDiskRemoveRestorePoint_Click(object sender, EventArgs e)
		{
			DiskRemoveRestorePointAction();
		}

		private void lvDiskRestorePoint_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.lvDiskRestorePoint.SelectedItems.Count > 0)
			{
				btnDiskRestoreRestorePoint.Enabled = SystemRestorePointLogic.SysRestoreAvailable();
				btnDiskRemoveRestorePoint.Enabled = SystemRestorePointLogic.SysRestoreAvailable();
			}
			else
			{
				btnDiskRestoreRestorePoint.Enabled = false;
				btnDiskRemoveRestorePoint.Enabled = false;
			}
		}
		private void DiskCreateRestorePointAction()
		{
			if (string.IsNullOrEmpty(txtDistRestorePointDescription.Text))
				return;

			if (!SystemRestorePointLogic.SysRestoreAvailable())
				return;

			if (MessageBox.Show("Are you sure you want to create Restore Point?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				Cursor.Current = Cursors.WaitCursor;
				try
				{
					SystemRestorePointLogic.Create(txtDistRestorePointDescription.Text);
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					MessageBox.Show("Error while trying to create a Restore Point.");
					return;
				}
				FillLvDiskRestorePointManager();
				Cursor.Current = Cursors.Default;
				MessageBox.Show("The Restore Point has been successfully created.");
			}
		}

		private void DiskRestoreRestorePointAction()
		{
			if (lvDiskRestorePoint.SelectedItems.Count < 1)
				return;
			if (!SystemRestorePointLogic.SysRestoreAvailable())
				return;

			if (MessageBox.Show("Are you sure you want to rollback your system to this Restore Point (reboot required)?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				var systemRestorePoint = lvDiskRestorePoint.SelectedItems[0].Tag as SystemRestorePoint;
				Cursor.Current = Cursors.WaitCursor;
				try
				{
					SystemRestorePointLogic.Rollback(systemRestorePoint.SequenceNumber);
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					MessageBox.Show("Error while trying to rollback your system.");
					return;
				}
				FillLvDiskRestorePointManager();
				Cursor.Current = Cursors.Default;
				// reboot
				RestartHelper.DoExitWin(RestartHelper.EWX_REBOOT);
			}
		}

		private void DiskRemoveRestorePointAction()
		{
			if (lvDiskRestorePoint.SelectedItems.Count < 1)
				return;
			if (!SystemRestorePointLogic.SysRestoreAvailable())
				return;

			if (MessageBox.Show("Are you sure you want to delete this Restore Point?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				var systemRestorePoint = lvDiskRestorePoint.SelectedItems[0].Tag as SystemRestorePoint;
				Cursor.Current = Cursors.WaitCursor;
				try
				{
					SystemRestorePointLogic.Delete(systemRestorePoint.SequenceNumber);
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					MessageBox.Show("Error while trying to delete this Restore Point.");
					return;
				}
				FillLvDiskRestorePointManager();
				Cursor.Current = Cursors.Default;
			}
		}

		/// <summary>
		/// Fill lvDiskUninstallManager with data
		/// </summary>
		private void FillLvDiskRestorePointManager()
		{
			try
			{
				if (!SystemRestorePointLogic.SysRestoreAvailable())
					return;

				var systemRestorePoints = SystemRestorePointLogic.List();
				lvDiskRestorePoint.Items.Clear();
				btnDiskRestoreRestorePoint.Enabled = false;
				btnDiskRemoveRestorePoint.Enabled = false;

				var type = typeof(RestoreType);
				foreach (var systemRestorePoint in systemRestorePoints)
				{
					// getting destription if Restore Type
					var memInfo = type.GetMember(systemRestorePoint.RestoreType.ToString());
					var restoreTypeDescription = systemRestorePoint.RestoreType.ToString();
					try
					{
						var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
						restoreTypeDescription = ((DescriptionAttribute)attributes[0]).Description;
					}
					catch (Exception)
					{
					}

					var item = new ListViewItem(systemRestorePoint.SequenceNumber.ToString());
					item.SubItems.Add(systemRestorePoint.CreationTime.ToLocalTime().ToString());
					item.SubItems.Add(systemRestorePoint.Description);
					item.SubItems.Add(restoreTypeDescription);
					item.Tag = systemRestorePoint;

					lvDiskRestorePoint.Items.Add(item);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				MessageBox.Show("Error while trying to get Restore Point list.");
			}
		}
		#endregion Disk -> RestorePoint Manager

		#region ListViewItemComparer

		public class ListViewItemComparer : IComparer
		{
			private int col;
			private SortOrder order;
			public ListViewItemComparer()
			{
				col = 0;
				order = SortOrder.Ascending;
			}
			public ListViewItemComparer(int column, SortOrder order)
			{
				col = column;
				this.order = order;
			}
			public int Compare(object x, object y)
			{
				int returnVal = -1;
				returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
										((ListViewItem)y).SubItems[col].Text);
				// Determine whether the sort order is descending.
				if (order == SortOrder.Descending)
					// Invert the value returned by String.Compare.
					returnVal *= -1;
				return returnVal;
			}
		}
		#endregion

		#region Optmize->Optimize Windows
		/// <summary>
		/// User change settings on "Settings->Registry Scan Settings" tab page
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbOptimizeOptimizeWindows_CheckedChanged(object sender, EventArgs e)
		{
			if (_disableCheckedChangedHandler)
				return;

			// store changes on Options
			OptimizeWindowsLogic.IsAllowHibernationEnabled = cbOptimizeAllowHybernation.Checked;
			OptimizeWindowsLogic.IsErrorReportingEnabled = cbOptimizeSendErrorsToMicrosoft.Checked;
			OptimizeWindowsLogic.IsPrefetchEnabled = cbOptimizeEnablePrefetch.Checked;
			OptimizeWindowsLogic.IsDesktopSearchEnabled = cbOptimizeEnableDesktopSearch.Checked;
			OptimizeWindowsLogic.IsClearPageFileAtShutdownEnabled = cbOptimizeClearPageFileOnReboot.Checked;
		}

		/// <summary>
		/// Refresh data showing on "Optmize->Optimize Windows" tab page
		/// </summary>
		public void OptmizeOptimizeWindowsRefresh()
		{
			// thread safe
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate() { OptmizeOptimizeWindowsRefresh(); }));
			}
			else
			{
				_disableCheckedChangedHandler = true;
				cbOptimizeAllowHybernation.Checked = OptimizeWindowsLogic.IsAllowHibernationEnabled;
				cbOptimizeSendErrorsToMicrosoft.Checked = OptimizeWindowsLogic.IsErrorReportingEnabled;
				cbOptimizeEnablePrefetch.Checked = OptimizeWindowsLogic.IsPrefetchEnabled;
				cbOptimizeEnableDesktopSearch.Enabled = OptimizeWindowsLogic.IsDesktopSearchAvailable;
				cbOptimizeEnableDesktopSearch.Checked = OptimizeWindowsLogic.IsDesktopSearchEnabled;
				cbOptimizeClearPageFileOnReboot.Checked = OptimizeWindowsLogic.IsClearPageFileAtShutdownEnabled;
				_disableCheckedChangedHandler = false;
			}
		}
		#endregion Optmize->Optimize Windows

		#region Moving form by mouse
		private bool _mouseDown;
		private Point _mouseDownPosition;

		private void MainForm_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				_mouseDown = true;
				_mouseDownPosition = new Point(e.X, e.Y);
			}
		}

		private void MainForm_MouseUp(object sender, MouseEventArgs e)
		{
			_mouseDown = false;
		}

		private void MainForm_MouseMove(object sender, MouseEventArgs e)
		{
			if (_mouseDown)
			{
				this.Left = Cursor.Position.X - _mouseDownPosition.X;
				this.Top = Cursor.Position.Y - _mouseDownPosition.Y;
			}
		}
		#endregion Moving form by mouse

		#region Min, max, close control box
		private void cbMain_Maximize(object sender, EventArgs e)
		{
			//if (this.WindowState != FormWindowState.Maximized)
			//    this.WindowState = FormWindowState.Maximized;
			//else
			//    this.WindowState = FormWindowState.Normal;
		}

		private void cbMain_Minimize(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		private void cbMain_Close(object sender, EventArgs e)
		{
			Close();
		}
		#endregion Min, max, close control box
	}
}