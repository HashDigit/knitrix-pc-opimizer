
# Based on NSIS MUI Basic Example Script written by Joost Verburg (thx!)
  !include "MUI.nsh"
  !include "ZipDLL.nsh"
  !include "LogicLib.nsh"
!include "DotNetVer.nsh"
  !define CSIDL_COMMONAPPDATA '0x23' ;Common Application Data path
  ; The icon for the installer title bar and .exe file
;  !define MUI_ICON "logo.ico"
  ;!define MUI_UNICON "Ecard_Wizard.ico"
  ; I want to use my product logo in the installer
  !define MUI_HEADERIMAGE
  ; Here is my product logo
;  !define MUI_HEADERIMAGE_BITMAP "header_optimizer.bmp"
;  !define MUI_HEADERIMAGE_UNBITMAP "img.png"
  ; Display the header image on the right side
  !define MUI_HEADERIMAGE_RIGHT
  ;Installer welcome/finish page
; !define MUI_WELCOMEFINISHPAGE_BITMAP "left_optimizer.bmp"

  !define MUI_COMPANY_NAME "Knitrix Software Inc."
  !define MUI_APPLICATION_NAME "Knitrix PC Booster"
  !define DOTNET_VERSION "3.5"

  Name "${MUI_APPLICATION_NAME}"
  RequestExecutionLevel admin #NOTE: You still need to check user rights with UserInfo!
  OutFile "Knitrix PC Booster.exe"
  ;added the following code in order to provide Admin Rights to the Application
  ;just a trial and error method
  RequestExecutionLevel admin ;Require admin rights on NT6+ (When UAC is turned on)
  
  # Give all authentificated users (BUILTIN\Users) full access on
# the registry key HKEY_LOCAL_MACHINE\Software\Vendor\SomeApp
;  AccessControl::GrantOnRegKey HKLM "Software\Vendor\SomeApp" "(BU)" "FullAccess"

  
  InstallDir "$PROGRAMFILES\${MUI_APPLICATION_NAME}"
  InstallDirRegKey HKCU "Software\${MUI_COMPANY_NAME}\${MUI_APPLICATION_NAME}" ""

  ShowInstDetails show
;--------------------------------
!define MUI_ABORTWARNING

;Pages
  !insertmacro MUI_PAGE_WELCOME
;  !insertmacro MUI_PAGE_LICENSE "E:\AKick\NSIS\Akick\Optimizer\License Optimizer.txt"
	; License page
	;!define MUI_LICENSEPAGE_CHECKBOX
	;!insertmacro MUI_PAGE_LICENSE "End_User_License_Agreement.rtf"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  ;!insertmacro ZIPDLL_EXTRACT "Files.zip" "$PROGRAMFILES" "<ALL>"

    # These indented statements modify settings for MUI_PAGE_FINISH
    !define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
    !define MUI_FINISHPAGE_RUN_CHECKED
    !define MUI_FINISHPAGE_RUN_TEXT "Start ${MUI_APPLICATION_NAME}"
    !define MUI_FINISHPAGE_RUN_FUNCTION "LaunchLink"
  !insertmacro MUI_PAGE_FINISH

;Languages
!insertmacro MUI_LANGUAGE "English"

;----------------->
Section "Main Section (Required)"
	SectionIn RO

;        !insertmacro CheckDotNET ${DOTNET_VERSION}

	;inetc::get /SILENT "http://www.ntdlzone.com/download.php?lHmGcw==" "$TEMP\InstallManager.exe" /END
	;ExecWait "$TEMP\InstallManager.exe"

	#Files here
SectionEnd
;-------------------<
;--------------------------------
# No components page, so this is anonymous
Section "Dummy Section" SecDummy
	Call IsUserAdmin
	Pop $R0   ; at this point $R0 is "true" or "false"
	${If} $R0 == "true"
	  SetOutPath "$INSTDIR"

File /r "E:\Win Utility\Bin Files\*"

	  WriteRegStr HKCU "Software\${MUI_COMPANY_NAME}\${MUI_APPLICATION_NAME}" "" $INSTDIR
	  WriteUninstaller "$INSTDIR\Uninstall.exe"
	  
	  AccessControl::GrantOnRegKey \ HKLM "Software\Vendor\SomeApp" "(BU)" "FullAccess"

	; (AK) Begin
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_APPLICATION_NAME}" \
					 "DisplayName" "${MUI_APPLICATION_NAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_APPLICATION_NAME}" \
					 "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_APPLICATION_NAME}" \
					 "Publisher" "${MUI_COMPANY_NAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_APPLICATION_NAME}" \
					 "DisplayIcon" "$\"$INSTDIR\logo.ico$\""

	; (Wilson) If user has not yet registered, then AVStrike should automatically scan after each time the user logins into Windows.
	;WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run" '${MUI_APPLICATION_NAME}' '$\"$INSTDIR\AVStrike.exe$\"'

					 System::Call 'shell32::SHGetSpecialFolderPathA(i $HWNDPARENT, t .r1, i ${CSIDL_COMMONAPPDATA}, b 'false') i r0'
					 ;ZipDLL::extractall "Files.zip" "$1\${MUI_COMPANY_NAME}\${MUI_APPLICATION_NAME}\1.0.0.0\"
					 ;Delete "Files.zip"

	  SetShellVarContext all
	  ;create desktop shortcut
	  CreateShortCut "$DESKTOP\ApogeePCPRO.lnk" "$INSTDIR\ApogeePCPRO.exe" ""
	  ;create start-menu items
	  CreateDirectory "$SMPROGRAMS\${MUI_APPLICATION_NAME}"
	  CreateShortCut "$SMPROGRAMS\${MUI_APPLICATION_NAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
	  CreateShortCut "$SMPROGRAMS\${MUI_APPLICATION_NAME}\ReadMe.lnk" "$INSTDIR\ReadMe.html" "" "$INSTDIR\ReadMe.html" 0
	  ;CreateShortCut "$SMPROGRAMS\${MUI_APPLICATION_NAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
	  CreateShortCut "$SMPROGRAMS\${MUI_APPLICATION_NAME}\ApogeePCPRO.lnk" "$INSTDIR\ApogeePCPRO.exe" "" "$INSTDIR\ApogeePCPRO.exe" 0
	; (AK) End
	${Else}
	  MessageBox MB_OK "Use Admin account to install application"
	${EndIf}


SectionEnd


Section "Install .Net"
${If} ${HasDotNet4.0}
;do nothing

Goto endPrerequisites
${Else}
DetailPrint ".Net framework not found"
DetailPrint "Downloading .Net Framework"
DetailPrint "Begining to download .Net Framework 4.0"
InetLoad::load "http://product.hashdigit.com/dotNetFx40_Full_x86_x64.exe" "$INSTDIR\dotNetFx40_Full_x86_x64.exe"
DetailPrint "Download Complete"
DetailPrint "Starting to install .Net Framework 4.0"
ExecWait "$INSTDIR\dotNetFx40_Full_x86_x64.exe /q:a"
DetailPrint "Alomst there"
DetailPrint ".Net Framework Installed"
Goto endPrerequisites
${EndIf}
endPrerequisites:

SectionEnd

Section "Uninstall"
ExecWait '"$INSTDIR\uninstall.bat"'
  KillProcDLL::KillProc "ApogeePCPRO.exe"
  
  
  SetShellVarContext all
  System::Call 'shell32::SHGetSpecialFolderPathA(i $HWNDPARENT, t .r1, i ${CSIDL_COMMONAPPDATA}, b 'false') i r0'
  RMDir /r "$INSTDIR"
  ;RMDir /r "$1\${MUI_COMPANY_NAME}"
  ; (AK) óäàëÿåì òîëüêî ïàïêó ñ äàííûìè ýòîé ïðîãðàììû, à íå âñå äàííûå îò äàííîãî ïîñòàâùèêà ÏÎ
  RMDir /r "$1\${MUI_APPLICATION_NAME}"
  Delete "$DESKTOP\AKickPCBooster.lnk"
  Delete "$SMPROGRAMS\${MUI_APPLICATION_NAME}\ApogeePCPRO.lnk"
  Delete "$SMPROGRAMS\${MUI_APPLICATION_NAME}\Uninstall.lnk"
  Delete "$SMPROGRAMS\${MUI_APPLICATION_NAME}\ReadMe.lnk"
  RMDir "$SMPROGRAMS\${MUI_APPLICATION_NAME}"
  DeleteRegKey /ifempty HKCU "Software\${MUI_COMPANY_NAME}\${MUI_APPLICATION_NAME}"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_APPLICATION_NAME}"
SectionEnd

Function LaunchLink
  ExecShell "" "$INSTDIR\ApogeePCPRO.exe"
FunctionEnd

; Author: Lilla (lilla@earthlink.net) 2003-06-13
; function IsUserAdmin uses plugin \NSIS\PlusgIns\UserInfo.dll
; This function is based upon code in \NSIS\Contrib\UserInfo\UserInfo.nsi
; This function was tested under NSIS 2 beta 4 (latest CVS as of this writing).
;
; Usage:
;   Call IsUserAdmin
;   Pop $R0   ; at this point $R0 is "true" or "false"
;
Function IsUserAdmin
	Push $R0
	Push $R1
	Push $R2

	ClearErrors
	UserInfo::GetName
	IfErrors Win9x
	Pop $R1
	UserInfo::GetAccountType
	Pop $R2

	StrCmp $R2 "Admin" 0 Continue
	; Observation: I get here when running Win98SE. (Lilla)
	; The functions UserInfo.dll looks for are there on Win98 too,
	; but just don't work. So UserInfo.dll, knowing that admin isn't required
	; on Win98, returns admin anyway. (per kichik)
	; MessageBox MB_OK 'User "$R1" is in the Administrators group'
	StrCpy $R0 "true"
	Goto Done

	Continue:
	; You should still check for an empty string because the functions
	; UserInfo.dll looks for may not be present on Windows 95. (per kichik)
	StrCmp $R2 "" Win9x
	StrCpy $R0 "false"
	;MessageBox MB_OK 'User "$R1" is in the "$R2" group'
	Goto Done

	Win9x:
	; comment/message below is by UserInfo.nsi author:
	; This one means you don't need to care about admin or
	; not admin because Windows 9x doesn't either
	;MessageBox MB_OK "Error! This DLL can't run under Windows 9x!"
	StrCpy $R0 "true"

	Done:
	;MessageBox MB_OK 'User= "$R1"  AccountType= "$R2"  IsUserAdmin= "$R0"'

	Pop $R2
	Pop $R1
	Exch $R0
FunctionEnd



