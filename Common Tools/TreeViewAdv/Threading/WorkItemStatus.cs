using System;
using System.Collections.Generic;

namespace Common_Tools.TreeViewAdv.Threading
{
	public enum WorkItemStatus 
	{ 
		Completed, 
		Queued, 
		Executing, 
		Aborted 
	}
}
